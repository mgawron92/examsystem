-- 
-- Ustawienia domyślnego konta administratora
-- 

\set ADMIN_LOGIN 'admin'
\set ADMIN_EMAIL 'admin@email.com'
\set ADMIN_NAME 'imie'
\set ADMIN_SURNAME 'nazwisko'

--
-- Create database
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: choice_answer_part; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE choice_answer_part (
    dtype character varying(31) NOT NULL,
    id bigint NOT NULL,
    selected boolean,
    simple_choice_answer_id bigint,
    simple_choice_pattern_id bigint,
    complex_choice_answer_id bigint,
    complex_choice_pattern_id bigint
);


ALTER TABLE choice_answer_part OWNER TO postgres;

--
-- Name: choice_answer_part_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE choice_answer_part_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE choice_answer_part_id_seq OWNER TO postgres;

--
-- Name: choice_answer_part_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE choice_answer_part_id_seq OWNED BY choice_answer_part.id;


--
-- Name: choice_pattern; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE choice_pattern (
    dtype character varying(31) NOT NULL,
    id bigint NOT NULL,
    label character varying(255),
    deselected_points integer,
    selected_points integer,
    correct_if_selected boolean,
    complex_choice_question_id bigint,
    simple_choice_question_id bigint
);


ALTER TABLE choice_pattern OWNER TO postgres;

--
-- Name: choice_pattern_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE choice_pattern_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE choice_pattern_id_seq OWNER TO postgres;

--
-- Name: choice_pattern_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE choice_pattern_id_seq OWNED BY choice_pattern.id;


--
-- Name: image; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE image (
    id bigint NOT NULL,
    added_on timestamp without time zone,
    image bytea,
    examiner_id bigint
);


ALTER TABLE image OWNER TO postgres;

--
-- Name: image_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE image_id_seq OWNER TO postgres;

--
-- Name: image_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE image_id_seq OWNED BY image.id;


--
-- Name: join_answer_part; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE join_answer_part (
    id bigint NOT NULL,
    first_option character varying(255),
    second_option character varying(255),
    join_answer_id bigint,
    join_pattern_id bigint
);


ALTER TABLE join_answer_part OWNER TO postgres;

--
-- Name: join_answer_part_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE join_answer_part_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE join_answer_part_id_seq OWNER TO postgres;

--
-- Name: join_answer_part_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE join_answer_part_id_seq OWNED BY join_answer_part.id;


--
-- Name: join_pattern; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE join_pattern (
    id bigint NOT NULL,
    first_option character varying(255),
    second_option character varying(255),
    join_question_id bigint
);


ALTER TABLE join_pattern OWNER TO postgres;

--
-- Name: join_pattern_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE join_pattern_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE join_pattern_id_seq OWNER TO postgres;

--
-- Name: join_pattern_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE join_pattern_id_seq OWNED BY join_pattern.id;


--
-- Name: person; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE person (
    dtype character varying(31) NOT NULL,
    id bigint NOT NULL,
    active boolean,
    email character varying(255),
    join_date timestamp without time zone,
    ldap_login character varying(255),
    name character varying(255),
    surname character varying(255),
    added_by character varying(255)
);


ALTER TABLE person OWNER TO postgres;

--
-- Name: person_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE person_id_seq OWNER TO postgres;

--
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE person_id_seq OWNED BY person.id;


--
-- Name: question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE question (
    dtype character varying(31) NOT NULL,
    id bigint NOT NULL,
    content character varying(255),
    has_time_limit boolean,
    image_id bigint,
    max_points integer,
    "position" integer,
    time_limit integer,
    type integer,
    multiple boolean,
    correct_points integer,
    incorrect_points integer,
    test_id bigint
);


ALTER TABLE question OWNER TO postgres;

--
-- Name: question_answer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE question_answer (
    dtype character varying(31) NOT NULL,
    id bigint NOT NULL,
    finished_on timestamp without time zone,
    started_on timestamp without time zone,
    type integer,
    comment character varying(255),
    content character varying(255),
    points integer,
    test_answer_id bigint,
    freetext_question_id bigint,
    simple_choice_question_id bigint,
    join_question_id bigint,
    complex_choice_question_id bigint
);


ALTER TABLE question_answer OWNER TO postgres;

--
-- Name: question_answer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE question_answer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE question_answer_id_seq OWNER TO postgres;

--
-- Name: question_answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE question_answer_id_seq OWNED BY question_answer.id;


--
-- Name: question_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE question_id_seq OWNER TO postgres;

--
-- Name: question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE question_id_seq OWNED BY question.id;


--
-- Name: question_sequence; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE question_sequence (
    id bigint NOT NULL,
    sequence character varying(255),
    test_answer_id bigint
);


ALTER TABLE question_sequence OWNER TO postgres;

--
-- Name: question_sequence_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE question_sequence_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE question_sequence_id_seq OWNER TO postgres;

--
-- Name: question_sequence_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE question_sequence_id_seq OWNED BY question_sequence.id;


--
-- Name: settings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE settings (
    id bigint NOT NULL,
    key character varying(255),
    settings_type integer,
    value character varying(255)
);


ALTER TABLE settings OWNER TO postgres;

--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE settings_id_seq OWNER TO postgres;

--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE settings_id_seq OWNED BY settings.id;


--
-- Name: student_test_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE student_test_group (
    id bigint NOT NULL,
    join_date timestamp without time zone,
    student_id bigint,
    test_group_id bigint
);


ALTER TABLE student_test_group OWNER TO postgres;

--
-- Name: student_test_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE student_test_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE student_test_group_id_seq OWNER TO postgres;

--
-- Name: student_test_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE student_test_group_id_seq OWNED BY student_test_group.id;


--
-- Name: test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE test (
    id bigint NOT NULL,
    active_from timestamp without time zone,
    active_till timestamp without time zone,
    created_date timestamp without time zone,
    description character varying(255),
    has_time_limit boolean,
    name character varying(255),
    restrict_questions_number boolean,
    time_limit integer,
    use_random_sequence boolean,
    used_questions_number integer,
    examiner_id bigint
);


ALTER TABLE test OWNER TO postgres;

--
-- Name: test_answer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE test_answer (
    id bigint NOT NULL,
    checked boolean,
    finished_on timestamp without time zone,
    max_points integer,
    started_on timestamp without time zone,
    question_sequence_id bigint,
    student_id bigint,
    test_id bigint
);


ALTER TABLE test_answer OWNER TO postgres;

--
-- Name: test_answer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE test_answer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE test_answer_id_seq OWNER TO postgres;

--
-- Name: test_answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE test_answer_id_seq OWNED BY test_answer.id;


--
-- Name: test_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE test_group (
    id bigint NOT NULL,
    created_date timestamp without time zone,
    name character varying(255),
    examiner_id bigint
);


ALTER TABLE test_group OWNER TO postgres;

--
-- Name: test_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE test_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE test_group_id_seq OWNER TO postgres;

--
-- Name: test_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE test_group_id_seq OWNED BY test_group.id;


--
-- Name: test_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE test_id_seq OWNER TO postgres;

--
-- Name: test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE test_id_seq OWNED BY test.id;


--
-- Name: test_test_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE test_test_group (
    id bigint NOT NULL,
    test_id bigint,
    test_group_id bigint
);


ALTER TABLE test_test_group OWNER TO postgres;

--
-- Name: test_test_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE test_test_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE test_test_group_id_seq OWNER TO postgres;

--
-- Name: test_test_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE test_test_group_id_seq OWNED BY test_test_group.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY choice_answer_part ALTER COLUMN id SET DEFAULT nextval('choice_answer_part_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY choice_pattern ALTER COLUMN id SET DEFAULT nextval('choice_pattern_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY image ALTER COLUMN id SET DEFAULT nextval('image_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY join_answer_part ALTER COLUMN id SET DEFAULT nextval('join_answer_part_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY join_pattern ALTER COLUMN id SET DEFAULT nextval('join_pattern_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY person ALTER COLUMN id SET DEFAULT nextval('person_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question ALTER COLUMN id SET DEFAULT nextval('question_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question_answer ALTER COLUMN id SET DEFAULT nextval('question_answer_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question_sequence ALTER COLUMN id SET DEFAULT nextval('question_sequence_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY settings ALTER COLUMN id SET DEFAULT nextval('settings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_test_group ALTER COLUMN id SET DEFAULT nextval('student_test_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test ALTER COLUMN id SET DEFAULT nextval('test_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_answer ALTER COLUMN id SET DEFAULT nextval('test_answer_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_group ALTER COLUMN id SET DEFAULT nextval('test_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_test_group ALTER COLUMN id SET DEFAULT nextval('test_test_group_id_seq'::regclass);


--
-- Data for Name: choice_answer_part; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY choice_answer_part (dtype, id, selected, simple_choice_answer_id, simple_choice_pattern_id, complex_choice_answer_id, complex_choice_pattern_id) FROM stdin;
\.


--
-- Name: choice_answer_part_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('choice_answer_part_id_seq', 1, false);


--
-- Data for Name: choice_pattern; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY choice_pattern (dtype, id, label, deselected_points, selected_points, correct_if_selected, complex_choice_question_id, simple_choice_question_id) FROM stdin;
\.


--
-- Name: choice_pattern_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('choice_pattern_id_seq', 1, false);


--
-- Data for Name: image; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY image (id, added_on, image, examiner_id) FROM stdin;
\.


--
-- Name: image_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('image_id_seq', 1, false);


--
-- Data for Name: join_answer_part; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY join_answer_part (id, first_option, second_option, join_answer_id, join_pattern_id) FROM stdin;
\.


--
-- Name: join_answer_part_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('join_answer_part_id_seq', 1, false);


--
-- Data for Name: join_pattern; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY join_pattern (id, first_option, second_option, join_question_id) FROM stdin;
\.


--
-- Name: join_pattern_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('join_pattern_id_seq', 1, false);


--
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY person (dtype, id, active, email, join_date, ldap_login, name, surname, added_by) FROM stdin;
\.


--
-- Name: person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('person_id_seq', 1, false);


--
-- Data for Name: question; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY question (dtype, id, content, has_time_limit, image_id, max_points, "position", time_limit, type, multiple, correct_points, incorrect_points, test_id) FROM stdin;
\.


--
-- Data for Name: question_answer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY question_answer (dtype, id, finished_on, started_on, type, comment, content, points, test_answer_id, freetext_question_id, simple_choice_question_id, join_question_id, complex_choice_question_id) FROM stdin;
\.


--
-- Name: question_answer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('question_answer_id_seq', 1, false);


--
-- Name: question_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('question_id_seq', 1, false);


--
-- Data for Name: question_sequence; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY question_sequence (id, sequence, test_answer_id) FROM stdin;
\.


--
-- Name: question_sequence_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('question_sequence_id_seq', 1, false);


--
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY settings (id, key, settings_type, value) FROM stdin;
\.


--
-- Name: settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('settings_id_seq', 1, false);


--
-- Data for Name: student_test_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY student_test_group (id, join_date, student_id, test_group_id) FROM stdin;
\.


--
-- Name: student_test_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('student_test_group_id_seq', 1, false);


--
-- Data for Name: test; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY test (id, active_from, active_till, created_date, description, has_time_limit, name, restrict_questions_number, time_limit, use_random_sequence, used_questions_number, examiner_id) FROM stdin;
\.


--
-- Data for Name: test_answer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY test_answer (id, checked, finished_on, max_points, started_on, question_sequence_id, student_id, test_id) FROM stdin;
\.


--
-- Name: test_answer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('test_answer_id_seq', 1, false);


--
-- Data for Name: test_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY test_group (id, created_date, name, examiner_id) FROM stdin;
\.


--
-- Name: test_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('test_group_id_seq', 1, false);


--
-- Name: test_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('test_id_seq', 1, false);


--
-- Data for Name: test_test_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY test_test_group (id, test_id, test_group_id) FROM stdin;
\.


--
-- Name: test_test_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('test_test_group_id_seq', 1, false);


--
-- Name: choice_answer_part_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY choice_answer_part
    ADD CONSTRAINT choice_answer_part_pkey PRIMARY KEY (id);


--
-- Name: choice_pattern_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY choice_pattern
    ADD CONSTRAINT choice_pattern_pkey PRIMARY KEY (id);


--
-- Name: image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY image
    ADD CONSTRAINT image_pkey PRIMARY KEY (id);


--
-- Name: join_answer_part_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY join_answer_part
    ADD CONSTRAINT join_answer_part_pkey PRIMARY KEY (id);


--
-- Name: join_pattern_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY join_pattern
    ADD CONSTRAINT join_pattern_pkey PRIMARY KEY (id);


--
-- Name: person_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);


--
-- Name: question_answer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question_answer
    ADD CONSTRAINT question_answer_pkey PRIMARY KEY (id);


--
-- Name: question_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question
    ADD CONSTRAINT question_pkey PRIMARY KEY (id);


--
-- Name: question_sequence_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question_sequence
    ADD CONSTRAINT question_sequence_pkey PRIMARY KEY (id);


--
-- Name: settings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: student_test_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_test_group
    ADD CONSTRAINT student_test_group_pkey PRIMARY KEY (id);


--
-- Name: test_answer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_answer
    ADD CONSTRAINT test_answer_pkey PRIMARY KEY (id);


--
-- Name: test_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_group
    ADD CONSTRAINT test_group_pkey PRIMARY KEY (id);


--
-- Name: test_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test
    ADD CONSTRAINT test_pkey PRIMARY KEY (id);


--
-- Name: test_test_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_test_group
    ADD CONSTRAINT test_test_group_pkey PRIMARY KEY (id);


--
-- Name: fk_1qxe8k3gwy7eq8tn2nue304oj; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_test_group
    ADD CONSTRAINT fk_1qxe8k3gwy7eq8tn2nue304oj FOREIGN KEY (student_id) REFERENCES person(id);


--
-- Name: fk_2523w21xpl0sxufv9o3ex1yv8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_answer
    ADD CONSTRAINT fk_2523w21xpl0sxufv9o3ex1yv8 FOREIGN KEY (test_id) REFERENCES test(id);


--
-- Name: fk_67lnya0b56vedcmlsqnue9rv1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question_answer
    ADD CONSTRAINT fk_67lnya0b56vedcmlsqnue9rv1 FOREIGN KEY (join_question_id) REFERENCES question(id);


--
-- Name: fk_84d3v19gb5iu05wsp1ridomub; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY choice_answer_part
    ADD CONSTRAINT fk_84d3v19gb5iu05wsp1ridomub FOREIGN KEY (simple_choice_answer_id) REFERENCES question_answer(id);


--
-- Name: fk_b0sdtqfxsvqxgmvduccucl6xr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test
    ADD CONSTRAINT fk_b0sdtqfxsvqxgmvduccucl6xr FOREIGN KEY (examiner_id) REFERENCES person(id);


--
-- Name: fk_b1p636ioku1d64t26l9vl90os; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY choice_answer_part
    ADD CONSTRAINT fk_b1p636ioku1d64t26l9vl90os FOREIGN KEY (simple_choice_pattern_id) REFERENCES choice_pattern(id);


--
-- Name: fk_bnuo9gk2u9u1idhstughcwer7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_test_group
    ADD CONSTRAINT fk_bnuo9gk2u9u1idhstughcwer7 FOREIGN KEY (test_group_id) REFERENCES test_group(id);


--
-- Name: fk_dwgwm1s61v0au1yjsgq0pg2y0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question_answer
    ADD CONSTRAINT fk_dwgwm1s61v0au1yjsgq0pg2y0 FOREIGN KEY (simple_choice_question_id) REFERENCES question(id);


--
-- Name: fk_dxio5rricmvb91n45lll346bs; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_answer
    ADD CONSTRAINT fk_dxio5rricmvb91n45lll346bs FOREIGN KEY (student_id) REFERENCES person(id);


--
-- Name: fk_easaaglwl095immj071w20su6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question_answer
    ADD CONSTRAINT fk_easaaglwl095immj071w20su6 FOREIGN KEY (test_answer_id) REFERENCES test_answer(id);


--
-- Name: fk_eiuajvs3ulwgmatmdfwx5qhd9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question_answer
    ADD CONSTRAINT fk_eiuajvs3ulwgmatmdfwx5qhd9 FOREIGN KEY (freetext_question_id) REFERENCES question(id);


--
-- Name: fk_ej8xd5qiydbjjx7ucuq73x85b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_answer
    ADD CONSTRAINT fk_ej8xd5qiydbjjx7ucuq73x85b FOREIGN KEY (question_sequence_id) REFERENCES question_sequence(id);


--
-- Name: fk_elfdmigxkrja61lgm8dpfwxh3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY choice_answer_part
    ADD CONSTRAINT fk_elfdmigxkrja61lgm8dpfwxh3 FOREIGN KEY (complex_choice_answer_id) REFERENCES question_answer(id);


--
-- Name: fk_fjof4vvccmqhtt043qp6hacvh; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY image
    ADD CONSTRAINT fk_fjof4vvccmqhtt043qp6hacvh FOREIGN KEY (examiner_id) REFERENCES person(id);


--
-- Name: fk_fy53rob66931hy0exc5xcq1jb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_group
    ADD CONSTRAINT fk_fy53rob66931hy0exc5xcq1jb FOREIGN KEY (examiner_id) REFERENCES person(id);


--
-- Name: fk_gcxrnddjo1mlkssv3xt1h32a7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY join_pattern
    ADD CONSTRAINT fk_gcxrnddjo1mlkssv3xt1h32a7 FOREIGN KEY (join_question_id) REFERENCES question(id);


--
-- Name: fk_hs3onv55avv2g6ldrcg94atjw; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY join_answer_part
    ADD CONSTRAINT fk_hs3onv55avv2g6ldrcg94atjw FOREIGN KEY (join_answer_id) REFERENCES question_answer(id);


--
-- Name: fk_kaeoxiqtunw02xmor9u6ysibk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question_answer
    ADD CONSTRAINT fk_kaeoxiqtunw02xmor9u6ysibk FOREIGN KEY (complex_choice_question_id) REFERENCES question(id);


--
-- Name: fk_l08yygxev3u7dhhnhwtnkvk24; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question_sequence
    ADD CONSTRAINT fk_l08yygxev3u7dhhnhwtnkvk24 FOREIGN KEY (test_answer_id) REFERENCES test_answer(id);


--
-- Name: fk_mgrybgqj20dm2dxs0h19yc7nx; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY choice_pattern
    ADD CONSTRAINT fk_mgrybgqj20dm2dxs0h19yc7nx FOREIGN KEY (simple_choice_question_id) REFERENCES question(id);


--
-- Name: fk_no074tuc603npng4e05db90ft; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY choice_pattern
    ADD CONSTRAINT fk_no074tuc603npng4e05db90ft FOREIGN KEY (complex_choice_question_id) REFERENCES question(id);


--
-- Name: fk_nvwo2n2reo9by9c74x884h5mk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY join_answer_part
    ADD CONSTRAINT fk_nvwo2n2reo9by9c74x884h5mk FOREIGN KEY (join_pattern_id) REFERENCES join_pattern(id);


--
-- Name: fk_oy5irork41ev38dgnkk9vn9lj; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_test_group
    ADD CONSTRAINT fk_oy5irork41ev38dgnkk9vn9lj FOREIGN KEY (test_group_id) REFERENCES test_group(id);


--
-- Name: fk_pe3veqrwoxdsgv7dpu0mnxqoh; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY choice_answer_part
    ADD CONSTRAINT fk_pe3veqrwoxdsgv7dpu0mnxqoh FOREIGN KEY (complex_choice_pattern_id) REFERENCES choice_pattern(id);


--
-- Name: fk_pr11ygjdvfcae1ug6pdcvsx00; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_test_group
    ADD CONSTRAINT fk_pr11ygjdvfcae1ug6pdcvsx00 FOREIGN KEY (test_id) REFERENCES test(id);


--
-- Name: fk_rox73wc9m1nkopm1nmgr70713; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question
    ADD CONSTRAINT fk_rox73wc9m1nkopm1nmgr70713 FOREIGN KEY (test_id) REFERENCES test(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;

--
-- Adding default admin account
--

INSERT INTO person(dtype, active, email, join_date, ldap_login, name, surname, added_by) 
	VALUES('Admin', true, :'ADMIN_EMAIL', current_timestamp, :'ADMIN_LOGIN', :'ADMIN_NAME', :'ADMIN_SURNAME', 'system');

--
-- Adding default settings
--

INSERT INTO public.settings(key, settings_type, value)
    VALUES ('password', 0, null);
INSERT INTO public.settings(key, settings_type, value)
    VALUES ('frequency', 0, 0);
INSERT INTO public.settings(key, settings_type, value)
    VALUES ('host', 0, '');
INSERT INTO public.settings(key, settings_type, value)
    VALUES ('username', 0, '');
INSERT INTO public.settings(key, settings_type, value)
    VALUES ('remotePath', 0, '');
INSERT INTO public.settings(key, settings_type, value)
    VALUES ('enableZip', 0, false);

