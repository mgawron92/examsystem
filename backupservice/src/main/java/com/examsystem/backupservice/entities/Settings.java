package com.examsystem.backupservice.entities;

import javax.persistence.*;

/**
 * Created by Mateusz Gawron on 2016-07-01.
 */
@Entity
public class Settings {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String key;

    private String value;

    @Column(name = "settings_type")
    private SettingsType settingsType = SettingsType.BACKUPS;

    public Settings() {
    }

    public Settings(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public SettingsType getSettingsType() {
        return settingsType;
    }

    public void setSettingsType(SettingsType settingsType) {
        this.settingsType = settingsType;
    }
}
