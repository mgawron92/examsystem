package com.examsystem.backupservice.entities;

/**
 * Created by Mateusz Gawron on 2016-07-21.
 */
public enum SettingsType {
    BACKUPS, DEFAULTS, LISTS
}
