package com.examsystem.backupservice.dao.api;

import com.examsystem.backupservice.entities.Settings;

import javax.ejb.Local;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-07-01.
 */
@Local
public interface BackupProperty {

    List<Settings> getBackupProperties() throws Exception;

    Settings getBackupPropertyByKey(String key) throws Exception;

}
