package com.examsystem.backupservice.dao.impl;

import com.examsystem.backupservice.dao.api.BackupProperty;
import com.examsystem.backupservice.entities.Settings;
import com.examsystem.backupservice.entities.SettingsType;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-07-01.
 */
@Stateful
public class BackupPropertyImpl implements BackupProperty {

    @PersistenceContext(unitName = "backupUnit", type = PersistenceContextType.EXTENDED)
    private EntityManager entityManager;

    public List<Settings> getBackupProperties() throws Exception {
        Query query = entityManager.createQuery("select s from Settings as s where s.settingsType = 0");
        return (List<Settings>) query.getResultList();
    }

    public Settings getBackupPropertyByKey(String key) throws Exception {
        Query query = entityManager.createQuery("select s from Settings as s where s.key = \'" + key + "\' and s.settingsType = 0");
        return (Settings)query.getSingleResult();
    }
}
