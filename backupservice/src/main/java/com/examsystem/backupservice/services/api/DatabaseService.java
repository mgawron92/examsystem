package com.examsystem.backupservice.services.api;

import javax.ejb.Local;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-07-01.
 */
@Local
public interface DatabaseService {

    Map<String, String> getBackupPropertiesMap();

    String getBackupPropertyByKey(String key) throws Exception;

}
