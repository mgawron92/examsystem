package com.examsystem.backupservice.services.impl;

import com.examsystem.backupservice.services.api.BackupService;
import com.examsystem.backupservice.services.api.DatabaseService;
import com.examsystem.backupservice.services.api.SSHService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by Mateusz Gawron on 2016-07-01.
 */
@Singleton
@Startup
public class BackupServiceImpl implements BackupService {

    private static final Logger logger = LoggerFactory.getLogger(BackupServiceImpl.class);

    @EJB
    private DatabaseService databaseService;

    @EJB
    private SSHService sshService;

    @Resource(name = "database.host")
    private String databaseHost;

    @Resource(name = "database.port")
    private String databasePort;

    @Resource(name = "database.username")
    private String databaseUsername;

    @Resource(name = "database.password")
    private String databasePassword;

    @Resource(name = "database.name")
    private String databaseName;

    private Boolean enableZip;

    private void refreshBackupProperties() throws Exception {
        Map<String, String> backupPropertiesMap = databaseService.getBackupPropertiesMap();
        sshService.setHost(backupPropertiesMap.get("host"));
        sshService.setPort(Integer.valueOf(backupPropertiesMap.get("port")));
        sshService.setCredentials(backupPropertiesMap.get("username"), backupPropertiesMap.get("password"));
        sshService.setRemoteDir(backupPropertiesMap.get("remotePath"));
        enableZip = Boolean.valueOf(backupPropertiesMap.get("enableZip"));
    }

    public void performBackup() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date now = new Date();

        String backupFilenameBase = "examsystem-db-" + format.format(now);
        String backupFilename = backupFilenameBase + ".dump";
        String zipFilename = backupFilenameBase + ".zip";

        File backupFile = new File(backupFilename);
        File zipFile = new File(zipFilename);
        File fileToUpload;

        logger.info("Refreshing backup properties from database");
        refreshBackupProperties();

        logger.info("Saving database backup: " + backupFile.getAbsolutePath());
        performDatabaseBackup(backupFilename);

        if (enableZip) {
            logger.info("Creating zip archive: " + zipFile.getAbsolutePath());
            createZip(backupFile.getAbsolutePath(), zipFile.getAbsolutePath());
            fileToUpload = zipFile;
        } else {
            fileToUpload = backupFile;
        }

        logger.info("Uploading file to remote server: " + fileToUpload.getAbsolutePath());
        uploadFile(fileToUpload);

        removeFileIfExists(backupFile);
        removeFileIfExists(zipFile);
    }

    private void performDatabaseBackup(String backupFilename) {
        String connectionString = String.format("postgresql://%s:%s@%s:%s/%s", databaseUsername, databasePassword, databaseHost, databasePort, databaseName);
        String executable = "pg_dump";
        String[] args = {executable, "--dbname=" + connectionString, "--file=" + backupFilename};
        runDatabaseBackupProcess(args);
    }

    private void runDatabaseBackupProcess(String[] args) {
        try {
            ProcessBuilder processBuilder = new ProcessBuilder(args);
            Process process = processBuilder.start();
            process.waitFor();
            if (process.exitValue() != 0) {
                logger.warn("Creating backup file failed");
            }
        } catch (Exception e) {
            logger.warn("Database dump file has not been created");
        }
    }

    private void uploadFile(File file) {
        try {
            if (file.exists()) {
                sshService.initConnector();
                sshService.sendFile(file.getAbsolutePath());
                sshService.closeConnector();
                logger.info("Successfully sent file: " + file.getAbsolutePath());
            } else {
                logger.warn("Attempting to upload file which does not exist: " + file.getAbsolutePath());
            }
        } catch (Exception e) {
            logger.warn("An exception occurred when trying to upload dump file");
        }
    }

    private void createZip(String dumpFilePath, String zipFilePath) {
        try {
            FileOutputStream fos = new FileOutputStream(zipFilePath);
            ZipOutputStream zos = new ZipOutputStream(fos);
            File dumpFile = new File(dumpFilePath);
            createZipEntry(zos, dumpFile);
            zos.close();
            logger.info("Zip archive has been created: " + zipFilePath);
        } catch(IOException e) {
            logger.warn("Process of creating zip archive has failed");
            e.printStackTrace();
        }
    }

    private void createZipEntry(ZipOutputStream zos, File file) throws IOException {
        ZipEntry ze = new ZipEntry(file.getName());
        zos.putNextEntry(ze);
        writeFileToZipOutputStream(zos, file);
        zos.closeEntry();
    }

    private void writeFileToZipOutputStream(ZipOutputStream zos, File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = fis.read(buffer)) > 0) {
            zos.write(buffer, 0, length);
        }
        fis.close();
    }

    private void removeFileIfExists(File file) {
        if (file.exists()) {
            if (file.delete()) {
                logger.info("Removed backup file: " + file.getAbsolutePath());
            } else {
                logger.warn("Unsuccessful attempt to remove backup file: " + file.getAbsolutePath());
            }
        }
    }

}
