package com.examsystem.backupservice.services.api;

import javax.ejb.Local;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Local
public interface CipherService {

    String encrypt(String text);

    String decrypt(String encrypted);

}
