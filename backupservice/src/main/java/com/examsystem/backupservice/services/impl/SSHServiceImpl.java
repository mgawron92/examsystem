package com.examsystem.backupservice.services.impl;

import com.examsystem.backupservice.services.api.SSHService;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.JSch;
import org.slf4j.*;

import javax.ejb.Stateless;
import java.io.*;
import java.util.Properties;

@Stateless
public class SSHServiceImpl implements SSHService {

	private static final Logger logger = LoggerFactory.getLogger(SSHServiceImpl.class);
	
	private String host;
	private int port;
	private String username;
	private String password;
	private String remoteDir;
	
	private Session session;
	private ChannelSftp channelSftp;
	
	public void setHost(String host) {
		this.host = host;
	}
	
	public void setPort(int port) {
		this.port = port;
	}
	
	public void setCredentials(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public void setRemoteDir(String remoteDir) 
		throws SftpException {
		this.remoteDir = remoteDir;
		if (channelSftp != null && !channelSftp.isClosed() && this.remoteDir != null) {
			channelSftp.cd(this.remoteDir);
		}
	}
	
	public void initConnector()
		throws JSchException, SftpException {
		logger.info("Initializing SSH connector");
		initSession();
		session.connect();
		initChannelSftp();
		channelSftp.connect();
		setRemoteDirIfSpecified();
	}
	
	public void closeConnector() {
		if (session == null || channelSftp == null) {
			return;
		}
		logger.info("Closing SSH connector");
		session.disconnect();
		channelSftp.disconnect();
	}
	
	private void initSession() 
		throws JSchException {
		session = new JSch().getSession(username, host, port);
		session.setPassword(password);
		initSessionProperties();
	}
	
	private void initSessionProperties() {
		Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
	}
	
	private void initChannelSftp()
		throws JSchException {
		channelSftp = (ChannelSftp)session.openChannel("sftp");
	}
	
	private void setRemoteDirIfSpecified()
		throws SftpException {
		if (this.remoteDir != null) {
			setRemoteDir(this.remoteDir);			
		}
	}
	
	public void sendFile(String localFilePath)
		throws IOException, SftpException {
		File file = new File(localFilePath);
		FileInputStream fis = new FileInputStream(file);
		channelSftp.put(fis, file.getName());
		fis.close();
		logger.info("Successfully sent file over SSH: " + localFilePath);
	}
	
	public void recieveFile(String localFilePath, String remoteFileName)
		throws IOException, SftpException {
		BufferedInputStream remoteFileInputStream = getRemoteFileInputStream(remoteFileName);
		BufferedOutputStream localFileOutputStream = getLocalFileOutputStream(localFilePath);
		
		transferData(remoteFileInputStream, localFileOutputStream);
		
		remoteFileInputStream.close();
		localFileOutputStream.close();

		logger.info("Successfully recieved file over SSH: " + localFilePath);
	}
	
	private BufferedInputStream getRemoteFileInputStream(String remoteFileName)
		throws SftpException {
		InputStream inputStream = channelSftp.get(remoteFileName);
		return new BufferedInputStream(inputStream);
	}
	
	private BufferedOutputStream getLocalFileOutputStream(String localFilePath)
		throws FileNotFoundException {
		File localFile = new File(localFilePath);
		OutputStream outputStream = new FileOutputStream(localFile);
		return new BufferedOutputStream(outputStream);
	}
	
	private void transferData(BufferedInputStream input, BufferedOutputStream output)
		throws IOException {
		byte[] buffer = new byte[1024];
		int readCount;
		
		while( (readCount = input.read(buffer)) > 0) {
			output.write(buffer, 0, readCount);
		}
	}
	
}