package com.examsystem.backupservice.services.impl;

import com.examsystem.backupservice.dao.api.BackupProperty;
import com.examsystem.backupservice.entities.Settings;
import com.examsystem.backupservice.services.api.CipherService;
import com.examsystem.backupservice.services.api.DatabaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-07-01.
 */
@Stateless
public class DatabaseServiceImpl implements DatabaseService {

    private static final String PASSWORD_KEY = "password";

    private static final Logger logger = LoggerFactory.getLogger(DatabaseServiceImpl.class);

    @EJB
    private BackupProperty backupProperty;

    @EJB
    private CipherService cipherService;

    public Map<String, String> getBackupPropertiesMap() {
        Map<String, String> backupPropertiesMap = new HashMap<>();

        try {
            List<Settings> backupProperties = backupProperty.getBackupProperties();
            logger.info("Retrieved " + backupProperties.size() + " properites from database");

            for (Settings settings : backupProperties) {
                logger.info("Setting property with key: " + settings.getKey());
                if (PASSWORD_KEY.equals(settings.getKey())) {
                    backupPropertiesMap.put(settings.getKey(), cipherService.decrypt(settings.getValue()));
                } else {
                    backupPropertiesMap.put(settings.getKey(), settings.getValue());
                }
            }
        } catch (Exception e) {
            logger.warn("Unable to retrieve backup properties from database");
        }
        return backupPropertiesMap;
    }

    public String getBackupPropertyByKey(String key) throws Exception {
        return backupProperty.getBackupPropertyByKey(key).getValue();
    }

}
