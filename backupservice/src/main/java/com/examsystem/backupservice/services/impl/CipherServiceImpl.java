package com.examsystem.backupservice.services.impl;

import com.examsystem.backupservice.services.api.CipherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.Stateless;
import java.nio.charset.StandardCharsets;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Stateless
public class CipherServiceImpl implements CipherService {

    private static final Logger logger = LoggerFactory.getLogger(CipherServiceImpl.class);

    @Resource(name = "cipher.algorithm")
    private String algorithm;

    @Resource(name = "cipher.key")
    private String key;

    private SecretKey secretKey;

    private Cipher cipher;

    @PostConstruct
    public void construct() throws Exception {
        secretKey = new SecretKeySpec(key.getBytes(), algorithm);
        cipher = Cipher.getInstance(algorithm);
    }

    public String encrypt(String text) {
        try {
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] bytes = cipher.doFinal(text.getBytes(StandardCharsets.UTF_16));
            return encryptedToString(bytes);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String decrypt(String encrypted) {
        try {
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] bytes = cipher.doFinal(stringToEncrypted(encrypted));
            return new String(bytes, StandardCharsets.UTF_16);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private byte[] stringToEncrypted(String encrypted) {
        char[] chars = encrypted.toCharArray();
        byte[] bytes = new byte[encrypted.length()];
        for (int i = 0; i < chars.length; ++i) {
            bytes[i] = (byte) (chars[i] - 128);
        }
        return bytes;
    }

    private String encryptedToString(byte[] bytes) {
        char[] chars = new char[bytes.length];
        for (int i = 0; i < bytes.length; ++i) {
            chars[i] = (char) (bytes[i] + 128);
        }
        return new String(chars);
    }

}
