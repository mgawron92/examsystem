package com.examsystem.backupservice.services.api;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import javax.ejb.Local;
import java.io.FileNotFoundException;
import java.io.IOException;

@Local
public interface SSHService {

	void setHost(String host);
	
	void setPort(int port);
	
	void setCredentials(String username, String password);
	
	void setRemoteDir(String remoteDir) throws SftpException;

	void initConnector() throws JSchException, SftpException;
	
	void closeConnector();
	
	void sendFile(String localFilePath) throws IOException, SftpException;
	
	void recieveFile(String localFilePath, String remoteFileName) throws IOException, SftpException;

}