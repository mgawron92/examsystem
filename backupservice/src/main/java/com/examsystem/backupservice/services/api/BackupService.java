package com.examsystem.backupservice.services.api;

import javax.ejb.Local;

/**
 * Created by Mateusz Gawron on 2016-07-01.
 */
@Local
public interface BackupService {

    void performBackup() throws Exception;

}
