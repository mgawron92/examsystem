package com.examsystem.backupservice.services.impl;

import com.examsystem.backupservice.services.api.BackupService;
import com.examsystem.backupservice.services.api.DatabaseService;
import com.examsystem.backupservice.services.api.TimerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Mateusz Gawron on 2016-07-01.
 */
@Singleton
@Startup
public class TimerServiceImpl implements TimerService {

    private static final Logger logger = LoggerFactory.getLogger(TimerServiceImpl.class);

    @EJB
    private BackupService backupService;

    @EJB
    private DatabaseService databaseService;

    private int lastFrequency = 0;

    private int cyclesToBackup = 0;

    @Schedule(minute = "*/10", hour = "*")
    public void checkForBackup() throws Exception {
        int frequency = Integer.valueOf(databaseService.getBackupPropertyByKey("frequency"));
        if (frequency != lastFrequency) {
            cyclesToBackup = 0;
            lastFrequency = frequency;
            if (frequency != 0) {
                logger.info(getCurrentDate() + ": Detected backup frequency change [new frequency: " + frequency + " hours]");
            } else {
                logger.info(getCurrentDate() + ": Detected backup frequency change [new frequency: never]");
            }
        }
        if (lastFrequency != 0) {
            if (cyclesToBackup <= 0) {
                logger.info(getCurrentDate() + ": Performing backup");
                cyclesToBackup = 6 * lastFrequency;
                backupService.performBackup();
            } else {
                logger.info(getCurrentDate() + ": Checking if backup is necessary [next backup in: " + (10 * cyclesToBackup) + " minutes]");
            }
            --cyclesToBackup;
        } else {
            logger.info(getCurrentDate() + ": Checking if backup is necessary [next backup in: never]");
        }
    }

    private String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        return dateFormat.format(now);
    }

}
