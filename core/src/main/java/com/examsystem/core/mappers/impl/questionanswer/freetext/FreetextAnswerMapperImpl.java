package com.examsystem.core.mappers.impl.questionanswer.freetext;

import com.examsystem.core.entitites.questionanswer.freetext.FreetextAnswer;
import com.examsystem.core.mappers.api.questionanswer.freetext.FreetextAnswerMapper;
import com.examsystem.core.resources.questionanswer.freetext.FreetextAnswerResource;
import com.examsystem.core.updaters.api.questionanswer.freetext.FreetextAnswerUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class FreetextAnswerMapperImpl implements FreetextAnswerMapper {

    @Autowired
    private FreetextAnswerUpdater freetextAnswerUpdater;

    @Override
    public FreetextAnswerResource mapEntityToResource(FreetextAnswer entity) {
        FreetextAnswerResource resource = new FreetextAnswerResource();
        resource.setId(entity.getId());
        resource.setContent(entity.getContent());
        resource.setStartedOn(entity.getStartedOn());
        resource.setFinishedOn(entity.getFinishedOn());
        resource.setPoints(entity.getPoints());
        resource.setComment(entity.getComment());
        if (entity.getTestAnswer() != null) {
            resource.setTestAnswerId(entity.getTestAnswer().getId());
        }
        if (entity.getFreetextQuestion() != null) {
            resource.setFreetextQuestionId(entity.getFreetextQuestion().getId());
        }
        resource.setMaxPoints(entity.getFreetextQuestion().getMaxPoints());
        resource.setQuestionId(entity.getFreetextQuestion().getId());
        return resource;
    }

    @Override
    public FreetextAnswer mapResourceToEntity(FreetextAnswerResource resource) {
        FreetextAnswer entity = new FreetextAnswer();
        entity.setId(resource.getId());
        freetextAnswerUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
