package com.examsystem.core.mappers.impl.question.choice.simple;

import com.examsystem.core.entitites.question.choice.simple.SimpleChoicePattern;
import com.examsystem.core.entitites.questionanswer.choice.ChoiceAnswerPart;
import com.examsystem.core.mappers.api.question.choice.simple.SimpleChoicePatternMapper;
import com.examsystem.core.resources.question.choice.simple.SimpleChoicePatternResource;
import com.examsystem.core.updaters.api.question.choice.simple.SimpleChoicePatternUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class SimpleChoicePatternMapperImpl implements SimpleChoicePatternMapper {

    @Autowired
    private SimpleChoicePatternUpdater simpleChoicePatternUpdater;

    @Override
    public SimpleChoicePatternResource mapEntityToResource(SimpleChoicePattern entity) {
        SimpleChoicePatternResource resource = new SimpleChoicePatternResource();
        resource.setId(entity.getId());
        resource.setLabel(entity.getLabel());
        resource.setCorrectIfSelected(entity.getCorrectIfSelected());
        if (entity.getChoiceQuestion() != null) {
            resource.setChoiceQuestionId(entity.getChoiceQuestion().getId());
        }
        resource.setChoiceAnswerPartIds(entity.getChoiceAnswerParts().stream().map(ChoiceAnswerPart::getId).collect(Collectors.toList()));
        return resource;
    }

    @Override
    public SimpleChoicePattern mapResourceToEntity(SimpleChoicePatternResource resource) {
        SimpleChoicePattern entity = new SimpleChoicePattern();
        entity.setId(resource.getId());
        simpleChoicePatternUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
