package com.examsystem.core.mappers.impl.questionanswer.choice.simple;

import com.examsystem.core.entitites.questionanswer.choice.simple.SimpleChoiceAnswerPart;
import com.examsystem.core.mappers.api.questionanswer.choice.simple.SimpleChoiceAnswerPartMapper;
import com.examsystem.core.resources.questionanswer.choice.simple.SimpleChoiceAnswerPartResource;
import com.examsystem.core.updaters.api.questionanswer.choice.simple.SimpleChoiceAnswerPartUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class SimpleChoiceAnswerPartMapperImpl implements SimpleChoiceAnswerPartMapper {

    @Autowired
    private SimpleChoiceAnswerPartUpdater simpleChoiceAnswerPartUpdater;

    @Override
    public SimpleChoiceAnswerPartResource mapEntityToResource(SimpleChoiceAnswerPart entity) {
        SimpleChoiceAnswerPartResource resource = new SimpleChoiceAnswerPartResource();
        resource.setId(entity.getId());
        resource.setSelected(entity.getSelected());
        if (entity.getChoiceAnswer() != null) {
            resource.setChoiceAnswerId(entity.getChoiceAnswer().getId());
        }
        if (entity.getChoicePattern() != null) {
            resource.setChoicePatternId(entity.getChoicePattern().getId());
        }
        boolean correctIfSelected = entity.getChoicePattern().getCorrectIfSelected();
        boolean selected = entity.getSelected();
        boolean correct = (correctIfSelected == selected);
        resource.setCorrect(correct);
        return resource;
    }

    @Override
    public SimpleChoiceAnswerPart mapResourceToEntity(SimpleChoiceAnswerPartResource resource) {
        SimpleChoiceAnswerPart entity = new SimpleChoiceAnswerPart();
        entity.setId(resource.getId());
        simpleChoiceAnswerPartUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
