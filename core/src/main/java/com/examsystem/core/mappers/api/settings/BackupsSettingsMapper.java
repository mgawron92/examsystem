package com.examsystem.core.mappers.api.settings;

import com.examsystem.core.entitites.other.Settings;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.settings.BackupsResource;

import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface BackupsSettingsMapper extends FullMapper<List<Settings>, BackupsResource> {

}
