package com.examsystem.core.mappers.api.test;

import com.examsystem.core.entitites.test.Test;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.test.TestResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface TestMapper extends FullMapper<Test, TestResource> {

}
