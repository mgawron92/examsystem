package com.examsystem.core.mappers.impl.questionanswer.join;

import com.examsystem.core.entitites.questionanswer.join.JoinAnswer;
import com.examsystem.core.mappers.api.questionanswer.join.JoinAnswerMapper;
import com.examsystem.core.mappers.api.questionanswer.join.JoinAnswerPartMapper;
import com.examsystem.core.resources.questionanswer.join.JoinAnswerPartResource;
import com.examsystem.core.resources.questionanswer.join.JoinAnswerResource;
import com.examsystem.core.updaters.api.questionanswer.join.JoinAnswerUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class JoinAnswerMapperImpl implements JoinAnswerMapper {

    @Autowired
    private JoinAnswerPartMapper joinAnswerPartMapper;

    @Autowired
    private JoinAnswerUpdater joinAnswerUpdater;

    @Override
    public JoinAnswerResource mapEntityToResource(JoinAnswer entity) {
        JoinAnswerResource resource = new JoinAnswerResource();
        resource.setId(entity.getId());
        resource.setStartedOn(entity.getStartedOn());
        resource.setFinishedOn(entity.getFinishedOn());
        if (entity.getTestAnswer() != null) {
            resource.setTestAnswerId(entity.getTestAnswer().getId());
        }
        if (entity.getJoinQuestion() != null) {
            resource.setJoinQuestionId(entity.getJoinQuestion().getId());
        }
        resource.setJoinAnswerParts(entity.getJoinAnswerParts().stream().map(joinAnswerPartMapper::mapEntityToResource).collect(Collectors.toList()));
        boolean correct = true;
        for (JoinAnswerPartResource joinAnswerPartResource : resource.getJoinAnswerParts()) {
            if (!joinAnswerPartResource.getCorrect()) {
                correct = false;
                break;
            }
        }
        int points = correct ? entity.getJoinQuestion().getCorrectPoints() : entity.getJoinQuestion().getIncorrectPoints();
        int maxPoints = Math.max(entity.getJoinQuestion().getCorrectPoints(), entity.getJoinQuestion().getIncorrectPoints());
        resource.setPoints(points);
        resource.setMaxPoints(maxPoints);
        resource.setQuestionId(entity.getJoinQuestion().getId());
        return resource;
    }

    @Override
    public JoinAnswer mapResourceToEntity(JoinAnswerResource resource) {
        JoinAnswer entity = new JoinAnswer();
        entity.setId(resource.getId());
        joinAnswerUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
