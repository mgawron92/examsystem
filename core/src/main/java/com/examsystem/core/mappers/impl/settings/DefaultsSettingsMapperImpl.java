package com.examsystem.core.mappers.impl.settings;

import com.examsystem.core.entitites.other.Settings;
import com.examsystem.core.enumerations.SettingsType;
import com.examsystem.core.mappers.api.settings.DefaultsSettingsMapper;
import com.examsystem.core.resources.settings.DefaultsResource;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class DefaultsSettingsMapperImpl implements DefaultsSettingsMapper {

    @Override
    public DefaultsResource mapEntityToResource(List<Settings> entities) {
        DefaultsResource resource = new DefaultsResource();
        for (Settings s : entities) {
            switch (s.getKey()) {
                case "testActiveAfterDays":
                    resource.setTestActiveAfterDays(Integer.valueOf(s.getValue()));
                    break;
                case "testActiveTime":
                    resource.setTestActiveTime(Integer.valueOf(s.getValue()));
                    break;
                case "testTimeLimit":
                    resource.setTestLimitTime(Integer.valueOf(s.getValue()));
                    break;
                case "hasTestTimeLimit":
                    resource.setHasTestTimeLimit(Boolean.valueOf(s.getValue()));
                    break;
                case "restrictQuestionsNumber":
                    resource.setRestrictQuestionsNumber(Boolean.valueOf(s.getValue()));
                    break;
                case "useRandomSequence":
                    resource.setUseRandomSequence(Boolean.valueOf(s.getValue()));
                    break;
                case "questionTimeLimitMinutes":
                    resource.setQuestionLimitTimeMinutes(Integer.valueOf(s.getValue()));
                    break;
                case "questionTimeLimitSeconds":
                    resource.setQuestionLimitTimeSeconds(Integer.valueOf(s.getValue()));
                    break;
                case "hasQuestionTimeLimit":
                    resource.setHasQuestionTimeLimit(Boolean.valueOf(s.getValue()));
                    break;
            }
        }
        return resource;
    }

    @Override
    public List<Settings> mapResourceToEntity(DefaultsResource resource) {
        List<Settings> entities = new LinkedList<>();
        entities.add(new Settings("testActiveAfterDays", resource.getTestActiveAfterDays().toString(), SettingsType.LISTS));
        entities.add(new Settings("testActiveTime", resource.getTestActiveTime().toString(), SettingsType.LISTS));
        entities.add(new Settings("testTimeLimit", resource.getTestLimitTime().toString(), SettingsType.LISTS));
        entities.add(new Settings("hasTestTimeLimit", resource.getHasTestTimeLimit().toString(), SettingsType.LISTS));
        entities.add(new Settings("restrictQuestionsNumber", resource.getRestrictQuestionsNumber().toString(), SettingsType.LISTS));
        entities.add(new Settings("useRandomSequence", resource.getUseRandomSequence().toString(), SettingsType.LISTS));
        entities.add(new Settings("questionTimeLimitMinutes", resource.getQuestionLimitTimeMinutes().toString(), SettingsType.LISTS));
        entities.add(new Settings("questionTimeLimitSeconds", resource.getQuestionLimitTimeSeconds().toString(), SettingsType.LISTS));
        entities.add(new Settings("hasQuestionTimeLimit", resource.getHasQuestionTimeLimit().toString(), SettingsType.LISTS));
        return entities;
    }

}
