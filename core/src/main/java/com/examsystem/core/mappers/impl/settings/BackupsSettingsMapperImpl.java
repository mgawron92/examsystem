package com.examsystem.core.mappers.impl.settings;

import com.examsystem.core.entitites.other.Settings;
import com.examsystem.core.enumerations.SettingsType;
import com.examsystem.core.mappers.api.settings.BackupsSettingsMapper;
import com.examsystem.core.resources.settings.BackupsResource;
import com.examsystem.core.services.api.cipher.CipherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class BackupsSettingsMapperImpl implements BackupsSettingsMapper {

    @Autowired
    private CipherService cipherService;

    @Override
    public BackupsResource mapEntityToResource(List<Settings> entities) {
        BackupsResource backupsResource = new BackupsResource();
        for (Settings s : entities) {
            switch (s.getKey()) {
                case "frequency":
                    backupsResource.setFrequency(Integer.valueOf(s.getValue()));
                    break;
                case "host":
                    backupsResource.setHost(s.getValue());
                    break;
                case "port":
                    backupsResource.setPort(Integer.valueOf(s.getValue()));
                    break;
                case "username":
                    backupsResource.setUsername(s.getValue());
                    break;
                case "remotePath":
                    backupsResource.setRemotePath(s.getValue());
                    break;
                case "enableZip":
                    backupsResource.setEnableZip(Boolean.valueOf(s.getValue()));
                    break;
            }
        }
        return backupsResource;
    }

    @Override
    public List<Settings> mapResourceToEntity(BackupsResource resource) {
        List<Settings> entities = new LinkedList<>();
        entities.add(new Settings("frequency", resource.getFrequency().toString(), SettingsType.BACKUPS));
        entities.add(new Settings("host", resource.getHost(), SettingsType.BACKUPS));
        if (resource.getPort() != null) {
            entities.add(new Settings("port", resource.getPort().toString(), SettingsType.BACKUPS));
        }
        entities.add(new Settings("username", resource.getUsername(), SettingsType.BACKUPS));
        if (resource.getChangePassword()) {
            entities.add(new Settings("password", cipherService.encrypt(resource.getPassword()), SettingsType.BACKUPS));
        }
        entities.add(new Settings("remotePath", resource.getRemotePath(), SettingsType.BACKUPS));
        entities.add(new Settings("enableZip", resource.getEnableZip().toString(), SettingsType.BACKUPS));
        return entities;
    }

}
