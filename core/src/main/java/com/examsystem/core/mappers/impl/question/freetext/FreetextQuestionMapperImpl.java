package com.examsystem.core.mappers.impl.question.freetext;

import com.examsystem.core.entitites.question.freetext.FreetextQuestion;
import com.examsystem.core.entitites.questionanswer.freetext.FreetextAnswer;
import com.examsystem.core.mappers.api.question.freetext.FreetextQuestionMapper;
import com.examsystem.core.resources.question.freetext.FreetextQuestionResource;
import com.examsystem.core.updaters.api.question.freetext.FreetextQuestionUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class FreetextQuestionMapperImpl implements FreetextQuestionMapper {

    @Autowired
    private FreetextQuestionUpdater freetextQuestionUpdater;

    @Override
    public FreetextQuestionResource mapEntityToResource(FreetextQuestion entity) {
        FreetextQuestionResource resource = new FreetextQuestionResource();
        resource.setId(entity.getId());
        resource.setContent(entity.getContent());
        resource.setPosition(entity.getPosition());
        resource.setImageId(entity.getImageId());
        int timeLimitMinutes = entity.getTimeLimit() / 60;
        int timeLimitSeconds = entity.getTimeLimit() % 60;
        resource.setTimeLimitMinutes(timeLimitMinutes);
        resource.setTimeLimitSeconds(timeLimitSeconds);
        resource.setHasTimeLimit(entity.getHasTimeLimit());
        resource.setMaxPoints(entity.getMaxPoints());
        if (entity.getTest() != null) {
            resource.setTestId(entity.getTest().getId());
        }
        resource.setFreetextAnswerIds(entity.getFreetextAnswers().stream().map(FreetextAnswer::getId).collect(Collectors.toList()));
        resource.setQuestionAnswerIds(entity.getFreetextAnswers().stream().map(FreetextAnswer::getId).collect(Collectors.toList()));
        return resource;
    }

    @Override
    public FreetextQuestion mapResourceToEntity(FreetextQuestionResource resource) {
        FreetextQuestion entity = new FreetextQuestion();
        entity.setId(resource.getId());
        freetextQuestionUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
