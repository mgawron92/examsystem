package com.examsystem.core.mappers.api.user;

import com.examsystem.core.entitites.user.Examiner;
import com.examsystem.core.resources.user.ExaminerResource;
import com.examsystem.core.mappers.api.FullMapper;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface ExaminerMapper extends FullMapper<Examiner, ExaminerResource> {

}
