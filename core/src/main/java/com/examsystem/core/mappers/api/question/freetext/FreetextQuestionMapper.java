package com.examsystem.core.mappers.api.question.freetext;

import com.examsystem.core.entitites.question.freetext.FreetextQuestion;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.question.freetext.FreetextQuestionResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface FreetextQuestionMapper extends FullMapper<FreetextQuestion, FreetextQuestionResource> {

}
