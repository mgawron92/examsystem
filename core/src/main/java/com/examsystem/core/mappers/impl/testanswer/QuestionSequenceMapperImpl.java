package com.examsystem.core.mappers.impl.testanswer;

import com.examsystem.core.entitites.testanswer.QuestionSequence;
import com.examsystem.core.mappers.api.testanswer.QuestionSequenceMapper;
import com.examsystem.core.repositories.TestAnswerRepository;
import com.examsystem.core.resources.testanswer.QuestionSequenceResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class QuestionSequenceMapperImpl implements QuestionSequenceMapper {

    @Autowired
    private TestAnswerRepository testAnswerRepository;

    @Override
    public QuestionSequenceResource mapEntityToResource(QuestionSequence entity) {
        QuestionSequenceResource resource = new QuestionSequenceResource();
        resource.setId(entity.getId());
        if (entity.getTestAnswer() != null) {
            resource.setTestAnswerId(entity.getTestAnswer().getId());
        }
        resource.setSequence(Arrays.stream(entity.getSequence().split(",")).map(Long::valueOf).collect(Collectors.toList()));
        return resource;
    }

    @Override
    public QuestionSequence mapResourceToEntity(QuestionSequenceResource resource) {
        QuestionSequence entity = new QuestionSequence();
        entity.setId(resource.getId());
        entity.setTestAnswer(testAnswerRepository.findTestAnswerById(resource.getTestAnswerId()));
        entity.setSequence(resource.getSequence().stream().map(String::valueOf).reduce((s1, s2) -> s1 + "," + s2).get());
        return entity;
    }

}
