package com.examsystem.core.mappers.impl.question.choice.complex;

import com.examsystem.core.entitites.question.choice.complex.ComplexChoicePattern;
import com.examsystem.core.entitites.questionanswer.choice.ChoiceAnswerPart;
import com.examsystem.core.mappers.api.question.choice.complex.ComplexChoicePatternMapper;
import com.examsystem.core.resources.question.choice.complex.ComplexChoicePatternResource;
import com.examsystem.core.updaters.api.question.choice.complex.ComplexChoicePatternUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class ComplexChoicePatternMapperImpl implements ComplexChoicePatternMapper {

    @Autowired
    private ComplexChoicePatternUpdater complexChoicePatternUpdater;

    @Override
    public ComplexChoicePatternResource mapEntityToResource(ComplexChoicePattern entity) {
        ComplexChoicePatternResource resource = new ComplexChoicePatternResource();
        resource.setId(entity.getId());
        resource.setLabel(entity.getLabel());
        resource.setSelectedPoints(entity.getSelectedPoints());
        resource.setDeselectedPoints(entity.getDeselectedPoints());
        if (entity.getChoiceQuestion() != null) {
            resource.setChoiceQuestionId(entity.getChoiceQuestion().getId());
        }
        resource.setChoiceAnswerPartIds(entity.getChoiceAnswerParts().stream().map(ChoiceAnswerPart::getId).collect(Collectors.toList()));
        return resource;
    }

    @Override
    public ComplexChoicePattern mapResourceToEntity(ComplexChoicePatternResource resource) {
        ComplexChoicePattern entity = new ComplexChoicePattern();
        entity.setId(resource.getId());
        complexChoicePatternUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
