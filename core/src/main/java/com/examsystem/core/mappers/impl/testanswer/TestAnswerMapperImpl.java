package com.examsystem.core.mappers.impl.testanswer;

import com.examsystem.core.entitites.questionanswer.QuestionAnswer;
import com.examsystem.core.entitites.testanswer.TestAnswer;
import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.mappers.api.testanswer.TestAnswerMapper;
import com.examsystem.core.repositories.TestAnswerRepository;
import com.examsystem.core.resources.questionanswer.QuestionAnswerResource;
import com.examsystem.core.resources.testanswer.TestAnswerResource;
import com.examsystem.core.services.api.questionanswer.QuestionAnswerService;
import com.examsystem.core.services.api.questionanswer.choice.complex.ComplexChoiceAnswerService;
import com.examsystem.core.services.api.questionanswer.choice.simple.SimpleChoiceAnswerService;
import com.examsystem.core.services.api.questionanswer.freetext.FreetextAnswerService;
import com.examsystem.core.services.api.questionanswer.join.JoinAnswerService;
import com.examsystem.core.updaters.api.testanswer.TestAnswerUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class TestAnswerMapperImpl implements TestAnswerMapper {

    @Autowired
    private TestAnswerUpdater testAnswerUpdater;

    @Autowired
    private TestAnswerRepository testAnswerRepository;

    @Autowired
    private ComplexChoiceAnswerService complexChoiceAnswerService;

    @Autowired
    private SimpleChoiceAnswerService simpleChoiceAnswerService;

    @Autowired
    private JoinAnswerService joinAnswerService;

    @Autowired
    private FreetextAnswerService freetextAnswerService;

    @Override
    public TestAnswerResource mapEntityToResource(TestAnswer entity) {
        TestAnswerResource resource = new TestAnswerResource();
        resource.setId(entity.getId());
        resource.setStartedOn(entity.getStartedOn());
        resource.setFinishedOn(entity.getFinishedOn());
        resource.setChecked(entity.getChecked());
        if (entity.getStudent() != null) {
            resource.setStudentId(entity.getStudent().getId());
        }
        if (entity.getQuestionSequence() != null) {
            resource.setQuestionSequenceId(entity.getQuestionSequence().getId());
        }
        if (entity.getTest() != null) {
            resource.setTestId(entity.getTest().getId());
        }
        resource.setQuestionAnswerIds(entity.getQuestionAnswers().stream().map(QuestionAnswer::getId).collect(Collectors.toList()));
        int points = 0;
        for (Long questionAnswerId : resource.getQuestionAnswerIds()) {
            QuestionAnswerService questionAnswerService = getService(getQuestionType(entity.getId(), questionAnswerId));
            QuestionAnswerResource questionAnswerResource = questionAnswerService.getQuestionAnswerById(questionAnswerId);
            if (questionAnswerResource.getPoints() != null) {
                points += questionAnswerResource.getPoints();
            }
        }
        resource.setPoints(points);
        resource.setMaxPoints(entity.getMaxPoints());
        return resource;
    }

    @Override
    public TestAnswer mapResourceToEntity(TestAnswerResource resource) {
        TestAnswer entity = new TestAnswer();
        entity.setId(resource.getId());
        testAnswerUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

    public QuestionType getQuestionType(Long tid, Long qid) {
        List<QuestionAnswer> questionAnswers = testAnswerRepository.findTestAnswerById(tid).getQuestionAnswers();
        for (QuestionAnswer questionAnswer : questionAnswers) {
            if (questionAnswer.getId().equals(qid)) {
                return questionAnswer.getType();
            }
        }
        return null;
    }

    private QuestionAnswerService getService(QuestionType questionType) {
        switch (questionType) {
            case SINGLE_SIMPLE_CHOICE:
            case MULTIPLE_SIMPLE_CHOICE:
                return simpleChoiceAnswerService;
            case SIMPLE_COMPLEX_CHOICE:
            case MULTIPLE_COMPLEX_CHOICE:
                return complexChoiceAnswerService;
            case FREETEXT:
                return freetextAnswerService;
            case JOIN:
                return joinAnswerService;
        }
        return null;
    }

}
