package com.examsystem.core.mappers.api.question.choice.complex;

import com.examsystem.core.entitites.question.choice.complex.ComplexChoicePattern;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.question.choice.complex.ComplexChoicePatternResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface ComplexChoicePatternMapper extends FullMapper<ComplexChoicePattern, ComplexChoicePatternResource> {

}
