package com.examsystem.core.mappers.impl.user;

import com.examsystem.core.entitites.testanswer.TestAnswer;
import com.examsystem.core.entitites.user.Student;
import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.mappers.api.intermediary.StudentTestGroupMapper;
import com.examsystem.core.repositories.TestAnswerRepository;
import com.examsystem.core.resources.user.StudentResource;
import com.examsystem.core.mappers.api.user.StudentMapper;
import com.examsystem.core.updaters.api.user.PersonUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class StudentMapperImpl implements StudentMapper {

    @Autowired
    private PersonUpdater personUpdater;

    @Autowired
    private StudentTestGroupMapper studentTestGroupMapper;

    @Autowired
    private TestAnswerRepository testAnswerRepository;

    @Override
    public StudentResource mapEntityToResource(Student entity) {
        StudentResource resource = new StudentResource();
        personUpdater.updateResourceFromEntity(resource, entity);
        resource.setRole(UserRole.STUDENT);
        resource.setStudentTestGroups(entity.getStudentTestGroups().stream().map(studentTestGroupMapper::mapEntityToResource).collect(Collectors.toList()));
        resource.setTestAnswerIds(entity.getTestAnswers().stream().map(TestAnswer::getId).collect(Collectors.toList()));
        return resource;
    }

    @Override
    public Student mapResourceToEntity(StudentResource resource) {
        Student entity = new Student();
        personUpdater.updateEntityFromResource(entity, resource);
        entity.setTestAnswers(resource.getTestAnswerIds().stream().map((Long id) -> testAnswerRepository.findTestAnswerById(id)).collect(Collectors.toList()));
        entity.setStudentTestGroups(resource.getStudentTestGroups().stream().map(studentTestGroupMapper::mapResourceToEntity).collect(Collectors.toList()));
        return entity;
    }

}
