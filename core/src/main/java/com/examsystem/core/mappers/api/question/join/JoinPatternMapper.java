package com.examsystem.core.mappers.api.question.join;

import com.examsystem.core.entitites.question.join.JoinPattern;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.question.join.JoinPatternResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface JoinPatternMapper extends FullMapper<JoinPattern, JoinPatternResource> {

}
