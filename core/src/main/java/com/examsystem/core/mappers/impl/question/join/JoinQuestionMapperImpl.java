package com.examsystem.core.mappers.impl.question.join;

import com.examsystem.core.entitites.question.join.JoinQuestion;
import com.examsystem.core.entitites.questionanswer.join.JoinAnswer;
import com.examsystem.core.mappers.api.question.join.JoinPatternMapper;
import com.examsystem.core.mappers.api.question.join.JoinQuestionMapper;
import com.examsystem.core.resources.question.join.JoinQuestionResource;
import com.examsystem.core.updaters.api.question.join.JoinQuestionUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class JoinQuestionMapperImpl implements JoinQuestionMapper {

    @Autowired
    private JoinPatternMapper joinPatternMapper;

    @Autowired
    private JoinQuestionUpdater joinQuestionUpdater;

    @Override
    public JoinQuestionResource mapEntityToResource(JoinQuestion entity) {
        JoinQuestionResource resource = new JoinQuestionResource();
        resource.setId(entity.getId());
        resource.setContent(entity.getContent());
        resource.setPosition(entity.getPosition());
        resource.setImageId(entity.getImageId());
        int timeLimitMinutes = entity.getTimeLimit() / 60;
        int timeLimitSeconds = entity.getTimeLimit() % 60;
        resource.setTimeLimitMinutes(timeLimitMinutes);
        resource.setTimeLimitSeconds(timeLimitSeconds);
        resource.setHasTimeLimit(entity.getHasTimeLimit());
        resource.setCorrectPoints(entity.getCorrectPoints());
        resource.setIncorrectPoints(entity.getIncorrectPoints());
        resource.setMaxPoints(entity.getMaxPoints());
        if (entity.getTest() != null) {
            resource.setTestId(entity.getTest().getId());
        }
        resource.setJoinPatterns(entity.getJoinPatterns().stream().map(joinPatternMapper::mapEntityToResource).collect(Collectors.toList()));
        resource.setJoinAnswerIds(entity.getJoinAnswers().stream().map(JoinAnswer::getId).collect(Collectors.toList()));
        resource.setQuestionAnswerIds(entity.getJoinAnswers().stream().map(JoinAnswer::getId).collect(Collectors.toList()));
        return resource;
    }

    @Override
    public JoinQuestion mapResourceToEntity(JoinQuestionResource resource) {
        JoinQuestion entity = new JoinQuestion();
        entity.setId(resource.getId());
        joinQuestionUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
