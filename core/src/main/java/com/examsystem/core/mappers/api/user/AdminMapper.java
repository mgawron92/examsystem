package com.examsystem.core.mappers.api.user;

import com.examsystem.core.entitites.user.Admin;
import com.examsystem.core.resources.user.AdminResource;
import com.examsystem.core.mappers.api.FullMapper;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface AdminMapper extends FullMapper<Admin, AdminResource> {

}
