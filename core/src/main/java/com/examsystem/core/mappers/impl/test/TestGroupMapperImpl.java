package com.examsystem.core.mappers.impl.test;

import com.examsystem.core.entitites.test.TestGroup;
import com.examsystem.core.mappers.api.intermediary.StudentTestGroupMapper;
import com.examsystem.core.mappers.api.intermediary.TestTestGroupMapper;
import com.examsystem.core.mappers.api.test.TestGroupMapper;
import com.examsystem.core.resources.test.TestGroupResource;
import com.examsystem.core.updaters.api.test.TestGroupUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class TestGroupMapperImpl implements TestGroupMapper {

    @Autowired
    private TestGroupUpdater testGroupUpdater;

    @Autowired
    private TestTestGroupMapper testTestGroupMapper;

    @Autowired
    private StudentTestGroupMapper studentTestGroupMapper;

    @Override
    public TestGroupResource mapEntityToResource(TestGroup entity) {
        TestGroupResource resource = new TestGroupResource();
        resource.setId(entity.getId());
        resource.setName(entity.getName());
        resource.setCreatedDate(entity.getCreatedDate());
        if (entity.getExaminer() != null) {
            resource.setExaminerId(entity.getExaminer().getId());
        }
        resource.setTestTestGroups(entity.getTestTestGroups().stream().map(testTestGroupMapper::mapEntityToResource).collect(Collectors.toList()));
        resource.setStudentTestGroups(entity.getStudentTestGroups().stream().map(studentTestGroupMapper::mapEntityToResource).collect(Collectors.toList()));
        return resource;
    }

    @Override
    public TestGroup mapResourceToEntity(TestGroupResource resource) {
        TestGroup entity = new TestGroup();
        entity.setId(resource.getId());
        testGroupUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
