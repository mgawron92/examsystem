package com.examsystem.core.mappers.api.user;

import com.examsystem.core.entitites.user.Student;
import com.examsystem.core.resources.user.StudentResource;
import com.examsystem.core.mappers.api.FullMapper;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface StudentMapper extends FullMapper<Student, StudentResource> {

}
