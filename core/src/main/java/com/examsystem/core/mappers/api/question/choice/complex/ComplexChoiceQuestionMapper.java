package com.examsystem.core.mappers.api.question.choice.complex;

import com.examsystem.core.entitites.question.choice.complex.ComplexChoiceQuestion;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.question.choice.complex.ComplexChoiceQuestionResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface ComplexChoiceQuestionMapper extends FullMapper<ComplexChoiceQuestion, ComplexChoiceQuestionResource> {

}
