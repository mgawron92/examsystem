package com.examsystem.core.mappers.api;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface EntityMapper<E, R> {

    R mapEntityToResource(E entity);

}
