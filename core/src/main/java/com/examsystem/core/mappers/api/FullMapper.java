package com.examsystem.core.mappers.api;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface FullMapper<E, R> extends EntityMapper<E, R>, ResourceMapper<E, R> {

}
