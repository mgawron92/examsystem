package com.examsystem.core.mappers.api.questionanswer.join;

import com.examsystem.core.entitites.questionanswer.join.JoinAnswer;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.questionanswer.join.JoinAnswerResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface JoinAnswerMapper extends FullMapper<JoinAnswer, JoinAnswerResource> {

}
