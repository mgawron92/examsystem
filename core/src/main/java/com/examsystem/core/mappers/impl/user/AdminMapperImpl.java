package com.examsystem.core.mappers.impl.user;

import com.examsystem.core.entitites.user.Admin;
import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.resources.user.AdminResource;
import com.examsystem.core.mappers.api.user.AdminMapper;
import com.examsystem.core.updaters.api.user.PersonUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class AdminMapperImpl implements AdminMapper {

    @Autowired
    private PersonUpdater personUpdater;

    @Override
    public AdminResource mapEntityToResource(Admin entity) {
        AdminResource resource = new AdminResource();
        personUpdater.updateResourceFromEntity(resource, entity);
        resource.setAddedBy(entity.getAddedBy());
        resource.setRole(UserRole.ADMIN);
        return resource;
    }

    @Override
    public Admin mapResourceToEntity(AdminResource resource) {
        Admin entity = new Admin();
        personUpdater.updateEntityFromResource(entity, resource);
        entity.setAddedBy(resource.getAddedBy());
        return entity;
    }

}
