package com.examsystem.core.mappers.impl.test;

import com.examsystem.core.entitites.question.Question;
import com.examsystem.core.entitites.test.Test;
import com.examsystem.core.entitites.testanswer.TestAnswer;
import com.examsystem.core.mappers.api.intermediary.TestTestGroupMapper;
import com.examsystem.core.mappers.api.test.TestMapper;
import com.examsystem.core.repositories.StudentRepository;
import com.examsystem.core.resources.test.TestResource;
import com.examsystem.core.updaters.api.test.TestUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class TestMapperImpl implements TestMapper {

    @Autowired
    private TestTestGroupMapper testTestGroupMapper;

    @Autowired
    private TestUpdater testUpdater;

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public TestResource mapEntityToResource(Test entity) {
        TestResource resource = new TestResource();
        resource.setId(entity.getId());
        resource.setName(entity.getName());
        resource.setDescription(entity.getDescription());
        resource.setTimeLimit(entity.getTimeLimit());
        resource.setHasTimeLimit(entity.getHasTimeLimit());
        resource.setActiveFrom(entity.getActiveFrom());
        long milisecondsActive = entity.getActiveTill().getTime() - entity.getActiveFrom().getTime();
        milisecondsActive /= (60 * 1000);
        resource.setMinutesActive((int)milisecondsActive);
        int maxPoints = 0;
        for (Question question : entity.getQuestions()) {
            maxPoints += question.getMaxPoints();
        }
        resource.setMaxPoints(maxPoints);
        resource.setCreatedDate(entity.getCreatedDate());
        if (entity.getExaminer() != null) {
            resource.setExaminerId(entity.getExaminer().getId());
        }
        int testAnswersToCheck = 0;
        int testAnswersChecked = 0;
        for (TestAnswer testAnswer : entity.getTestAnswers()) {
            if (testAnswer.getChecked() != null) {
                boolean checked = testAnswer.getChecked();
                if (checked) {
                    ++testAnswersChecked;
                } else {
                    ++testAnswersToCheck;
                }
            }
        }
        resource.setTestAnswersToCheck(testAnswersToCheck);
        resource.setTestAnswersChecked(testAnswersChecked);
        resource.setStudentsCount(studentRepository.getStudentsCountByTestId(entity.getId()));
        resource.setTestTestGroups(entity.getTestTestGroups().stream().map(testTestGroupMapper::mapEntityToResource).collect(Collectors.toList()));
        resource.setRestrictQuestionsNumber(entity.getRestrictQuestionsNumber());
        resource.setUsedQuestionsNumber(entity.getUsedQuestionsNumber());
        resource.setUseRandomSequence(entity.getUseRandomSequence());
        resource.setQuestionIds(entity.getQuestions().stream().map(Question::getId).collect(Collectors.toList()));
        resource.setTestAnswerIds(entity.getTestAnswers().stream().map(TestAnswer::getId).collect(Collectors.toList()));
        return resource;
    }

    @Override
    public Test mapResourceToEntity(TestResource resource) {
        Test entity = new Test();
        entity.setId(resource.getId());
        testUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
