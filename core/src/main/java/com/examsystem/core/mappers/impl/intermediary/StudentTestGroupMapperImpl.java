package com.examsystem.core.mappers.impl.intermediary;

import com.examsystem.core.entitites.intermediary.StudentTestGroup;
import com.examsystem.core.mappers.api.intermediary.StudentTestGroupMapper;
import com.examsystem.core.repositories.StudentRepository;
import com.examsystem.core.repositories.TestGroupRepository;
import com.examsystem.core.resources.intermediary.StudentTestGroupResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class StudentTestGroupMapperImpl implements StudentTestGroupMapper {

    @Autowired
    private TestGroupRepository testGroupRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public StudentTestGroupResource mapEntityToResource(StudentTestGroup entity) {
        StudentTestGroupResource resource = new StudentTestGroupResource();
        resource.setId(entity.getId());
        resource.setJoinDate(entity.getJoinDate());
        if (entity.getTestGroup() != null) {
            resource.setTestGroupId(entity.getTestGroup().getId());
        }
        if (entity.getStudent() != null) {
            resource.setStudentId(entity.getStudent().getId());
        }
        return resource;
    }

    @Override
    public StudentTestGroup mapResourceToEntity(StudentTestGroupResource resource) {
        StudentTestGroup entity = new StudentTestGroup();
        entity.setId(resource.getId());
        entity.setJoinDate(resource.getJoinDate());
        entity.setTestGroup(testGroupRepository.findTestGroupById(resource.getTestGroupId()));
        entity.setStudent(studentRepository.findStudentById(resource.getStudentId()));
        return entity;
    }
}
