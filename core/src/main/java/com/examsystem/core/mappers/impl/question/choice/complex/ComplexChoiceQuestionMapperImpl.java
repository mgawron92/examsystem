package com.examsystem.core.mappers.impl.question.choice.complex;

import com.examsystem.core.entitites.question.choice.complex.ComplexChoiceQuestion;
import com.examsystem.core.entitites.questionanswer.choice.complex.ComplexChoiceAnswer;
import com.examsystem.core.mappers.api.question.choice.complex.ComplexChoicePatternMapper;
import com.examsystem.core.mappers.api.question.choice.complex.ComplexChoiceQuestionMapper;
import com.examsystem.core.resources.question.choice.complex.ComplexChoiceQuestionResource;
import com.examsystem.core.resources.question.choice.complex.multiple.MultipleComplexChoiceQuestionResource;
import com.examsystem.core.resources.question.choice.complex.single.SingleComplexChoiceQuestionResource;
import com.examsystem.core.updaters.api.question.choice.complex.ComplexChoiceQuestionUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class ComplexChoiceQuestionMapperImpl implements ComplexChoiceQuestionMapper {

    @Autowired
    private ComplexChoicePatternMapper complexChoicePatternMapper;

    @Autowired
    private ComplexChoiceQuestionUpdater complexChoiceQuestionUpdater;

    @Override
    public ComplexChoiceQuestionResource mapEntityToResource(ComplexChoiceQuestion entity) {
        ComplexChoiceQuestionResource resource;
        if (entity.getMultiple()) {
            resource = new MultipleComplexChoiceQuestionResource();
        } else {
            resource = new SingleComplexChoiceQuestionResource();
        }
        resource.setId(entity.getId());
        int timeLimitMinutes = entity.getTimeLimit() / 60;
        int timeLimitSeconds = entity.getTimeLimit() % 60;
        resource.setTimeLimitMinutes(timeLimitMinutes);
        resource.setTimeLimitSeconds(timeLimitSeconds);
        resource.setHasTimeLimit(entity.getHasTimeLimit());
        resource.setContent(entity.getContent());
        resource.setPosition(entity.getPosition());
        resource.setImageId(entity.getImageId());
        resource.setMultiple(entity.getMultiple());
        resource.setMaxPoints(entity.getMaxPoints());
        if (entity.getTest() != null) {
            resource.setTestId(entity.getTest().getId());
        }
        resource.setChoicePatterns(entity.getChoicePatterns().stream().map(complexChoicePatternMapper::mapEntityToResource).collect(Collectors.toList()));
        resource.setChoiceAnswerIds(entity.getChoiceAnswers().stream().map(ComplexChoiceAnswer::getId).collect(Collectors.toList()));
        resource.setQuestionAnswerIds(entity.getChoiceAnswers().stream().map(ComplexChoiceAnswer::getId).collect(Collectors.toList()));
        return resource;
    }

    @Override
    public ComplexChoiceQuestion mapResourceToEntity(ComplexChoiceQuestionResource resource) {
        ComplexChoiceQuestion entity = new ComplexChoiceQuestion();
        entity.setId(resource.getId());
        complexChoiceQuestionUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
