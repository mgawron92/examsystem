package com.examsystem.core.mappers.api.questionanswer.freetext;

import com.examsystem.core.entitites.questionanswer.freetext.FreetextAnswer;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.questionanswer.freetext.FreetextAnswerResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface FreetextAnswerMapper extends FullMapper<FreetextAnswer, FreetextAnswerResource> {

}
