package com.examsystem.core.mappers.api.questionanswer.choice.simple;

import com.examsystem.core.entitites.questionanswer.choice.simple.SimpleChoiceAnswerPart;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.questionanswer.choice.simple.SimpleChoiceAnswerPartResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface SimpleChoiceAnswerPartMapper extends FullMapper<SimpleChoiceAnswerPart, SimpleChoiceAnswerPartResource> {

}
