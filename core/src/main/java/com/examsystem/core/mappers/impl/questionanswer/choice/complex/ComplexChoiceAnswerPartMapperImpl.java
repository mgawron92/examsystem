package com.examsystem.core.mappers.impl.questionanswer.choice.complex;

import com.examsystem.core.entitites.questionanswer.choice.complex.ComplexChoiceAnswerPart;
import com.examsystem.core.mappers.api.questionanswer.choice.complex.ComplexChoiceAnswerPartMapper;
import com.examsystem.core.resources.questionanswer.choice.complex.ComplexChoiceAnswerPartResource;
import com.examsystem.core.updaters.api.questionanswer.choice.complex.ComplexChoiceAnswerPartUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class ComplexChoiceAnswerPartMapperImpl implements ComplexChoiceAnswerPartMapper {

    @Autowired
    private ComplexChoiceAnswerPartUpdater complexChoiceAnswerPartUpdater;

    @Override
    public ComplexChoiceAnswerPartResource mapEntityToResource(ComplexChoiceAnswerPart entity) {
        ComplexChoiceAnswerPartResource resource = new ComplexChoiceAnswerPartResource();
        resource.setId(entity.getId());
        resource.setSelected(entity.getSelected());
        if (entity.getChoiceAnswer() != null) {
            resource.setChoiceAnswerId(entity.getChoiceAnswer().getId());
        }
        if (entity.getChoicePattern() != null) {
            resource.setChoicePatternId(entity.getChoicePattern().getId());
        }
        int points = entity.getSelected() ? entity.getChoicePattern().getSelectedPoints() : entity.getChoicePattern().getDeselectedPoints();
        int maxPoints = Math.max(entity.getChoicePattern().getSelectedPoints(), entity.getChoicePattern().getDeselectedPoints());
        boolean correct = (points == maxPoints);
        resource.setCorrect(correct);
        resource.setPoints(points);
        return resource;
    }

    @Override
    public ComplexChoiceAnswerPart mapResourceToEntity(ComplexChoiceAnswerPartResource resource) {
        ComplexChoiceAnswerPart entity = new ComplexChoiceAnswerPart();
        entity.setId(resource.getId());
        complexChoiceAnswerPartUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
