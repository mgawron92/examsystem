package com.examsystem.core.mappers.api.questionanswer.choice.complex;

import com.examsystem.core.entitites.questionanswer.choice.complex.ComplexChoiceAnswer;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.questionanswer.choice.complex.ComplexChoiceAnswerResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface ComplexChoiceAnswerMapper extends FullMapper<ComplexChoiceAnswer, ComplexChoiceAnswerResource> {

}
