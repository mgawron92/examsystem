package com.examsystem.core.mappers.impl.question.join;

import com.examsystem.core.entitites.question.join.JoinPattern;
import com.examsystem.core.entitites.questionanswer.join.JoinAnswerPart;
import com.examsystem.core.mappers.api.question.join.JoinPatternMapper;
import com.examsystem.core.resources.question.join.JoinPatternResource;
import com.examsystem.core.updaters.api.question.join.JoinPatternUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class JoinPatternMapperImpl implements JoinPatternMapper {

    @Autowired
    private JoinPatternUpdater joinPatternUpdater;

    @Override
    public JoinPatternResource mapEntityToResource(JoinPattern entity) {
        JoinPatternResource resource = new JoinPatternResource();
        resource.setId(entity.getId());
        resource.setFirstOption(entity.getFirstOption());
        resource.setSecondOption(entity.getSecondOption());
        if (entity.getJoinQuestion() != null) {
            resource.setJoinQuestionId(entity.getJoinQuestion().getId());
        }
        resource.setJoinAnswerPartIds(entity.getJoinAnswerParts().stream().map(JoinAnswerPart::getId).collect(Collectors.toList()));
        return resource;
    }

    @Override
    public JoinPattern mapResourceToEntity(JoinPatternResource resource) {
        JoinPattern entity = new JoinPattern();
        entity.setId(resource.getId());
        joinPatternUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
