package com.examsystem.core.mappers.impl.intermediary;

import com.examsystem.core.entitites.intermediary.TestTestGroup;
import com.examsystem.core.mappers.api.intermediary.TestTestGroupMapper;
import com.examsystem.core.repositories.TestGroupRepository;
import com.examsystem.core.repositories.TestRepository;
import com.examsystem.core.resources.intermediary.TestTestGroupResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class TestTestGroupMapperImpl implements TestTestGroupMapper {

    @Autowired
    private TestGroupRepository testGroupRepository;

    @Autowired
    private TestRepository testRepository;

    @Override
    public TestTestGroupResource mapEntityToResource(TestTestGroup entity) {
        TestTestGroupResource resource = new TestTestGroupResource();
        resource.setId(entity.getId());
        if (entity.getTestGroup() != null) {
            resource.setTestGroupId(entity.getTestGroup().getId());
        }
        if (entity.getTest() != null) {
            resource.setTestId(entity.getTest().getId());
        }
        return resource;
    }

    @Override
    public TestTestGroup mapResourceToEntity(TestTestGroupResource resource) {
        TestTestGroup entity = new TestTestGroup();
        entity.setId(resource.getId());
        entity.setTestGroup(testGroupRepository.findTestGroupById(resource.getTestGroupId()));
        entity.setTest(testRepository.findTestById(resource.getTestId()));
        return entity;
    }
}
