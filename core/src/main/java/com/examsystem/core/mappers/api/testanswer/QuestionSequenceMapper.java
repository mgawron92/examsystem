package com.examsystem.core.mappers.api.testanswer;

import com.examsystem.core.entitites.testanswer.QuestionSequence;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.testanswer.QuestionSequenceResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface QuestionSequenceMapper extends FullMapper<QuestionSequence, QuestionSequenceResource> {

}
