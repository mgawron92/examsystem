package com.examsystem.core.mappers.api.test;

import com.examsystem.core.entitites.test.TestGroup;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.test.TestGroupResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface TestGroupMapper extends FullMapper<TestGroup, TestGroupResource> {

}
