package com.examsystem.core.mappers.impl.questionanswer.choice.simple;

import com.examsystem.core.entitites.questionanswer.choice.simple.SimpleChoiceAnswer;
import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.mappers.api.questionanswer.choice.simple.SimpleChoiceAnswerMapper;
import com.examsystem.core.mappers.api.questionanswer.choice.simple.SimpleChoiceAnswerPartMapper;
import com.examsystem.core.resources.questionanswer.choice.ChoiceAnswerPartResource;
import com.examsystem.core.resources.questionanswer.choice.simple.SimpleChoiceAnswerResource;
import com.examsystem.core.resources.questionanswer.choice.simple.multiple.MultipleSimpleChoiceAnswerResource;
import com.examsystem.core.resources.questionanswer.choice.simple.single.SingleSimpleChoiceAnswerResource;
import com.examsystem.core.updaters.api.questionanswer.choice.simple.SimpleChoiceAnswerUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class SimpleChoiceAnswerMapperImpl implements SimpleChoiceAnswerMapper {

    @Autowired
    private SimpleChoiceAnswerPartMapper simpleChoiceAnswerPartMapper;

    @Autowired
    private SimpleChoiceAnswerUpdater simpleChoiceAnswerUpdater;

    @Override
    public SimpleChoiceAnswerResource mapEntityToResource(SimpleChoiceAnswer entity) {
        SimpleChoiceAnswerResource resource = null;
        if (QuestionType.SINGLE_SIMPLE_CHOICE.equals(entity.getType())) {
            resource = new SingleSimpleChoiceAnswerResource();
        }
        if (QuestionType.MULTIPLE_SIMPLE_CHOICE.equals(entity.getType())) {
            resource = new MultipleSimpleChoiceAnswerResource();
        }
        resource.setId(entity.getId());
        resource.setStartedOn(entity.getStartedOn());
        resource.setFinishedOn(entity.getFinishedOn());
        if (entity.getChoiceQuestion() != null) {
            resource.setChoiceQuestionId(entity.getChoiceQuestion().getId());
        }
        if (entity.getTestAnswer() != null) {
            resource.setTestAnswerId(entity.getTestAnswer().getId());
        }
        resource.setChoiceAnswerParts(entity.getChoiceAnswerParts().stream().map(simpleChoiceAnswerPartMapper::mapEntityToResource).collect(Collectors.toList()));
        boolean correct = true;
        for (ChoiceAnswerPartResource choiceAnswerPartResource : resource.getChoiceAnswerParts()) {
            if (!choiceAnswerPartResource.getCorrect()) {
                correct = false;
                break;
            }
        }
        if (correct) {
            resource.setPoints(entity.getChoiceQuestion().getCorrectPoints());
        } else {
            resource.setPoints(entity.getChoiceQuestion().getIncorrectPoints());
        }
        resource.setMaxPoints(entity.getChoiceQuestion().getMaxPoints());
        resource.setQuestionId(entity.getChoiceQuestion().getId());
        return resource;
    }

    @Override
    public SimpleChoiceAnswer mapResourceToEntity(SimpleChoiceAnswerResource resource) {
        SimpleChoiceAnswer entity = new SimpleChoiceAnswer();
        entity.setId(resource.getId());
        simpleChoiceAnswerUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
