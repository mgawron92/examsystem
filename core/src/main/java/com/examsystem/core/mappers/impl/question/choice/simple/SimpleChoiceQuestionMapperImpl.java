package com.examsystem.core.mappers.impl.question.choice.simple;

import com.examsystem.core.entitites.question.choice.simple.SimpleChoiceQuestion;
import com.examsystem.core.entitites.questionanswer.choice.simple.SimpleChoiceAnswer;
import com.examsystem.core.mappers.api.question.choice.simple.SimpleChoicePatternMapper;
import com.examsystem.core.mappers.api.question.choice.simple.SimpleChoiceQuestionMapper;
import com.examsystem.core.resources.question.choice.simple.SimpleChoiceQuestionResource;
import com.examsystem.core.resources.question.choice.simple.multiple.MultipleSimpleChoiceQuestionResource;
import com.examsystem.core.resources.question.choice.simple.single.SingleSimpleChoiceQuestionResource;
import com.examsystem.core.updaters.api.question.choice.simple.SimpleChoiceQuestionUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class SimpleChoiceQuestionMapperImpl implements SimpleChoiceQuestionMapper {

    @Autowired
    private SimpleChoicePatternMapper simpleChoicePatternMapper;

    @Autowired
    private SimpleChoiceQuestionUpdater simpleChoiceQuestionUpdater;

    @Override
    public SimpleChoiceQuestionResource mapEntityToResource(SimpleChoiceQuestion entity) {
        SimpleChoiceQuestionResource resource;
        if (entity.getMultiple()) {
            resource = new MultipleSimpleChoiceQuestionResource();
        } else {
            resource = new SingleSimpleChoiceQuestionResource();
        }
        resource.setMultiple(entity.getMultiple());
        resource.setId(entity.getId());
        int timeLimitMinutes = entity.getTimeLimit() / 60;
        int timeLimitSeconds = entity.getTimeLimit() % 60;
        resource.setTimeLimitMinutes(timeLimitMinutes);
        resource.setTimeLimitSeconds(timeLimitSeconds);
        resource.setCorrectPoints(entity.getCorrectPoints());
        resource.setIncorrectPoints(entity.getIncorrectPoints());
        resource.setHasTimeLimit(entity.getHasTimeLimit());
        resource.setContent(entity.getContent());
        resource.setPosition(entity.getPosition());
        resource.setImageId(entity.getImageId());
        resource.setMultiple(entity.getMultiple());
        resource.setMaxPoints(entity.getMaxPoints());
        if (entity.getTest() != null) {
            resource.setTestId(entity.getTest().getId());
        }
        resource.setChoicePatterns(entity.getChoicePatterns().stream().map(simpleChoicePatternMapper::mapEntityToResource).collect(Collectors.toList()));
        resource.setChoiceAnswerIds(entity.getChoiceAnswers().stream().map(SimpleChoiceAnswer::getId).collect(Collectors.toList()));
        resource.setQuestionAnswerIds(entity.getChoiceAnswers().stream().map(SimpleChoiceAnswer::getId).collect(Collectors.toList()));
        return resource;
    }

    @Override
    public SimpleChoiceQuestion mapResourceToEntity(SimpleChoiceQuestionResource resource) {
        SimpleChoiceQuestion entity = new SimpleChoiceQuestion();
        entity.setId(resource.getId());
        simpleChoiceQuestionUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
