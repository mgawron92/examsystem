package com.examsystem.core.mappers.impl.user;

import com.examsystem.core.entitites.other.Image;
import com.examsystem.core.entitites.test.Test;
import com.examsystem.core.entitites.test.TestGroup;
import com.examsystem.core.entitites.user.Examiner;
import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.repositories.ImageRepository;
import com.examsystem.core.repositories.TestGroupRepository;
import com.examsystem.core.repositories.TestRepository;
import com.examsystem.core.resources.user.ExaminerResource;
import com.examsystem.core.mappers.api.user.ExaminerMapper;
import com.examsystem.core.updaters.api.user.PersonUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class ExaminerMapperImpl implements ExaminerMapper {

    @Autowired
    private PersonUpdater personUpdater;

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private TestGroupRepository testGroupRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Override
    public ExaminerResource mapEntityToResource(Examiner entity) {
        ExaminerResource resource = new ExaminerResource();
        personUpdater.updateResourceFromEntity(resource, entity);
        resource.setAddedBy(entity.getAddedBy());
        resource.setRole(UserRole.EXAMINER);
        resource.setTestGroupIds(entity.getTestGroups().stream().map(TestGroup::getId).collect(Collectors.toList()));
        resource.setTestIds(entity.getTests().stream().map(Test::getId).collect(Collectors.toList()));
        resource.setImageIds(entity.getImages().stream().map(Image::getId).collect(Collectors.toList()));
        return resource;
    }

    @Override
    public Examiner mapResourceToEntity(ExaminerResource resource) {
        Examiner entity = new Examiner();
        personUpdater.updateEntityFromResource(entity, resource);
        entity.setAddedBy(resource.getAddedBy());
        entity.setTestGroups(resource.getTestGroupIds().stream().map((Long id)-> testGroupRepository.findTestGroupById(id)).collect(Collectors.toList()));
        entity.setTests(resource.getTestIds().stream().map((Long id) -> testRepository.findTestById(id)).collect(Collectors.toList()));
        entity.setImages(resource.getImageIds().stream().map((Long id) -> imageRepository.findImageById(id)).collect(Collectors.toList()));
        return entity;
    }

}
