package com.examsystem.core.mappers.api.question.choice.simple;

import com.examsystem.core.entitites.question.choice.simple.SimpleChoicePattern;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.question.choice.simple.SimpleChoicePatternResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface SimpleChoicePatternMapper extends FullMapper<SimpleChoicePattern, SimpleChoicePatternResource> {

}
