package com.examsystem.core.mappers.api.question.join;

import com.examsystem.core.entitites.question.join.JoinQuestion;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.question.join.JoinQuestionResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface JoinQuestionMapper extends FullMapper<JoinQuestion, JoinQuestionResource> {

}
