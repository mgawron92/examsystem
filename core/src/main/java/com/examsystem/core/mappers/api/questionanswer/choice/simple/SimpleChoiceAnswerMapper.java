package com.examsystem.core.mappers.api.questionanswer.choice.simple;

import com.examsystem.core.entitites.questionanswer.choice.simple.SimpleChoiceAnswer;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.questionanswer.choice.simple.SimpleChoiceAnswerResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface SimpleChoiceAnswerMapper extends FullMapper<SimpleChoiceAnswer, SimpleChoiceAnswerResource> {

}
