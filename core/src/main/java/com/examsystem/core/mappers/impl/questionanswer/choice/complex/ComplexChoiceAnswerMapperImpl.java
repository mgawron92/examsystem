package com.examsystem.core.mappers.impl.questionanswer.choice.complex;

import com.examsystem.core.entitites.questionanswer.choice.complex.ComplexChoiceAnswer;
import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.mappers.api.questionanswer.choice.complex.ComplexChoiceAnswerMapper;
import com.examsystem.core.mappers.api.questionanswer.choice.complex.ComplexChoiceAnswerPartMapper;
import com.examsystem.core.resources.questionanswer.choice.complex.ComplexChoiceAnswerPartResource;
import com.examsystem.core.resources.questionanswer.choice.complex.ComplexChoiceAnswerResource;
import com.examsystem.core.resources.questionanswer.choice.complex.multiple.MultipleComplexChoiceAnswerResource;
import com.examsystem.core.resources.questionanswer.choice.complex.single.SingleComplexChoiceAnswerResource;
import com.examsystem.core.updaters.api.questionanswer.choice.complex.ComplexChoiceAnswerUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class ComplexChoiceAnswerMapperImpl implements ComplexChoiceAnswerMapper {

    @Autowired
    private ComplexChoiceAnswerPartMapper complexChoiceAnswerPartMapper;

    @Autowired
    private ComplexChoiceAnswerUpdater complexChoiceAnswerUpdater;

    @Override
    public ComplexChoiceAnswerResource mapEntityToResource(ComplexChoiceAnswer entity) {
        ComplexChoiceAnswerResource resource = null;
        if (QuestionType.SIMPLE_COMPLEX_CHOICE.equals(entity.getType())) {
            resource = new SingleComplexChoiceAnswerResource();
        }
        if (QuestionType.MULTIPLE_COMPLEX_CHOICE.equals(entity.getType())) {
            resource = new MultipleComplexChoiceAnswerResource();
        }
        resource.setId(entity.getId());
        resource.setStartedOn(entity.getStartedOn());
        resource.setFinishedOn(entity.getFinishedOn());
        if (entity.getChoiceQuestion() != null) {
            resource.setChoiceQuestionId(entity.getChoiceQuestion().getId());
        }
        if (entity.getTestAnswer() != null) {
            resource.setTestAnswerId(entity.getTestAnswer().getId());
        }
        resource.setChoiceAnswerParts(entity.getChoiceAnswerParts().stream().map(complexChoiceAnswerPartMapper::mapEntityToResource).collect(Collectors.toList()));
        int points = 0;
        for (ComplexChoiceAnswerPartResource complexChoiceAnswerPartResource : resource.getChoiceAnswerParts()) {
            points += complexChoiceAnswerPartResource.getPoints();
        }
        resource.setPoints(points);
        resource.setMaxPoints(entity.getChoiceQuestion().getMaxPoints());
        resource.setQuestionId(entity.getChoiceQuestion().getId());
        return resource;
    }

    @Override
    public ComplexChoiceAnswer mapResourceToEntity(ComplexChoiceAnswerResource resource) {
        ComplexChoiceAnswer entity = new ComplexChoiceAnswer();
        entity.setId(resource.getId());
        complexChoiceAnswerUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
