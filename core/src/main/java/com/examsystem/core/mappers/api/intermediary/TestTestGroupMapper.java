package com.examsystem.core.mappers.api.intermediary;

import com.examsystem.core.entitites.intermediary.TestTestGroup;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.intermediary.TestTestGroupResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface TestTestGroupMapper extends FullMapper<TestTestGroup, TestTestGroupResource> {

}
