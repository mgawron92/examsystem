package com.examsystem.core.mappers.api.testanswer;

import com.examsystem.core.entitites.testanswer.TestAnswer;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.testanswer.TestAnswerResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface TestAnswerMapper extends FullMapper<TestAnswer, TestAnswerResource> {

}
