package com.examsystem.core.mappers.impl.settings;

import com.examsystem.core.entitites.other.Settings;
import com.examsystem.core.enumerations.SettingsType;
import com.examsystem.core.mappers.api.settings.ListsSettingsMapper;
import com.examsystem.core.resources.settings.ListsResource;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class ListsSettingsMapperImpl implements ListsSettingsMapper {

    @Override
    public ListsResource mapEntityToResource(List<Settings> entities) {
        ListsResource resource = new ListsResource();
        for (Settings s : entities) {
            switch (s.getKey()) {
                case "students":
                    resource.setStudents(Integer.valueOf(s.getValue()));
                    break;
                case "examiners":
                    resource.setExaminers(Integer.valueOf(s.getValue()));
                    break;
                case "admins":
                    resource.setAdmins(Integer.valueOf(s.getValue()));
                    break;
                case "tests":
                    resource.setTests(Integer.valueOf(s.getValue()));
                    break;
                case "testsToCheck":
                    resource.setTestsToCheck(Integer.valueOf(s.getValue()));
                    break;
                case "testResults":
                    resource.setTestResults(Integer.valueOf(s.getValue()));
                    break;
                case "testGroups":
                    resource.setTestGroups(Integer.valueOf(s.getValue()));
                    break;
                case "associatedTests":
                    resource.setAssociatedTests(Integer.valueOf(s.getValue()));
                    break;
            }
        }
        return resource;
    }

    @Override
    public List<Settings> mapResourceToEntity(ListsResource resource) {
        List<Settings> entities = new LinkedList<>();
        entities.add(new Settings("students", resource.getStudents().toString(), SettingsType.DEFAULTS));
        entities.add(new Settings("examiners", resource.getExaminers().toString(), SettingsType.DEFAULTS));
        entities.add(new Settings("admins", resource.getAdmins().toString(), SettingsType.DEFAULTS));
        entities.add(new Settings("tests", resource.getTests().toString(), SettingsType.DEFAULTS));
        entities.add(new Settings("testsToCheck", resource.getTestsToCheck().toString(), SettingsType.DEFAULTS));
        entities.add(new Settings("testResults", resource.getTestResults().toString(), SettingsType.DEFAULTS));
        entities.add(new Settings("testGroups", resource.getTestGroups().toString(), SettingsType.DEFAULTS));
        entities.add(new Settings("associatedTests", resource.getAssociatedTests().toString(), SettingsType.DEFAULTS));
        return entities;
    }

}
