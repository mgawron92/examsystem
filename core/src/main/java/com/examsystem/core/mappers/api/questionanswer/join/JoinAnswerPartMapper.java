package com.examsystem.core.mappers.api.questionanswer.join;

import com.examsystem.core.entitites.questionanswer.join.JoinAnswerPart;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.questionanswer.join.JoinAnswerPartResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface JoinAnswerPartMapper extends FullMapper<JoinAnswerPart, JoinAnswerPartResource> {

}
