package com.examsystem.core.mappers.api.intermediary;

import com.examsystem.core.entitites.intermediary.StudentTestGroup;
import com.examsystem.core.mappers.api.FullMapper;
import com.examsystem.core.resources.intermediary.StudentTestGroupResource;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface StudentTestGroupMapper extends FullMapper<StudentTestGroup, StudentTestGroupResource> {

}
