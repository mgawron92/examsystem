package com.examsystem.core.mappers.impl.questionanswer.join;

import com.examsystem.core.entitites.questionanswer.join.JoinAnswerPart;
import com.examsystem.core.mappers.api.questionanswer.join.JoinAnswerPartMapper;
import com.examsystem.core.resources.questionanswer.join.JoinAnswerPartResource;
import com.examsystem.core.updaters.api.questionanswer.join.JoinAnswerPartUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class JoinAnswerPartMapperImpl implements JoinAnswerPartMapper {

    @Autowired
    private JoinAnswerPartUpdater joinAnswerPartUpdater;

    @Override
    public JoinAnswerPartResource mapEntityToResource(JoinAnswerPart entity) {
        JoinAnswerPartResource resource = new JoinAnswerPartResource();
        resource.setId(entity.getId());
        resource.setFirstOption(entity.getFirstOption());
        resource.setSecondOption(entity.getSecondOption());
        if (entity.getJoinAnswer() != null) {
            resource.setJoinAnswerId(entity.getJoinAnswer().getId());
        }
        if (entity.getJoinPattern() != null) {
            resource.setJoinPatternId(entity.getJoinPattern().getId());
        }
        boolean firstOptionMatches = entity.getJoinPattern().getFirstOption().equals(entity.getFirstOption());
        boolean secondOptionMatches = entity.getJoinPattern().getSecondOption().equals(entity.getSecondOption());
        boolean correct = firstOptionMatches && secondOptionMatches;
        resource.setCorrect(correct);
        return resource;
    }

    @Override
    public JoinAnswerPart mapResourceToEntity(JoinAnswerPartResource resource) {
        JoinAnswerPart entity = new JoinAnswerPart();
        entity.setId(resource.getId());
        joinAnswerPartUpdater.updateEntityFromResource(entity, resource);
        return entity;
    }

}
