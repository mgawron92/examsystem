package com.examsystem.core.specifications;

import com.examsystem.core.entitites.intermediary.StudentTestGroup;
import com.examsystem.core.entitites.intermediary.TestTestGroup;
import com.examsystem.core.entitites.test.Test;
import com.examsystem.core.entitites.testanswer.TestAnswer;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.*;

/**
 * Created by Mateusz Gawron on 2016-07-24.
 */
public abstract class TestSpecification {

    private static final String EXAMINER_ID = "examinerId";

    private static final String STUDENT_ID = "studentId";

    private static final String TEST_GROUP_ID = "testGroupId";

    private static final String CHECKED = "checked";

    private static final String ACTIVE_ONLY = "activeOnly";

    private static final String NOT_STARTED = "notStarted";

    private static final String ANSWERED = "answered";

    private static final String LATEST_FIRST = "latestFirst";

    private static final String NOT_FINISHED = "notFinished";

    public static Specification<Test> getSpecification(Map<String, Object> params) {

        return new Specification<Test>() {

            private Predicate getExaminerIdPredicate(Root<Test> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object examinerId, Object latestFirst) {
                Path<Long> testIdPath = root.get("id");
                Path<Long> examinerIdPath = root.join("examiner").get("id");
                Path<Date> activeFromPath = root.get("activeFrom");
                if ((boolean)latestFirst) {
                    cq.groupBy(testIdPath, activeFromPath).orderBy(cb.desc(activeFromPath));
                } else {
                    cq.groupBy(testIdPath, activeFromPath).orderBy(cb.asc(activeFromPath));
                }
                return cb.equal(examinerIdPath, examinerId);
            }

            private Predicate getStudentIdPredicate(Root<Test> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object studentId) {
                Path<Long> testIdPath = root.get("id");

                Subquery<Long> subquery = cq.subquery(Long.class);
                Root<TestTestGroup> testTestGroup = subquery.from(TestTestGroup.class);
                Path<Long> subqueryTestIdPath = testTestGroup.join("test").get("id");
                Path<Long> testGroupIdPath = testTestGroup.join("testGroup").get("id");

                Subquery<Long> subquery1 = subquery.subquery(Long.class);
                Root<StudentTestGroup> studentTestGroup = subquery1.from(StudentTestGroup.class);
                Path<Long> testGroupIdPath1 = studentTestGroup.join("testGroup").get("id");
                Path<Long> studentIdPath1 = studentTestGroup.join("student").get("id");
                subquery1.select(testGroupIdPath1).where(cb.equal(studentIdPath1, studentId));

                Predicate subqueryPredicate = cb.in(testGroupIdPath).value(subquery1);
                subquery.select(subqueryTestIdPath).distinct(true).where(subqueryPredicate);

                Path<Date> activeFromPath = root.get("activeFrom");
                Path<Date> activeTillPath = root.get("activeTill");
                cq.groupBy(testIdPath, activeFromPath, activeTillPath).orderBy(cb.asc(activeFromPath), cb.asc(activeTillPath));

                return cb.in(testIdPath).value(subquery);
            }

            private Predicate getTestGroupIdPredicate(Root<Test> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object testGroupId, Object latestFirst) {
                Path<Long> testIdPath = root.get("id");

                Subquery<Long> subquery = cq.subquery(Long.class);
                Root<TestTestGroup> testTestGroup = subquery.from(TestTestGroup.class);
                Path<Long> subqueryTestIdPath = testTestGroup.join("test").get("id");
                Path<Long> testGroupIdPath = testTestGroup.join("testGroup").get("id");

                subquery.select(subqueryTestIdPath).where(cb.equal(testGroupIdPath, testGroupId));

                Path<Date> activeFromPath = root.get("activeFrom");
                Path<Date> activeTillPath = root.get("activeTill");

                if ((boolean)latestFirst) {
                    cq.groupBy(testIdPath, activeFromPath, activeTillPath).orderBy(cb.desc(activeFromPath), cb.asc(activeTillPath));
                } else {
                    cq.groupBy(testIdPath, activeFromPath, activeTillPath).orderBy(cb.asc(activeFromPath), cb.asc(activeTillPath));
                }

                return cb.in(testIdPath).value(subquery);
            }

            private Predicate getAnsweredPredicate(Root<Test> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object studentId, Object answered, Object notFinished) {
                Path<Long> testIdPath = root.get("id");

                Subquery<Long> subquery = cq.subquery(Long.class);
                Root<TestTestGroup> testTestGroup = subquery.from(TestTestGroup.class);
                Path<Long> subqueryTestIdPath = testTestGroup.join("test").get("id");
                Path<Long> testGroupIdPath = testTestGroup.join("testGroup").get("id");

                Subquery<Long> subquery2 = subquery.subquery(Long.class);
                Root<TestAnswer> testAnswer2 = subquery2.from(TestAnswer.class);
                Path<Long> testIdPath2 = testAnswer2.join("test").get("id");
                Path<Long> studentIdPath2 = testAnswer2.join("student").get("id");
                subquery2.select(testIdPath2).where(cb.equal(studentIdPath2, studentId));

                Subquery<Long> subquery3 = subquery.subquery(Long.class);
                Root<TestAnswer> testAnswer3 = subquery3.from(TestAnswer.class);
                Path<Long> testIdPath3 = testAnswer3.join("test").get("id");
                Path<Long> studentIdPath3 = testAnswer3.join("student").get("id");
                Path<Long> finishedOnPath3 = testAnswer3.get("finishedOn");
                subquery3.select(testIdPath3).where(cb.and(cb.equal(studentIdPath3, studentId), cb.isNull(finishedOnPath3)));

                Predicate subqueryPredicate;
                if ((boolean)answered == false) {
                    subqueryPredicate = cb.not(cb.in(subqueryTestIdPath).value(subquery2));
                } else {
                    subqueryPredicate = cb.in(subqueryTestIdPath).value(subquery2);
                }
                if ((boolean)notFinished) {
                    subqueryPredicate = cb.or(subqueryPredicate, cb.in(subqueryTestIdPath).value(subquery3));
                }
                subquery.select(subqueryTestIdPath).distinct(true).where(subqueryPredicate);

                return cb.in(testIdPath).value(subquery);
            }

            private Predicate getCheckedPredicate(Root<Test> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object checked) {
                Path<Long> testIdPath = root.get("id");

                Subquery<Long> subquery = cq.subquery(Long.class);
                Root<TestAnswer> testAnswer = subquery.from(TestAnswer.class);
                Path<Long> subqueryTestIdPath = testAnswer.join("test").get("id");
                Path<Long> checkedPath = testAnswer.get("checked");

                subquery.select(subqueryTestIdPath).distinct(true).where(cb.equal(checkedPath, checked));

                return cb.in(testIdPath).value(subquery);
            }

            private Predicate getActiveOnlyPredicate(Root<Test> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object activeOnly) {
                if ((boolean)activeOnly) {
                    Path<Date> activeTillPath = root.get("activeTill");
                    return cb.greaterThan(activeTillPath, Calendar.getInstance().getTime());
                } else {
                    return cb.and();
                }
            }

            private boolean containsKeyNotNull(String key, Map<String, Object> params) {
                return params.containsKey(key) && (params.get(key) != null);
            }

            private Predicate getNotStartedPredicate(Root<Test> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object studentId, Object notStarted) {
                Path<Long> testIdPath = root.get("id");

                Subquery<Long> subquery = cq.subquery(Long.class);
                Root<TestAnswer> testAnswer = subquery.from(TestAnswer.class);
                Path<Long> subqueryTestIdPath = testAnswer.join("test").get("id");
                Path<Long> studentIdPath = testAnswer.join("student").get("id");

                Predicate subqueryPredicate = cb.equal(studentIdPath, studentId);
                subquery.select(subqueryTestIdPath).distinct(true).where(subqueryPredicate);
                if ((boolean)notStarted) {
                    return cb.not(cb.in(testIdPath).value(subquery));
                } else {
                    return cb.in(testIdPath).value(subquery);
                }
            }

            @Override
            public Predicate toPredicate(Root<Test> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                List<Predicate> predicates = new LinkedList<>();

                if (containsKeyNotNull(STUDENT_ID, params)) {
                    predicates.add(getStudentIdPredicate(root, cq, cb, params.get(STUDENT_ID)));
                }

                if (containsKeyNotNull(EXAMINER_ID, params) && containsKeyNotNull(LATEST_FIRST, params)) {
                    predicates.add(getExaminerIdPredicate(root, cq, cb, params.get(EXAMINER_ID), params.get(LATEST_FIRST)));
                }

                if (containsKeyNotNull(TEST_GROUP_ID, params) && containsKeyNotNull(LATEST_FIRST, params)) {
                    predicates.add(getTestGroupIdPredicate(root, cq, cb, params.get(TEST_GROUP_ID), params.get(LATEST_FIRST)));
                }

                if (containsKeyNotNull(CHECKED, params)) {
                    predicates.add(getCheckedPredicate(root, cq, cb, params.get(CHECKED)));
                }

                if (containsKeyNotNull(ACTIVE_ONLY, params) ) {
                    predicates.add(getActiveOnlyPredicate(root, cq, cb, params.get(ACTIVE_ONLY)));
                }

                if (containsKeyNotNull(STUDENT_ID, params) && containsKeyNotNull(ANSWERED, params) && containsKeyNotNull(NOT_FINISHED, params)) {
                    predicates.add(getAnsweredPredicate(root, cq, cb, params.get(STUDENT_ID), params.get(ANSWERED), params.get(NOT_FINISHED)));
                }

                if (containsKeyNotNull(STUDENT_ID, params) && containsKeyNotNull(NOT_STARTED, params)) {
                    predicates.add(getNotStartedPredicate(root, cq, cb, params.get(STUDENT_ID), params.get(NOT_STARTED)));
                }

                Predicate[] predicateArray = new Predicate[predicates.size()];
                predicates.toArray(predicateArray);
                return cb.and(predicateArray);
            }

        };

    }

}
