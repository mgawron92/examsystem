package com.examsystem.core.specifications;

import com.examsystem.core.entitites.intermediary.StudentTestGroup;
import com.examsystem.core.entitites.intermediary.TestTestGroup;
import com.examsystem.core.entitites.test.TestGroup;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-07-24.
 */
public abstract class TestGroupSpecification {

    private static final String EXAMINER_ID = "examinerId";

    private static final String STUDENT_ID = "studentId";

    private static final String TEST_ID = "testId";

    public static Specification<TestGroup> getSpecification(Map<String, Object> params) {

        return new Specification<TestGroup>() {

            private Predicate getExaminerIdPredicate(Root<TestGroup> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object examinerId) {
                Path<Long> examinerIdPath = root.join("examiner").get("id");
                Path<Long> testGroupIdPath = root.get("id");
                Path<Long> testGroupNamePath = root.get("name");

                cq.groupBy(testGroupIdPath, testGroupNamePath).orderBy(cb.asc(testGroupNamePath));

                return cb.equal(examinerIdPath, examinerId);
            }

            private Predicate getStudentIdPredicate(Root<TestGroup> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object studentId) {
                Path<Long> testGroupIdPath = root.get("id");
                Path<Long> testGroupNamePath = root.get("name");

                Subquery<Long> subquery = cq.subquery(Long.class);
                Root<StudentTestGroup> studentTestGroup = subquery.from(StudentTestGroup.class);
                Path<Long> studentIdPath = studentTestGroup.join("student").get("id");
                Path<Long> subqueryTestGroupIdPath = studentTestGroup.join("testGroup").get("id");
                subquery.select(subqueryTestGroupIdPath).where(cb.equal(studentIdPath, studentId));

                cq.groupBy(testGroupIdPath, testGroupNamePath).orderBy(cb.asc(testGroupNamePath));

                return cb.in(testGroupIdPath).value(subquery);
            }

            private Predicate getTestIdPredicate(Root<TestGroup> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object testId) {
                Path<Long> testGroupIdPath = root.get("id");
                Path<Long> testGroupNamePath = root.get("name");

                Subquery<Long> subquery = cq.subquery(Long.class);
                Root<TestTestGroup> testTestGroup = subquery.from(TestTestGroup.class);
                Path<Long> testIdPath = testTestGroup.join("test").get("id");
                Path<Long> subqueryTestGroupIdPath = testTestGroup.join("testGroup").get("id");

                cq.groupBy(testGroupIdPath, testGroupNamePath).orderBy(cb.asc(testGroupNamePath));

                subquery.select(subqueryTestGroupIdPath).where(cb.equal(testIdPath, testId));
                return cb.in(testGroupIdPath).value(subquery);
            }

            private boolean containsKeyNotNull(String key, Map<String, Object> params) {
                return params.containsKey(key) && (params.get(key) != null);
            }

            @Override
            public Predicate toPredicate(Root<TestGroup> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                List<Predicate> predicates = new LinkedList<>();

                if (containsKeyNotNull(EXAMINER_ID, params)) {
                    predicates.add(getExaminerIdPredicate(root, cq, cb, params.get(EXAMINER_ID)));
                }

                if (containsKeyNotNull(STUDENT_ID, params)) {
                    predicates.add(getStudentIdPredicate(root, cq, cb, params.get(STUDENT_ID)));
                }

                if (containsKeyNotNull(TEST_ID, params)) {
                    predicates.add(getTestIdPredicate(root, cq, cb, params.get(TEST_ID)));
                }

                Predicate[] predicateArray = new Predicate[predicates.size()];
                predicates.toArray(predicateArray);
                return cb.and(predicateArray);
            }

        };

    }

}
