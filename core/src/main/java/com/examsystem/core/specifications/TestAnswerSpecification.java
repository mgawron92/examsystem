package com.examsystem.core.specifications;

import com.examsystem.core.entitites.intermediary.TestTestGroup;
import com.examsystem.core.entitites.testanswer.TestAnswer;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-07-24.
 */
public abstract class TestAnswerSpecification {

    private static final String STUDENT_ID = "studentId";

    private static final String TEST_GROUP_ID = "testGroupId";

    private static final String TEST_ID = "testId";

    private static final String CHECKED = "checked";

    private static final String FINISHED_ONLY = "finishedOnly";

    private static final String NOT_FINISHED = "notFinished";

    public static Specification<TestAnswer> getSpecification(Map<String, Object> params) {

        return new Specification<TestAnswer>() {

            private Predicate getStudentIdPredicate(Root<TestAnswer> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object studentId) {
                Path<Long> testAnswerIdPath = root.get("id");
                Path<Long> startedOnPath = root.get("startedOn");
                Path<Long> studentIdPath = root.join("student").get("id");
                cq.groupBy(testAnswerIdPath, startedOnPath).orderBy(cb.desc(startedOnPath));
                return cb.equal(studentIdPath, studentId);
            }

            private Predicate getTestGroupIdPredicate(Root<TestAnswer> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object testGroupId) {
                Path<Long> testIdPath = root.join("test").get("id");

                Subquery<Long> subquery = cq.subquery(Long.class);
                Root<TestTestGroup> testTestGroup = subquery.from(TestTestGroup.class);
                Path<Long> subqueryTestIdPath = testTestGroup.join("test").get("id");
                Path<Long> testGroupIdPath = testTestGroup.join("testGroup").get("id");

                subquery.select(subqueryTestIdPath).where(cb.equal(testGroupIdPath, testGroupId));

                return cb.in(testIdPath).value(subquery);
            }

            private Predicate getTestIdPredicate(Root<TestAnswer> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object testId) {
                Path<Long> testAnswerIdPath = root.get("id");
                Path<Long> testIdPath = root.join("test").get("id");
                Path<Long> studentIdPath = root.join("student").get("id");
                Path<Long> studentSurnamePath = root.join("student").get("surname");

                cq.groupBy(testAnswerIdPath, studentIdPath, studentSurnamePath).orderBy(cb.asc(studentSurnamePath));

                return cb.equal(testIdPath, testId);
            }

            private Predicate getCheckedPredicate(Root<TestAnswer> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object checked) {
                Path<Long> checkedPath = root.get("checked");
                return cb.equal(checkedPath, checked);
            }

            private Predicate getFinishedOnNotNullPredicate(Root<TestAnswer> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object finishedOnly) {
                Path<Date> finishDatePath = root.get("finishedOn");
                return ((boolean)finishedOnly) ? cb.isNotNull(finishDatePath) : cb.and();
            }

            private Predicate getFinishedOnNullPredicate(Root<TestAnswer> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object notFinished) {
                Path<Date> finishDatePath = root.get("finishedOn");
                return ((boolean)notFinished) ? cb.isNull(finishDatePath) : cb.and();
            }

            private boolean containsKeyNotNull(String key, Map<String, Object> params) {
                return params.containsKey(key) && (params.get(key) != null);
            }

            @Override
            public Predicate toPredicate(Root<TestAnswer> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                List<Predicate> predicates = new LinkedList<>();

                if (containsKeyNotNull(STUDENT_ID, params)) {
                    predicates.add(getStudentIdPredicate(root, cq, cb, params.get(STUDENT_ID)));
                }

                if (containsKeyNotNull(TEST_GROUP_ID, params)) {
                    predicates.add(getTestGroupIdPredicate(root, cq, cb, params.get(TEST_GROUP_ID)));
                }

                if (containsKeyNotNull(TEST_ID, params)) {
                    predicates.add(getTestIdPredicate(root, cq, cb, params.get(TEST_ID)));
                }

                if (containsKeyNotNull(CHECKED, params)) {
                    predicates.add(getCheckedPredicate(root, cq, cb, params.get(CHECKED)));
                }

                if (containsKeyNotNull(FINISHED_ONLY, params)) {
                    predicates.add(getFinishedOnNotNullPredicate(root, cq, cb, params.get(FINISHED_ONLY)));
                }

                if (containsKeyNotNull(NOT_FINISHED, params)) {
                    predicates.add(getFinishedOnNullPredicate(root, cq, cb, params.get(NOT_FINISHED)));
                }

                Predicate[] predicateArray = new Predicate[predicates.size()];
                predicates.toArray(predicateArray);
                return cb.and(predicateArray);
            }

        };

    }

}
