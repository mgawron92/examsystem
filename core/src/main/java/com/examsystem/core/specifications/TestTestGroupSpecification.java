package com.examsystem.core.specifications;

import com.examsystem.core.entitites.intermediary.StudentTestGroup;
import com.examsystem.core.entitites.intermediary.TestTestGroup;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-07-24.
 */
public abstract class TestTestGroupSpecification  {

    private static final String TEST_ID = "testId";

    private static final String STUDENT_ID = "studentId";

    public static Specification<TestTestGroup> getSpecification(Map<String, Object> params) {

        return new Specification<TestTestGroup>() {

            private Predicate getTestIdPredicate(Root<TestTestGroup> root, CriteriaBuilder cb, Object testId) {
                Path<Long> testIdPath = root.join("test").get("id");
                return cb.equal(testIdPath, testId);
            }

            private Predicate getStudentIdPredicate(Root<TestTestGroup> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object studentId) {
                Path<Long> testGroupIdPath = root.join("testGroup").get("id");
                Subquery<Long> subquery = cq.subquery(Long.class);
                Root<StudentTestGroup> studentTestGroup = subquery.from(StudentTestGroup.class);
                Path<Long> studentIdPath = studentTestGroup.join("student").get("id");
                Path<Long> subqueryTestGroupIdPath = studentTestGroup.join("testGroup").get("id");
                subquery.select(subqueryTestGroupIdPath).where(cb.equal(studentIdPath, studentId));
                return cb.in(testGroupIdPath).value(subquery);
            }

            private boolean containsKeyNotNull(String key, Map<String, Object> params) {
                return params.containsKey(key) && (params.get(key) != null);
            }

            @Override
            public Predicate toPredicate(Root<TestTestGroup> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                List<Predicate> predicates = new LinkedList<>();

                if (containsKeyNotNull(TEST_ID, params)) {
                    predicates.add(getTestIdPredicate(root, cb, params.get(TEST_ID)));
                }

                if (containsKeyNotNull(STUDENT_ID, params)) {
                    predicates.add(getStudentIdPredicate(root, cq, cb, params.get(STUDENT_ID)));
                }

                Predicate[] predicateArray = new Predicate[predicates.size()];
                predicates.toArray(predicateArray);
                return cb.and(predicateArray);
            }

        };

    }

}
