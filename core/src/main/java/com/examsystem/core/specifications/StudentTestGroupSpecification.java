package com.examsystem.core.specifications;

import com.examsystem.core.entitites.intermediary.StudentTestGroup;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-07-24.
 */
public abstract class StudentTestGroupSpecification {

    private static final String TEST_GROUP_ID = "testGroupId";

    private static final String STUDENT_ID = "studentId";

    private static final String EXAMINER_ID = "examinerId";

    public static Specification<StudentTestGroup> getSpecification(Map<String, Object> params) {

        return new Specification<StudentTestGroup>() {

            private Predicate getTestGroupIdPredicate(Root<StudentTestGroup> root, CriteriaBuilder cb, Object testGroupId) {
                Path<Long> testGroupIdPath = root.join("testGroup").get("id");
                return cb.equal(testGroupIdPath, testGroupId);
            }

            private Predicate getStudentIdPredicate(Root<StudentTestGroup> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object studentId) {
                Path<Long> studentIdPath = root.join("student").get("id");
                return cb.equal(studentIdPath, studentId);
            }

            private Predicate getExaminerIdPredicate(Root<StudentTestGroup> root, CriteriaQuery<?> cq, CriteriaBuilder cb, Object examinerId) {
                Path<Long> examinerIdPath = root.join("testGroup").join("examiner").get("id");
                return cb.equal(examinerIdPath, examinerId);
            }

            private boolean containsKeyNotNull(String key, Map<String, Object> params) {
                return params.containsKey(key) && (params.get(key) != null);
            }

            @Override
            public Predicate toPredicate(Root<StudentTestGroup> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                List<Predicate> predicates = new LinkedList<>();

                if (containsKeyNotNull(TEST_GROUP_ID, params)) {
                    predicates.add(getTestGroupIdPredicate(root, cb, params.get(TEST_GROUP_ID)));
                }

                if (containsKeyNotNull(STUDENT_ID, params)) {
                    predicates.add(getStudentIdPredicate(root, cq, cb, params.get(STUDENT_ID)));
                }

                if (containsKeyNotNull(EXAMINER_ID, params)) {
                    predicates.add(getExaminerIdPredicate(root, cq, cb, params.get(EXAMINER_ID)));
                }

                Predicate[] predicateArray = new Predicate[predicates.size()];
                predicates.toArray(predicateArray);
                return cb.and(predicateArray);
            }

        };

    }

}
