package com.examsystem.core.scheduled.impl;

import com.examsystem.core.entitites.question.Question;
import com.examsystem.core.entitites.questionanswer.QuestionAnswer;
import com.examsystem.core.entitites.test.Test;
import com.examsystem.core.entitites.testanswer.TestAnswer;
import com.examsystem.core.entitites.user.Student;
import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.providers.api.QuestionAnswerServiceProvider;
import com.examsystem.core.repositories.TestAnswerRepository;
import com.examsystem.core.repositories.TestRepository;
import com.examsystem.core.resources.testanswer.TestAnswerResource;
import com.examsystem.core.scheduled.api.TestGradingService;
import com.examsystem.core.services.api.testanswer.TestAnswerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

/**
 * Created by Mateusz Gawron on 2016-07-05.
 */
@Service
public class TestGradingServiceImpl implements TestGradingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestGradingServiceImpl.class);

    @Autowired
    private TestAnswerService testAnswerService;

    @Autowired
    private TestAnswerRepository testAnswerRepository;

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private QuestionAnswerServiceProvider questionAnswerServiceProvider;

    @Scheduled(fixedRate = 60 * 1000)
    @Transactional(readOnly = false)
    @Override
    public void completeUnfinishedTestAnswers() {
        Map<String, Object> params = new HashMap<>();
        params.put("finishedOnly", false);
        params.put("notFinished", true);
        List<TestAnswerResource> testAnswerResources = testAnswerService.query(params);

        for (TestAnswerResource testAnswerResource : testAnswerResources) {
            TestAnswer testAnswer = testAnswerRepository.findTestAnswerById(testAnswerResource.getId());
            Test test = testRepository.findTestById(testAnswerResource.getTestId());
            Long testTimeLimit;
            if (test.getHasTimeLimit()) {
                testTimeLimit = test.getTimeLimit() * 60L * 1000L;
            } else {
                testTimeLimit = 6L * 60L * 60L * 1000L;
            }
            Long testTimeBuffer = 5L * 1000L;
            Long testTimeSpent = Calendar.getInstance().getTime().getTime() - testAnswer.getStartedOn().getTime();
            if (testTimeSpent > testTimeLimit + testTimeBuffer) {
                if (test.getHasTimeLimit()) {
                    Long maxFinishDate = testAnswer.getStartedOn().getTime() + test.getTimeLimit() * 60L * 1000L;
                    Calendar finishedOn = Calendar.getInstance();
                    finishedOn.setTimeInMillis(maxFinishDate);
                    testAnswer.setFinishedOn(finishedOn.getTime());
                } else {
                    testAnswer.setFinishedOn(Calendar.getInstance().getTime());
                }
                for (QuestionAnswer questionAnswer : testAnswer.getQuestionAnswers()) {
                    if (questionAnswer.getFinishedOn() == null) {
                        completeUnfinishedQuestionAnswer(questionAnswer);
                    }
                }
                boolean checked = true;
                for (QuestionAnswer questionAnswer : testAnswer.getQuestionAnswers()) {
                    if (QuestionType.FREETEXT.equals(questionAnswer.getType())) {
                        checked = false;
                    }
                }
                testAnswer.setChecked(checked);
                testAnswer = testAnswerRepository.save(testAnswer);
                Student student = testAnswer.getStudent();
                LOGGER.info(
                    "Automatically saved test answer [id: " + testAnswer.getId() + "] " +
                    "for student [id: " + student.getId() + ", ldapLogin: " + student.getLdapLogin() + "] " +
                    "and test [id: " + test.getId() + "]"
                );
            }
        }
    }

    private void completeUnfinishedQuestionAnswer(QuestionAnswer questionAnswer) {
        Question question = questionAnswer.getQuestion();
        if (question.getHasTimeLimit()) {
            Long timeLimit = question.getTimeLimit() * 1000L;
            Long maxFinishTime = questionAnswer.getStartedOn().getTime() + timeLimit;
            if (Calendar.getInstance().getTime().getTime() < maxFinishTime) {
                questionAnswer.setFinishedOn(Calendar.getInstance().getTime());
            } else {
                Calendar finishedOn = Calendar.getInstance();
                finishedOn.setTimeInMillis(maxFinishTime);
                questionAnswer.setFinishedOn(finishedOn.getTime());
            }
        } else {
            questionAnswer.setFinishedOn(Calendar.getInstance().getTime());
        }
    }

}
