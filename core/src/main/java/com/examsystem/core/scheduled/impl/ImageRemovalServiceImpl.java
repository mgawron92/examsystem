package com.examsystem.core.scheduled.impl;

import com.examsystem.core.scheduled.api.ImageRemovalService;
import com.examsystem.core.services.api.other.ImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Mateusz Gawron on 2016-07-05.
 */
@Service
public class ImageRemovalServiceImpl implements ImageRemovalService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageRemovalServiceImpl.class);

    @Autowired
    private ImageService imageService;

    @Scheduled(fixedRate = 60 * 60 * 1000)
    @Transactional(readOnly = false)
    @Override
    public void removeUnusedImages() {
        LOGGER.info("Started scheduled service for removing unused images");
        int removedImagesCount = imageService.removeUnusedImages();
        LOGGER.info("Finished removing unused images [removed images count: " + removedImagesCount + "]");
    }


}
