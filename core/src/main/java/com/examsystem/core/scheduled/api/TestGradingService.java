package com.examsystem.core.scheduled.api;

/**
 * Created by Mateusz Gawron on 2016-07-05.
 */
public interface TestGradingService {

    void completeUnfinishedTestAnswers();

}
