package com.examsystem.core.enumerations;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public enum SettingsType {
    BACKUPS, DEFAULTS, LISTS
}
