package com.examsystem.core.enumerations;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public enum QuestionType {
    SINGLE_SIMPLE_CHOICE,
    MULTIPLE_SIMPLE_CHOICE,
    SIMPLE_COMPLEX_CHOICE,
    MULTIPLE_COMPLEX_CHOICE,
    FREETEXT,
    JOIN
}
