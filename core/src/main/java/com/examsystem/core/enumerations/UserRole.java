package com.examsystem.core.enumerations;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public enum UserRole {
    STUDENT, EXAMINER, ADMIN;

    private static Map<UserRole, String> stringMap = new HashMap<>();

    static {
        stringMap.put(UserRole.ADMIN, "admin");
        stringMap.put(UserRole.EXAMINER, "examiner");
        stringMap.put(UserRole.STUDENT, "student");
    }

    public static String mapToString(UserRole userRole) {
        if (stringMap.containsKey(userRole)) {
            return stringMap.get(userRole);
        }
        return "";
    }

}
