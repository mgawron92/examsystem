package com.examsystem.core.services.api.settings;

import com.examsystem.core.resources.settings.DefaultsResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface DefaultsSettingsService {

    DefaultsResource getDefaultsSettings();

    DefaultsResource saveDefaultsSettings(DefaultsResource defaultsResource);

    void deleteDefaultsSettings();

//    List<Settings> resourceToEntity(DefaultsResource defaultsResource);

//    DefaultsResource entityToResource(List<Settings> settings);

}
