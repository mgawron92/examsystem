package com.examsystem.core.services.api.questionanswer.choice;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface ChoiceAnswerPartService {

    void deleteChoiceAnswerPartById(Long id);

}
