package com.examsystem.core.services.api.test;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.test.TestResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface TestService {

    Page<TestResource> query(Map<String, Object> params, Pageable pageable);

    TestResource getTestById(Long id);

    TestResource saveTest(TestResource testResource);

    TestResource updateTest(TestResource testResource);

    void deleteTestById(Long id);

    QuestionType getQuestionType(Long tid, Long qid);

}
