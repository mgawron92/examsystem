package com.examsystem.core.services.api.settings;

import com.examsystem.core.resources.settings.BackupsResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface BackupsSettingsService {

    BackupsResource getBackupsSettings();

    BackupsResource saveBackupsSettings(BackupsResource backupsResource);

    void deleteBackupsSettings(boolean changePassword);

//    List<Settings> resourceToEntity(BackupsResource backupsResource);

//    BackupsResource entityToResource(List<Settings> settings);

}
