package com.examsystem.core.services.impl.questionanswer.freetext;

import com.examsystem.core.entitites.questionanswer.freetext.FreetextAnswer;
import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.mappers.api.questionanswer.freetext.FreetextAnswerMapper;
import com.examsystem.core.repositories.FreetextAnswerRepository;
import com.examsystem.core.resources.questionanswer.freetext.FreetextAnswerResource;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.security.services.api.CurrentUserService;
import com.examsystem.core.services.api.questionanswer.freetext.FreetextAnswerService;
import com.examsystem.core.updaters.api.questionanswer.freetext.FreetextAnswerUpdater;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class FreetextAnswerServiceImpl implements FreetextAnswerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FreetextAnswerService.class);

    @Autowired
    private FreetextAnswerMapper freetextAnswerMapper;

    @Autowired
    private FreetextAnswerUpdater freetextAnswerUpdater;

    @Autowired
    private FreetextAnswerRepository freetextAnswerRepository;

    @Autowired
    private CurrentUserService currentUserService;

    @Transactional(readOnly = true)
    @NotNull
    public List<FreetextAnswerResource> getAllQuestionAnswers() {
        return freetextAnswerRepository.findAll().stream().map(freetextAnswerMapper::mapEntityToResource).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Nullable
    public FreetextAnswerResource getQuestionAnswerById(@NotNull Long id) {
        FreetextAnswer freetextAnswer = freetextAnswerRepository.findFreetextAnswerById(id);
        return freetextAnswer == null ? null : freetextAnswerMapper.mapEntityToResource(freetextAnswer);
    }

    @Transactional(readOnly = false)
    public FreetextAnswerResource saveQuestionAnswer(@NotNull FreetextAnswerResource questionAnswerResource) {
        FreetextAnswer freetextAnswer = freetextAnswerMapper.mapResourceToEntity(questionAnswerResource);
        freetextAnswer = freetextAnswerRepository.save(freetextAnswer);
        questionAnswerResource = freetextAnswerMapper.mapEntityToResource(freetextAnswer);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Question answer [id: " + questionAnswerResource.getId() + "] " +
            "saved to test answer [id: " + questionAnswerResource.getTestAnswerId() + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
        return questionAnswerResource;
    }

    @Transactional(readOnly = false)
    public FreetextAnswerResource updateQuestionAnswer(@NotNull FreetextAnswerResource questionAnswerResource) {
        FreetextAnswerResource freetextAnswerResource = questionAnswerResource;
        FreetextAnswer freetextAnswer = freetextAnswerRepository.findFreetextAnswerById(freetextAnswerResource.getId());
        freetextAnswerUpdater.updateEntityFromResource(freetextAnswer, freetextAnswerResource);
        freetextAnswer = freetextAnswerRepository.save(freetextAnswer);
        freetextAnswerResource = freetextAnswerMapper.mapEntityToResource(freetextAnswer);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Question answer [id: " + freetextAnswerResource.getId() + "] " +
            "updated in test answer [id: " + freetextAnswerResource.getTestAnswerId() + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
        return freetextAnswerResource;
    }

    @Transactional(readOnly = false)
    public void deleteQuestionAnswerById(@NotNull Long id) {
        FreetextAnswer freetextAnswer = freetextAnswerRepository.findFreetextAnswerById(id);
        Long testAnswerId = freetextAnswer.getTestAnswer().getId();
        freetextAnswerRepository.removeById(id);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Question answer [id: " + id + "] " +
            "removed from test answer [id: " + testAnswerId + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
    }

}
