package com.examsystem.core.services.impl.questionanswer.join;

import com.examsystem.core.entitites.questionanswer.join.JoinAnswer;
import com.examsystem.core.entitites.questionanswer.join.JoinAnswerPart;
import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.mappers.api.questionanswer.join.JoinAnswerMapper;
import com.examsystem.core.repositories.JoinAnswerPartRepository;
import com.examsystem.core.repositories.JoinAnswerRepository;
import com.examsystem.core.resources.questionanswer.join.JoinAnswerResource;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.security.services.api.CurrentUserService;
import com.examsystem.core.services.api.questionanswer.join.JoinAnswerService;
import com.examsystem.core.updaters.api.questionanswer.join.JoinAnswerUpdater;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class JoinAnswerServiceImpl implements JoinAnswerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(JoinAnswerService.class);

    @Autowired
    private JoinAnswerMapper joinAnswerMapper;

    @Autowired
    private JoinAnswerUpdater joinAnswerUpdater;

    @Autowired
    private JoinAnswerRepository joinAnswerRepository;

    @Autowired
    private JoinAnswerPartRepository joinAnswerPartRepository;

    @Autowired
    private CurrentUserService currentUserService;

    @Transactional(readOnly = true)
    @Nullable
    public JoinAnswerResource getQuestionAnswerById(@NotNull Long id) {
        JoinAnswer joinAnswer = joinAnswerRepository.findJoinAnswerById(id);
        return joinAnswer == null ? null : joinAnswerMapper.mapEntityToResource(joinAnswer);
    }

    @Transactional(readOnly = false)
    public JoinAnswerResource saveQuestionAnswer(@NotNull JoinAnswerResource questionAnswerResource) {
        JoinAnswer joinAnswer = joinAnswerMapper.mapResourceToEntity(questionAnswerResource);
        joinAnswer = joinAnswerRepository.save(joinAnswer);
        for (JoinAnswerPart joinAnswerPart : joinAnswer.getJoinAnswerParts()) {
            joinAnswerPart.setJoinAnswer(joinAnswer);
            joinAnswerPartRepository.save(joinAnswerPart);
        }
        questionAnswerResource = joinAnswerMapper.mapEntityToResource(joinAnswer);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Question answer [id: " + questionAnswerResource.getId() + "] " +
            "saved to test answer [id: " + questionAnswerResource.getTestAnswerId() + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
        return questionAnswerResource;
    }

    @Transactional(readOnly = false)
    public JoinAnswerResource updateQuestionAnswer(@NotNull JoinAnswerResource questionAnswerResource) {
        JoinAnswerResource joinAnswerResource = questionAnswerResource;
        JoinAnswer joinAnswer = joinAnswerRepository.findJoinAnswerById(joinAnswerResource.getId());
        for (JoinAnswerPart joinAnswerPart : joinAnswer.getJoinAnswerParts()) {
            joinAnswerPartRepository.removeById(joinAnswerPart.getId());
        }
        joinAnswerUpdater.updateEntityFromResource(joinAnswer, joinAnswerResource);
        List<JoinAnswerPart> updatedJoinAnswerParts = new LinkedList<>();
        for (JoinAnswerPart joinAnswerPart : joinAnswer.getJoinAnswerParts()) {
            joinAnswerPart.setId(null);
            updatedJoinAnswerParts.add(joinAnswerPartRepository.save(joinAnswerPart));
        }
        joinAnswer.setJoinAnswerParts(updatedJoinAnswerParts);
        joinAnswer = joinAnswerRepository.save(joinAnswer);
        joinAnswerResource = joinAnswerMapper.mapEntityToResource(joinAnswer);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Question answer [id: " + joinAnswerResource.getId() + "] " +
            "updated in test answer [id: " + joinAnswerResource.getTestAnswerId() + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
        return joinAnswerResource;
    }

    @Transactional(readOnly = false)
    public void deleteQuestionAnswerById(@NotNull Long id) {
        JoinAnswer joinAnswer = joinAnswerRepository.findJoinAnswerById(id);
        Long testAnswerId = joinAnswer.getTestAnswer().getId();
        for (JoinAnswerPart joinAnswerPart : joinAnswer.getJoinAnswerParts()) {
            joinAnswerPartRepository.removeById(joinAnswerPart.getId());
        }
        joinAnswerRepository.removeById(id);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Question answer [id: " + id + "] " +
            "removed from test answer [id: " + testAnswerId + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
    }

}
