package com.examsystem.core.services.api.cipher;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface CipherService {

    String encrypt(String text);

    String decrypt(String encrypted);

}
