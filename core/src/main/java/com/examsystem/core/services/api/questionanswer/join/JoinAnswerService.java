package com.examsystem.core.services.api.questionanswer.join;

import com.examsystem.core.resources.questionanswer.join.JoinAnswerResource;
import com.examsystem.core.services.api.questionanswer.QuestionAnswerService;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface JoinAnswerService extends QuestionAnswerService<JoinAnswerResource> {

}
