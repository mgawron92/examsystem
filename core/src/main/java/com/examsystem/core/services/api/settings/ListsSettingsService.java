package com.examsystem.core.services.api.settings;

import com.examsystem.core.resources.settings.ListsResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface ListsSettingsService {

    ListsResource getListsSettings();

    ListsResource saveListsSettings(ListsResource listsResource);

    void deleteListsSettings();

//    List<Settings> resourceToEntity(ListsResource listsResource);

//    ListsResource entityToResource(List<Settings> settings);

}
