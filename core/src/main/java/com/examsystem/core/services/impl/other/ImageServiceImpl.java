package com.examsystem.core.services.impl.other;

import com.examsystem.core.entitites.other.Image;
import com.examsystem.core.entitites.question.Question;
import com.examsystem.core.repositories.ImageRepository;
import com.examsystem.core.services.api.other.ImageService;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class ImageServiceImpl implements ImageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageServiceImpl.class);

    @Autowired
    private ImageRepository imageRepository;

    @Transactional(readOnly = false)
    public Long save(@NotNull Image image) {
        return imageRepository.save(image).getId();
    }

    @Transactional(readOnly = true)
    @Nullable
    public Image getById(@NotNull Long id) {
        return imageRepository.findImageById(id);
    }

    @Transactional(readOnly = true)
    public Integer getQuestionsCountByImageId(Long imageId) {
        return imageRepository.getQuestionsCountByImageId(imageId);
    }

    @Transactional(readOnly = true)
    public List<Long> getTestIdsByImageId(Long imageId) {
        return imageRepository.getTestIdsByImageId(imageId);
    }

    @Transactional(readOnly = false)
    public void removeIfLastQuestionRemoved(Question question) {
        if (question.getImageId() != null) {
            Long imageId = question.getImageId();
            int questionsWithImage = getQuestionsCountByImageId(imageId);
            if (questionsWithImage == 1) {
                deleteById(imageId);
            }
        }
    }

    @Transactional(readOnly = false)
    public int removeUnusedImages() {
        Date date = Calendar.getInstance().getTime();
        date.setTime(Calendar.getInstance().getTime().getTime() - 24L * 60L * 60L * 1000L);
        List<Long> unusedImageIds = imageRepository.getUnusedImageIds(date);
        for (Long unusedImageId : unusedImageIds) {
            LOGGER.info("Removing unused image [id: " + unusedImageId + "]");
        }
        imageRepository.removeUnusedImages(date);
        return unusedImageIds.size();
    }

    @Transactional(readOnly = false)
    public void deleteById(@NotNull Long id) {
        imageRepository.removeById(id);
    }

}
