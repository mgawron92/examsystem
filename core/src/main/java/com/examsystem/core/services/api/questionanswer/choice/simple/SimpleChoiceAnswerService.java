package com.examsystem.core.services.api.questionanswer.choice.simple;

import com.examsystem.core.resources.questionanswer.choice.simple.SimpleChoiceAnswerResource;
import com.examsystem.core.services.api.questionanswer.QuestionAnswerService;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface SimpleChoiceAnswerService extends QuestionAnswerService<SimpleChoiceAnswerResource> {

}
