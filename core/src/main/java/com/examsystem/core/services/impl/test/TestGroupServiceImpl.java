package com.examsystem.core.services.impl.test;

import com.examsystem.core.entitites.intermediary.TestTestGroup;
import com.examsystem.core.entitites.test.Test;
import com.examsystem.core.entitites.test.TestGroup;
import com.examsystem.core.mappers.api.test.TestGroupMapper;
import com.examsystem.core.mappers.api.user.StudentMapper;
import com.examsystem.core.repositories.TestGroupRepository;
import com.examsystem.core.repositories.TestTestGroupRepository;
import com.examsystem.core.resources.test.TestGroupResource;
import com.examsystem.core.resources.user.StudentResource;
import com.examsystem.core.services.api.test.TestGroupService;
import com.examsystem.core.services.api.test.TestService;
import com.examsystem.core.specifications.TestGroupSpecification;
import com.examsystem.core.updaters.api.test.TestGroupUpdater;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class TestGroupServiceImpl implements TestGroupService {

    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private TestGroupMapper testGroupMapper;

    @Autowired
    private TestGroupUpdater testGroupUpdater;

    @Autowired
    private TestService testService;

    @Autowired
    private TestGroupRepository testGroupRepository;

    @Autowired
    private TestTestGroupRepository testTestGroupRepository;

    @Transactional(readOnly = true)
    @NotNull
    public Page<TestGroupResource> query(Map<String, Object> params, Pageable pageable) {
        Specification<TestGroup> specification = TestGroupSpecification.getSpecification(params);
        return testGroupRepository.findAll(specification, pageable).map(testGroupMapper::mapEntityToResource);
    }

    @Transactional(readOnly = true)
    @NotNull
    public List<TestGroupResource> query(Map<String, Object> params) {
        Specification<TestGroup> specification = TestGroupSpecification.getSpecification(params);
        return testGroupRepository.findAll(specification).stream().map(testGroupMapper::mapEntityToResource).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @NotNull
    public Page<StudentResource> getStudentsByTestGroupId(Long id, Pageable pageable) {
        return testGroupRepository.findStudentsByTestGroupId(id, pageable).map(studentMapper::mapEntityToResource);
    }

    @Transactional(readOnly = true)
    @Nullable
    public TestGroupResource getTestGroupById(@NotNull Long id) {
        TestGroup testGroup = testGroupRepository.findTestGroupById(id);
        return testGroup == null ? null : testGroupMapper.mapEntityToResource(testGroup);
    }

    @Transactional(readOnly = false)
    public TestGroupResource saveTestGroup(@NotNull TestGroupResource testGroupResource) {
        TestGroup testGroup = testGroupMapper.mapResourceToEntity(testGroupResource);
        testGroup = testGroupRepository.save(testGroup);
        testGroupResource = testGroupMapper.mapEntityToResource(testGroup);
        return testGroupResource;
    }

    @Transactional(readOnly = false)
    public TestGroupResource updateTestGroup(@NotNull TestGroupResource testGroupResource) {
        TestGroup testGroup = testGroupRepository.findTestGroupById(testGroupResource.getId());
        testGroupUpdater.updateEntityFromResource(testGroup, testGroupResource);
        testGroup = testGroupRepository.save(testGroup);
        testGroupResource = testGroupMapper.mapEntityToResource(testGroup);
        return testGroupResource;
    }

    @Transactional(readOnly = false)
    public void deleteTestGroupById(@NotNull Long id) {
        TestGroup testGroup = testGroupRepository.findTestGroupById(id);
        for (TestTestGroup testTestGroup : testGroup.getTestTestGroups()) {
            Test test = testTestGroup.getTest();
            testTestGroupRepository.removeById(testTestGroup.getId());
            if (test.getTestTestGroups().size() == 1) {
                testService.deleteTestById(test.getId());
            }
        }
        testGroupRepository.removeById(id);
    }

}
