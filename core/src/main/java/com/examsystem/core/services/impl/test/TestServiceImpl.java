package com.examsystem.core.services.impl.test;

import com.examsystem.core.entitites.test.Test;
import com.examsystem.core.entitites.intermediary.TestTestGroup;
import com.examsystem.core.entitites.question.Question;
import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.mappers.api.test.TestMapper;
import com.examsystem.core.repositories.TestRepository;
import com.examsystem.core.resources.test.TestResource;
import com.examsystem.core.services.api.intermediary.TestTestGroupService;
import com.examsystem.core.services.api.other.ImageService;
import com.examsystem.core.services.api.test.TestService;
import com.examsystem.core.specifications.TestSpecification;
import com.examsystem.core.updaters.api.test.TestUpdater;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private TestMapper testMapper;

    @Autowired
    private TestUpdater testUpdater;

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private TestTestGroupService testTestGroupService;

    @Autowired
    private ImageService imageService;

    @Transactional(readOnly = true)
    @NotNull
    public Page<TestResource> query(Map<String, Object> params, Pageable pageable) {
        Specification<Test> specification = TestSpecification.getSpecification(params);
        return testRepository.findAll(specification, pageable).map(testMapper::mapEntityToResource);
    }

    @Transactional(readOnly = true)
    @Nullable
    public TestResource getTestById(@NotNull Long id) {
        Test test = testRepository.findTestById(id);
        return test == null ? null : testMapper.mapEntityToResource(test);
    }

    @Transactional(readOnly = false)
    public TestResource saveTest(@NotNull TestResource testResource) {
        Test test = testMapper.mapResourceToEntity(testResource);
        test = testRepository.save(test);
        testResource = testMapper.mapEntityToResource(test);
        return testResource;
    }

    @Transactional(readOnly = false)
    public TestResource updateTest(@NotNull TestResource testResource) {
        Test test = testRepository.findTestById(testResource.getId());
        testUpdater.updateEntityFromResource(test, testResource);
        test = testRepository.save(test);
        testResource = testMapper.mapEntityToResource(test);
        return testResource;
    }

    @Transactional(readOnly = false)
    public void deleteTestById(@NotNull Long id) {
        Test test = testRepository.findTestById(id);

        for (TestTestGroup testTestGroup : test.getTestTestGroups()) {
            testTestGroupService.deleteTestTestGroupById(testTestGroup.getId());
        }
        for (Question question : test.getQuestions()) {
            imageService.removeIfLastQuestionRemoved(question);
        }

        testRepository.removeById(id);
    }

    @Transactional(readOnly = true)
    public QuestionType getQuestionType(Long tid, Long qid) {
        List<Question> questions = testRepository.findTestById(tid).getQuestions();
        for (Question question : questions) {
            if (question.getId().equals(qid)) {
                return question.getType();
            }
        }
        return null;
    }

}
