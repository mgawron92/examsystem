package com.examsystem.core.services.api.question.join;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface JoinPatternService {

    void deleteJoinPatternById(Long id);

//    JoinPattern resourceToEntity(JoinPatternResource joinPatternResource);

//    JoinPattern updateEntity(JoinPattern joinPattern, JoinPatternResource joinPatternResource);

//    JoinPatternResource entityToResource(JoinPattern joinPattern);

}
