package com.examsystem.core.services.impl.questionanswer.choice;

import com.examsystem.core.repositories.SimpleChoiceAnswerPartRepository;
import com.examsystem.core.services.api.questionanswer.choice.ChoiceAnswerPartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class ChoiceAnswerPartServiceImpl implements ChoiceAnswerPartService {

    @Autowired
    private SimpleChoiceAnswerPartRepository choiceAnswerPartRepository;

    @Transactional(readOnly = false)
    public void deleteChoiceAnswerPartById(@NotNull Long id) {
        choiceAnswerPartRepository.removeById(id);
    }

}
