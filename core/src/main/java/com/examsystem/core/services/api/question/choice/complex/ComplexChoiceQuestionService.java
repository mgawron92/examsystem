package com.examsystem.core.services.api.question.choice.complex;

import com.examsystem.core.resources.question.choice.complex.ComplexChoiceQuestionResource;
import com.examsystem.core.services.api.question.QuestionService;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface ComplexChoiceQuestionService extends QuestionService<ComplexChoiceQuestionResource> {

}
