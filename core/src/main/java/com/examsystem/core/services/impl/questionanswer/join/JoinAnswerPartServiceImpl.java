package com.examsystem.core.services.impl.questionanswer.join;

import com.examsystem.core.repositories.JoinAnswerPartRepository;
import com.examsystem.core.services.api.questionanswer.join.JoinAnswerPartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class JoinAnswerPartServiceImpl implements JoinAnswerPartService {

    @Autowired
    private JoinAnswerPartRepository joinAnswerPartRepository;

    @Transactional(readOnly = false)
    public void deleteJoinAnswerPartById(@NotNull Long id) {
        joinAnswerPartRepository.removeById(id);
    }

}
