package com.examsystem.core.services.api.other;

import com.examsystem.core.entitites.other.Image;
import com.examsystem.core.entitites.question.Question;

import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface ImageService {

    Long save(Image image);

    Image getById(Long id);

    Integer getQuestionsCountByImageId(Long imageId);

    List<Long> getTestIdsByImageId(Long imageId);

    void removeIfLastQuestionRemoved(Question question);

    int removeUnusedImages();

    void deleteById(Long id);

}
