package com.examsystem.core.services.impl.user;

import com.examsystem.core.entitites.user.Student;
import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.mappers.api.user.StudentMapper;
import com.examsystem.core.repositories.StudentRepository;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.resources.user.StudentResource;
import com.examsystem.core.security.services.api.CurrentUserService;
import com.examsystem.core.services.api.user.StudentService;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class StudentServiceImpl implements StudentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentServiceImpl.class);

    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private CurrentUserService currentUserService;

    @Transactional(readOnly = true)
    @NotNull
    public Page<StudentResource> getAll(Pageable pageable) {
        return studentRepository.findAll(pageable).map(studentMapper::mapEntityToResource);
    }

    @Transactional(readOnly = true)
    @NotNull
    public Page<StudentResource> getByTestId(Long testId, Pageable pageable) {
        return studentRepository.findStudentsByTestId(testId, pageable).map(studentMapper::mapEntityToResource);
    }

    @Transactional(readOnly = true)
    @Nullable
    public StudentResource getByLdapLogin(@NotNull String ldapLogin) {
        Student student = studentRepository.findStudentByLdapLogin(ldapLogin);
        return student == null ? null : studentMapper.mapEntityToResource(student);
    }

    @Transactional(readOnly = true)
    @Nullable
    public StudentResource getById(@NotNull Long id) {
        Student student = studentRepository.findStudentById(id);
        return student == null ? null : studentMapper.mapEntityToResource(student);
    }

    @Transactional(readOnly = false)
    public StudentResource save(@NotNull StudentResource studentResource) {
        Student student = studentMapper.mapResourceToEntity(studentResource);
        student = studentRepository.save(student);
        studentResource = studentMapper.mapEntityToResource(student);
        PersonResource currentUser = currentUserService.getCurrentUser();
        if (currentUser != null) {
            LOGGER.info(
                "New student [id: " + studentResource.getId() + ", ldapLogin: " + studentResource.getLdapLogin() + "] " +
                "added by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
            );
        } else {
            LOGGER.info(
                "New student [id: " + studentResource.getId() + ", ldapLogin: " + studentResource.getLdapLogin() + "] " +
                "added automatically by system on first login attempt"
            );
        }
        return studentResource;
    }

    @Transactional(readOnly = false)
    public void deleteById(@NotNull Long id) {
        StudentResource studentResource = getById(id);
        studentRepository.removeById(id);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Existing student [id: " + studentResource.getId() + ", ldapLogin: " + studentResource.getLdapLogin() + "] " +
            "removed by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
    }

    @Transactional(readOnly = true)
    @Override
    public String mapLdapLogin(String ldapLogin) {
        String lowercaseLdapLogin = ldapLogin.toLowerCase();
        Map<String, String> systemLdapLoginsMap = new HashMap<>();
        List<String> systemLdapLogins = studentRepository.getStudentLdapLogins();
        for (String systemLdapLogin : systemLdapLogins) {
            systemLdapLoginsMap.put(systemLdapLogin.toLowerCase(), systemLdapLogin);
        }
        if (systemLdapLoginsMap.containsKey(lowercaseLdapLogin)) {
            return systemLdapLoginsMap.get(lowercaseLdapLogin);
        } else {
            return ldapLogin;
        }
    }

}
