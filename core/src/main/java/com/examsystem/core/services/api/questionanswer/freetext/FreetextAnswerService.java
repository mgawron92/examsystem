package com.examsystem.core.services.api.questionanswer.freetext;

import com.examsystem.core.resources.questionanswer.freetext.FreetextAnswerResource;
import com.examsystem.core.services.api.questionanswer.QuestionAnswerService;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface FreetextAnswerService extends QuestionAnswerService<FreetextAnswerResource> {

}
