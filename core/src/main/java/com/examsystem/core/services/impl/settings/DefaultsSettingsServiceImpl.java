package com.examsystem.core.services.impl.settings;

import com.examsystem.core.entitites.other.Settings;
import com.examsystem.core.enumerations.SettingsType;
import com.examsystem.core.mappers.api.settings.DefaultsSettingsMapper;
import com.examsystem.core.repositories.SettingsRepository;
import com.examsystem.core.resources.settings.DefaultsResource;
import com.examsystem.core.services.api.settings.DefaultsSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class DefaultsSettingsServiceImpl implements DefaultsSettingsService {

    @Autowired
    private DefaultsSettingsMapper defaultsSettingsMapper;

    @Autowired
    private SettingsRepository settingsRepository;

    @Transactional(readOnly = true)
    @NotNull
    public DefaultsResource getDefaultsSettings() {
        return defaultsSettingsMapper.mapEntityToResource(settingsRepository.findSettingsBySettingsType(SettingsType.LISTS));
    }

    @Transactional(readOnly = false)
    public DefaultsResource saveDefaultsSettings(@NotNull DefaultsResource defaultsResource) {
        deleteDefaultsSettings();
        List<Settings> settings = defaultsSettingsMapper.mapResourceToEntity(defaultsResource);
        List<Settings> savedSettings = new LinkedList<>();
        for (Settings s : settings) {
            savedSettings.add(settingsRepository.save(s));
        }
        defaultsResource = defaultsSettingsMapper.mapEntityToResource(savedSettings);
        return defaultsResource;
    }

    @Transactional(readOnly = false)
    public void deleteDefaultsSettings() {
        settingsRepository.removeSettingsBySettingsType(SettingsType.LISTS);
    }

}
