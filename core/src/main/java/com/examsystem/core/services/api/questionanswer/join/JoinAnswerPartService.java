package com.examsystem.core.services.api.questionanswer.join;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface JoinAnswerPartService {

    void deleteJoinAnswerPartById(Long id);

}
