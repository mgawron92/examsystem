package com.examsystem.core.services.impl.user;

import com.examsystem.core.entitites.user.Examiner;
import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.mappers.api.user.ExaminerMapper;
import com.examsystem.core.repositories.ExaminerRepository;
import com.examsystem.core.resources.user.ExaminerResource;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.security.ldap.services.api.LdapService;
import com.examsystem.core.security.services.api.CurrentUserService;
import com.examsystem.core.services.api.user.ExaminerService;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class ExaminerServiceImpl implements ExaminerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExaminerServiceImpl.class);

    @Autowired
    private ExaminerMapper examinerMapper;

    @Autowired
    private ExaminerRepository examinerRepository;

    @Autowired
    private LdapService ldapServices;

    @Autowired
    private CurrentUserService currentUserService;

    @Transactional(readOnly = true)
    @NotNull
    public Page<ExaminerResource> getAll(Pageable pageable) {
        return examinerRepository.findAll(pageable).map(examinerMapper::mapEntityToResource);
    }

    @Transactional(readOnly = true)
    @Nullable
    public ExaminerResource getByLdapLogin(@NotNull String ldapLogin) {
        Examiner examiner = examinerRepository.findExaminerByLdapLogin(ldapLogin);
        return examiner == null ? null : examinerMapper.mapEntityToResource(examiner);
    }

    @Transactional(readOnly = true)
    @Nullable
    public ExaminerResource getById(@NotNull Long id) {
        Examiner examiner = examinerRepository.findExaminerById(id);
        return examiner == null ? null : examinerMapper.mapEntityToResource(examiner);
    }

    @Transactional(readOnly = false)
    public ExaminerResource save(@NotNull ExaminerResource examinerResource) {
        ldapServices.populateResourceWithLdapData(examinerResource.getLdapLogin(), examinerResource);
        Examiner examiner = examinerMapper.mapResourceToEntity(examinerResource);
        examiner = examinerRepository.save(examiner);
        examinerResource = examinerMapper.mapEntityToResource(examiner);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "New examiner [id: " + examinerResource.getId() + ", ldapLogin: " + examinerResource.getLdapLogin() + "] " +
            "added by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
        return examinerResource;
    }

    @Transactional(readOnly = false)
    public void deleteById(@NotNull Long id) {
        ExaminerResource examinerResource = getById(id);
        examinerRepository.removeExaminerById(id);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Existing examiner [id: " + examinerResource.getId() + ", ldapLogin: " + examinerResource.getLdapLogin() + "] " +
            "removed by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
    }

    @Transactional(readOnly = true)
    @Override
    public String mapLdapLogin(String ldapLogin) {
        String lowercaseLdapLogin = ldapLogin.toLowerCase();
        Map<String, String> systemLdapLoginsMap = new HashMap<>();
        List<String> systemLdapLogins = examinerRepository.getExaminerLdapLogins();
        for (String systemLdapLogin : systemLdapLogins) {
            systemLdapLoginsMap.put(systemLdapLogin.toLowerCase(), systemLdapLogin);
        }
        if (systemLdapLoginsMap.containsKey(lowercaseLdapLogin)) {
            return systemLdapLoginsMap.get(lowercaseLdapLogin);
        } else {
            return ldapLogin;
        }
    }

}
