package com.examsystem.core.services.impl.intermediary;

import com.examsystem.core.entitites.intermediary.StudentTestGroup;
import com.examsystem.core.entitites.intermediary.TestTestGroup;
import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.mappers.api.intermediary.StudentTestGroupMapper;
import com.examsystem.core.repositories.StudentTestGroupRepository;
import com.examsystem.core.resources.intermediary.StudentTestGroupResource;
import com.examsystem.core.resources.testanswer.TestAnswerResource;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.security.services.api.CurrentUserService;
import com.examsystem.core.services.api.intermediary.StudentTestGroupService;
import com.examsystem.core.services.api.testanswer.TestAnswerService;
import com.examsystem.core.services.api.user.StudentService;
import com.examsystem.core.services.impl.user.AdminServiceImpl;
import com.examsystem.core.specifications.StudentTestGroupSpecification;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class StudentTestGroupServiceImpl implements StudentTestGroupService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentTestGroupServiceImpl.class);

    @Autowired
    private StudentTestGroupMapper studentTestGroupMapper;

    @Autowired
    private StudentTestGroupRepository studentTestGroupRepository;

    @Autowired
    private TestAnswerService testAnswerService;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private StudentService studentService;

    @Transactional(readOnly = true)
    @NotNull
    public List<StudentTestGroupResource> query(Map<String, Object> params) {
        Specification<StudentTestGroup> specification = StudentTestGroupSpecification.getSpecification(params);
        return studentTestGroupRepository.findAll(specification).stream().map(studentTestGroupMapper::mapEntityToResource).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Nullable
    public StudentTestGroupResource getStudentTestGroupById(@NotNull Long id) {
        StudentTestGroup studentTestGroup = studentTestGroupRepository.findStudentTestGroupById(id);
        return studentTestGroup == null ? null : studentTestGroupMapper.mapEntityToResource(studentTestGroup);
    }

    @Transactional(readOnly = false)
    public StudentTestGroupResource saveStudentTestGroup(@NotNull StudentTestGroupResource studentTestGroupResource) {
        StudentTestGroup studentTestGroup = studentTestGroupMapper.mapResourceToEntity(studentTestGroupResource);
        studentTestGroup = studentTestGroupRepository.save(studentTestGroup);
        studentTestGroupResource = studentTestGroupMapper.mapEntityToResource(studentTestGroup);
        PersonResource currentUser = currentUserService.getCurrentUser();
        String studentLdapLogin = studentService.getById(studentTestGroupResource.getStudentId()).getLdapLogin();
        LOGGER.info(
            "Student [id: " + studentTestGroupResource.getStudentId() + ", ldapLogin: " + studentLdapLogin + "] " +
            "added to test group [id: " + studentTestGroupResource.getTestGroupId() + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
        return studentTestGroupResource;
    }

    @Transactional(readOnly = false)
    public void deleteStudentTestGroupById(@NotNull Long id) {
        StudentTestGroup studentTestGroup = studentTestGroupRepository.findStudentTestGroupById(id);
        Long studentId = studentTestGroup.getStudent().getId();
        String studentLdapLogin = studentService.getById(studentId).getLdapLogin();
        Long testGroupId = studentTestGroup.getTestGroup().getId();
        List<TestTestGroup> testTestGroups = studentTestGroup.getTestGroup().getTestTestGroups();
        for (TestTestGroup testTestGroup : testTestGroups) {
            Map<String, Object> params = new HashMap<>();
            params.put("testId", testTestGroup.getTest().getId());
            params.put("studentId", studentTestGroup.getStudent().getId());
            List<TestAnswerResource> testAnswers = testAnswerService.query(params);
            for (TestAnswerResource testAnswer : testAnswers) {
                testAnswerService.deleteTestAnswerById(testAnswer.getId());
            }
        }
        studentTestGroupRepository.removeById(id);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Student [id: " + studentId + ", ldapLogin: " + studentLdapLogin + "] " +
            "removed from test group [id: " + testGroupId + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
    }

}
