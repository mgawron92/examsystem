package com.examsystem.core.services.impl.question.join;

import com.examsystem.core.entitites.question.join.JoinPattern;
import com.examsystem.core.entitites.question.join.JoinQuestion;
import com.examsystem.core.mappers.api.question.join.JoinQuestionMapper;
import com.examsystem.core.repositories.JoinPatternRepository;
import com.examsystem.core.repositories.JoinQuestionRepository;
import com.examsystem.core.resources.question.join.JoinPatternResource;
import com.examsystem.core.resources.question.join.JoinQuestionResource;
import com.examsystem.core.services.api.other.ImageService;
import com.examsystem.core.services.api.question.join.JoinQuestionService;
import com.examsystem.core.updaters.api.question.join.JoinQuestionUpdater;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class JoinQuestionServiceImpl implements JoinQuestionService {

    @Autowired
    private JoinQuestionMapper joinQuestionMapper;

    @Autowired
    private JoinQuestionUpdater joinQuestionUpdater;

    @Autowired
    private JoinQuestionRepository joinQuestionRepository;

    @Autowired
    private JoinPatternRepository joinPatternRepository;

    @Autowired
    private ImageService imageService;

    @Transactional(readOnly = true)
    @NotNull
    public List<JoinQuestionResource> getAllQuestions() {
        return joinQuestionRepository.findAll().stream().map(joinQuestionMapper::mapEntityToResource).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Nullable
    public JoinQuestionResource getQuestionById(@NotNull Long id) {
        JoinQuestion joinQuestion = joinQuestionRepository.findJoinQuestionById(id);
        return joinQuestion == null ? null : joinQuestionMapper.mapEntityToResource(joinQuestion);
    }

    @Transactional(readOnly = false)
    public JoinQuestionResource saveQuestion(@NotNull JoinQuestionResource questionResource) {
        JoinQuestion joinQuestion = joinQuestionMapper.mapResourceToEntity(questionResource);
        joinQuestion = joinQuestionRepository.save(joinQuestion);
        for (JoinPattern joinPattern : joinQuestion.getJoinPatterns()) {
            joinPattern.setJoinQuestion(joinQuestion);
            joinPatternRepository.save(joinPattern);
        }
        return joinQuestionMapper.mapEntityToResource(joinQuestion);
    }

    @Transactional(readOnly = false)
    public JoinQuestionResource updateQuestion(@NotNull JoinQuestionResource questionResource) {
        JoinQuestionResource joinQuestionResource = questionResource;
        JoinQuestion joinQuestion = joinQuestionRepository.findJoinQuestionById(joinQuestionResource.getId());
        for (JoinPattern joinPattern : joinQuestion.getJoinPatterns()) {
            joinPatternRepository.removeById(joinPattern.getId());
        }
        for (JoinPatternResource joinPatternResource : joinQuestionResource.getJoinPatterns()) {
            joinPatternResource.setJoinAnswerPartIds(new LinkedList<>());
        }
        joinQuestionUpdater.updateEntityFromResource(joinQuestion, joinQuestionResource);
        List<JoinPattern> updatedJoinPatterns = new LinkedList<>();
        for (JoinPattern joinPattern : joinQuestion.getJoinPatterns()) {
            joinPattern.setId(null);
            updatedJoinPatterns.add(joinPatternRepository.save(joinPattern));
        }
        joinQuestion.setJoinPatterns(updatedJoinPatterns);
        joinQuestion = joinQuestionRepository.save(joinQuestion);
        return joinQuestionMapper.mapEntityToResource(joinQuestion);
    }

    @Transactional(readOnly = false)
    public void deleteQuestionById(@NotNull Long id) {
        JoinQuestion joinQuestion = joinQuestionRepository.findJoinQuestionById(id);
        imageService.removeIfLastQuestionRemoved(joinQuestion);
        joinQuestionRepository.removeById(id);
    }

}
