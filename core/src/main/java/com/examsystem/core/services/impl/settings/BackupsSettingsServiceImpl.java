package com.examsystem.core.services.impl.settings;

import com.examsystem.core.entitites.other.Settings;
import com.examsystem.core.enumerations.SettingsType;
import com.examsystem.core.mappers.api.settings.BackupsSettingsMapper;
import com.examsystem.core.repositories.SettingsRepository;
import com.examsystem.core.resources.settings.BackupsResource;
import com.examsystem.core.services.api.settings.BackupsSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class BackupsSettingsServiceImpl implements BackupsSettingsService {

    @Autowired
    private BackupsSettingsMapper backupsSettingsMapper;

    @Autowired
    private SettingsRepository settingsRepository;

    @Transactional(readOnly = true)
    @NotNull
    public BackupsResource getBackupsSettings() {
        return backupsSettingsMapper.mapEntityToResource(settingsRepository.findSettingsBySettingsType(SettingsType.BACKUPS));
    }

    @Transactional(readOnly = false)
    public BackupsResource saveBackupsSettings(@NotNull BackupsResource backupsResource) {
        deleteBackupsSettings(backupsResource.getChangePassword());
        List<Settings> settings = backupsSettingsMapper.mapResourceToEntity(backupsResource);
        List<Settings> savedSettings = new LinkedList<>();
        for (Settings s : settings) {
            savedSettings.add(settingsRepository.save(s));
        }
        backupsResource = backupsSettingsMapper.mapEntityToResource(savedSettings);
        return backupsResource;
    }

    @Transactional(readOnly = false)
    public void deleteBackupsSettings(boolean changePassword) {
        if (changePassword) {
            settingsRepository.removeSettingsBySettingsType(SettingsType.BACKUPS);
        } else {
            Settings settings = settingsRepository.findSettingsByKey("password");
            if (settings == null) {
                settings = new Settings("password", null, SettingsType.BACKUPS);
            }
            settingsRepository.removeSettingsBySettingsType(SettingsType.BACKUPS);
            Settings passwordSettings = new Settings();
            passwordSettings.setKey(settings.getKey());
            passwordSettings.setValue(settings.getValue());
            passwordSettings.setSettingsType(SettingsType.BACKUPS);
            settingsRepository.save(passwordSettings);
        }
    }

}
