package com.examsystem.core.services.api.questionanswer.choice.complex;

import com.examsystem.core.resources.questionanswer.choice.complex.ComplexChoiceAnswerResource;
import com.examsystem.core.services.api.questionanswer.QuestionAnswerService;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface ComplexChoiceAnswerService extends QuestionAnswerService<ComplexChoiceAnswerResource> {

}
