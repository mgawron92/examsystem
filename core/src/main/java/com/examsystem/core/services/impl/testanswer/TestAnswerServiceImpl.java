package com.examsystem.core.services.impl.testanswer;

import com.examsystem.core.entitites.question.Question;
import com.examsystem.core.entitites.test.Test;
import com.examsystem.core.entitites.testanswer.QuestionSequence;
import com.examsystem.core.entitites.testanswer.TestAnswer;
import com.examsystem.core.entitites.questionanswer.QuestionAnswer;
import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.mappers.api.testanswer.TestAnswerMapper;
import com.examsystem.core.repositories.QuestionSequenceRepository;
import com.examsystem.core.repositories.TestAnswerRepository;
import com.examsystem.core.resources.testanswer.TestAnswerResource;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.security.services.api.CurrentUserService;
import com.examsystem.core.services.api.testanswer.TestAnswerService;
import com.examsystem.core.services.impl.intermediary.StudentTestGroupServiceImpl;
import com.examsystem.core.specifications.TestAnswerSpecification;
import com.examsystem.core.updaters.api.testanswer.TestAnswerUpdater;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class TestAnswerServiceImpl implements TestAnswerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestAnswerServiceImpl.class);

    @Autowired
    private TestAnswerMapper testAnswerMapper;

    @Autowired
    private TestAnswerUpdater testAnswerUpdater;

    @Autowired
    private TestAnswerRepository testAnswerRepository;

    @Autowired
    private QuestionSequenceRepository questionSequenceRepository;

    @Autowired
    private CurrentUserService currentUserService;

    @Transactional(readOnly = true)
    @NotNull
    public Page<TestAnswerResource> query(Map<String, Object> params, Pageable pageable) {
        Specification<TestAnswer> specification = TestAnswerSpecification.getSpecification(params);
        return testAnswerRepository.findAll(specification, pageable).map(testAnswerMapper::mapEntityToResource);
    }

    @Transactional(readOnly = true)
    @NotNull
    public List<TestAnswerResource> query(Map<String, Object> params) {
        Specification<TestAnswer> specification = TestAnswerSpecification.getSpecification(params);
        return testAnswerRepository.findAll(specification).stream().map(testAnswerMapper::mapEntityToResource).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Nullable
    public TestAnswerResource getTestAnswerById(@NotNull Long id) {
        TestAnswer testAnswer = testAnswerRepository.findTestAnswerById(id);
        return testAnswer == null ? null : testAnswerMapper.mapEntityToResource(testAnswer);
    }

    @Transactional(readOnly = false)
    public TestAnswerResource saveTestAnswer(@NotNull TestAnswerResource testAnswerResource) {
        TestAnswer testAnswer = testAnswerMapper.mapResourceToEntity(testAnswerResource);
        Long studentId = testAnswer.getStudent().getId();
        String studentLdapLogin = testAnswer.getStudent().getLdapLogin();
        generateQuestionSequence(testAnswer);
        computeMaxPoints(testAnswer);
        testAnswer = testAnswerRepository.save(testAnswer);
        testAnswerResource = testAnswerMapper.mapEntityToResource(testAnswer);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Test answer [id: " + testAnswerResource.getId() + "] " +
            "for test [id: " + testAnswerResource.getTestId() + "] and student [id: " + studentId + ", ldapLogin: " + studentLdapLogin + "] " +
            "added by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
        LOGGER.info(
            "Generated question sequence [id: " + testAnswerResource.getQuestionSequenceId() + "] " +
            "for test answer [id: " + testAnswerResource.getId() + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
        return testAnswerResource;
    }

    @Transactional(readOnly = false)
    private void generateQuestionSequence(TestAnswer testAnswer) {
        Test test = testAnswer.getTest();
        List<Long> questionIdsInSequence = test.getQuestions().stream().map(Question::getId).collect(Collectors.toList());
        if (test.getRestrictQuestionsNumber()) {
            int allQuestionsNumber = test.getQuestions().size();
            int usedQuestionsNumber = test.getUsedQuestionsNumber();
            int excludedQuestionsNumber = allQuestionsNumber - usedQuestionsNumber;
            List<Integer> allQuestionIdIndexes = IntStream.range(0, allQuestionsNumber).boxed().collect(Collectors.toList());
            Collections.shuffle(allQuestionIdIndexes);
            List<Integer> excludedQuestionIdIndexes = allQuestionIdIndexes.subList(0, excludedQuestionsNumber);
            excludedQuestionIdIndexes.sort((i1, i2) -> i2 - i1);
            excludedQuestionIdIndexes.forEach((Integer index) -> questionIdsInSequence.remove((int) index));
        }
        if (test.getUseRandomSequence()) {
            Collections.shuffle(questionIdsInSequence);
        }
        QuestionSequence questionSequence = new QuestionSequence();
        if (questionIdsInSequence.size() > 0) {
            questionSequence.setSequence(questionIdsInSequence.stream().map(String::valueOf).reduce((s1, s2) -> s1 + "," + s2).get());
        } else {
            questionSequence.setSequence("");
        }
        questionSequence.setTestAnswer(testAnswer);
        questionSequenceRepository.save(questionSequence);
        testAnswer.setQuestionSequence(questionSequence);
    }

    @Transactional(readOnly = false)
    private void computeMaxPoints(TestAnswer testAnswer) {
        List<Long> questionSequenceIds = Arrays.stream(testAnswer.getQuestionSequence().getSequence().split(",")).map(Long::valueOf).collect(Collectors.toList());
        int maxPoints = 0;
        List<Question> questionsInSequence = new LinkedList<>();
        for (Question question : testAnswer.getTest().getQuestions()) {
            if (questionSequenceIds.contains(question.getId())) {
                questionsInSequence.add(question);
            }
        }
        for (Question question : questionsInSequence) {
            maxPoints += question.getMaxPoints();
        }
        testAnswer.setMaxPoints(maxPoints);
    }

    @Transactional(readOnly = false)
    public TestAnswerResource updateTestAnswer(@NotNull TestAnswerResource testAnswerResource) {
        TestAnswer testAnswer = testAnswerRepository.findTestAnswerById(testAnswerResource.getId());
        testAnswerUpdater.updateEntityFromResource(testAnswer, testAnswerResource);
        for (QuestionAnswer questionAnswer : testAnswer.getQuestionAnswers()) {
            if (questionAnswer.getFinishedOn() == null) {
                completeUnfinishedQuestionAnswer(questionAnswer);
            }
        }

        testAnswer = testAnswerRepository.save(testAnswer);
        testAnswerResource = testAnswerMapper.mapEntityToResource(testAnswer);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Test answer [id: " + testAnswerResource.getId() + "] " +
            "for test [id: " + testAnswerResource.getTestId() + "] " +
            "updated by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
        return testAnswerResource;
    }

    private void completeUnfinishedQuestionAnswer(QuestionAnswer questionAnswer) {
        Question question = questionAnswer.getQuestion();
        if (question.getHasTimeLimit()) {
            Long timeLimit = question.getTimeLimit() * 1000L;
            Long maxFinishDate = questionAnswer.getStartedOn().getTime() + timeLimit;
            if (Calendar.getInstance().getTime().getTime() > maxFinishDate) {
                Calendar finishedOn = Calendar.getInstance();
                finishedOn.setTimeInMillis(maxFinishDate);
                questionAnswer.setFinishedOn(finishedOn.getTime());
            } else {
                questionAnswer.setFinishedOn(Calendar.getInstance().getTime());
            }
        } else {
            questionAnswer.setFinishedOn(Calendar.getInstance().getTime());
        }
    }

    @Transactional(readOnly = false)
    public void deleteTestAnswerById(@NotNull Long id) {
        TestAnswer testAnswer = testAnswerRepository.findTestAnswerById(id);
        Long testAnswerId = testAnswer.getId();
        Long testId = testAnswer.getTest().getId();
        testAnswerRepository.removeById(id);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Test answer [id: " + testAnswerId + "] " +
            "for test [id: " + testId + "] " +
            "removed by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
    }

    @Transactional(readOnly = true)
    public QuestionType getQuestionType(Long tid, Long qid) {
        List<QuestionAnswer> questionAnswers = testAnswerRepository.findTestAnswerById(tid).getQuestionAnswers();
        for (QuestionAnswer questionAnswer : questionAnswers) {
            if (questionAnswer.getId().equals(qid)) {
                return questionAnswer.getType();
            }
        }
        return null;
    }

}
