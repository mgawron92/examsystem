package com.examsystem.core.services.impl.questionanswer.choice.simple;

import com.examsystem.core.entitites.questionanswer.choice.simple.SimpleChoiceAnswer;
import com.examsystem.core.entitites.questionanswer.choice.simple.SimpleChoiceAnswerPart;
import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.mappers.api.questionanswer.choice.simple.SimpleChoiceAnswerMapper;
import com.examsystem.core.repositories.SimpleChoiceAnswerPartRepository;
import com.examsystem.core.repositories.SimpleChoiceAnswerRepository;
import com.examsystem.core.resources.questionanswer.choice.simple.SimpleChoiceAnswerResource;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.security.services.api.CurrentUserService;
import com.examsystem.core.services.api.questionanswer.choice.simple.SimpleChoiceAnswerService;
import com.examsystem.core.updaters.api.questionanswer.choice.simple.SimpleChoiceAnswerUpdater;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SimpleChoiceAnswerServiceImpl implements SimpleChoiceAnswerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleChoiceAnswerService.class);

    @Autowired
    private SimpleChoiceAnswerMapper simpleChoiceAnswerMapper;

    @Autowired
    private SimpleChoiceAnswerUpdater simpleChoiceAnswerUpdater;

    @Autowired
    private SimpleChoiceAnswerRepository simpleChoiceAnswerRepository;

    @Autowired
    private SimpleChoiceAnswerPartRepository simpleChoiceAnswerPartRepository;

    @Autowired
    private CurrentUserService currentUserService;

    @Transactional(readOnly = true)
    @Nullable
    public SimpleChoiceAnswerResource getQuestionAnswerById(@NotNull Long id) {
        SimpleChoiceAnswer simpleChoiceAnswer = simpleChoiceAnswerRepository.findSimpleChoiceAnswerById(id);
        return simpleChoiceAnswer == null ? null : simpleChoiceAnswerMapper.mapEntityToResource(simpleChoiceAnswer);
    }

    @Transactional(readOnly = false)
    public SimpleChoiceAnswerResource saveQuestionAnswer(@NotNull SimpleChoiceAnswerResource questionAnswerResource) {
        SimpleChoiceAnswer simpleChoiceAnswer = simpleChoiceAnswerMapper.mapResourceToEntity(questionAnswerResource);
        simpleChoiceAnswer = simpleChoiceAnswerRepository.save(simpleChoiceAnswer);
        for (SimpleChoiceAnswerPart simpleChoiceAnswerPart : simpleChoiceAnswer.getChoiceAnswerParts()) {
            simpleChoiceAnswerPart.setChoiceAnswer(simpleChoiceAnswer);
            simpleChoiceAnswerPartRepository.save(simpleChoiceAnswerPart);
        }
        questionAnswerResource = simpleChoiceAnswerMapper.mapEntityToResource(simpleChoiceAnswer);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Question answer [id: " + questionAnswerResource.getId() + "] " +
            "saved to test answer [id: " + questionAnswerResource.getTestAnswerId() + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
        return questionAnswerResource;
    }

    @Transactional(readOnly = false)
    public SimpleChoiceAnswerResource updateQuestionAnswer(@NotNull SimpleChoiceAnswerResource questionAnswerResource) {
        SimpleChoiceAnswerResource simpleChoiceAnswerResource = questionAnswerResource;
        SimpleChoiceAnswer simpleChoiceAnswer = simpleChoiceAnswerRepository.findSimpleChoiceAnswerById(simpleChoiceAnswerResource.getId());
        for (SimpleChoiceAnswerPart simpleChoiceAnswerPart : simpleChoiceAnswer.getChoiceAnswerParts()) {
            simpleChoiceAnswerPartRepository.removeById(simpleChoiceAnswerPart.getId());
        }
        simpleChoiceAnswerUpdater.updateEntityFromResource(simpleChoiceAnswer, simpleChoiceAnswerResource);
        List<SimpleChoiceAnswerPart> updatedSimpleChoiceAnswerParts = new LinkedList<>();
        for (SimpleChoiceAnswerPart simpleChoiceAnswerPart : simpleChoiceAnswer.getChoiceAnswerParts()) {
            simpleChoiceAnswerPart.setId(null);
            updatedSimpleChoiceAnswerParts.add(simpleChoiceAnswerPartRepository.save(simpleChoiceAnswerPart));
        }
        simpleChoiceAnswer.setChoiceAnswerParts(updatedSimpleChoiceAnswerParts);
        simpleChoiceAnswer = simpleChoiceAnswerRepository.save(simpleChoiceAnswer);
        simpleChoiceAnswerResource = simpleChoiceAnswerMapper.mapEntityToResource(simpleChoiceAnswer);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Question answer [id: " + simpleChoiceAnswerResource.getId() + "] " +
            "updated in test answer [id: " + simpleChoiceAnswerResource.getTestAnswerId() + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
        return simpleChoiceAnswerResource;
    }

    @Transactional(readOnly = false)
    public void deleteQuestionAnswerById(@NotNull Long id) {
        SimpleChoiceAnswer simpleChoiceAnswer = simpleChoiceAnswerRepository.findSimpleChoiceAnswerById(id);
        Long testAnswerId = simpleChoiceAnswer.getTestAnswer().getId();
        for (SimpleChoiceAnswerPart simpleChoiceAnswerPart : simpleChoiceAnswer.getChoiceAnswerParts()) {
            simpleChoiceAnswerPartRepository.removeById(simpleChoiceAnswerPart.getId());
        }
        simpleChoiceAnswerRepository.removeById(id);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Question answer [id: " + id + "] " +
            "removed from test answer [id: " + testAnswerId + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
    }

}
