package com.examsystem.core.services.api.question;

import com.examsystem.core.resources.question.QuestionResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface QuestionService<T extends QuestionResource> {

    T getQuestionById(Long id);

    T updateQuestion(T questionResource);

    T saveQuestion(T questionResource);

    void deleteQuestionById(Long id);

}
