package com.examsystem.core.services.impl.user;

import com.examsystem.core.entitites.user.Admin;
import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.mappers.api.user.AdminMapper;
import com.examsystem.core.repositories.AdminRepository;
import com.examsystem.core.resources.user.AdminResource;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.scheduled.impl.ImageRemovalServiceImpl;
import com.examsystem.core.security.ldap.services.api.LdapService;
import com.examsystem.core.security.services.api.CurrentUserService;
import com.examsystem.core.services.api.user.AdminService;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class AdminServiceImpl implements AdminService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminServiceImpl.class);

    @Autowired
    private AdminMapper adminMapper;

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private LdapService ldapServices;

    @Autowired
    private CurrentUserService currentUserService;

    @Transactional(readOnly = true)
    @NotNull
    public Page<AdminResource> getAll(Pageable pageable) {
        return adminRepository.findAll(pageable).map(adminMapper::mapEntityToResource);
    }

    @Transactional(readOnly = true)
    @Nullable
    public AdminResource getByLdapLogin(@NotNull String ldapLogin) {
        Admin admin = adminRepository.findAdminByLdapLogin(ldapLogin);
        return admin == null ? null : adminMapper.mapEntityToResource(admin);
    }

    @Transactional(readOnly = true)
    @Nullable
    public AdminResource getById(@NotNull Long id) {
        Admin admin = adminRepository.findAdminById(id);
        return admin == null ? null : adminMapper.mapEntityToResource(admin);
    }

    @Transactional(readOnly = false)
    public AdminResource save(@NotNull AdminResource adminResource) {
        ldapServices.populateResourceWithLdapData(adminResource.getLdapLogin(), adminResource);
        Admin admin = adminMapper.mapResourceToEntity(adminResource);
        admin = adminRepository.save(admin);
        adminResource = adminMapper.mapEntityToResource(admin);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "New admin [id: " + adminResource.getId() + ", ldapLogin: " + adminResource.getLdapLogin() + "] " +
            "added by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
        return adminResource;
    }

    @Transactional(readOnly = false)
    public void deleteById(@NotNull Long id) {
        AdminResource adminResource = getById(id);
        adminRepository.removeAdminById(id);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Existing admin [id: " + adminResource.getId() + ", ldapLogin: " + adminResource.getLdapLogin() + "] " +
            "removed by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
    }

    @Transactional(readOnly = true)
    @Override
    public String mapLdapLogin(String ldapLogin) {
        String lowercaseLdapLogin = ldapLogin.toLowerCase();
        Map<String, String> systemLdapLoginsMap = new HashMap<>();
        List<String> systemLdapLogins = adminRepository.getAdminLdapLogins();
        for (String systemLdapLogin : systemLdapLogins) {
            systemLdapLoginsMap.put(systemLdapLogin.toLowerCase(), systemLdapLogin);
        }
        if (systemLdapLoginsMap.containsKey(lowercaseLdapLogin)) {
            return systemLdapLoginsMap.get(lowercaseLdapLogin);
        } else {
            return ldapLogin;
        }
    }

}
