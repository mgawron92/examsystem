package com.examsystem.core.services.api.testanswer;

import com.examsystem.core.resources.testanswer.QuestionSequenceResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface QuestionSequenceService {

    QuestionSequenceResource getQuestionSequenceById(Long id);

    QuestionSequenceResource saveQuestionSequence(QuestionSequenceResource questionSequenceResource);

    void deleteQuestionSequenceById(Long id);

}
