package com.examsystem.core.services.api.testanswer;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.testanswer.TestAnswerResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface TestAnswerService {

    Page<TestAnswerResource> query(Map<String, Object> params, Pageable pageable);

    List<TestAnswerResource> query(Map<String, Object> params);

    TestAnswerResource getTestAnswerById(@NotNull Long id);

    TestAnswerResource saveTestAnswer(@NotNull TestAnswerResource testAnswerResource);

    TestAnswerResource updateTestAnswer(@NotNull TestAnswerResource testAnswerResource);

    void deleteTestAnswerById(@NotNull Long id);

    QuestionType getQuestionType(Long tid, Long qid);

}
