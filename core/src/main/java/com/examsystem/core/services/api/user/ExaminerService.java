package com.examsystem.core.services.api.user;

import com.examsystem.core.resources.user.ExaminerResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface ExaminerService extends UserService<ExaminerResource> {

    String mapLdapLogin(String ldapLogin);

}
