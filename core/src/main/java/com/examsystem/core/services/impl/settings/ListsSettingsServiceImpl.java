package com.examsystem.core.services.impl.settings;

import com.examsystem.core.entitites.other.Settings;
import com.examsystem.core.enumerations.SettingsType;
import com.examsystem.core.mappers.api.settings.ListsSettingsMapper;
import com.examsystem.core.repositories.SettingsRepository;
import com.examsystem.core.resources.settings.ListsResource;
import com.examsystem.core.services.api.settings.ListsSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class ListsSettingsServiceImpl implements ListsSettingsService {

    @Autowired
    private ListsSettingsMapper listsSettingsMapper;

    @Autowired
    private SettingsRepository settingsRepository;

    @Transactional(readOnly = true)
    @NotNull
    public ListsResource getListsSettings() {
        return listsSettingsMapper.mapEntityToResource(settingsRepository.findSettingsBySettingsType(SettingsType.DEFAULTS));
    }

    @Transactional(readOnly = false)
    public ListsResource saveListsSettings(@NotNull ListsResource listsResource) {
        deleteListsSettings();
        List<Settings> settings = listsSettingsMapper.mapResourceToEntity(listsResource);
        List<Settings> savedSettings = new LinkedList<>();
        for (Settings s : settings) {
            savedSettings.add(settingsRepository.save(s));
        }
        listsResource = listsSettingsMapper.mapEntityToResource(savedSettings);
        return listsResource;
    }

    @Transactional(readOnly = false)
    public void deleteListsSettings() {
        settingsRepository.removeSettingsBySettingsType(SettingsType.DEFAULTS);
    }

}
