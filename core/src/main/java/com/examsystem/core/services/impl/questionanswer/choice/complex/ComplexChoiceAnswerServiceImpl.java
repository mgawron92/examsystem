package com.examsystem.core.services.impl.questionanswer.choice.complex;

import com.examsystem.core.entitites.questionanswer.choice.ChoiceAnswerPart;
import com.examsystem.core.entitites.questionanswer.choice.complex.ComplexChoiceAnswer;
import com.examsystem.core.entitites.questionanswer.choice.complex.ComplexChoiceAnswerPart;
import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.mappers.api.questionanswer.choice.complex.ComplexChoiceAnswerMapper;
import com.examsystem.core.repositories.ComplexChoiceAnswerPartRepository;
import com.examsystem.core.repositories.ComplexChoiceAnswerRepository;
import com.examsystem.core.resources.questionanswer.choice.complex.ComplexChoiceAnswerResource;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.security.services.api.CurrentUserService;
import com.examsystem.core.services.api.questionanswer.choice.complex.ComplexChoiceAnswerService;
import com.examsystem.core.updaters.api.questionanswer.choice.complex.ComplexChoiceAnswerUpdater;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ComplexChoiceAnswerServiceImpl implements ComplexChoiceAnswerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComplexChoiceAnswerService.class);

    @Autowired
    private ComplexChoiceAnswerMapper complexChoiceAnswerMapper;

    @Autowired
    private ComplexChoiceAnswerUpdater complexChoiceAnswerUpdater;

    @Autowired
    private ComplexChoiceAnswerRepository complexChoiceAnswerRepository;

    @Autowired
    private ComplexChoiceAnswerPartRepository complexChoiceAnswerPartRepository;

    @Autowired
    private CurrentUserService currentUserService;

    @Transactional(readOnly = true)
    @Nullable
    public ComplexChoiceAnswerResource getQuestionAnswerById(@NotNull Long id) {
        ComplexChoiceAnswer complexChoiceAnswerById = complexChoiceAnswerRepository.findComplexChoiceAnswerById(id);
        return complexChoiceAnswerById == null ? null : complexChoiceAnswerMapper.mapEntityToResource(complexChoiceAnswerById);
    }

    @Transactional(readOnly = false)
    public ComplexChoiceAnswerResource saveQuestionAnswer(@NotNull ComplexChoiceAnswerResource questionAnswerResource) {
        ComplexChoiceAnswer complexChoiceAnswer = complexChoiceAnswerMapper.mapResourceToEntity(questionAnswerResource);
        complexChoiceAnswer = complexChoiceAnswerRepository.save(complexChoiceAnswer);
        for (ComplexChoiceAnswerPart complexChoiceAnswerPart : complexChoiceAnswer.getChoiceAnswerParts()) {
            complexChoiceAnswerPart.setChoiceAnswer(complexChoiceAnswer);
            complexChoiceAnswerPartRepository.save(complexChoiceAnswerPart);
        }
        questionAnswerResource = complexChoiceAnswerMapper.mapEntityToResource(complexChoiceAnswer);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Question answer [id: " + questionAnswerResource.getId() + "] " +
            "saved to test answer [id: " + questionAnswerResource.getTestAnswerId() + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
        return questionAnswerResource;
    }

    @Transactional(readOnly = false)
    public ComplexChoiceAnswerResource updateQuestionAnswer(@NotNull ComplexChoiceAnswerResource questionAnswerResource) {
        ComplexChoiceAnswerResource complexChoiceAnswerResource = questionAnswerResource;
        ComplexChoiceAnswer complexChoiceAnswer = complexChoiceAnswerRepository.findComplexChoiceAnswerById(complexChoiceAnswerResource.getId());
        for (ComplexChoiceAnswerPart complexChoiceAnswerPart : complexChoiceAnswer.getChoiceAnswerParts()) {
            complexChoiceAnswerPartRepository.removeById(complexChoiceAnswerPart.getId());
        }
        complexChoiceAnswerUpdater.updateEntityFromResource(complexChoiceAnswer, complexChoiceAnswerResource);
        List<ComplexChoiceAnswerPart> updatedComplexChoiceAnswerParts = new LinkedList<>();
        for (ComplexChoiceAnswerPart complexChoiceAnswerPart : complexChoiceAnswer.getChoiceAnswerParts()) {
            complexChoiceAnswerPart.setId(null);
            updatedComplexChoiceAnswerParts.add(complexChoiceAnswerPartRepository.save(complexChoiceAnswerPart));
        }
        complexChoiceAnswer.setChoiceAnswerParts(updatedComplexChoiceAnswerParts);
        complexChoiceAnswer = complexChoiceAnswerRepository.save(complexChoiceAnswer);
        complexChoiceAnswerResource = complexChoiceAnswerMapper.mapEntityToResource(complexChoiceAnswer);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Question answer [id: " + complexChoiceAnswerResource.getId() + "] " +
            "updated in test answer [id: " + complexChoiceAnswerResource.getTestAnswerId() + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
        return complexChoiceAnswerResource;
    }

    @Transactional(readOnly = false)
    public void deleteQuestionAnswerById(@NotNull Long id) {
        ComplexChoiceAnswer complexChoiceAnswer = complexChoiceAnswerRepository.findComplexChoiceAnswerById(id);
        Long testAnswerId = complexChoiceAnswer.getTestAnswer().getId();
        for (ChoiceAnswerPart choiceAnswerPart : complexChoiceAnswer.getChoiceAnswerParts()) {
            complexChoiceAnswerPartRepository.removeById(choiceAnswerPart.getId());
        }
        complexChoiceAnswerRepository.removeById(id);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Question answer [id: " + id + "] " +
            "removed from test answer [id: " + testAnswerId + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
    }

}
