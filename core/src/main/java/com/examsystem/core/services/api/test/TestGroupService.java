package com.examsystem.core.services.api.test;

import com.examsystem.core.resources.test.TestGroupResource;
import com.examsystem.core.resources.user.StudentResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface TestGroupService {

    Page<TestGroupResource> query(Map<String, Object> params, Pageable pageable);

    List<TestGroupResource> query(Map<String, Object> params);

    Page<StudentResource> getStudentsByTestGroupId(Long id, Pageable pageable);

//    Page<TestResource> getTestsByTestGroupId(Long id, Boolean active, Pageable pageable);

    TestGroupResource getTestGroupById(@NotNull Long id);

    TestGroupResource saveTestGroup(@NotNull TestGroupResource testGroupResource);

    TestGroupResource updateTestGroup(@NotNull TestGroupResource testGroupResource);

    void deleteTestGroupById(@NotNull Long id);

}
