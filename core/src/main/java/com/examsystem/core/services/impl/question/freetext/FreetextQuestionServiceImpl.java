package com.examsystem.core.services.impl.question.freetext;

import com.examsystem.core.entitites.question.freetext.FreetextQuestion;
import com.examsystem.core.mappers.api.question.freetext.FreetextQuestionMapper;
import com.examsystem.core.repositories.FreetextQuestionRepository;
import com.examsystem.core.resources.question.freetext.FreetextQuestionResource;
import com.examsystem.core.services.api.other.ImageService;
import com.examsystem.core.services.api.question.freetext.FreetextQuestionService;
import com.examsystem.core.updaters.api.question.freetext.FreetextQuestionUpdater;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class FreetextQuestionServiceImpl implements FreetextQuestionService {

    @Autowired
    private FreetextQuestionMapper freetextQuestionMapper;

    @Autowired
    private FreetextQuestionUpdater freetextQuestionUpdater;

    @Autowired
    private FreetextQuestionRepository freetextQuestionRepository;

    @Autowired
    private ImageService imageService;

    @Transactional(readOnly = true)
    @NotNull
    public List<FreetextQuestionResource> getAllQuestions() {
        return freetextQuestionRepository.findAll().stream().map(freetextQuestionMapper::mapEntityToResource).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Nullable
    public FreetextQuestionResource getQuestionById(@NotNull Long id) {
        FreetextQuestion freetextQuestion = freetextQuestionRepository.findFreetextQuestionById(id);
        return freetextQuestion == null ? null : freetextQuestionMapper.mapEntityToResource(freetextQuestion);
    }

    @Transactional(readOnly = false)
    public FreetextQuestionResource saveQuestion(@NotNull FreetextQuestionResource questionResource) {
        FreetextQuestion freetextQuestion = freetextQuestionMapper.mapResourceToEntity(questionResource);
        freetextQuestion = freetextQuestionRepository.save(freetextQuestion);
        return freetextQuestionMapper.mapEntityToResource(freetextQuestion);
    }

    @Transactional(readOnly = false)
    public FreetextQuestionResource updateQuestion(@NotNull FreetextQuestionResource questionResource) {
        FreetextQuestionResource freetextQuestionResource = questionResource;
        FreetextQuestion freetextQuestion = freetextQuestionRepository.findFreetextQuestionById(freetextQuestionResource.getId());
        freetextQuestionUpdater.updateEntityFromResource(freetextQuestion, freetextQuestionResource);
        freetextQuestion = freetextQuestionRepository.save(freetextQuestion);
        return freetextQuestionMapper.mapEntityToResource(freetextQuestion);
    }

    @Transactional(readOnly = false)
    public void deleteQuestionById(@NotNull Long id) {
        FreetextQuestion freetextQuestion = freetextQuestionRepository.findFreetextQuestionById(id);
        imageService.removeIfLastQuestionRemoved(freetextQuestion);
        freetextQuestionRepository.removeById(id);
    }

}
