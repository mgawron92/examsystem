package com.examsystem.core.services.impl.question.choice.simple;

import com.examsystem.core.entitites.question.choice.simple.SimpleChoicePattern;
import com.examsystem.core.entitites.question.choice.simple.SimpleChoiceQuestion;
import com.examsystem.core.mappers.api.question.choice.simple.SimpleChoiceQuestionMapper;
import com.examsystem.core.repositories.SimpleChoicePatternRepository;
import com.examsystem.core.repositories.SimpleChoiceQuestionRepository;
import com.examsystem.core.resources.question.choice.ChoicePatternResource;
import com.examsystem.core.resources.question.choice.simple.SimpleChoiceQuestionResource;
import com.examsystem.core.services.api.other.ImageService;
import com.examsystem.core.services.api.question.choice.simple.SimpleChoiceQuestionService;
import com.examsystem.core.updaters.api.question.choice.simple.SimpleChoiceQuestionUpdater;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SimpleChoiceQuestionServiceImpl implements SimpleChoiceQuestionService {

    @Autowired
    private SimpleChoiceQuestionMapper simpleChoiceQuestionMapper;

    @Autowired
    private SimpleChoiceQuestionUpdater simpleChoiceQuestionUpdater;

    @Autowired
    private SimpleChoiceQuestionRepository simpleChoiceQuestionRepository;

    @Autowired
    private SimpleChoicePatternRepository simpleChoicePatternRepository;

    @Autowired
    private ImageService imageService;

    @Transactional(readOnly = true)
    @Nullable
    public SimpleChoiceQuestionResource getQuestionById(@NotNull Long id) {
        SimpleChoiceQuestion simpleChoiceQuestion = simpleChoiceQuestionRepository.findSimpleChoiceQuestionById(id);
        return simpleChoiceQuestion == null ? null : simpleChoiceQuestionMapper.mapEntityToResource(simpleChoiceQuestion);
    }

    @Transactional(readOnly = false)
    public SimpleChoiceQuestionResource saveQuestion(@NotNull SimpleChoiceQuestionResource questionResource) {
        SimpleChoiceQuestion simpleChoiceQuestion = simpleChoiceQuestionMapper.mapResourceToEntity(questionResource);
        simpleChoiceQuestion = simpleChoiceQuestionRepository.save(simpleChoiceQuestion);
        for (SimpleChoicePattern simpleChoicePattern : simpleChoiceQuestion.getChoicePatterns()) {
            simpleChoicePattern.setChoiceQuestion(simpleChoiceQuestion);
            simpleChoicePatternRepository.save(simpleChoicePattern);
        }
        return simpleChoiceQuestionMapper.mapEntityToResource(simpleChoiceQuestion);
    }

    @Transactional(readOnly = false)
    public SimpleChoiceQuestionResource updateQuestion(@NotNull SimpleChoiceQuestionResource questionResource) {
        SimpleChoiceQuestionResource simpleChoiceQuestionResource = questionResource;
        SimpleChoiceQuestion simpleChoiceQuestion = simpleChoiceQuestionRepository.findSimpleChoiceQuestionById(simpleChoiceQuestionResource.getId());
        for (SimpleChoicePattern simpleChoicePattern : simpleChoiceQuestion.getChoicePatterns()) {
            simpleChoicePatternRepository.removeById(simpleChoicePattern.getId());
        }
        for (ChoicePatternResource choicePatternResource : simpleChoiceQuestionResource.getChoicePatterns()) {
            choicePatternResource.setChoiceAnswerPartIds(new LinkedList<>());
        }
        simpleChoiceQuestionUpdater.updateEntityFromResource(simpleChoiceQuestion, simpleChoiceQuestionResource);
        List<SimpleChoicePattern> updatedSimpleChoicePatterns = new LinkedList<>();
        for (SimpleChoicePattern simpleChoicePattern : simpleChoiceQuestion.getChoicePatterns()) {
            simpleChoicePattern.setId(null);
            updatedSimpleChoicePatterns.add(simpleChoicePatternRepository.save(simpleChoicePattern));
        }
        simpleChoiceQuestion.setChoicePatterns(updatedSimpleChoicePatterns);
        simpleChoiceQuestion = simpleChoiceQuestionRepository.save(simpleChoiceQuestion);
        return simpleChoiceQuestionMapper.mapEntityToResource(simpleChoiceQuestion);
    }

    @Transactional(readOnly = false)
    public void deleteQuestionById(@NotNull Long id) {
        SimpleChoiceQuestion simpleChoiceQuestion = simpleChoiceQuestionRepository.findSimpleChoiceQuestionById(id);
        imageService.removeIfLastQuestionRemoved(simpleChoiceQuestion);
        simpleChoiceQuestionRepository.removeById(id);
    }

}
