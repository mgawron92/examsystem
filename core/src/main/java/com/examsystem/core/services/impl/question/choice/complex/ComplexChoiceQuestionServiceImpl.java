package com.examsystem.core.services.impl.question.choice.complex;

import com.examsystem.core.entitites.question.choice.complex.ComplexChoicePattern;
import com.examsystem.core.entitites.question.choice.complex.ComplexChoiceQuestion;
import com.examsystem.core.mappers.api.question.choice.complex.ComplexChoiceQuestionMapper;
import com.examsystem.core.repositories.ComplexChoicePatternRepository;
import com.examsystem.core.repositories.ComplexChoiceQuestionRepository;
import com.examsystem.core.resources.question.choice.ChoicePatternResource;
import com.examsystem.core.resources.question.choice.complex.ComplexChoiceQuestionResource;
import com.examsystem.core.services.api.other.ImageService;
import com.examsystem.core.services.api.question.choice.complex.ComplexChoiceQuestionService;
import com.examsystem.core.updaters.api.question.choice.complex.ComplexChoiceQuestionUpdater;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ComplexChoiceQuestionServiceImpl implements ComplexChoiceQuestionService {

    @Autowired
    private ComplexChoiceQuestionMapper complexChoiceQuestionMapper;

    @Autowired
    private ComplexChoiceQuestionUpdater complexChoiceQuestionUpdater;

    @Autowired
    private ComplexChoiceQuestionRepository complexChoiceQuestionRepository;

    @Autowired
    private ComplexChoicePatternRepository complexChoicePatternRepository;

    @Autowired
    private ImageService imageService;

    @Transactional(readOnly = true)
    @Nullable
    public ComplexChoiceQuestionResource getQuestionById(@NotNull Long id) {
        ComplexChoiceQuestion complexChoiceQuestion = complexChoiceQuestionRepository.findComplexChoiceQuestionById(id);
        return complexChoiceQuestion == null ? null : complexChoiceQuestionMapper.mapEntityToResource(complexChoiceQuestion);
    }

    @Transactional(readOnly = false)
    public ComplexChoiceQuestionResource saveQuestion(@NotNull ComplexChoiceQuestionResource questionResource) {
        ComplexChoiceQuestion complexChoiceQuestion = complexChoiceQuestionMapper.mapResourceToEntity(questionResource);
        complexChoiceQuestion = complexChoiceQuestionRepository.save(complexChoiceQuestion);
        for (ComplexChoicePattern complexChoicePattern : complexChoiceQuestion.getChoicePatterns()) {
            complexChoicePattern.setChoiceQuestion(complexChoiceQuestion);
            complexChoicePatternRepository.save(complexChoicePattern);
        }
        return complexChoiceQuestionMapper.mapEntityToResource(complexChoiceQuestion);
    }

    @Transactional(readOnly = false)
    public ComplexChoiceQuestionResource updateQuestion(@NotNull ComplexChoiceQuestionResource questionResource) {
        ComplexChoiceQuestionResource complexChoiceQuestionResource = questionResource;
        ComplexChoiceQuestion complexChoiceQuestion = complexChoiceQuestionRepository.findComplexChoiceQuestionById(complexChoiceQuestionResource.getId());
        for (ComplexChoicePattern complexChoicePattern : complexChoiceQuestion.getChoicePatterns()) {
            complexChoicePatternRepository.removeById(complexChoicePattern.getId());
        }
        for (ChoicePatternResource choicePatternResource : complexChoiceQuestionResource.getChoicePatterns()) {
            choicePatternResource.setChoiceAnswerPartIds(new LinkedList<>());
        }
        complexChoiceQuestionUpdater.updateEntityFromResource(complexChoiceQuestion, complexChoiceQuestionResource);
        List<ComplexChoicePattern> updatedComplexChoicePatterns = new LinkedList<>();
        for (ComplexChoicePattern complexChoicePattern : complexChoiceQuestion.getChoicePatterns()) {
            complexChoicePattern.setId(null);
            updatedComplexChoicePatterns.add(complexChoicePatternRepository.save(complexChoicePattern));
        }
        complexChoiceQuestion.setChoicePatterns(updatedComplexChoicePatterns);
        complexChoiceQuestion = complexChoiceQuestionRepository.save(complexChoiceQuestion);
        return complexChoiceQuestionMapper.mapEntityToResource(complexChoiceQuestion);
    }

    @Transactional(readOnly = false)
    public void deleteQuestionById(@NotNull Long id) {
        ComplexChoiceQuestion complexChoiceQuestion = complexChoiceQuestionRepository.findComplexChoiceQuestionById(id);
        imageService.removeIfLastQuestionRemoved(complexChoiceQuestion);
        complexChoiceQuestionRepository.removeById(id);
    }

}
