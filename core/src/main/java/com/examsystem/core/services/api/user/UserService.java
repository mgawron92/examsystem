package com.examsystem.core.services.api.user;

import com.examsystem.core.resources.user.PersonResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Mateusz Gawron on 2016-07-04.
 */
public interface UserService<T extends PersonResource> {

    Page<T> getAll(Pageable pageable);

    T getByLdapLogin(String ldapLogin);

    T getById(Long id);

    T save(T resource);

    void deleteById(Long id);

}
