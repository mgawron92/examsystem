package com.examsystem.core.services.api.intermediary;

import com.examsystem.core.resources.intermediary.StudentTestGroupResource;

import java.util.List;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface StudentTestGroupService {

    List<StudentTestGroupResource> query(Map<String, Object> params);

    StudentTestGroupResource getStudentTestGroupById(Long id);

    StudentTestGroupResource saveStudentTestGroup(StudentTestGroupResource studentTestGroupResource);

    void deleteStudentTestGroupById(Long id);

}
