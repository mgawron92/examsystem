package com.examsystem.core.services.api.question.choice.simple;

import com.examsystem.core.resources.question.choice.simple.SimpleChoiceQuestionResource;
import com.examsystem.core.services.api.question.QuestionService;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface SimpleChoiceQuestionService extends QuestionService<SimpleChoiceQuestionResource> {

}
