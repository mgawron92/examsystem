package com.examsystem.core.services.api.intermediary;

import com.examsystem.core.resources.intermediary.TestTestGroupResource;

import java.util.List;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface TestTestGroupService {

    List<TestTestGroupResource> query(Map<String, Object> params);

    TestTestGroupResource getTestTestGroupById(Long id);

    TestTestGroupResource saveTestTestGroup(TestTestGroupResource testTestGroupResource);

    void deleteTestTestGroupById(Long id);

}
