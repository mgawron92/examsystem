package com.examsystem.core.services.api.user;

import com.examsystem.core.resources.user.StudentResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface StudentService extends UserService<StudentResource> {

    Page<StudentResource> getByTestId(Long testId, Pageable pageable);

    String mapLdapLogin(String ldapLogin);

}
