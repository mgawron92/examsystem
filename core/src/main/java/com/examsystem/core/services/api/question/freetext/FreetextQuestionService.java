package com.examsystem.core.services.api.question.freetext;

import com.examsystem.core.resources.question.freetext.FreetextQuestionResource;
import com.examsystem.core.services.api.question.QuestionService;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface FreetextQuestionService extends QuestionService<FreetextQuestionResource> {

}
