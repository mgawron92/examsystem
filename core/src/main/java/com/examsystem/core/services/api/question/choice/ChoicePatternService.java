package com.examsystem.core.services.api.question.choice;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface ChoicePatternService {

    void deleteChoicePatternById(Long id);

//    ChoicePattern resourceToEntity(ChoicePatternResource choicePatternResource);

//    ChoicePattern updateEntity(ChoicePattern choicePattern, ChoicePatternResource choicePatternResource);

//    ChoicePatternResource entityToResource(ChoicePattern choicePattern);
    
}
