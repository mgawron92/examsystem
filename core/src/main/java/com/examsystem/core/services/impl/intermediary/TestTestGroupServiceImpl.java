package com.examsystem.core.services.impl.intermediary;

import com.examsystem.core.entitites.intermediary.TestTestGroup;
import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.mappers.api.intermediary.TestTestGroupMapper;
import com.examsystem.core.repositories.TestTestGroupRepository;
import com.examsystem.core.resources.intermediary.TestTestGroupResource;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.security.services.api.CurrentUserService;
import com.examsystem.core.services.api.intermediary.TestTestGroupService;
import com.examsystem.core.specifications.TestTestGroupSpecification;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class TestTestGroupServiceImpl implements TestTestGroupService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestTestGroupServiceImpl.class);

    @Autowired
    private TestTestGroupMapper testTestGroupMapper;

    @Autowired
    private TestTestGroupRepository testTestGroupRepository;

    @Autowired
    private CurrentUserService currentUserService;

    @Transactional(readOnly = true)
    @NotNull
    public List<TestTestGroupResource> query(Map<String, Object> params) {
        Specification<TestTestGroup> specification = TestTestGroupSpecification.getSpecification(params);
        return testTestGroupRepository.findAll(specification).stream().map(testTestGroupMapper::mapEntityToResource).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Nullable
    public TestTestGroupResource getTestTestGroupById(@NotNull Long id) {
        TestTestGroup testTestGroup = testTestGroupRepository.findTestTestGroupById(id);
        return testTestGroup == null ? null : testTestGroupMapper.mapEntityToResource(testTestGroup);
    }

    @Transactional(readOnly = false)
    public TestTestGroupResource saveTestTestGroup(@NotNull TestTestGroupResource testTestGroupResource) {
        TestTestGroup testTestGroup = testTestGroupMapper.mapResourceToEntity(testTestGroupResource);
        testTestGroup = testTestGroupRepository.save(testTestGroup);
        testTestGroupResource = testTestGroupMapper.mapEntityToResource(testTestGroup);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Test [id: " + testTestGroupResource.getTestId() + "] " +
            "added to test group [id: " + testTestGroupResource.getTestGroupId() + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
        return testTestGroupResource;
    }

    @Transactional(readOnly = false)
    public void deleteTestTestGroupById(@NotNull Long id) {
        TestTestGroup testTestGroup = testTestGroupRepository.findTestTestGroupById(id);
        Long testId = testTestGroup.getTest().getId();
        Long testGroupId = testTestGroup.getTestGroup().getId();
        testTestGroupRepository.removeById(id);
        PersonResource currentUser = currentUserService.getCurrentUser();
        LOGGER.info(
            "Test [id: " + testId + "] " +
            "removed from test group [id: " + testGroupId + "] " +
            "by " + UserRole.mapToString(currentUser.getRole()) + " [id: " + currentUser.getId() + ", ldapLogin: " + currentUser.getLdapLogin() + "]"
        );
    }

}
