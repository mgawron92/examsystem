package com.examsystem.core.services.api.user;

import com.examsystem.core.resources.user.AdminResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface AdminService extends UserService<AdminResource> {

    String mapLdapLogin(String ldapLogin);

}
