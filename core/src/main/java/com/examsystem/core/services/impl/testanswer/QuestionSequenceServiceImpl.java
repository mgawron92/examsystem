package com.examsystem.core.services.impl.testanswer;

import com.examsystem.core.entitites.testanswer.QuestionSequence;
import com.examsystem.core.mappers.api.testanswer.QuestionSequenceMapper;
import com.examsystem.core.repositories.QuestionSequenceRepository;
import com.examsystem.core.resources.testanswer.QuestionSequenceResource;
import com.examsystem.core.services.api.testanswer.QuestionSequenceService;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class QuestionSequenceServiceImpl implements QuestionSequenceService {

    @Autowired
    private QuestionSequenceMapper questionSequenceMapper;

    @Autowired
    private QuestionSequenceRepository questionSequenceRepository;

    @Transactional(readOnly = true)
    @Nullable
    public QuestionSequenceResource getQuestionSequenceById(@NotNull Long id) {
        QuestionSequence questionSequence = questionSequenceRepository.findQuestionSequenceById(id);
        return questionSequence == null ? null : questionSequenceMapper.mapEntityToResource(questionSequence);
    }

    @Transactional(readOnly = false)
    public QuestionSequenceResource saveQuestionSequence(@NotNull QuestionSequenceResource questionSequenceResource) {
        QuestionSequence questionSequence = questionSequenceMapper.mapResourceToEntity(questionSequenceResource);
        questionSequence = questionSequenceRepository.save(questionSequence);
        questionSequenceResource = questionSequenceMapper.mapEntityToResource(questionSequence);
        return questionSequenceResource;
    }

    @Transactional(readOnly = false)
    public void deleteQuestionSequenceById(@NotNull Long id) {
        questionSequenceRepository.removeById(id);
    }

}
