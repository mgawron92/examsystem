package com.examsystem.core.services.api.question.join;

import com.examsystem.core.resources.question.join.JoinQuestionResource;
import com.examsystem.core.services.api.question.QuestionService;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface JoinQuestionService extends QuestionService<JoinQuestionResource> {

}
