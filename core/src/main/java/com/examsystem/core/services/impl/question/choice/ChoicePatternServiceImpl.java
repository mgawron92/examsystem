package com.examsystem.core.services.impl.question.choice;

import com.examsystem.core.repositories.ComplexChoicePatternRepository;
import com.examsystem.core.services.api.question.choice.ChoicePatternService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class ChoicePatternServiceImpl implements ChoicePatternService {

    @Autowired
    private ComplexChoicePatternRepository choicePatternRepository;

    @Transactional(readOnly = false)
    public void deleteChoicePatternById(@NotNull Long id) {
        choicePatternRepository.removeById(id);
    }

}
