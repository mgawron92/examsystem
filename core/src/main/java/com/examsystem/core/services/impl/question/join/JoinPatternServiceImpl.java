package com.examsystem.core.services.impl.question.join;

import com.examsystem.core.repositories.JoinPatternRepository;
import com.examsystem.core.services.api.question.join.JoinPatternService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@Service
public class JoinPatternServiceImpl implements JoinPatternService {

    @Autowired
    private JoinPatternRepository joinPatternRepository;

    @Transactional(readOnly = false)
    public void deleteJoinPatternById(@NotNull Long id) {
        joinPatternRepository.removeById(id);
    }

}
