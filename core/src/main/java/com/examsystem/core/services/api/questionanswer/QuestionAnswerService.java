package com.examsystem.core.services.api.questionanswer;

import com.examsystem.core.resources.questionanswer.QuestionAnswerResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public interface QuestionAnswerService<T extends QuestionAnswerResource> {

    T getQuestionAnswerById(Long id);

    T updateQuestionAnswer(T questionAnswerResource);

    T saveQuestionAnswer(T questionAnswerResource);

    void deleteQuestionAnswerById(Long id);

}
