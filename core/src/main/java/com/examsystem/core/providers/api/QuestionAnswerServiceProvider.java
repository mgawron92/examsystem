package com.examsystem.core.providers.api;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.questionanswer.QuestionAnswerResource;
import com.examsystem.core.services.api.questionanswer.QuestionAnswerService;

/**
 * Created by Mateusz Gawron on 2016-07-03.
 */
public interface QuestionAnswerServiceProvider {

    QuestionAnswerService<? extends QuestionAnswerResource> getQuestionAnswerService(QuestionType questionType);

}
