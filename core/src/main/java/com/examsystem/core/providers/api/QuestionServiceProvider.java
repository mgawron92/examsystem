package com.examsystem.core.providers.api;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.question.QuestionResource;
import com.examsystem.core.services.api.question.QuestionService;

/**
 * Created by Mateusz Gawron on 2016-07-03.
 */
public interface QuestionServiceProvider {

    QuestionService<? extends QuestionResource> getQuestionService(QuestionType questionType);

}
