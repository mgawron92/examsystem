package com.examsystem.core.providers.impl;

import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.providers.api.UserServiceProvider;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.services.api.user.AdminService;
import com.examsystem.core.services.api.user.ExaminerService;
import com.examsystem.core.services.api.user.StudentService;
import com.examsystem.core.services.api.user.UserService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-07-03.
 */
@Component
public class UserServiceProviderImpl implements UserServiceProvider, InitializingBean {

    @Autowired
    private AdminService adminService;

    @Autowired
    private ExaminerService examinerService;

    @Autowired
    private StudentService studentService;

    private Map<UserRole, UserService<? extends PersonResource>> userServiceMap = new HashMap<>();

    @Override
    public void afterPropertiesSet() throws Exception {
        userServiceMap.put(UserRole.ADMIN, adminService);
        userServiceMap.put(UserRole.EXAMINER, examinerService);
        userServiceMap.put(UserRole.STUDENT, studentService);
    }

    @Override
    public UserService<? extends PersonResource> getUserService(UserRole userRole) {
        return userServiceMap.get(userRole);
    }

}
