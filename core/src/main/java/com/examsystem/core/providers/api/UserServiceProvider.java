package com.examsystem.core.providers.api;

import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.services.api.user.UserService;

/**
 * Created by Mateusz Gawron on 2016-07-03.
 */
public interface UserServiceProvider {

    UserService<? extends PersonResource> getUserService(UserRole userRole);

}
