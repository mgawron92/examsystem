package com.examsystem.core.providers.impl;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.providers.api.QuestionServiceProvider;
import com.examsystem.core.resources.question.QuestionResource;
import com.examsystem.core.services.api.question.QuestionService;
import com.examsystem.core.services.api.question.choice.complex.ComplexChoiceQuestionService;
import com.examsystem.core.services.api.question.choice.simple.SimpleChoiceQuestionService;
import com.examsystem.core.services.api.question.freetext.FreetextQuestionService;
import com.examsystem.core.services.api.question.join.JoinQuestionService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-07-03.
 */
@Component
public class QuestionServiceProviderImpl implements QuestionServiceProvider, InitializingBean {

    @Autowired
    private ComplexChoiceQuestionService complexChoiceQuestionService;

    @Autowired
    private SimpleChoiceQuestionService simpleChoiceQuestionService;

    @Autowired
    private FreetextQuestionService freetextQuestionService;

    @Autowired
    private JoinQuestionService joinQuestionService;

    private Map<QuestionType, QuestionService<? extends QuestionResource>> questionServiceMap = new HashMap<>();

    @Override
    public void afterPropertiesSet() throws Exception {
        questionServiceMap.put(QuestionType.SINGLE_SIMPLE_CHOICE, simpleChoiceQuestionService);
        questionServiceMap.put(QuestionType.MULTIPLE_SIMPLE_CHOICE, simpleChoiceQuestionService);
        questionServiceMap.put(QuestionType.SIMPLE_COMPLEX_CHOICE, complexChoiceQuestionService);
        questionServiceMap.put(QuestionType.MULTIPLE_COMPLEX_CHOICE, complexChoiceQuestionService);
        questionServiceMap.put(QuestionType.JOIN, joinQuestionService);
        questionServiceMap.put(QuestionType.FREETEXT, freetextQuestionService);
    }

    @Override
    public QuestionService<? extends QuestionResource> getQuestionService(QuestionType questionType) {
        return questionServiceMap.get(questionType);
    }

}
