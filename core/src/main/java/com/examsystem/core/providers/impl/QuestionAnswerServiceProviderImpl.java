package com.examsystem.core.providers.impl;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.providers.api.QuestionAnswerServiceProvider;
import com.examsystem.core.resources.questionanswer.QuestionAnswerResource;
import com.examsystem.core.services.api.questionanswer.QuestionAnswerService;
import com.examsystem.core.services.api.questionanswer.choice.complex.ComplexChoiceAnswerService;
import com.examsystem.core.services.api.questionanswer.choice.simple.SimpleChoiceAnswerService;
import com.examsystem.core.services.api.questionanswer.freetext.FreetextAnswerService;
import com.examsystem.core.services.api.questionanswer.join.JoinAnswerService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-07-03.
 */
@Component
public class QuestionAnswerServiceProviderImpl implements QuestionAnswerServiceProvider, InitializingBean {

    @Autowired
    private ComplexChoiceAnswerService complexChoiceAnswerService;

    @Autowired
    private SimpleChoiceAnswerService simpleChoiceAnswerService;

    @Autowired
    private FreetextAnswerService freetextAnswerService;

    @Autowired
    private JoinAnswerService joinAnswerService;

    private Map<QuestionType, QuestionAnswerService<? extends QuestionAnswerResource>> questionAnswerServiceMap = new HashMap<>();

    @Override
    public void afterPropertiesSet() throws Exception {
        questionAnswerServiceMap.put(QuestionType.SINGLE_SIMPLE_CHOICE, simpleChoiceAnswerService);
        questionAnswerServiceMap.put(QuestionType.MULTIPLE_SIMPLE_CHOICE, simpleChoiceAnswerService);
        questionAnswerServiceMap.put(QuestionType.SIMPLE_COMPLEX_CHOICE, complexChoiceAnswerService);
        questionAnswerServiceMap.put(QuestionType.MULTIPLE_COMPLEX_CHOICE, complexChoiceAnswerService);
        questionAnswerServiceMap.put(QuestionType.JOIN, joinAnswerService);
        questionAnswerServiceMap.put(QuestionType.FREETEXT, freetextAnswerService);
    }

    @Override
    public QuestionAnswerService<? extends QuestionAnswerResource> getQuestionAnswerService(QuestionType questionType) {
        return questionAnswerServiceMap.get(questionType);
    }

}
