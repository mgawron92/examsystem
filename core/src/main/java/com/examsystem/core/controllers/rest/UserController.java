package com.examsystem.core.controllers.rest;

import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.resources.user.TypeaheadResource;
import com.examsystem.core.security.ldap.services.api.LdapService;
import com.examsystem.core.security.services.api.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-03.
 */
@RestController
@RequestMapping(value = "/rest/users")
public class UserController {

    @Autowired
    private LdapService ldapServices;

    @Autowired
    private CurrentUserService currentUserService;

    @PreAuthorize(value = "hasAnyAuthority({'ROLE_EXAMINER', 'ROLE_ADMIN'})")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<TypeaheadResource> query(
            @RequestParam(required = true) Boolean available
    ) {
        if (Boolean.TRUE.equals(available)) {
            return ldapServices.getAvailableLdapUsers();
        } else {
            return new LinkedList<>();
        }
    }

    @PreAuthorize(value = "permitAll()")
    @RequestMapping(value = "/me", method = RequestMethod.GET)
    public PersonResource get() {
        return currentUserService.getCurrentUser();
    }

}
