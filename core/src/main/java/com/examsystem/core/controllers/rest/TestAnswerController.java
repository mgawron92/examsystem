package com.examsystem.core.controllers.rest;

import com.examsystem.core.resources.testanswer.TestAnswerResource;
import com.examsystem.core.services.api.testanswer.TestAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@RestController
@RequestMapping(value = "/rest/testAnswers")
public class TestAnswerController {

    @Autowired
    private TestAnswerService testAnswerService;

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and (#studentId != null and @studentPermissionsService.isCurrentUser(#studentId))) or (hasAuthority('ROLE_EXAMINER') and ((#testGroupId != null and @examinerPermissionsService.isTestGroupOwner(#testGroupId)) or (#testId != null and @examinerPermissionsService.hasAccessToTest(#testId)))) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<TestAnswerResource> query(
            @RequestParam(required = false) Long studentId,
            @RequestParam(required = false) Long testGroupId,
            @RequestParam(required = false) Long testId,
            @RequestParam(required = false) Boolean checked,
            @RequestParam(required = true, defaultValue = "true") Boolean finishedOnly,
            @RequestParam(required = true, defaultValue = "false") Boolean notFinished,
            @PageableDefault(value = 10) Pageable pageable
    ) {
        Map<String, Object> params = new HashMap<>();
        params.put("studentId", studentId);
        params.put("testGroupId", testGroupId);
        params.put("testId", testId);
        params.put("checked", checked);
        params.put("finishedOnly", finishedOnly);
        params.put("notFinished", notFinished);
        return testAnswerService.query(params, pageable);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.hasAccessToTestAnswer(#id)) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTestAnswer(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public TestAnswerResource get(@PathVariable Long id) {
        return testAnswerService.getTestAnswerById(id);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.hasAccessToTest(#testAnswerResource.testId) and @studentPermissionsService.isCurrentUser(#testAnswerResource.studentId) and @studentPermissionsService.testAnswerNotStartedYet(#testAnswerResource)) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTest(#testAnswerResource.testId)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public TestAnswerResource save(@Valid @RequestBody TestAnswerResource testAnswerResource, Errors errors) {
        return testAnswerService.saveTestAnswer(testAnswerResource);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.hasAccessToTest(#testAnswerResource.testId) and @studentPermissionsService.isCurrentUser(#testAnswerResource.studentId) and @studentPermissionsService.testAnswerNotFinishedYet(#testAnswerResource) and @studentPermissionsService.testAnswerTimeoutNotExceeded(#testAnswerResource)) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTest(#testAnswerResource.testId)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public TestAnswerResource update(@PathVariable Long id, @Valid @RequestBody TestAnswerResource testAnswerResource, Errors errors) {
        TestAnswerResource responseResource = testAnswerService.getTestAnswerById(id);
        if (responseResource != null) {
            testAnswerResource.setId(responseResource.getId());
            responseResource = testAnswerService.updateTestAnswer(testAnswerResource);
        }
        return responseResource;
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTest(#testAnswerResource.testId)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public TestAnswerResource delete(@PathVariable Long id) {
        TestAnswerResource testAnswerResource = testAnswerService.getTestAnswerById(id);
        if (testAnswerResource != null) {
            testAnswerService.deleteTestAnswerById(testAnswerResource.getId());
        }
        return testAnswerResource;
    }

}
