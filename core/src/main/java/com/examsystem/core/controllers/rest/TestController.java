package com.examsystem.core.controllers.rest;

import com.examsystem.core.resources.user.StudentResource;
import com.examsystem.core.resources.testanswer.TestAnswerResource;
import com.examsystem.core.resources.test.TestGroupResource;
import com.examsystem.core.resources.test.TestResource;
import com.examsystem.core.services.api.testanswer.TestAnswerService;
import com.examsystem.core.services.api.test.TestGroupService;
import com.examsystem.core.services.api.test.TestService;
import com.examsystem.core.services.api.user.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@RestController
@RequestMapping(value = "/rest/tests")
public class TestController {

    @Autowired
    private TestService testService;

    @Autowired
    private TestAnswerService testAnswerService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private TestGroupService testGroupService;

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and (#studentId != null and @studentPermissionsService.isCurrentUser(#studentId))) or (hasAuthority('ROLE_EXAMINER') and (#examinerId != null and @examinerPermissionsService.isCurrentUser(#examinerId))) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<TestResource> query(
            @RequestParam(required = false) Long examinerId,
            @RequestParam(required = false) Long studentId,
            @RequestParam(required = false) Boolean checked,
            @RequestParam(required = false) Boolean activeOnly,
            @RequestParam(required = false) Boolean answered,
            @RequestParam(required = false) Boolean notStarted,
            @RequestParam(required = true, defaultValue = "true") Boolean latestFirst,
            @RequestParam(required = true, defaultValue = "false") Boolean notFinished,
            @PageableDefault(value = 10) Pageable pageable
    ) {
        Map<String, Object> params = new HashMap<>();
        params.put("examinerId", examinerId);
        params.put("studentId", studentId);
        params.put("checked", checked);
        params.put("activeOnly", activeOnly);
        params.put("answered", answered);
        params.put("notStarted", notStarted);
        params.put("latestFirst", latestFirst);
        params.put("notFinished", notFinished);
        return testService.query(params, pageable);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.hasAccessToTest(#id)) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTest(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public TestResource get(@PathVariable Long id) {
        return testService.getTestById(id);
    }

    @PreAuthorize(value = "hasAnyAuthority({'ROLE_EXAMINER', 'ROLE_ADMIN'})")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public TestResource save(@Valid @RequestBody TestResource testResource, Errors errors) {
        return testService.saveTest(testResource);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTest(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public TestResource update(@PathVariable Long id, @Valid @RequestBody TestResource testResource, Errors errors) {
        TestResource responseResource = testService.getTestById(id);
        if (responseResource != null) {
            testResource.setId(responseResource.getId());
            responseResource = testService.updateTest(testResource);
        }
        return responseResource;
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTest(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public TestResource delete(@PathVariable Long id) {
        TestResource testResource = testService.getTestById(id);
        if (testResource != null) {
            testService.deleteTestById(testResource.getId());
        }
        return testResource;
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.hasAccessToTest(#id) and (#studentId != null and @studentPermissionsService.isCurrentUser(#studentId))) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTest(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}/testAnswers", method = RequestMethod.GET)
    public List<TestAnswerResource> getTestAnswers(
            @PathVariable Long id,
            @RequestParam(required = false) Long studentId,
            @RequestParam(required = false) Boolean checked
    ) {
        Map<String, Object> params = new HashMap<>();
        params.put("testId", id);
        params.put("studentId", studentId);
        params.put("checked", checked);
        return testAnswerService.query(params);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTest(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}/students", method = RequestMethod.GET)
    public Page<StudentResource> getStudents(
            @PathVariable Long id,
            @PageableDefault(value = 10) Pageable pageable
    ) {
        return studentService.getByTestId(id, pageable);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.hasAccessToTest(#id) and (#studentId != null and @studentPermissionsService.isCurrentUser(#studentId))) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTest(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}/testGroups", method = RequestMethod.GET)
    public Page<TestGroupResource> getTestGroups(
            @PathVariable Long id,
            @RequestParam(required = false) Long studentId,
            @PageableDefault(value = 10) Pageable pageable
    ) {
        Map<String, Object> params = new HashMap<>();
        params.put("testId", id);
        params.put("studentId", studentId);
        return testGroupService.query(params, pageable);
    }

}
