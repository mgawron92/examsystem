package com.examsystem.core.controllers.rest;

import com.examsystem.core.entitites.other.Image;
import com.examsystem.core.repositories.ExaminerRepository;
import com.examsystem.core.services.api.other.ImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Calendar;

/**
 * Created by Mateusz Gawron on 2016-06-26.
 */
@RestController
@RequestMapping(value = "/rest/images")
public class ImageController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageController.class);

    @Autowired
    private ImageService imageService;

    @Autowired
    private ExaminerRepository examinerRepository;

    @PreAuthorize(value = "hasAnyAuthority({'ROLE_EXAMINER', 'ROLE_ADMIN'})")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public Long save(@RequestParam("file") MultipartFile image, @RequestParam Long examinerId) throws IOException {

        String filename = image.getOriginalFilename();
        LOGGER.info("Original filename of uploaded image: " + filename + ", size = " + image.getSize());

        Image imageEntity = new Image();
        imageEntity.setImage(image.getBytes());
        imageEntity.setAddedOn(Calendar.getInstance().getTime());
        imageEntity.setExaminer(examinerRepository.findExaminerById(examinerId));

        return imageService.save(imageEntity);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.hasAccessToImage(#id)) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.isImageOwner(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] get(@PathVariable Long id) {
        return imageService.getById(id).getImage();
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.isImageOwner(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        imageService.deleteById(id);
    }

}
