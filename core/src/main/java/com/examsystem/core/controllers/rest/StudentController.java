package com.examsystem.core.controllers.rest;

import com.examsystem.core.resources.user.StudentResource;
import com.examsystem.core.services.api.user.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@RestController
@RequestMapping(value = "/rest/students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PreAuthorize(value = "hasAnyAuthority({'ROLE_EXAMINER', 'ROLE_ADMIN'})")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<StudentResource> query(
            @PageableDefault(size = 10, sort = "surname", direction = Sort.Direction.ASC) Pageable pageable
    ) {
        return studentService.getAll(pageable);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.isCurrentUser(#id)) or hasAuthority('ROLE_EXAMINER') or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public StudentResource get(@PathVariable Long id) {
        return studentService.getById(id);
    }

    @PreAuthorize(value = "hasAuthority('ROLE_EXAMINER') or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public StudentResource save(@Valid @RequestBody StudentResource studentResource, Errors errors) {
        return studentService.save(studentResource);
    }

    @PreAuthorize(value = "hasAuthority('ROLE_EXAMINER') or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public StudentResource update(@PathVariable Long id, @Valid @RequestBody StudentResource studentResource, Errors errors) {
        StudentResource responseResource = studentService.getById(id);
        if (responseResource != null) {
            studentResource.setId(responseResource.getId());
            responseResource = studentService.save(studentResource);
        }
        return responseResource;
    }

    @PreAuthorize(value = "hasAuthority('ROLE_EXAMINER') or hasAuthority('ROLE_ADMIN')")
    public StudentResource delete(@PathVariable Long id) {
        StudentResource studentResource = studentService.getById(id);
        if (studentResource != null) {
            studentService.deleteById(studentResource.getId());
        }
        return studentResource;
    }

}
