package com.examsystem.core.controllers.rest;

import com.examsystem.core.resources.user.AdminResource;
import com.examsystem.core.services.api.user.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by Mateusz Gawron on 2016-04-03.
 */
@RestController
@RequestMapping(value = "/rest/admins")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<AdminResource> query(
            @PageableDefault(size = 10, sort = "surname", direction = Sort.Direction.ASC) Pageable pageable
    ) {
        return adminService.getAll(pageable);
    }

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public AdminResource get(@PathVariable Long id) {
        return adminService.getById(id);
    }

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public AdminResource save(@Valid @RequestBody AdminResource adminResource, Errors errors) {
        return adminService.save(adminResource);
    }

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public AdminResource update(@PathVariable Long id, @Valid @RequestBody AdminResource adminResource, Errors errors) {
        AdminResource responseResource = adminService.getById(id);
        if (responseResource != null) {
            adminResource.setId(responseResource.getId());
            responseResource = adminService.save(adminResource);
        }
        return responseResource;
    }

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public AdminResource delete(@PathVariable Long id) {
        AdminResource adminResource = adminService.getById(id);
        if (adminResource != null) {
            adminService.deleteById(adminResource.getId());
        }
        return adminResource;
    }

}
