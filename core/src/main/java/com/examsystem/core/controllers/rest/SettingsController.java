package com.examsystem.core.controllers.rest;

import com.examsystem.core.resources.settings.BackupsResource;
import com.examsystem.core.resources.settings.DefaultsResource;
import com.examsystem.core.resources.settings.ListsResource;
import com.examsystem.core.services.api.settings.BackupsSettingsService;
import com.examsystem.core.services.api.settings.DefaultsSettingsService;
import com.examsystem.core.services.api.settings.ListsSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@RestController
@RequestMapping(value = "/rest/settings")
public class SettingsController {

    @Autowired
    private BackupsSettingsService backupsSettingsService;

    @Autowired
    private DefaultsSettingsService defaultsSettingsService;

    @Autowired
    private ListsSettingsService listsSettingsService;

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/backups", method = RequestMethod.GET)
    public BackupsResource getBackups() {
        return backupsSettingsService.getBackupsSettings();
    }

    @RequestMapping(value = "/defaults", method = RequestMethod.GET)
    public DefaultsResource getDefaults() {
        return defaultsSettingsService.getDefaultsSettings();
    }

    @RequestMapping(value = "/lists", method = RequestMethod.GET)
    public ListsResource getLists() {
        return listsSettingsService.getListsSettings();
    }

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/backups", method = RequestMethod.POST)
    public BackupsResource saveBackups(@Valid @RequestBody BackupsResource backupsResource, Errors errors) {
        return backupsSettingsService.saveBackupsSettings(backupsResource);
    }

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/defaults", method = RequestMethod.POST)
    public DefaultsResource saveDefaults(@Valid @RequestBody DefaultsResource defaultsResource, Errors errors) {
        return defaultsSettingsService.saveDefaultsSettings(defaultsResource);
    }

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/lists", method = RequestMethod.POST)
    public ListsResource saveLists(@Valid @RequestBody ListsResource listsResource, Errors errors) {
        return listsSettingsService.saveListsSettings(listsResource);
    }

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/backups", method = RequestMethod.DELETE)
    public BackupsResource deleteBackups(@RequestParam("changePassword") Boolean changePassword) {
        BackupsResource backupsResource = backupsSettingsService.getBackupsSettings();
        if (backupsResource != null) {
            backupsSettingsService.deleteBackupsSettings(changePassword);
        }
        return backupsResource;
    }

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/defaults", method = RequestMethod.DELETE)
    public DefaultsResource deleteDefaults() {
        DefaultsResource defaultsResource = defaultsSettingsService.getDefaultsSettings();
        if (defaultsResource != null) {
            defaultsSettingsService.deleteDefaultsSettings();
        }
        return defaultsResource;
    }

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/lists", method = RequestMethod.DELETE)
    public ListsResource deleteLists() {
        ListsResource listsResource = listsSettingsService.getListsSettings();
        if (listsResource != null) {
            listsSettingsService.deleteListsSettings();
        }
        return listsResource;
    }

}
