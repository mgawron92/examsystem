package com.examsystem.core.controllers.rest;

import com.examsystem.core.resources.testanswer.QuestionSequenceResource;
import com.examsystem.core.services.api.testanswer.QuestionSequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@RestController
@RequestMapping(value = "/rest/questionSequences")
public class QuestionSequenceController {

    @Autowired
    private QuestionSequenceService questionSequenceService;

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.hasAccessToQuestionSequence(#id)) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToQuestionSequence(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public QuestionSequenceResource get(@PathVariable Long id) {
        return questionSequenceService.getQuestionSequenceById(id);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToQuestionSequence(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public QuestionSequenceResource save(@Valid @RequestBody QuestionSequenceResource questionSequenceResource, Errors errors) {
        return questionSequenceService.saveQuestionSequence(questionSequenceResource);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToQuestionSequence(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public QuestionSequenceResource delete(@PathVariable Long id) {
        QuestionSequenceResource questionSequenceResource = questionSequenceService.getQuestionSequenceById(id);
        if (questionSequenceResource != null) {
            questionSequenceService.deleteQuestionSequenceById(questionSequenceResource.getId());
        }
        return questionSequenceResource;
    }

}
