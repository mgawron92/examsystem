package com.examsystem.core.controllers.rest;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.providers.api.QuestionAnswerServiceProvider;
import com.examsystem.core.resources.questionanswer.QuestionAnswerResource;
import com.examsystem.core.services.api.questionanswer.QuestionAnswerService;
import com.examsystem.core.services.api.testanswer.TestAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@RestController
@RequestMapping(value = "/rest/testAnswers/{tid}/questionAnswers")
public class QuestionAnswerController {

    @Autowired
    private TestAnswerService testAnswerService;

    @Autowired
    private QuestionAnswerServiceProvider questionAnswerServiceProvider;

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and (@studentPermissionsService.addedPreviousQuestionAnswersForAnswer(#tid, #qid) or @studentPermissionsService.finishedTestAnswer(#tid) or @studentPermissionsService.testAnswerTimeoutExceeded(#tid))) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTestAnswer(#tid)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{qid}", method = RequestMethod.GET)
    public QuestionAnswerResource get(@PathVariable Long tid, @PathVariable Long qid) {
        QuestionType questionType = testAnswerService.getQuestionType(tid, qid);
        QuestionAnswerService questionAnswerService = questionAnswerServiceProvider.getQuestionAnswerService(questionType);
        return questionAnswerService.getQuestionAnswerById(qid);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and (@studentPermissionsService.addedPreviousQuestionAnswersForQuestion(#tid, #questionAnswerResource.questionId) and @studentPermissionsService.questionAnswerNotStartedYet(#tid, #questionAnswerResource)) and #tid == #questionAnswerResource.testAnswerId) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTest(#tid) and #tid == #questionAnswerResource.testAnswerId) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.POST)
    @SuppressWarnings("unchecked")
    public QuestionAnswerResource save(@PathVariable Long tid, @Valid @RequestBody QuestionAnswerResource questionAnswerResource, Errors errors) {
        QuestionType questionType = questionAnswerResource.getQuestionType();
        QuestionAnswerService questionAnswerService = questionAnswerServiceProvider.getQuestionAnswerService(questionType);
        return questionAnswerService.saveQuestionAnswer(questionAnswerResource);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and (@studentPermissionsService.addedPreviousQuestionAnswersForQuestion(#tid, #questionAnswerResource.questionId) and @studentPermissionsService.questionAnswerNotFinishedYet(#tid, #questionAnswerResource) and @studentPermissionsService.questionAnswerTimeoutNotExceeded(#tid, #questionAnswerResource))  and #qid == #questionAnswerResource.id and #tid == #questionAnswerResource.testAnswerId) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTestAnswer(#tid) and #qid == #questionAnswerResource.id and #tid == #questionAnswerResource.testAnswerId) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{qid}", method = RequestMethod.PUT)
    @SuppressWarnings("unchecked")
    public QuestionAnswerResource update(@PathVariable Long tid, @PathVariable Long qid, @Valid @RequestBody QuestionAnswerResource questionAnswerResource, Errors errors) {
        QuestionType questionType = questionAnswerResource.getQuestionType();
        QuestionAnswerService questionAnswerService = questionAnswerServiceProvider.getQuestionAnswerService(questionType);
        QuestionAnswerResource responseResource = questionAnswerService.getQuestionAnswerById(qid);
        if (responseResource != null) {
            questionAnswerResource.setId(responseResource.getId());
            responseResource = questionAnswerService.updateQuestionAnswer(questionAnswerResource);
        }
        return responseResource;
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTestAnswer(#tid)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{qid}", method = RequestMethod.DELETE)
    public QuestionAnswerResource delete(@PathVariable Long tid, @PathVariable Long qid) {
        QuestionType questionType = testAnswerService.getQuestionType(tid, qid);
        QuestionAnswerService questionAnswerService = questionAnswerServiceProvider.getQuestionAnswerService(questionType);
        QuestionAnswerResource questionAnswerResource = questionAnswerService.getQuestionAnswerById(qid);
        if (questionAnswerResource != null) {
            questionAnswerService.deleteQuestionAnswerById(questionAnswerResource.getId());
        }
        return questionAnswerResource;
    }

}
