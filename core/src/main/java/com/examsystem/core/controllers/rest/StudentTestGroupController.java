package com.examsystem.core.controllers.rest;

import com.examsystem.core.resources.intermediary.StudentTestGroupResource;
import com.examsystem.core.services.api.intermediary.StudentTestGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@RestController
@RequestMapping(value = "/rest/studentTestGroups")
public class StudentTestGroupController {

    @Autowired
    private StudentTestGroupService studentTestGroupService;

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and (#studentId != null and @studentPermissionsService.isCurrentUser(#studentId))) or (hasAuthority('ROLE_EXAMINER') and ((#examinerId != null and @examinerPermissionsService.isCurrentUser(#examinerId)) or (#testGroupId != null and @examinerPermissionsService.isTestGroupOwner(#testGroupId)))) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<StudentTestGroupResource> query(
            @RequestParam(required = false) Long studentId,
            @RequestParam(required = false) Long testGroupId,
            @RequestParam(required = false) Long examinerId
    ) {
        Map<String, Object> params = new HashMap<>();
        params.put("studentId", studentId);
        params.put("testGroupId", testGroupId);
        params.put("examinerId", examinerId);
        return studentTestGroupService.query(params);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.isStudentTestGroupOwner(#id)) or (hasAuthority('ROLE_EXAMINER') and (#id != null and @examinerPermissionsService.isStudentTestGroupOwner(#id))) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public StudentTestGroupResource get(@PathVariable Long id) {
        return studentTestGroupService.getStudentTestGroupById(id);
    }

    @PreAuthorize(value = "hasAnyAuthority({'ROLE_EXAMINER', 'ROLE_ADMIN'})")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public StudentTestGroupResource save(@Valid @RequestBody StudentTestGroupResource studentTestGroupResource, Errors errors) {
        return studentTestGroupService.saveStudentTestGroup(studentTestGroupResource);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.isStudentTestGroupOwner(#id)) or (hasAuthority('ROLE_EXAMINER') and (#id != null and @examinerPermissionsService.isStudentTestGroupOwner(#id))) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public StudentTestGroupResource delete(@PathVariable Long id) {
        StudentTestGroupResource studentTestGroupResource = studentTestGroupService.getStudentTestGroupById(id);
        if (studentTestGroupResource != null) {
            studentTestGroupService.deleteStudentTestGroupById(studentTestGroupResource.getId());
        }
        return studentTestGroupResource;
    }

}
