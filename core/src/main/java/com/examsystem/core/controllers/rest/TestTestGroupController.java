package com.examsystem.core.controllers.rest;

import com.examsystem.core.resources.intermediary.TestTestGroupResource;
import com.examsystem.core.services.api.intermediary.TestTestGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@RestController
@RequestMapping(value = "/rest/testTestGroups")
public class TestTestGroupController {

    @Autowired
    private TestTestGroupService testTestGroupService;

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and (#studentId != null and @studentPermissionsService.isCurrentUser(#studentId))) or (hasAuthority('ROLE_EXAMINER') and (#testId != null and @examinerPermissionsService.hasAccessToTest(#testId))) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<TestTestGroupResource> query(
            @RequestParam(required = false) Long testId,
            @RequestParam(required = false) Long studentId
    ) {
        Map<String, Object> params = new HashMap<>();
        params.put("testId", testId);
        params.put("studentId", studentId);
        return testTestGroupService.query(params);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.hasAccessToTestTestGroup(#id)) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTestTestGroup(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public TestTestGroupResource get(@PathVariable Long id) {
        return testTestGroupService.getTestTestGroupById(id);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTest(#testTestGroupResource.testId)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public TestTestGroupResource save(@Valid @RequestBody TestTestGroupResource testTestGroupResource, Errors errors) {
        return testTestGroupService.saveTestTestGroup(testTestGroupResource);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.hasAccessToTestTestGroup(#id)) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTestTestGroup(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public TestTestGroupResource delete(@PathVariable Long id) {
        TestTestGroupResource testTestGroupResource = testTestGroupService.getTestTestGroupById(id);
        if (testTestGroupResource != null) {
            testTestGroupService.deleteTestTestGroupById(testTestGroupResource.getId());
        }
        return testTestGroupResource;
    }

}
