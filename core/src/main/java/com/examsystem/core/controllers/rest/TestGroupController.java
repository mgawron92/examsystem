package com.examsystem.core.controllers.rest;

import com.examsystem.core.resources.user.StudentResource;
import com.examsystem.core.resources.test.TestGroupResource;
import com.examsystem.core.resources.test.TestResource;
import com.examsystem.core.services.api.test.TestGroupService;
import com.examsystem.core.services.api.test.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@RestController
@RequestMapping(value = "/rest/testGroups")
public class TestGroupController {

    @Autowired
    private TestGroupService testGroupService;

    @Autowired
    private TestService testService;

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and (#studentId != null and @studentPermissionsService.isCurrentUser(#studentId))) or (hasAuthority('ROLE_EXAMINER') and (#examinerId != null and @examinerPermissionsService.isCurrentUser(#examinerId))) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<TestGroupResource> query(
            @RequestParam(required = false) Long examinerId,
            @RequestParam(required = false) Long studentId,
            @PageableDefault(value = 10, sort = "name", direction = Sort.Direction.ASC) Pageable pageable
    ) {
        Map<String, Object> params = new HashMap<>();
        params.put("examinerId", examinerId);
        params.put("studentId", studentId);
        return testGroupService.query(params, pageable);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and (#examinerId != null and @examinerPermissionsService.isCurrentUser(#examinerId))) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/names", method = RequestMethod.GET)
    public List<String> getNames(
            @RequestParam(required = true) Long examinerId
    ) {
        Map<String, Object> params = new HashMap<>();
        params.put("examinerId", examinerId);
        return testGroupService.query(params).stream().map(TestGroupResource::getName).collect(Collectors.toList());
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.isTestGroupMember(#id)) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.isTestGroupOwner(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public TestGroupResource get(@PathVariable Long id) {
        return testGroupService.getTestGroupById(id);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.isTestGroupOwner(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}/students", method = RequestMethod.GET)
    public Page<StudentResource> getStudents(
            @PathVariable Long id,
            @PageableDefault(size = 10) Pageable pageable
    ) {
        return testGroupService.getStudentsByTestGroupId(id, pageable);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.isTestGroupMember(#id)) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.isTestGroupOwner(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}/tests", method = RequestMethod.GET)
    public Page<TestResource> getTests(
            @PathVariable Long id,
            @RequestParam(required = false) Long studentId,
            @RequestParam(required = false) Boolean notStarted,
            @RequestParam(defaultValue = "false") Boolean activeOnly,
            @RequestParam(defaultValue = "true") Boolean latestFirst,
            @PageableDefault(size = 10) Pageable pageable
    ) {
        Map<String, Object> params = new HashMap<>();
        params.put("testGroupId", id);
        params.put("studentId", studentId);
        params.put("notStarted", notStarted);
        params.put("activeOnly", activeOnly);
        params.put("latestFirst", latestFirst);
        return testService.query(params, pageable);
    }

    @PreAuthorize(value = "hasAnyAuthority({'ROLE_EXAMINER', 'ROLE_ADMIN'})")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public TestGroupResource save(@Valid @RequestBody TestGroupResource testGroupResource, Errors errors) {
        return testGroupService.saveTestGroup(testGroupResource);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.isTestGroupOwner(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public TestGroupResource update(@PathVariable Long id, @Valid @RequestBody TestGroupResource testGroupResource, Errors errors) {
        TestGroupResource responseResource = testGroupService.getTestGroupById(id);
        if (responseResource != null) {
            testGroupResource.setId(responseResource.getId());
            responseResource = testGroupService.updateTestGroup(testGroupResource);
        }
        return responseResource;
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.isTestGroupOwner(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public TestGroupResource delete(@PathVariable Long id) {
        TestGroupResource testGroupResource = testGroupService.getTestGroupById(id);
        if (testGroupResource != null) {
            testGroupService.deleteTestGroupById(testGroupResource.getId());
        }
        return testGroupResource;
    }

}
