package com.examsystem.core.controllers.rest;

import com.examsystem.core.resources.user.ExaminerResource;
import com.examsystem.core.services.api.user.ExaminerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by Mateusz Gawron on 2016-04-03.
 */
@RestController
@RequestMapping(value = "/rest/examiners")
public class ExaminerController {

    @Autowired
    private ExaminerService examinerService;

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<ExaminerResource> query(
            @PageableDefault(size = 10, sort = "surname", direction = Sort.Direction.ASC) Pageable pageable
    ) {
        return examinerService.getAll(pageable);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and @studentPermissionsService.isMemberOfExaminersTestGroup(#id)) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.isCurrentUser(#id)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ExaminerResource get(@PathVariable Long id) {
        return examinerService.getById(id);
    }

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ExaminerResource save(@Valid @RequestBody ExaminerResource examinerResource, Errors errors) {
        return examinerService.save(examinerResource);
    }

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ExaminerResource update(@PathVariable Long id, @Valid @RequestBody ExaminerResource examinerResource, Errors errors) {
        ExaminerResource responseResource = examinerService.getById(id);
        if (responseResource != null) {
            examinerResource.setId(responseResource.getId());
            responseResource = examinerService.save(examinerResource);
        }
        return responseResource;
    }

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ExaminerResource delete(@PathVariable Long id) {
        ExaminerResource examinerResource = examinerService.getById(id);
        if (examinerResource != null) {
            examinerService.deleteById(examinerResource.getId());
        }
        return examinerResource;
    }

}
