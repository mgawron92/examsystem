package com.examsystem.core.controllers.rest;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.providers.api.QuestionServiceProvider;
import com.examsystem.core.resources.question.QuestionResource;
import com.examsystem.core.services.api.question.QuestionService;
import com.examsystem.core.services.api.test.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@RestController
@RequestMapping(value = "/rest/tests/{tid}/questions")
public class QuestionController {

    @Autowired
    private TestService testService;

    @Autowired
    private QuestionServiceProvider questionServiceProvider;

    @PreAuthorize(value = "(hasAuthority('ROLE_STUDENT') and (@studentPermissionsService.answeredPreviousQuestions(#tid, #qid) or @studentPermissionsService.finishedTest(#tid) or @studentPermissionsService.testTimeoutExceeded(#tid))) or (hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTest(#tid)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{qid}", method = RequestMethod.GET)
    public QuestionResource get(@PathVariable Long tid, @PathVariable Long qid) {
        QuestionType questionType = testService.getQuestionType(tid, qid);
        QuestionService questionService = questionServiceProvider.getQuestionService(questionType);
        return questionService.getQuestionById(qid);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTest(#tid) and #qid == #questionResource.id and #tid == #questionResource.testId) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.POST)
    @SuppressWarnings("unchecked")
    public QuestionResource save(@PathVariable Long tid, @Valid @RequestBody QuestionResource questionResource, Errors errors) {
        QuestionType questionType = questionResource.getQuestionType();
        QuestionService questionService = questionServiceProvider.getQuestionService(questionType);
        return questionService.saveQuestion(questionResource);
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTest(#tid) and #qid == #questionResource.id and #tid == #questionResource.testId) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{qid}", method = RequestMethod.PUT)
    @SuppressWarnings("unchecked")
    public QuestionResource update(@PathVariable Long tid, @PathVariable Long qid, @Valid @RequestBody QuestionResource questionResource, Errors errors) {
        QuestionType questionType = questionResource.getQuestionType();
        QuestionService questionService = questionServiceProvider.getQuestionService(questionType);
        QuestionResource responseResource = questionService.getQuestionById(qid);
        if (responseResource != null) {
            questionResource.setId(responseResource.getId());
            responseResource = questionService.updateQuestion(questionResource);

        }
        return responseResource;
    }

    @PreAuthorize(value = "(hasAuthority('ROLE_EXAMINER') and @examinerPermissionsService.hasAccessToTest(#tid)) or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{qid}", method = RequestMethod.DELETE)
    public QuestionResource delete(@PathVariable Long tid, @PathVariable Long qid) {
        QuestionType questionType = testService.getQuestionType(tid, qid);
        QuestionService questionService = questionServiceProvider.getQuestionService(questionType);
        QuestionResource questionResource = questionService.getQuestionById(qid);
        if (questionResource != null) {
            questionService.deleteQuestionById(questionResource.getId());
        }
        return questionResource;
    }

}
