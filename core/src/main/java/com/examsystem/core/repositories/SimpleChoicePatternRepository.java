package com.examsystem.core.repositories;

import com.examsystem.core.entitites.question.choice.simple.SimpleChoicePattern;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface SimpleChoicePatternRepository extends JpaRepository<SimpleChoicePattern, Long> {

    SimpleChoicePattern findSimpleChoicePatternById(Long id);

    Long removeById(Long id);

}
