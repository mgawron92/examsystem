package com.examsystem.core.repositories;

import com.examsystem.core.entitites.questionanswer.choice.complex.ComplexChoiceAnswerPart;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface ComplexChoiceAnswerPartRepository extends JpaRepository<ComplexChoiceAnswerPart, Long> {

    ComplexChoiceAnswerPart findComplexChoiceAnswerPartById(Long id);

    Long removeById(Long id);

}
