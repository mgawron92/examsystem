package com.examsystem.core.repositories;

import com.examsystem.core.entitites.user.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query("select count(s.id) from Student s where s.id in (select distinct stg.student.id from StudentTestGroup stg where stg.testGroup.id in (select ttg.testGroup.id from TestTestGroup ttg where ttg.test.id = ?1))")
    Integer getStudentsCountByTestId(Long testId);

    @Query("select s.ldapLogin from Student s")
    List<String> getStudentLdapLogins();

    @Query("select s from Student s where s.id in (select distinct stg.student.id from StudentTestGroup stg where stg.testGroup.id in (select ttg.testGroup.id from TestTestGroup ttg where ttg.test.id = ?1))")
    Page<Student> findStudentsByTestId(Long testId, Pageable pageable);

    Student findStudentByLdapLogin(String ldapLogin);

    Student findStudentById(Long id);

    Long removeById(Long id);

}
