package com.examsystem.core.repositories;

import com.examsystem.core.entitites.question.choice.complex.ComplexChoiceQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface ComplexChoiceQuestionRepository extends JpaRepository<ComplexChoiceQuestion, Long> {

    ComplexChoiceQuestion findComplexChoiceQuestionById(Long id);

    Long removeById(Long id);

}
