package com.examsystem.core.repositories;

import com.examsystem.core.entitites.question.freetext.FreetextQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface FreetextQuestionRepository extends JpaRepository<FreetextQuestion, Long> {

    FreetextQuestion findFreetextQuestionById(Long id);

    Long removeById(Long id);

}
