package com.examsystem.core.repositories;

import com.examsystem.core.entitites.question.choice.complex.ComplexChoicePattern;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface ComplexChoicePatternRepository extends JpaRepository<ComplexChoicePattern, Long> {

    ComplexChoicePattern findComplexChoicePatternById(Long id);

    Long removeById(Long id);

}
