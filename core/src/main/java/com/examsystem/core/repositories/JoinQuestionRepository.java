package com.examsystem.core.repositories;

import com.examsystem.core.entitites.question.join.JoinQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface JoinQuestionRepository extends JpaRepository<JoinQuestion, Long> {

    JoinQuestion findJoinQuestionById(Long id);

    Long removeById(Long id);

}
