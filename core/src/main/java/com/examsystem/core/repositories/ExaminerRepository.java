package com.examsystem.core.repositories;

import com.examsystem.core.entitites.user.Examiner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface ExaminerRepository extends JpaRepository<Examiner, Long> {

    @Query("select e.ldapLogin from Examiner e")
    List<String> getExaminerLdapLogins();

    Examiner findExaminerByLdapLogin(String ldapLogin);

    Examiner findExaminerById(Long id);

    Long removeExaminerById(Long id);

}
