package com.examsystem.core.repositories;

import com.examsystem.core.entitites.questionanswer.choice.complex.ComplexChoiceAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface ComplexChoiceAnswerRepository extends JpaRepository<ComplexChoiceAnswer, Long> {

    ComplexChoiceAnswer findComplexChoiceAnswerById(Long id);

    Long removeById(Long id);

}
