package com.examsystem.core.repositories;

import com.examsystem.core.entitites.intermediary.StudentTestGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface StudentTestGroupRepository extends JpaRepository<StudentTestGroup, Long>, JpaSpecificationExecutor<StudentTestGroup> {

    StudentTestGroup findStudentTestGroupById(Long id);

    Long removeById(Long id);

}
