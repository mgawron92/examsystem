package com.examsystem.core.repositories;

import com.examsystem.core.entitites.user.Student;
import com.examsystem.core.entitites.test.TestGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface TestGroupRepository extends JpaRepository<TestGroup, Long>, JpaSpecificationExecutor<TestGroup> {

    TestGroup findTestGroupById(Long id);

    @Query("select stg.student from StudentTestGroup stg where stg.testGroup.id = ?1 order by stg.student.surname asc")
    Page<Student> findStudentsByTestGroupId(Long id, Pageable pageable);

    Long removeById(Long id);

}
