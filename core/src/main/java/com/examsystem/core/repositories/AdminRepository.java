package com.examsystem.core.repositories;

import com.examsystem.core.entitites.user.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface AdminRepository extends JpaRepository<Admin, Long> {

    @Query("select a.ldapLogin from Admin a")
    List<String> getAdminLdapLogins();

    Admin findAdminByLdapLogin(String ldapLogin);

    Admin findAdminById(Long id);

    Long removeAdminById(Long id);

}
