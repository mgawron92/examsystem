package com.examsystem.core.repositories;

import com.examsystem.core.entitites.questionanswer.choice.simple.SimpleChoiceAnswerPart;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface SimpleChoiceAnswerPartRepository extends JpaRepository<SimpleChoiceAnswerPart, Long> {

    SimpleChoiceAnswerPart findSimpleChoiceAnswerPartById(Long id);

    Long removeById(Long id);

}
