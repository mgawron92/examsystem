package com.examsystem.core.repositories;

import com.examsystem.core.entitites.questionanswer.join.JoinAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface JoinAnswerRepository extends JpaRepository<JoinAnswer, Long> {

    JoinAnswer findJoinAnswerById(Long id);

    Long removeById(Long id);

}
