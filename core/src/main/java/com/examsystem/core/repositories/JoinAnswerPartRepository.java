package com.examsystem.core.repositories;

import com.examsystem.core.entitites.questionanswer.join.JoinAnswerPart;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface JoinAnswerPartRepository extends JpaRepository<JoinAnswerPart, Long> {

    JoinAnswerPart findJoinAnswerPartById(Long id);

    Long removeById(Long id);

}
