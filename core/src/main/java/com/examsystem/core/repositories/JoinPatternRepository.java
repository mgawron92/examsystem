package com.examsystem.core.repositories;

import com.examsystem.core.entitites.question.join.JoinPattern;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface JoinPatternRepository extends JpaRepository<JoinPattern, Long> {

    JoinPattern findJoinPatternById(Long id);

    Long removeById(Long id);

}
