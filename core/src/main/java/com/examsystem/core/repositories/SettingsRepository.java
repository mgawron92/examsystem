package com.examsystem.core.repositories;

import com.examsystem.core.entitites.other.Settings;
import com.examsystem.core.enumerations.SettingsType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface SettingsRepository extends JpaRepository<Settings, Long> {

    Settings findSettingsByKey(String key);

    List<Settings> findSettingsBySettingsType(SettingsType settingsType);

    Long removeSettingsBySettingsType(SettingsType settingsType);

}
