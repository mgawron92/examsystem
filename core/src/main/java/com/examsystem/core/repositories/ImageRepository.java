package com.examsystem.core.repositories;

import com.examsystem.core.entitites.other.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-06-26.
 */
public interface ImageRepository extends JpaRepository<Image, Long> {

    @Query("select count(q.id) from Question q where q.imageId = ?1")
    Integer getQuestionsCountByImageId(Long imageId);

    @Query("select distinct q.test.id from Question q where q.imageId = ?1")
    List<Long> getTestIdsByImageId(Long imageId);

    @Query("select i.id from Image i where i.id not in (select q.imageId from Question q where q.imageId is not null) and i.addedOn < ?1")
    List<Long> getUnusedImageIds(Date date);

    @Modifying
    @Query("delete from Image i where i.id not in (select q.imageId from Question q where q.imageId is not null) and i.addedOn < ?1")
    void removeUnusedImages(Date date);

    Image findImageById(Long id);

    Long removeById(Long id);
}
