package com.examsystem.core.repositories;

import com.examsystem.core.entitites.testanswer.QuestionSequence;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface QuestionSequenceRepository extends JpaRepository<QuestionSequence, Long> {

    QuestionSequence findQuestionSequenceById(Long id);

    Long removeById(Long id);

}
