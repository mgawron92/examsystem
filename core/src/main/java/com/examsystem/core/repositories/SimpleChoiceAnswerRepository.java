package com.examsystem.core.repositories;

import com.examsystem.core.entitites.questionanswer.choice.simple.SimpleChoiceAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface SimpleChoiceAnswerRepository extends JpaRepository<SimpleChoiceAnswer, Long> {

    SimpleChoiceAnswer findSimpleChoiceAnswerById(Long id);

    Long removeById(Long id);

}
