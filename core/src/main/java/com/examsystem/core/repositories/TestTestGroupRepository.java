package com.examsystem.core.repositories;

import com.examsystem.core.entitites.intermediary.TestTestGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface TestTestGroupRepository extends JpaRepository<TestTestGroup, Long>, JpaSpecificationExecutor<TestTestGroup> {

    TestTestGroup findTestTestGroupById(Long id);

    Long removeById(Long id);

}
