package com.examsystem.core.repositories;

import com.examsystem.core.entitites.testanswer.TestAnswer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface TestAnswerRepository extends JpaRepository<TestAnswer, Long>, JpaSpecificationExecutor<TestAnswer> {

    TestAnswer findTestAnswerById(Long id);

    Long removeById(Long id);

}
