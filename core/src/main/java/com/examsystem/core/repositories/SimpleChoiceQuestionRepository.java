package com.examsystem.core.repositories;

import com.examsystem.core.entitites.question.choice.simple.SimpleChoiceQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface SimpleChoiceQuestionRepository extends JpaRepository<SimpleChoiceQuestion, Long> {

    SimpleChoiceQuestion findSimpleChoiceQuestionById(Long id);

    Long removeById(Long id);

}
