package com.examsystem.core.repositories;

import com.examsystem.core.entitites.questionanswer.freetext.FreetextAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface FreetextAnswerRepository extends JpaRepository<FreetextAnswer, Long> {

    FreetextAnswer findFreetextAnswerById(Long id);

    Long removeById(Long id);

}
