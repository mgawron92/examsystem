package com.examsystem.core.repositories;

import com.examsystem.core.entitites.test.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
public interface TestRepository extends JpaRepository<Test, Long>, JpaSpecificationExecutor<Test> {

    Test findTestById(Long id);

    Long removeById(Long id);

}
