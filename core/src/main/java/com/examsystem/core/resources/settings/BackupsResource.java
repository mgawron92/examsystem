package com.examsystem.core.resources.settings;

/**
 * Created by Mateusz Gawron on 2016-05-03.
 */
public class BackupsResource {

    private Integer frequency = 0;

    private String host = "";

    private Integer port;

    private String username = "";

    private String password = "";

    private Boolean changePassword = false;

    private String remotePath = "";

    private Boolean enableZip = false;

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getChangePassword() {
        return changePassword;
    }

    public void setChangePassword(Boolean changePassword) {
        this.changePassword = changePassword;
    }

    public String getRemotePath() {
        return remotePath;
    }

    public void setRemotePath(String remotePath) {
        this.remotePath = remotePath;
    }

    public Boolean getEnableZip() {
        return enableZip;
    }

    public void setEnableZip(Boolean enableZip) {
        this.enableZip = enableZip;
    }

}
