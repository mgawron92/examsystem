package com.examsystem.core.resources.question.join;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.question.QuestionResource;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class JoinQuestionResource extends QuestionResource {

    private List<Long> joinAnswerIds = new LinkedList<>();

    private List<JoinPatternResource> joinPatterns = new LinkedList<>();

    private Integer correctPoints;

    private Integer incorrectPoints;

    public List<Long> getJoinAnswerIds() {
        return joinAnswerIds;
    }

    public void setJoinAnswerIds(List<Long> joinAnswerIds) {
        this.joinAnswerIds = joinAnswerIds;
    }

    public List<JoinPatternResource> getJoinPatterns() {
        return joinPatterns;
    }

    public void setJoinPatterns(List<JoinPatternResource> joinPatterns) {
        this.joinPatterns = joinPatterns;
    }

    public QuestionType getQuestionType() {
        return QuestionType.JOIN;
    }

    public Integer getCorrectPoints() {
        return correctPoints;
    }

    public void setCorrectPoints(Integer correctPoints) {
        this.correctPoints = correctPoints;
    }

    public Integer getIncorrectPoints() {
        return incorrectPoints;
    }

    public void setIncorrectPoints(Integer incorrectPoints) {
        this.incorrectPoints = incorrectPoints;
    }

}
