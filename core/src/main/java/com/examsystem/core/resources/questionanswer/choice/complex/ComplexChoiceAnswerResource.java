package com.examsystem.core.resources.questionanswer.choice.complex;

import com.examsystem.core.resources.questionanswer.choice.ChoiceAnswerResource;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public abstract class ComplexChoiceAnswerResource extends ChoiceAnswerResource {

    private List<ComplexChoiceAnswerPartResource> choiceAnswerParts = new LinkedList<>();

    public List<ComplexChoiceAnswerPartResource> getChoiceAnswerParts() {
        return choiceAnswerParts;
    }

    public void setChoiceAnswerParts(List<ComplexChoiceAnswerPartResource> choiceAnswerParts) {
        this.choiceAnswerParts = choiceAnswerParts;
    }

}
