package com.examsystem.core.resources.question.choice.complex;

import com.examsystem.core.resources.question.choice.ChoicePatternResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class ComplexChoicePatternResource extends ChoicePatternResource {

    private Integer selectedPoints;

    private Integer deselectedPoints;

    public Integer getSelectedPoints() {
        return selectedPoints;
    }

    public void setSelectedPoints(Integer selectedPoints) {
        this.selectedPoints = selectedPoints;
    }

    public Integer getDeselectedPoints() {
        return deselectedPoints;
    }

    public void setDeselectedPoints(Integer deselectedPoints) {
        this.deselectedPoints = deselectedPoints;
    }
}
