package com.examsystem.core.resources.settings;

/**
 * Created by Mateusz Gawron on 2016-05-03.
 */
public class ListsResource {

    private Integer students = 5;

    private Integer examiners = 5;

    private Integer admins = 5;

    private Integer tests = 5;

    private Integer testResults = 5;

    private Integer testsToCheck = 5;

    private Integer testGroups = 5;

    private Integer associatedTests = 5;

    public Integer getStudents() {
        return students;
    }

    public void setStudents(Integer students) {
        this.students = students;
    }

    public Integer getExaminers() {
        return examiners;
    }

    public void setExaminers(Integer examiners) {
        this.examiners = examiners;
    }

    public Integer getAdmins() {
        return admins;
    }

    public void setAdmins(Integer admins) {
        this.admins = admins;
    }

    public Integer getTests() {
        return tests;
    }

    public void setTests(Integer tests) {
        this.tests = tests;
    }

    public Integer getTestResults() {
        return testResults;
    }

    public void setTestResults(Integer testResults) {
        this.testResults = testResults;
    }

    public Integer getTestsToCheck() {
        return testsToCheck;
    }

    public void setTestsToCheck(Integer testsToCheck) {
        this.testsToCheck = testsToCheck;
    }

    public Integer getTestGroups() {
        return testGroups;
    }

    public void setTestGroups(Integer testGroups) {
        this.testGroups = testGroups;
    }

    public Integer getAssociatedTests() {
        return associatedTests;
    }

    public void setAssociatedTests(Integer associatedTests) {
        this.associatedTests = associatedTests;
    }

}
