package com.examsystem.core.resources.questionanswer.freetext;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.questionanswer.QuestionAnswerResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class FreetextAnswerResource extends QuestionAnswerResource {

    private Long freetextQuestionId;

    private String content;

    private String comment;

    public Long getFreetextQuestionId() {
        return freetextQuestionId;
    }

    public void setFreetextQuestionId(Long freetextQuestionId) {
        this.freetextQuestionId = freetextQuestionId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public QuestionType getQuestionType() {
        return QuestionType.FREETEXT;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
