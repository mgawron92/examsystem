package com.examsystem.core.resources.question.choice.simple.multiple;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.question.choice.simple.SimpleChoiceQuestionResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class MultipleSimpleChoiceQuestionResource extends SimpleChoiceQuestionResource {

    public QuestionType getQuestionType() {
        return QuestionType.MULTIPLE_SIMPLE_CHOICE;
    }

}
