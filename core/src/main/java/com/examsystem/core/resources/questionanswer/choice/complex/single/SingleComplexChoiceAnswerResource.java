package com.examsystem.core.resources.questionanswer.choice.complex.single;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.questionanswer.choice.complex.ComplexChoiceAnswerResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class SingleComplexChoiceAnswerResource extends ComplexChoiceAnswerResource {

    public QuestionType getQuestionType() {
        return QuestionType.SIMPLE_COMPLEX_CHOICE;
    }

}
