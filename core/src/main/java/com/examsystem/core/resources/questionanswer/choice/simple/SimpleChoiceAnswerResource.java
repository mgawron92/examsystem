package com.examsystem.core.resources.questionanswer.choice.simple;

import com.examsystem.core.resources.questionanswer.choice.ChoiceAnswerResource;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public abstract class SimpleChoiceAnswerResource extends ChoiceAnswerResource {

    private List<SimpleChoiceAnswerPartResource> choiceAnswerParts = new LinkedList<>();

    private Boolean correct;

    public List<SimpleChoiceAnswerPartResource> getChoiceAnswerParts() {
        return choiceAnswerParts;
    }

    public void setChoiceAnswerParts(List<SimpleChoiceAnswerPartResource> choiceAnswerParts) {
        this.choiceAnswerParts = choiceAnswerParts;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }

}
