package com.examsystem.core.resources.user;

/**
 * Created by Mateusz Gawron on 2016-05-02.
 */
public class TypeaheadResource {

    private String ldapLogin;

    private String name;

    private String surname;

    private String email;

    public String getLdapLogin() {
        return ldapLogin;
    }

    public void setLdapLogin(String ldapLogin) {
        this.ldapLogin = ldapLogin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
