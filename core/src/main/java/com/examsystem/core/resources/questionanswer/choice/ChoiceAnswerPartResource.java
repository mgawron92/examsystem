package com.examsystem.core.resources.questionanswer.choice;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public abstract class ChoiceAnswerPartResource {

    private Long id;

    private Long choicePatternId;

    private Long choiceAnswerId;

    private Boolean selected;

    private Boolean correct;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChoicePatternId() {
        return choicePatternId;
    }

    public void setChoicePatternId(Long choicePatternId) {
        this.choicePatternId = choicePatternId;
    }

    public Long getChoiceAnswerId() {
        return choiceAnswerId;
    }

    public void setChoiceAnswerId(Long choiceAnswerId) {
        this.choiceAnswerId = choiceAnswerId;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }
}
