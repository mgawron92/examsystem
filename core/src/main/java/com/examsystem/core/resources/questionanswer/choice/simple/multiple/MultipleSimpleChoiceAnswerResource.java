package com.examsystem.core.resources.questionanswer.choice.simple.multiple;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.questionanswer.choice.simple.SimpleChoiceAnswerResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class MultipleSimpleChoiceAnswerResource extends SimpleChoiceAnswerResource {

    public QuestionType getQuestionType() {
        return QuestionType.MULTIPLE_SIMPLE_CHOICE;
    }

}
