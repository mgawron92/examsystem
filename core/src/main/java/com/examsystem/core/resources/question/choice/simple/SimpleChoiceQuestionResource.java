package com.examsystem.core.resources.question.choice.simple;

import com.examsystem.core.resources.question.choice.ChoiceQuestionResource;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public abstract class SimpleChoiceQuestionResource extends ChoiceQuestionResource {

    protected List<SimpleChoicePatternResource> choicePatterns = new LinkedList<>();

    private Integer correctPoints;

    private Integer incorrectPoints;

    public Integer getCorrectPoints() {
        return correctPoints;
    }

    public void setCorrectPoints(Integer correctPoints) {
        this.correctPoints = correctPoints;
    }

    public Integer getIncorrectPoints() {
        return incorrectPoints;
    }

    public void setIncorrectPoints(Integer incorrectPoints) {
        this.incorrectPoints = incorrectPoints;
    }

    public List<SimpleChoicePatternResource> getChoicePatterns() {
        return choicePatterns;
    }

    public void setChoicePatterns(List<SimpleChoicePatternResource> choicePatterns) {
        this.choicePatterns = choicePatterns;
    }

}
