package com.examsystem.core.resources.questionanswer.choice;

import com.examsystem.core.resources.questionanswer.QuestionAnswerResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public abstract class ChoiceAnswerResource extends QuestionAnswerResource {

    private Long choiceQuestionId;

    public Long getChoiceQuestionId() {
        return choiceQuestionId;
    }

    public void setChoiceQuestionId(Long choiceQuestionId) {
        this.choiceQuestionId = choiceQuestionId;
    }

}
