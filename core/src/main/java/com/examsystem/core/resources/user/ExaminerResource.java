package com.examsystem.core.resources.user;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */

public class ExaminerResource extends PersonResource {

    private List<Long> testGroupIds = new LinkedList<>();

    private List<Long> testIds = new LinkedList<>();

    private String addedBy;

    private List<Long> imageIds = new LinkedList<>();

    public List<Long> getTestGroupIds() {
        return testGroupIds;
    }

    public void setTestGroupIds(List<Long> testGroupIds) {
        this.testGroupIds = testGroupIds;
    }

    public List<Long> getTestIds() {
        return testIds;
    }

    public void setTestIds(List<Long> testIds) {
        this.testIds = testIds;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public List<Long> getImageIds() {
        return imageIds;
    }

    public void setImageIds(List<Long> imageIds) {
        this.imageIds = imageIds;
    }

}
