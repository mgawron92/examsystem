package com.examsystem.core.resources.test;

import com.examsystem.core.resources.intermediary.StudentTestGroupResource;
import com.examsystem.core.resources.intermediary.TestTestGroupResource;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class TestGroupResource {

    private Long id;

    private List<StudentTestGroupResource> studentTestGroups = new LinkedList<>();

    private List<TestTestGroupResource> testTestGroups = new LinkedList<>();

    private Long examinerId;

    private String name;

    private String description;

    private Date createdDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<StudentTestGroupResource> getStudentTestGroups() {
        return studentTestGroups;
    }

    public void setStudentTestGroups(List<StudentTestGroupResource> studentTestGroups) {
        this.studentTestGroups = studentTestGroups;
    }

    public List<TestTestGroupResource> getTestTestGroups() {
        return testTestGroups;
    }

    public void setTestTestGroups(List<TestTestGroupResource> testTestGroups) {
        this.testTestGroups = testTestGroups;
    }

    public Long getExaminerId() {
        return examinerId;
    }

    public void setExaminerId(Long examinerId) {
        this.examinerId = examinerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

}
