package com.examsystem.core.resources.question.choice.simple;

import com.examsystem.core.resources.question.choice.ChoicePatternResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class SimpleChoicePatternResource extends ChoicePatternResource {

    private Boolean correctIfSelected;

    public Boolean getCorrectIfSelected() {
        return correctIfSelected;
    }

    public void setCorrectIfSelected(Boolean correctIfSelected) {
        this.correctIfSelected = correctIfSelected;
    }

}
