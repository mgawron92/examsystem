package com.examsystem.core.resources.questionanswer.choice.complex.multiple;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.questionanswer.choice.complex.ComplexChoiceAnswerResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class MultipleComplexChoiceAnswerResource extends ComplexChoiceAnswerResource {

    public QuestionType getQuestionType() {
        return QuestionType.MULTIPLE_COMPLEX_CHOICE;
    }

}
