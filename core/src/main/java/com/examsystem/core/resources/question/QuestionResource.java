package com.examsystem.core.resources.question;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.question.choice.complex.multiple.MultipleComplexChoiceQuestionResource;
import com.examsystem.core.resources.question.choice.complex.single.SingleComplexChoiceQuestionResource;
import com.examsystem.core.resources.question.choice.simple.multiple.MultipleSimpleChoiceQuestionResource;
import com.examsystem.core.resources.question.choice.simple.single.SingleSimpleChoiceQuestionResource;
import com.examsystem.core.resources.question.freetext.FreetextQuestionResource;
import com.examsystem.core.resources.question.join.JoinQuestionResource;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = SingleSimpleChoiceQuestionResource.class, name = "single-simple-choice"),
        @JsonSubTypes.Type(value = MultipleSimpleChoiceQuestionResource.class, name = "multiple-simple-choice"),
        @JsonSubTypes.Type(value = SingleComplexChoiceQuestionResource.class, name = "single-complex-choice"),
        @JsonSubTypes.Type(value = MultipleComplexChoiceQuestionResource.class, name = "multiple-complex-choice"),
        @JsonSubTypes.Type(value = FreetextQuestionResource.class, name = "freetext"),
        @JsonSubTypes.Type(value = JoinQuestionResource.class, name = "join")
})
public abstract class QuestionResource {

    private Long id;

    private Long testId;

    private List<Long> questionAnswerIds;

    private String content;

    private Integer groupNumber;

    private Integer timeLimitMinutes;

    private Integer timeLimitSeconds;

    private Boolean hasTimeLimit;

    private Integer position;

    private Long imageId;

    private Integer maxPoints;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public List<Long> getQuestionAnswerIds() {
        return questionAnswerIds;
    }

    public void setQuestionAnswerIds(List<Long> questionAnswerIds) {
        this.questionAnswerIds = questionAnswerIds;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(Integer groupNumber) {
        this.groupNumber = groupNumber;
    }

    public Integer getTimeLimitMinutes() {
        return timeLimitMinutes;
    }

    public void setTimeLimitMinutes(Integer timeLimitMinutes) {
        this.timeLimitMinutes = timeLimitMinutes;
    }

    public Integer getTimeLimitSeconds() {
        return timeLimitSeconds;
    }

    public void setTimeLimitSeconds(Integer timeLimitSeconds) {
        this.timeLimitSeconds = timeLimitSeconds;
    }

    public Boolean getHasTimeLimit() {
        return hasTimeLimit;
    }

    public void setHasTimeLimit(Boolean hasTimeLimit) {
        this.hasTimeLimit = hasTimeLimit;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    public Integer getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
    }

    @JsonIgnore
    abstract public QuestionType getQuestionType();

}
