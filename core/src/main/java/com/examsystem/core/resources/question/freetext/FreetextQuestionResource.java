package com.examsystem.core.resources.question.freetext;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.question.QuestionResource;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class FreetextQuestionResource extends QuestionResource {

    private List<Long> freetextAnswerIds = new LinkedList<>();

    public List<Long> getFreetextAnswerIds() {
        return freetextAnswerIds;
    }

    public void setFreetextAnswerIds(List<Long> freetextAnswerIds) {
        this.freetextAnswerIds = freetextAnswerIds;
    }

    public QuestionType getQuestionType() {
        return QuestionType.FREETEXT;
    }

}
