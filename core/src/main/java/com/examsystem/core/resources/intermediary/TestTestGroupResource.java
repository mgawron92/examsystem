package com.examsystem.core.resources.intermediary;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class TestTestGroupResource {

    private Long id;

    private Long testId;

    private Long testGroupId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public Long getTestGroupId() {
        return testGroupId;
    }

    public void setTestGroupId(Long testGroupId) {
        this.testGroupId = testGroupId;
    }

}
