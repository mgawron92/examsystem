package com.examsystem.core.resources.test;

import com.examsystem.core.resources.intermediary.TestTestGroupResource;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class TestResource {

    private Long id;

    private List<TestTestGroupResource> testTestGroups = new LinkedList<>();

    private List<Long> testAnswerIds = new LinkedList<>();

    private List<Long> questionIds = new LinkedList<>();

    private Long examinerId;

    private String name;

    private String description;

    private Integer timeLimit;

    private Boolean hasTimeLimit;

    private Date activeFrom;

    private Integer minutesActive;

    private Date createdDate;

    private Integer groups;

    private Boolean restrictQuestionsNumber;

    private Integer usedQuestionsNumber;

    private Boolean useRandomSequence;

    private Integer maxPoints;

    private Integer testAnswersToCheck;

    private Integer studentsCount;

    private Integer testAnswersChecked;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<TestTestGroupResource> getTestTestGroups() {
        return testTestGroups;
    }

    public void setTestTestGroups(List<TestTestGroupResource> testTestGroups) {
        this.testTestGroups = testTestGroups;
    }

    public List<Long> getTestAnswerIds() {
        return testAnswerIds;
    }

    public void setTestAnswerIds(List<Long> testAnswerIds) {
        this.testAnswerIds = testAnswerIds;
    }

    public List<Long> getQuestionIds() {
        return questionIds;
    }

    public void setQuestionIds(List<Long> questionIds) {
        this.questionIds = questionIds;
    }

    public Long getExaminerId() {
        return examinerId;
    }

    public void setExaminerId(Long examinerId) {
        this.examinerId = examinerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(Integer timeLimit) {
        this.timeLimit = timeLimit;
    }

    public Boolean getHasTimeLimit() {
        return hasTimeLimit;
    }

    public void setHasTimeLimit(Boolean hasTimeLimit) {
        this.hasTimeLimit = hasTimeLimit;
    }

    public Date getActiveFrom() {
        return activeFrom;
    }

    public void setActiveFrom(Date activeFrom) {
        this.activeFrom = activeFrom;
    }

    public Integer getMinutesActive() {
        return minutesActive;
    }

    public void setMinutesActive(Integer minutesActive) {
        this.minutesActive = minutesActive;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getGroups() {
        return groups;
    }

    public void setGroups(Integer groups) {
        this.groups = groups;
    }

    public Integer getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
    }

    public Boolean getRestrictQuestionsNumber() {
        return restrictQuestionsNumber;
    }

    public void setRestrictQuestionsNumber(Boolean restrictQuestionsNumber) {
        this.restrictQuestionsNumber = restrictQuestionsNumber;
    }

    public Integer getUsedQuestionsNumber() {
        return usedQuestionsNumber;
    }

    public void setUsedQuestionsNumber(Integer usedQuestionsNumber) {
        this.usedQuestionsNumber = usedQuestionsNumber;
    }

    public Boolean getUseRandomSequence() {
        return useRandomSequence;
    }

    public void setUseRandomSequence(Boolean useRandomSequence) {
        this.useRandomSequence = useRandomSequence;
    }

    public Integer getTestAnswersToCheck() {
        return testAnswersToCheck;
    }

    public void setTestAnswersToCheck(Integer testAnswersToCheck) {
        this.testAnswersToCheck = testAnswersToCheck;
    }

    public Integer getStudentsCount() {
        return studentsCount;
    }

    public void setStudentsCount(Integer studentsCount) {
        this.studentsCount = studentsCount;
    }

    public Integer getTestAnswersChecked() {
        return testAnswersChecked;
    }

    public void setTestAnswersChecked(Integer testAnswersChecked) {
        this.testAnswersChecked = testAnswersChecked;
    }

}
