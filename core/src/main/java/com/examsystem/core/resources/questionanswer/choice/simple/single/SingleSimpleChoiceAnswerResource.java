package com.examsystem.core.resources.questionanswer.choice.simple.single;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.questionanswer.choice.simple.SimpleChoiceAnswerResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class SingleSimpleChoiceAnswerResource extends SimpleChoiceAnswerResource {

    public QuestionType getQuestionType() {
        return QuestionType.SINGLE_SIMPLE_CHOICE;
    }

}
