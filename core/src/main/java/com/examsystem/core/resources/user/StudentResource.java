package com.examsystem.core.resources.user;

import com.examsystem.core.resources.intermediary.StudentTestGroupResource;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class StudentResource extends PersonResource {

    private List<StudentTestGroupResource> studentTestGroups = new LinkedList<>();

    private List<Long> testAnswerIds = new LinkedList<>();

    public List<StudentTestGroupResource> getStudentTestGroups() {
        return studentTestGroups;
    }

    public void setStudentTestGroups(List<StudentTestGroupResource> studentTestGroups) {
        this.studentTestGroups = studentTestGroups;
    }

    public List<Long> getTestAnswerIds() {
        return testAnswerIds;
    }

    public void setTestAnswerIds(List<Long> testAnswerIds) {
        this.testAnswerIds = testAnswerIds;
    }

}
