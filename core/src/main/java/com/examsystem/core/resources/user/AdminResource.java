package com.examsystem.core.resources.user;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class AdminResource extends PersonResource {

    private String addedBy;

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }
}
