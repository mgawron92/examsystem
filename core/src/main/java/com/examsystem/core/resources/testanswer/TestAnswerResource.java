package com.examsystem.core.resources.testanswer;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class TestAnswerResource {

    private Long id;

    private Long testId;

    private List<Long> questionAnswerIds = new LinkedList<>();

    private Long studentId;

    private Long questionSequenceId;

    private Date startedOn;

    private Date finishedOn;

    private Boolean checked;

    private Integer points;

    private Integer maxPoints;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public List<Long> getQuestionAnswerIds() {
        return questionAnswerIds;
    }

    public void setQuestionAnswerIds(List<Long> questionAnswerIds) {
        this.questionAnswerIds = questionAnswerIds;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getQuestionSequenceId() {
        return questionSequenceId;
    }

    public void setQuestionSequenceId(Long questionSequenceId) {
        this.questionSequenceId = questionSequenceId;
    }

    public Date getStartedOn() {
        return startedOn;
    }

    public void setStartedOn(Date startedOn) {
        this.startedOn = startedOn;
    }

    public Date getFinishedOn() {
        return finishedOn;
    }

    public void setFinishedOn(Date finishedOn) {
        this.finishedOn = finishedOn;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
    }
}
