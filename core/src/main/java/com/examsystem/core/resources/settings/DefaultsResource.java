package com.examsystem.core.resources.settings;

/**
 * Created by Mateusz Gawron on 2016-05-03.
 */
public class DefaultsResource {

    private Integer testActiveAfterDays = 7;

    private Integer testActiveTime = 5;

    private Integer testLimitTime = 60;

    private Boolean hasTestTimeLimit = true;

    private Boolean restrictQuestionsNumber = false;

    private Boolean useRandomSequence = false;

    private Integer questionLimitTimeMinutes = 5;

    private Integer questionLimitTimeSeconds = 0;

    private Boolean hasQuestionTimeLimit = true;

    public Integer getTestActiveAfterDays() {
        return testActiveAfterDays;
    }

    public void setTestActiveAfterDays(Integer testActiveAfterDays) {
        this.testActiveAfterDays = testActiveAfterDays;
    }

    public Integer getTestActiveTime() {
        return testActiveTime;
    }

    public void setTestActiveTime(Integer testActiveTime) {
        this.testActiveTime = testActiveTime;
    }

    public Integer getTestLimitTime() {
        return testLimitTime;
    }

    public void setTestLimitTime(Integer testLimitTime) {
        this.testLimitTime = testLimitTime;
    }

    public Boolean getHasTestTimeLimit() {
        return hasTestTimeLimit;
    }

    public void setHasTestTimeLimit(Boolean hasTestTimeLimit) {
        this.hasTestTimeLimit = hasTestTimeLimit;
    }

    public Boolean getRestrictQuestionsNumber() {
        return restrictQuestionsNumber;
    }

    public void setRestrictQuestionsNumber(Boolean restrictQuestionsNumber) {
        this.restrictQuestionsNumber = restrictQuestionsNumber;
    }

    public Boolean getUseRandomSequence() {
        return useRandomSequence;
    }

    public void setUseRandomSequence(Boolean useRandomSequence) {
        this.useRandomSequence = useRandomSequence;
    }

    public Integer getQuestionLimitTimeMinutes() {
        return questionLimitTimeMinutes;
    }

    public void setQuestionLimitTimeMinutes(Integer questionLimitTimeMinutes) {
        this.questionLimitTimeMinutes = questionLimitTimeMinutes;
    }

    public Integer getQuestionLimitTimeSeconds() {
        return questionLimitTimeSeconds;
    }

    public void setQuestionLimitTimeSeconds(Integer questionLimitTimeSeconds) {
        this.questionLimitTimeSeconds = questionLimitTimeSeconds;
    }

    public Boolean getHasQuestionTimeLimit() {
        return hasQuestionTimeLimit;
    }

    public void setHasQuestionTimeLimit(Boolean hasQuestionTimeLimit) {
        this.hasQuestionTimeLimit = hasQuestionTimeLimit;
    }

}
