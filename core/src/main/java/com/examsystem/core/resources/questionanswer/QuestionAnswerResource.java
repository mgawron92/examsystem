package com.examsystem.core.resources.questionanswer;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.questionanswer.choice.complex.multiple.MultipleComplexChoiceAnswerResource;
import com.examsystem.core.resources.questionanswer.choice.complex.single.SingleComplexChoiceAnswerResource;
import com.examsystem.core.resources.questionanswer.choice.simple.multiple.MultipleSimpleChoiceAnswerResource;
import com.examsystem.core.resources.questionanswer.choice.simple.single.SingleSimpleChoiceAnswerResource;
import com.examsystem.core.resources.questionanswer.freetext.FreetextAnswerResource;
import com.examsystem.core.resources.questionanswer.join.JoinAnswerResource;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.*;

import java.util.Date;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = SingleSimpleChoiceAnswerResource.class, name = "single-simple-choice"),
        @JsonSubTypes.Type(value = MultipleSimpleChoiceAnswerResource.class, name = "multiple-simple-choice"),
        @JsonSubTypes.Type(value = SingleComplexChoiceAnswerResource.class, name = "single-complex-choice"),
        @JsonSubTypes.Type(value = MultipleComplexChoiceAnswerResource.class, name = "multiple-complex-choice"),
        @JsonSubTypes.Type(value = FreetextAnswerResource.class, name = "freetext"),
        @JsonSubTypes.Type(value = JoinAnswerResource.class, name = "join")
})
public abstract class QuestionAnswerResource {

    private Long id;

    private Long testAnswerId;

    private Long questionId;

    private Date startedOn;

    private Date finishedOn;

    private Integer points;

    private Integer maxPoints;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTestAnswerId() {
        return testAnswerId;
    }

    public void setTestAnswerId(Long testAnswerId) {
        this.testAnswerId = testAnswerId;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public Date getStartedOn() {
        return startedOn;
    }

    public void setStartedOn(Date startedOn) {
        this.startedOn = startedOn;
    }

    public Date getFinishedOn() {
        return finishedOn;
    }

    public void setFinishedOn(Date finishedOn) {
        this.finishedOn = finishedOn;
    }

    @JsonIgnore
    abstract public QuestionType getQuestionType();

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
    }
}
