package com.examsystem.core.resources.question.choice.complex.single;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.question.choice.complex.ComplexChoiceQuestionResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class SingleComplexChoiceQuestionResource extends ComplexChoiceQuestionResource {

    public QuestionType getQuestionType() {
        return QuestionType.SIMPLE_COMPLEX_CHOICE;
    }

}
