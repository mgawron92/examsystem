package com.examsystem.core.resources.question.choice.complex.multiple;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.question.choice.complex.ComplexChoiceQuestionResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class MultipleComplexChoiceQuestionResource extends ComplexChoiceQuestionResource {

    public QuestionType getQuestionType() {
        return QuestionType.MULTIPLE_COMPLEX_CHOICE;
    }

}
