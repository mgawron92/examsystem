package com.examsystem.core.resources.questionanswer.choice.complex;

import com.examsystem.core.resources.questionanswer.choice.ChoiceAnswerPartResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class ComplexChoiceAnswerPartResource extends ChoiceAnswerPartResource {

    private Integer points;

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

}
