package com.examsystem.core.resources.question.choice;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public abstract class ChoicePatternResource {

    private Long id;

    private Long choiceQuestionId;

    private List<Long> choiceAnswerPartIds = new LinkedList<>();

    private String label;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChoiceQuestionId() {
        return choiceQuestionId;
    }

    public void setChoiceQuestionId(Long choiceQuestionId) {
        this.choiceQuestionId = choiceQuestionId;
    }

    public List<Long> getChoiceAnswerPartIds() {
        return choiceAnswerPartIds;
    }

    public void setChoiceAnswerPartIds(List<Long> choiceAnswerPartIds) {
        this.choiceAnswerPartIds = choiceAnswerPartIds;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
