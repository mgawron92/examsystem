package com.examsystem.core.resources.testanswer;

import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class QuestionSequenceResource {

    private Long id;

    private Long testAnswerId;

    private List<Long> sequence;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTestAnswerId() {
        return testAnswerId;
    }

    public void setTestAnswerId(Long testAnswerId) {
        this.testAnswerId = testAnswerId;
    }

    public List<Long> getSequence() {
        return sequence;
    }

    public void setSequence(List<Long> sequence) {
        this.sequence = sequence;
    }

}
