package com.examsystem.core.resources.questionanswer.join;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class JoinAnswerPartResource {

    private Long id;

    private Long joinPatternId;

    private Long joinAnswerId;

    private String firstOption;

    private String secondOption;

    private Boolean correct;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getJoinPatternId() {
        return joinPatternId;
    }

    public void setJoinPatternId(Long joinPatternId) {
        this.joinPatternId = joinPatternId;
    }

    public Long getJoinAnswerId() {
        return joinAnswerId;
    }

    public void setJoinAnswerId(Long joinAnswerId) {
        this.joinAnswerId = joinAnswerId;
    }

    public String getFirstOption() {
        return firstOption;
    }

    public void setFirstOption(String firstOption) {
        this.firstOption = firstOption;
    }

    public String getSecondOption() {
        return secondOption;
    }

    public void setSecondOption(String secondOption) {
        this.secondOption = secondOption;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }
}
