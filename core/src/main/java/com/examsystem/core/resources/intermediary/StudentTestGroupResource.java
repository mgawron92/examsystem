package com.examsystem.core.resources.intermediary;

import java.util.Date;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class StudentTestGroupResource {

    private Long id;

    private Long studentId;

    private Long testGroupId;

    private Date joinDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTestGroupId() {
        return testGroupId;
    }

    public void setTestGroupId(Long testGroupId) {
        this.testGroupId = testGroupId;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

}
