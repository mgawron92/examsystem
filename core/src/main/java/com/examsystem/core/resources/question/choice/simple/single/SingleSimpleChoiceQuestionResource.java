package com.examsystem.core.resources.question.choice.simple.single;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.question.choice.simple.SimpleChoiceQuestionResource;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class SingleSimpleChoiceQuestionResource extends SimpleChoiceQuestionResource {

    public QuestionType getQuestionType() {
        return QuestionType.SINGLE_SIMPLE_CHOICE;
    }

}
