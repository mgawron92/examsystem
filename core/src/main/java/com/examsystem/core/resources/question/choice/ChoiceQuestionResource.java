package com.examsystem.core.resources.question.choice;

import com.examsystem.core.resources.question.QuestionResource;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public abstract class ChoiceQuestionResource extends QuestionResource {

    protected List<Long> choiceAnswerIds = new LinkedList<>();

    private Boolean multiple;

    public List<Long> getChoiceAnswerIds() {
        return choiceAnswerIds;
    }

    public void setChoiceAnswerIds(List<Long> choiceAnswerIds) {
        this.choiceAnswerIds = choiceAnswerIds;
    }

    public Boolean getMultiple() {
        return multiple;
    }

    public void setMultiple(Boolean multiple) {
        this.multiple = multiple;
    }

}
