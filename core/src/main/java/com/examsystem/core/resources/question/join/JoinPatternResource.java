package com.examsystem.core.resources.question.join;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class JoinPatternResource {

    private Long id;

    private Long joinQuestionId;

    private List<Long> joinAnswerPartIds = new LinkedList<>();

    private String firstOption;

    private String secondOption;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getJoinQuestionId() {
        return joinQuestionId;
    }

    public void setJoinQuestionId(Long joinQuestionId) {
        this.joinQuestionId = joinQuestionId;
    }

    public List<Long> getJoinAnswerPartIds() {
        return joinAnswerPartIds;
    }

    public void setJoinAnswerPartIds(List<Long> joinAnswerPartIds) {
        this.joinAnswerPartIds = joinAnswerPartIds;
    }

    public String getFirstOption() {
        return firstOption;
    }

    public void setFirstOption(String firstOption) {
        this.firstOption = firstOption;
    }

    public String getSecondOption() {
        return secondOption;
    }

    public void setSecondOption(String secondOption) {
        this.secondOption = secondOption;
    }

}
