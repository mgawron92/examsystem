package com.examsystem.core.resources.questionanswer.join;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.resources.questionanswer.QuestionAnswerResource;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public class JoinAnswerResource extends QuestionAnswerResource {

    private Long joinQuestionId;

    private List<JoinAnswerPartResource> joinAnswerParts = new LinkedList<>();

    public Long getJoinQuestionId() {
        return joinQuestionId;
    }

    public void setJoinQuestionId(Long joinQuestionId) {
        this.joinQuestionId = joinQuestionId;
    }

    public List<JoinAnswerPartResource> getJoinAnswerParts() {
        return joinAnswerParts;
    }

    public void setJoinAnswerParts(List<JoinAnswerPartResource> joinAnswerParts) {
        this.joinAnswerParts = joinAnswerParts;
    }

    public QuestionType getQuestionType() {
        return QuestionType.JOIN;
    }

}
