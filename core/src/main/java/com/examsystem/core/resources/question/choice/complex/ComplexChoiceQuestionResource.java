package com.examsystem.core.resources.question.choice.complex;

import com.examsystem.core.resources.question.choice.ChoiceQuestionResource;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-09.
 */
public abstract class ComplexChoiceQuestionResource extends ChoiceQuestionResource {

    protected List<ComplexChoicePatternResource> choicePatterns = new LinkedList<>();

    public List<ComplexChoicePatternResource> getChoicePatterns() {
        return choicePatterns;
    }

    public void setChoicePatterns(List<ComplexChoicePatternResource> choicePatterns) {
        this.choicePatterns = choicePatterns;
    }

}
