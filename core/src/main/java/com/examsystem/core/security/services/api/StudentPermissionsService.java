package com.examsystem.core.security.services.api;

import com.examsystem.core.resources.questionanswer.QuestionAnswerResource;
import com.examsystem.core.resources.testanswer.TestAnswerResource;

/**
 * Created by Mateusz Gawron on 2016-06-31.
 */
public interface StudentPermissionsService {

    boolean isCurrentUser(Long examinerId);

    boolean isMemberOfExaminersTestGroup(Long examinerId);

    boolean isStudentTestGroupOwner(Long studentTestGroupId);

    boolean hasAccessToTest(Long testId);

    boolean hasAccessToImage(Long imageId);

    boolean isTestGroupMember(Long testGroupId);

    boolean hasAccessToTestTestGroup(Long testTestGroupId);

    boolean hasAccessToTestAnswer(Long testAnswerId);

    boolean hasAccessToQuestionSequence(Long questionSequenceId);

    boolean answeredPreviousQuestions(Long testId, Long questionId);

    boolean finishedTest(Long testId);

    boolean testTimeoutExceeded(Long testId);

    boolean addedPreviousQuestionAnswersForQuestion(Long testAnswerId, Long questionId);

    boolean addedPreviousQuestionAnswersForAnswer(Long testAnswerId, Long questionAnswerId);

    boolean finishedTestAnswer(Long testAnswerId);

    boolean testAnswerTimeoutExceeded(Long testAnswerId);

    boolean testAnswerNotStartedYet(TestAnswerResource testAnswer);

    boolean testAnswerNotFinishedYet(TestAnswerResource testAnswer);

    boolean testAnswerTimeoutNotExceeded(TestAnswerResource testAnswer);

    boolean questionAnswerNotStartedYet(Long testAnswerId, QuestionAnswerResource questionAnswer);

    boolean questionAnswerNotFinishedYet(Long testAnswerId, QuestionAnswerResource questionAnswer);

    boolean questionAnswerTimeoutNotExceeded(Long testAnswerId, QuestionAnswerResource questionAnswer);

}
