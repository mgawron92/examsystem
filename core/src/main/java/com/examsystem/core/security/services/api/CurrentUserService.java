package com.examsystem.core.security.services.api;

import com.examsystem.core.resources.user.PersonResource;

/**
 * Created by Mateusz Gawron on 2016-06-31.
 */
public interface CurrentUserService {

    PersonResource getCurrentUser();

}
