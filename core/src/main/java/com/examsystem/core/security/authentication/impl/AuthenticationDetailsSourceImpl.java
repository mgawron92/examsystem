package com.examsystem.core.security.authentication.impl;

import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.security.authentication.api.AuthenticationDetails;
import org.springframework.security.authentication.AuthenticationDetailsSource;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-03-16.
 */
public class AuthenticationDetailsSourceImpl implements AuthenticationDetailsSource<HttpServletRequest, AuthenticationDetails> {

    public static final String ROLE_KEY = "role";

    @Override
    public AuthenticationDetails buildDetails(HttpServletRequest context) {
        Map<String, String[]> contextParameterMap = context.getParameterMap();
        String roleString = contextParameterMap.get(ROLE_KEY)[0];
        return new AuthenticationDetailsImpl(UserRole.valueOf(roleString));
    }

}
