package com.examsystem.core.security.ldap.providers.impl;

import com.examsystem.core.security.ldap.providers.api.LdapAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

/**
 * Created by Mateusz on 2016-03-29.
 */
@Component
public class LdapAuthenticationProviderImpl implements LdapAuthenticationProvider {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return authenticationManager.authenticate(authentication);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

}
