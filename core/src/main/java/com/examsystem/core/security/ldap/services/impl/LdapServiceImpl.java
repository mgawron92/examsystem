package com.examsystem.core.security.ldap.services.impl;

import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.resources.user.TypeaheadResource;
import com.examsystem.core.security.ldap.mappers.api.PersonContextMapper;
import com.examsystem.core.security.ldap.mappers.api.TypeaheadContextMapper;
import com.examsystem.core.security.ldap.services.api.LdapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-05-02.
 */
@Service
public class LdapServiceImpl implements LdapService {

    @Autowired
    @Lazy
    private LdapTemplate ldapTemplate;

    @Autowired
    private PersonContextMapper personContextMapper;

    @Autowired
    private TypeaheadContextMapper typeaheadContextMapper;

    public boolean authenticateLdapUser(String username, String password){
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter("objectclass", "Person")).and(new EqualsFilter("uid", username));
        return ldapTemplate.authenticate("", filter.toString(), password);
    }

    public void populateResourceWithLdapData(String username, PersonResource resource) {
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter("objectclass", "Person")).and(new EqualsFilter("uid", username));
        Map<String, String> attributes = ldapTemplate.searchForObject("", filter.toString(), personContextMapper);
        resource.setLdapLogin(attributes.get("ldapLogin"));
        resource.setName(attributes.get("name"));
        resource.setSurname(attributes.get("surname"));
        resource.setEmail(attributes.get("email"));
    }

    public List<TypeaheadResource> getAvailableLdapUsers(){
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter("objectclass", "Person"));
        List<Map<String, String>> attributesList = ldapTemplate.search("", filter.toString(), typeaheadContextMapper);
        List<TypeaheadResource> typeaheadResources = new LinkedList<>();
        for (Map<String, String> attributes : attributesList) {
            TypeaheadResource typeaheadResource = new TypeaheadResource();
            typeaheadResource.setLdapLogin(attributes.get("ldapLogin"));
            typeaheadResource.setName(attributes.get("name"));
            typeaheadResource.setSurname(attributes.get("surname"));
            typeaheadResource.setEmail(attributes.get("email"));
            typeaheadResources.add(typeaheadResource);
        }
        return typeaheadResources;
    }

}
