package com.examsystem.core.security.ldap.providers.api;

import org.springframework.security.authentication.AuthenticationProvider;

/**
 * Created by Mateusz on 2016-03-29.
 */
public interface LdapAuthenticationProvider extends AuthenticationProvider {

}
