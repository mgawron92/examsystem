package com.examsystem.core.security.services.impl;

import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.providers.api.QuestionAnswerServiceProvider;
import com.examsystem.core.providers.api.QuestionServiceProvider;
import com.examsystem.core.resources.intermediary.StudentTestGroupResource;
import com.examsystem.core.resources.question.QuestionResource;
import com.examsystem.core.resources.questionanswer.QuestionAnswerResource;
import com.examsystem.core.resources.test.TestResource;
import com.examsystem.core.resources.testanswer.QuestionSequenceResource;
import com.examsystem.core.resources.testanswer.TestAnswerResource;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.security.services.api.CurrentUserService;
import com.examsystem.core.security.services.api.StudentPermissionsService;
import com.examsystem.core.services.api.intermediary.StudentTestGroupService;
import com.examsystem.core.services.api.intermediary.TestTestGroupService;
import com.examsystem.core.services.api.other.ImageService;
import com.examsystem.core.services.api.question.QuestionService;
import com.examsystem.core.services.api.questionanswer.QuestionAnswerService;
import com.examsystem.core.services.api.test.TestService;
import com.examsystem.core.services.api.testanswer.QuestionSequenceService;
import com.examsystem.core.services.api.testanswer.TestAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Mateusz Gawron on 2016-06-31.
 */
@Service("studentPermissionsService")
public class StudentPermissionsServiceImpl implements StudentPermissionsService {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private StudentTestGroupService studentTestGroupService;

    @Autowired
    private TestTestGroupService testTestGroupService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private TestService testService;

    @Autowired
    private TestAnswerService testAnswerService;

    @Autowired
    private QuestionSequenceService questionSequenceService;

    @Autowired
    private QuestionServiceProvider questionServiceProvider;

    @Autowired
    private QuestionAnswerServiceProvider questionAnswerServiceProvider;

    @Override
    public boolean isCurrentUser(Long examinerId) {
        PersonResource currentUser = currentUserService.getCurrentUser();
        return currentUser.getId().equals(examinerId);
    }

    @Override
    public boolean isMemberOfExaminersTestGroup(Long examinerId) {
        Long studentId = currentUserService.getCurrentUser().getId();
        Map<String, Object> params = new HashMap<>();
        params.put("studentId", studentId);
        params.put("examinerId", examinerId);
        List<StudentTestGroupResource> studentTestGroups = studentTestGroupService.query(params);
        return (studentTestGroups.size() > 0);
    }

    @Override
    public boolean isStudentTestGroupOwner(Long studentTestGroupId) {
        PersonResource currentUser = currentUserService.getCurrentUser();
        return studentTestGroupService.getStudentTestGroupById(studentTestGroupId).getStudentId().equals(currentUser.getId());
    }

    @Override
    public boolean hasAccessToTest(Long testId) {
        PersonResource currentUser = currentUserService.getCurrentUser();
        Map<String, Object> params = new HashMap<>();
        params.put("studentId", currentUser.getId());
        params.put("testId", testId);
        return (testTestGroupService.query(params).size() > 0);
    }

    @Override
    public boolean hasAccessToImage(Long imageId) {
        List<Long> testIds = imageService.getTestIdsByImageId(imageId);
        for (Long testId : testIds) {
            if (hasAccessToTest(testId)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isTestGroupMember(Long testGroupId) {
        Long studentId = currentUserService.getCurrentUser().getId();
        Map<String, Object> params = new HashMap<>();
        params.put("studentId", studentId);
        params.put("testGroupId", testGroupId);
        List<StudentTestGroupResource> studentTestGroups = studentTestGroupService.query(params);
        return (studentTestGroups.size() > 0);
    }

    @Override
    public boolean hasAccessToTestTestGroup(Long testTestGroupId) {
        return hasAccessToTest(testTestGroupService.getTestTestGroupById(testTestGroupId).getTestId());
    }

    @Override
    public boolean hasAccessToTestAnswer(Long testAnswerId) {
        return hasAccessToTest(testAnswerService.getTestAnswerById(testAnswerId).getTestId());
    }

    @Override
    public boolean hasAccessToQuestionSequence(Long questionSequenceId) {
        return hasAccessToTestAnswer(questionSequenceService.getQuestionSequenceById(questionSequenceId).getTestAnswerId());
    }

    @Override
    public boolean answeredPreviousQuestions(Long testId, Long questionId) {
        Long studentId = currentUserService.getCurrentUser().getId();
        Map<String, Object> params = new HashMap<>();
        params.put("studentId", studentId);
        params.put("testId", testId);
        List<TestAnswerResource> testAnswers = testAnswerService.query(params);
        if (testAnswers.size() == 0) {
            return false;
        }
        TestAnswerResource testAnswer = testAnswers.get(0);
        Long questionSequenceId = testAnswer.getQuestionSequenceId();
        QuestionSequenceResource questionSequence = questionSequenceService.getQuestionSequenceById(questionSequenceId);
        List<Long> questionIds = questionSequence.getSequence();
        List<Long> questionAnswerIds = testAnswer.getQuestionAnswerIds();
        int questionPositionInSequence = questionIds.indexOf(questionId);
        boolean questionExistsInSequence = (questionPositionInSequence >= 0);
        boolean allPreviousQuestionsAreAnswered = (questionPositionInSequence <= questionAnswerIds.size());
        return (questionExistsInSequence && allPreviousQuestionsAreAnswered);
    }

    @Override
    public boolean finishedTest(Long testId) {
        Long studentId = currentUserService.getCurrentUser().getId();
        Map<String, Object> params = new HashMap<>();
        params.put("studentId", studentId);
        params.put("testId", testId);
        List<TestAnswerResource> testAnswers = testAnswerService.query(params);
        if (testAnswers.size() == 0) {
            return false;
        }
        TestAnswerResource testAnswer = testAnswers.get(0);
        return (testAnswer.getFinishedOn() != null);
    }

    @Override
    public boolean testTimeoutExceeded(Long testId) {
        Long studentId = currentUserService.getCurrentUser().getId();
        Map<String, Object> params = new HashMap<>();
        params.put("studentId", studentId);
        params.put("testId", testId);
        List<TestAnswerResource> testAnswers = testAnswerService.query(params);
        if (testAnswers.size() == 0) {
            return false;
        }
        TestAnswerResource testAnswer = testAnswers.get(0);
        TestResource test = testService.getTestById(testId);
        if (!test.getHasTimeLimit()) {
            return false;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(testAnswer.getStartedOn());
        calendar.add(Calendar.MINUTE, test.getTimeLimit());
        Date maxFinishDate = calendar.getTime();
        Date now = Calendar.getInstance().getTime();
        return now.after(maxFinishDate);
    }

    @Override
    public boolean addedPreviousQuestionAnswersForQuestion(Long testAnswerId, Long questionId) {
        Long testId = testAnswerService.getTestAnswerById(testAnswerId).getTestId();
        return answeredPreviousQuestions(testId, questionId);
    }

    @Override
    public boolean addedPreviousQuestionAnswersForAnswer(Long testAnswerId, Long questionAnswerId) {
        QuestionType questionType = testAnswerService.getQuestionType(testAnswerId, questionAnswerId);
        QuestionAnswerService questionAnswerService = questionAnswerServiceProvider.getQuestionAnswerService(questionType);
        QuestionAnswerResource questionAnswer = questionAnswerService.getQuestionAnswerById(questionAnswerId);
        Long questionId = questionAnswer.getQuestionId();
        return addedPreviousQuestionAnswersForQuestion(testAnswerId, questionId);
    }

    @Override
    public boolean finishedTestAnswer(Long testAnswerId) {
        Long testId = testAnswerService.getTestAnswerById(testAnswerId).getTestId();
        return finishedTest(testId);
    }

    @Override
    public boolean testAnswerTimeoutExceeded(Long testAnswerId) {
        Long testId = testAnswerService.getTestAnswerById(testAnswerId).getTestId();
        return testTimeoutExceeded(testId);
    }

    @Override
    public boolean testAnswerNotStartedYet(TestAnswerResource testAnswer) {
        if (testAnswer.getStartedOn().after(Calendar.getInstance().getTime())) {
            return false;
        }
        if (testAnswer.getFinishedOn() != null) {
            return false;
        }
        Map<String, Object> params = new HashMap<>();
        params.put("testId", testAnswer.getTestId());
        params.put("studentId", currentUserService.getCurrentUser().getId());
        return (testAnswerService.query(params).size() == 0);
    }

    @Override
    public boolean testAnswerNotFinishedYet(TestAnswerResource testAnswer) {
        TestAnswerResource testAnswerResource = testAnswerService.getTestAnswerById(testAnswer.getId());
        if (!testAnswer.getStartedOn().equals(testAnswerResource.getStartedOn())) {
            return false;
        }
        return (testAnswerResource.getFinishedOn() == null);
    }

    @Override
    public boolean testAnswerTimeoutNotExceeded(TestAnswerResource testAnswer) {
        TestResource test = testService.getTestById(testAnswer.getTestId());
        if (testAnswer.getFinishedOn() == null || testAnswer.getStartedOn() == null) {
            return false;
        }
        if (testAnswer.getFinishedOn().after(Calendar.getInstance().getTime())) {
            return false;
        }
        if (test.getHasTimeLimit()) {
            Long timeSpent = Calendar.getInstance().getTime().getTime() - testAnswer.getStartedOn().getTime();
            Long timeLimit = test.getTimeLimit() * 60L * 1000L;
            Long timeBuffer = 5L * 1000L;
            return (timeSpent < timeLimit + timeBuffer);
        } else {
            return true;
        }
    }

    @Override
    public boolean questionAnswerNotStartedYet(Long testAnswerId, QuestionAnswerResource questionAnswer) {
        if (questionAnswer.getStartedOn().after(Calendar.getInstance().getTime())) {
            return false;
        }
        if (questionAnswer.getFinishedOn() != null) {
            return false;
        }
        TestAnswerResource testAnswer = testAnswerService.getTestAnswerById(testAnswerId);
        Long questionSequenceId = testAnswer.getQuestionSequenceId();
        QuestionSequenceResource questionSequence = questionSequenceService.getQuestionSequenceById(questionSequenceId);
        List<Long> questionIds = questionSequence.getSequence();
        List<Long> questionAnswerIds = testAnswer.getQuestionAnswerIds();
        int questionPositionInSequence = questionIds.indexOf(questionAnswer.getQuestionId());
        boolean questionExistsInSequence = (questionPositionInSequence >= 0);
        boolean isNextQuestion = (questionPositionInSequence == questionAnswerIds.size());
        return (questionExistsInSequence && isNextQuestion);
    }

    @Override
    public boolean questionAnswerNotFinishedYet(Long testAnswerId, QuestionAnswerResource questionAnswer) {
        QuestionType questionType = testAnswerService.getQuestionType(testAnswerId, questionAnswer.getId());
        QuestionAnswerService questionAnswerService = questionAnswerServiceProvider.getQuestionAnswerService(questionType);
        QuestionAnswerResource questionAnswerResource = questionAnswerService.getQuestionAnswerById(questionAnswer.getId());
        if (!questionAnswer.getStartedOn().equals(questionAnswerResource.getStartedOn())) {
            return false;
        }
        return (questionAnswerResource.getFinishedOn() == null);
    }

    @Override
    public boolean questionAnswerTimeoutNotExceeded(Long testAnswerId, QuestionAnswerResource questionAnswer) {
        TestAnswerResource testAnswer = testAnswerService.getTestAnswerById(testAnswerId);
        TestResource test = testService.getTestById(testAnswer.getTestId());
        Long testId = test.getId();
        Long questionId = questionAnswer.getQuestionId();
        QuestionType questionType = testService.getQuestionType(testId, questionId);
        QuestionService questionService = questionServiceProvider.getQuestionService(questionType);
        QuestionResource question = questionService.getQuestionById(questionId);
        if (questionAnswer.getFinishedOn() == null || questionAnswer.getStartedOn() == null) {
            return false;
        }
        if (questionAnswer.getFinishedOn().after(Calendar.getInstance().getTime())) {
            return false;
        }
        if (question.getHasTimeLimit()) {
            Long timeSpent = Calendar.getInstance().getTime().getTime() - questionAnswer.getStartedOn().getTime();
            Long timeLimit = (question.getTimeLimitMinutes() * 60L + question.getTimeLimitSeconds()) * 1000L;
            Long timeBuffer = 5L * 1000L;
            return (timeSpent < timeLimit + timeBuffer);
        } else {
            return true;
        }
    }

}
