package com.examsystem.core.security.ldap.services.api;

import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.resources.user.TypeaheadResource;

import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-05-02.
 */
public interface LdapService {

    boolean authenticateLdapUser(String username, String password);

    void populateResourceWithLdapData(String username, PersonResource resource);

    List<TypeaheadResource> getAvailableLdapUsers();

}
