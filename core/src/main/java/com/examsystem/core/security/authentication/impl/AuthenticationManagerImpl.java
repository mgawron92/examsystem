package com.examsystem.core.security.authentication.impl;

/**
 * Created by Mateusz Gawron on 2016-03-16.
 */

import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.resources.user.AdminResource;
import com.examsystem.core.resources.user.ExaminerResource;
import com.examsystem.core.resources.user.StudentResource;
import com.examsystem.core.security.authentication.api.AuthenticationDetails;
import com.examsystem.core.security.ldap.services.api.LdapService;
import com.examsystem.core.services.api.user.AdminService;
import com.examsystem.core.services.api.user.ExaminerService;
import com.examsystem.core.services.api.user.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class AuthenticationManagerImpl implements AuthenticationManager {

    @Autowired
    private StudentService studentService;

    @Autowired
    private ExaminerService examinerService;

    @Autowired
    private AdminService adminService;

    @Autowired
    private LdapService ldapServices;

    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        AuthenticationDetails authenticationDetails = (AuthenticationDetails) authentication.getDetails();
        String ldapLogin = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();
        UserRole role = authenticationDetails.getUserRole();

        boolean ldapAuthenticated = ldapServices.authenticateLdapUser(ldapLogin, password);
        if (!ldapAuthenticated) {
            throw new BadCredentialsException("Invalid LDAP credentials");
        }

        String mappedLdapLogin;
        List<GrantedAuthority> authorities = new LinkedList<>();
        boolean userIsActive = false;
        switch(role) {
            case STUDENT:
                mappedLdapLogin = studentService.mapLdapLogin(ldapLogin);
                StudentResource studentResource = studentService.getByLdapLogin(mappedLdapLogin);

                if (studentResource != null) {
                    userIsActive = studentResource.getActive();
                } else {
                    studentResource = new StudentResource();
                    ldapServices.populateResourceWithLdapData(ldapLogin, studentResource);
                    studentResource.setActive(true);
                    studentService.save(studentResource);
                    mappedLdapLogin = studentResource.getLdapLogin();
                    userIsActive = true;
                }
                break;
            case EXAMINER:
                mappedLdapLogin = examinerService.mapLdapLogin(ldapLogin);
                ExaminerResource examinerResource = examinerService.getByLdapLogin(mappedLdapLogin);
                if (examinerResource != null) {
                    userIsActive = examinerResource.getActive();
                }
                break;
            case ADMIN:
                mappedLdapLogin = adminService.mapLdapLogin(ldapLogin);
                AdminResource adminResource = adminService.getByLdapLogin(mappedLdapLogin);
                if (adminResource != null) {
                    userIsActive = adminResource.getActive();
                }
                break;
            default:
                mappedLdapLogin = "";
        }

        if (userIsActive) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.toString()));
        }

        if (authorities.size() == 0) {
            throw new BadCredentialsException("User does not have appropriate authority");
        }

        UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(mappedLdapLogin, password, authorities);
        newAuthentication.setDetails(authenticationDetails);
        return newAuthentication;
    }

}