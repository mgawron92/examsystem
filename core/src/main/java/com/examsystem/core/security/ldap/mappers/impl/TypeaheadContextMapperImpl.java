package com.examsystem.core.security.ldap.mappers.impl;

import com.examsystem.core.security.ldap.mappers.api.TypeaheadContextMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mateusz on 2016-03-28.
 */

@Component
public class TypeaheadContextMapperImpl implements TypeaheadContextMapper {

    @Override
    public Map<String, String> mapFromContext(Object o) throws javax.naming.NamingException {
        DirContextAdapter context = (DirContextAdapter)o;
        Map<String, String> attributes = new HashMap<>();
        attributes.put("ldapLogin", (String)context.getObjectAttribute("uid"));
        attributes.put("name", (String)context.getObjectAttribute("givenname"));
        attributes.put("surname", (String)context.getObjectAttribute("sn"));
        attributes.put("email", (String)context.getObjectAttribute("mail"));
        return attributes;
    }

}