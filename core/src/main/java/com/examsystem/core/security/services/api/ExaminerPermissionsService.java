package com.examsystem.core.security.services.api;

/**
 * Created by Mateusz Gawron on 2016-06-31.
 */
public interface ExaminerPermissionsService {

    boolean isCurrentUser(Long examinerId);

    boolean isImageOwner(Long imageId);

    boolean isTestGroupOwner(Long testGroupId);

    boolean isStudentTestGroupOwner(Long studentTestGroupId);

    boolean hasAccessToTest(Long testId);

    boolean hasAccessToTestTestGroup(Long testTestGroupId);

    boolean hasAccessToTestAnswer(Long testAnswerId);

    boolean hasAccessToQuestionSequence(Long questionSequenceId);

}
