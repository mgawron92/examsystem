package com.examsystem.core.security.ldap.mappers.api;

import org.springframework.ldap.core.ContextMapper;

import java.util.Map;

/**
 * Created by Mateusz Gawron on 2016-07-16.
 */
public interface PersonContextMapper extends ContextMapper<Map<String, String>> {

}
