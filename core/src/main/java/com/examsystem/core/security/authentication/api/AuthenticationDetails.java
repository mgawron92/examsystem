package com.examsystem.core.security.authentication.api;

import com.examsystem.core.enumerations.UserRole;

/**
 * Created by Mateusz Gawron on 2016-03-16.
 */
public interface AuthenticationDetails {

    void setUserRole(UserRole userRole);

    UserRole getUserRole();

}
