package com.examsystem.core.security.ldap.mappers.impl;

import com.examsystem.core.security.ldap.mappers.api.PersonContextMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.stereotype.Component;

import javax.naming.NamingException;
import java.util.Map;
import java.util.HashMap;

/**
 * Created by Mateusz Gawron on 2016-04-17.
 */
@Component
public class PersonContextMapperImpl implements PersonContextMapper {

    @Override
    public Map<String, String> mapFromContext(Object o) throws NamingException {
        DirContextAdapter context = (DirContextAdapter)o;
        Map<String, String> attributes = new HashMap<>();
        attributes.put("ldapLogin", (String)context.getObjectAttribute("uid"));
        attributes.put("name", (String)context.getObjectAttribute("givenname"));
        attributes.put("surname", (String)context.getObjectAttribute("sn"));
        attributes.put("email", (String)context.getObjectAttribute("mail"));
        return attributes;
    }

}
