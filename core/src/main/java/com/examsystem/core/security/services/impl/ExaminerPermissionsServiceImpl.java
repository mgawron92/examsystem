package com.examsystem.core.security.services.impl;

import com.examsystem.core.repositories.ImageRepository;
import com.examsystem.core.resources.test.TestGroupResource;
import com.examsystem.core.resources.test.TestResource;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.security.services.api.CurrentUserService;
import com.examsystem.core.security.services.api.ExaminerPermissionsService;
import com.examsystem.core.services.api.intermediary.StudentTestGroupService;
import com.examsystem.core.services.api.intermediary.TestTestGroupService;
import com.examsystem.core.services.api.test.TestGroupService;
import com.examsystem.core.services.api.test.TestService;
import com.examsystem.core.services.api.testanswer.QuestionSequenceService;
import com.examsystem.core.services.api.testanswer.TestAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Mateusz Gawron on 2016-06-31.
 */
@Service("examinerPermissionsService")
public class ExaminerPermissionsServiceImpl implements ExaminerPermissionsService {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private TestGroupService testGroupService;

    @Autowired
    private StudentTestGroupService studentTestGroupService;

    @Autowired
    private TestService testService;

    @Autowired
    private TestTestGroupService testTestGroupService;

    @Autowired
    private TestAnswerService testAnswerService;

    @Autowired
    private QuestionSequenceService questionSequenceService;

    @Override
    public boolean isCurrentUser(Long examinerId) {
        PersonResource currentUser = currentUserService.getCurrentUser();
        return currentUser.getId().equals(examinerId);
    }

    @Override
    public boolean isImageOwner(Long imageId) {
        PersonResource currentUser = currentUserService.getCurrentUser();
        return imageRepository.findImageById(imageId).getExaminer().getId().equals(currentUser.getId());
    }

    @Override
    public boolean isTestGroupOwner(Long testGroupId) {
        PersonResource currentUser = currentUserService.getCurrentUser();
        TestGroupResource testGroup = testGroupService.getTestGroupById(testGroupId);
        return testGroup.getExaminerId().equals(currentUser.getId());
    }

    @Override
    public boolean isStudentTestGroupOwner(Long studentTestGroupId) {
        PersonResource currentUser = currentUserService.getCurrentUser();
        Long testGroupId = studentTestGroupService.getStudentTestGroupById(studentTestGroupId).getTestGroupId();
        return testGroupService.getTestGroupById(testGroupId).getExaminerId().equals(currentUser.getId());
    }

    @Override
    public boolean hasAccessToTest(Long testId) {
        PersonResource currentUser = currentUserService.getCurrentUser();
        TestResource test = testService.getTestById(testId);
        return test.getExaminerId().equals(currentUser.getId());
    }

    @Override
    public boolean hasAccessToTestTestGroup(Long testTestGroupId) {
        return hasAccessToTest(testTestGroupService.getTestTestGroupById(testTestGroupId).getTestId());
    }

    @Override
    public boolean hasAccessToTestAnswer(Long testAnswerId) {
        return hasAccessToTest(testAnswerService.getTestAnswerById(testAnswerId).getTestId());
    }

    @Override
    public boolean hasAccessToQuestionSequence(Long questionSequenceId) {
        return hasAccessToTestAnswer(questionSequenceService.getQuestionSequenceById(questionSequenceId).getTestAnswerId());
    }

}
