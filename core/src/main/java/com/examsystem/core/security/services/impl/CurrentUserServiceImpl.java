package com.examsystem.core.security.services.impl;

import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.providers.api.UserServiceProvider;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.security.authentication.api.AuthenticationDetails;
import com.examsystem.core.security.services.api.CurrentUserService;
import com.examsystem.core.services.api.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * Created by Mateusz Gawron on 2016-06-31.
 */
@Service
public class CurrentUserServiceImpl implements CurrentUserService {

    @Autowired
    private UserServiceProvider userServiceProvider;

    @Override
    public PersonResource getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !(authentication.getDetails() instanceof AuthenticationDetails)) {
            return null;
        }
        AuthenticationDetails authenticationDetails = (AuthenticationDetails) authentication.getDetails();
        String ldapLogin = authentication.getName();
        UserRole userRole = authenticationDetails.getUserRole();
        UserService<? extends PersonResource> userService = userServiceProvider.getUserService(userRole);
        return userService.getByLdapLogin(ldapLogin);
    }

}
