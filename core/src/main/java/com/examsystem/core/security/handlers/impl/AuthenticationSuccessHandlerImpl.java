package com.examsystem.core.security.handlers.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

/**
 * Created by Mateusz Gawron on 2016-04-03.
 */
@Component
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationSuccessHandlerImpl.class);
    private final ObjectMapper mapper;

    @Autowired
    public AuthenticationSuccessHandlerImpl(MappingJackson2HttpMessageConverter messageConverter) {
        this.mapper = messageConverter.getObjectMapper();
    }

    @Override
    public void onAuthenticationSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication
    ) throws IOException, ServletException {
        response.setStatus(HttpServletResponse.SC_OK);
        String authority = "";
        for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
            authority = grantedAuthority.getAuthority();
            if (authority.contains("_")) {
                authority = authority.split("_")[1].toLowerCase();
            }
        }
        LOGGER.info("User connected [ldapLogin: " + authentication.getPrincipal() + ", role: " + authority + "]");
        PrintWriter writer = response.getWriter();
        mapper.writeValue(writer, authentication.getPrincipal());
        writer.flush();
    }

}
