package com.examsystem.core.security.authentication.impl;

import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.security.authentication.api.AuthenticationDetails;

/**
 * Created by Mateusz Gawron on 2016-03-16.
 */
public class AuthenticationDetailsImpl implements AuthenticationDetails {

    private UserRole userRole;

    public AuthenticationDetailsImpl(UserRole userRole) {
        setUserRole(userRole);
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public UserRole getUserRole() {
        return userRole;
    }

}
