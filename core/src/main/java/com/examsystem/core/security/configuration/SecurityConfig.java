package com.examsystem.core.security.configuration;

/**
 * Created by Mateusz Gawron on 2016-03-16.
 */

import com.examsystem.core.enumerations.UserRole;
import com.examsystem.core.security.authentication.api.AuthenticationDetails;
import com.examsystem.core.security.authentication.impl.AuthenticationDetailsSourceImpl;
import com.examsystem.core.security.filters.CsrfHeaderFilter;
import com.examsystem.core.security.handlers.impl.LogoutSuccessHandlerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpServletRequest;

@Configuration
@ComponentScan(basePackages = "com.examsystem.core.security")
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, proxyTargetClass = true)
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    private AuthenticationSuccessHandler authSuccessHandler;

    @Autowired
    private AuthenticationFailureHandler authFailureHandler;

    @Autowired
    private LogoutSuccessHandlerImpl logoutSuccessHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .exceptionHandling()
            .authenticationEntryPoint(authenticationEntryPoint)
            .and()
                .formLogin()
                .loginProcessingUrl("/login")
                .authenticationDetailsSource(getAuthDetailsSource())
                .successHandler(authSuccessHandler)
                .failureHandler(authFailureHandler)
                .permitAll()
            .and()
                .authorizeRequests()
                .antMatchers("/rest/users/me")
                .permitAll()
            .and()
                .authorizeRequests()
                .antMatchers("/rest/**")
                .hasAnyAuthority("ROLE_" + UserRole.STUDENT, "ROLE_" + UserRole.EXAMINER, "ROLE_" + UserRole.ADMIN)
            .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessHandler(logoutSuccessHandler)
                .logoutSuccessUrl("/")
                .permitAll()
            .and()
                .addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class)
            .csrf()
                .csrfTokenRepository(csrfTokenRepository());
    }

    private CsrfTokenRepository csrfTokenRepository() {
        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
        repository.setHeaderName("X-XSRF-TOKEN");
        return repository;
    }

    @Bean
    AuthenticationDetailsSource<HttpServletRequest, AuthenticationDetails> getAuthDetailsSource() {
        return new AuthenticationDetailsSourceImpl();
    }

}
