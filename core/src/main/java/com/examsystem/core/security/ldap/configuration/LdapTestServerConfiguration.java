package com.examsystem.core.security.ldap.configuration;

import com.examsystem.core.security.ldap.providers.api.LdapAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;

/**
 * Created by Mateusz Gawron on 2016-03-28.
 */
@Configuration
@ComponentScan(basePackages = "com.examsystem.core.security")
@Profile("test")
public class LdapTestServerConfiguration extends GlobalAuthenticationConfigurerAdapter {

    @Autowired
    private LdapAuthenticationProvider ldapAuthenticationProvider;

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .ldapAuthentication()
            .groupRoleAttribute("ou")
            .userDnPatterns("uid={0},ou=2011,o=fis")
            .contextSource()
            .root("dc=springframework,dc=org")
            .ldif("classpath:ldap/test-server.ldif");
        auth
            .authenticationProvider(ldapAuthenticationProvider);
    }

}
