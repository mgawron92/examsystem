package com.examsystem.core.updaters.impl.questionanswer.choice.simple;

import com.examsystem.core.entitites.questionanswer.choice.simple.SimpleChoiceAnswer;
import com.examsystem.core.mappers.api.questionanswer.choice.simple.SimpleChoiceAnswerPartMapper;
import com.examsystem.core.repositories.SimpleChoiceQuestionRepository;
import com.examsystem.core.repositories.TestAnswerRepository;
import com.examsystem.core.resources.questionanswer.choice.simple.SimpleChoiceAnswerResource;
import com.examsystem.core.updaters.api.questionanswer.choice.simple.SimpleChoiceAnswerUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class SimpleChoiceAnswerUpdaterImpl implements SimpleChoiceAnswerUpdater {

    @Autowired
    private SimpleChoiceAnswerPartMapper simpleChoiceAnswerPartMapper;

    @Autowired
    private SimpleChoiceQuestionRepository simpleChoiceQuestionRepository;

    @Autowired
    private TestAnswerRepository testAnswerRepository;

    @Override
    public SimpleChoiceAnswer updateEntityFromResource(SimpleChoiceAnswer entity, SimpleChoiceAnswerResource resource) {
        entity.setStartedOn(resource.getStartedOn());
        entity.setFinishedOn(resource.getFinishedOn());
        entity.setType(resource.getQuestionType());
        entity.setChoiceQuestion(simpleChoiceQuestionRepository.findSimpleChoiceQuestionById(resource.getChoiceQuestionId()));
        entity.setTestAnswer(testAnswerRepository.findTestAnswerById(resource.getTestAnswerId()));
        entity.setChoiceAnswerParts(resource.getChoiceAnswerParts().stream().map(simpleChoiceAnswerPartMapper::mapResourceToEntity).collect(Collectors.toList()));
        return entity;
    }

}
