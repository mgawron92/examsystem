package com.examsystem.core.updaters.impl.questionanswer.choice.complex;

import com.examsystem.core.entitites.questionanswer.choice.complex.ComplexChoiceAnswerPart;
import com.examsystem.core.repositories.ComplexChoiceAnswerRepository;
import com.examsystem.core.repositories.ComplexChoicePatternRepository;
import com.examsystem.core.resources.questionanswer.choice.complex.ComplexChoiceAnswerPartResource;
import com.examsystem.core.updaters.api.questionanswer.choice.complex.ComplexChoiceAnswerPartUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class ComplexChoiceAnswerPartUpdaterImpl implements ComplexChoiceAnswerPartUpdater {

    @Autowired
    private ComplexChoiceAnswerRepository complexChoiceAnswerRepository;

    @Autowired
    private ComplexChoicePatternRepository complexChoicePatternRepository;

    @Override
    public ComplexChoiceAnswerPart updateEntityFromResource(ComplexChoiceAnswerPart entity, ComplexChoiceAnswerPartResource resource) {
        entity.setSelected(resource.getSelected());
        entity.setChoiceAnswer(complexChoiceAnswerRepository.findComplexChoiceAnswerById(resource.getChoiceAnswerId()));
        entity.setChoicePattern(complexChoicePatternRepository.findComplexChoicePatternById(resource.getChoicePatternId()));
        return entity;
    }

}
