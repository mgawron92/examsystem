package com.examsystem.core.updaters.api.questionanswer.freetext;

import com.examsystem.core.entitites.questionanswer.freetext.FreetextAnswer;
import com.examsystem.core.resources.questionanswer.freetext.FreetextAnswerResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface FreetextAnswerUpdater extends EntityUpdater<FreetextAnswer, FreetextAnswerResource> {

}
