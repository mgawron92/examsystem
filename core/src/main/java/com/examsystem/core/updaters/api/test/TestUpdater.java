package com.examsystem.core.updaters.api.test;

import com.examsystem.core.entitites.test.Test;
import com.examsystem.core.resources.test.TestResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface TestUpdater extends EntityUpdater<Test, TestResource> {

}
