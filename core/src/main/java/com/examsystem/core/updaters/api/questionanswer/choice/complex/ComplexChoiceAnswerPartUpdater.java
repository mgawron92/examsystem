package com.examsystem.core.updaters.api.questionanswer.choice.complex;

import com.examsystem.core.entitites.questionanswer.choice.complex.ComplexChoiceAnswerPart;
import com.examsystem.core.resources.questionanswer.choice.complex.ComplexChoiceAnswerPartResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface ComplexChoiceAnswerPartUpdater extends EntityUpdater<ComplexChoiceAnswerPart, ComplexChoiceAnswerPartResource> {

}
