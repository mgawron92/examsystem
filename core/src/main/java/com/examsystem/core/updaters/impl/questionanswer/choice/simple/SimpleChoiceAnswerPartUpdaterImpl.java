package com.examsystem.core.updaters.impl.questionanswer.choice.simple;

import com.examsystem.core.entitites.questionanswer.choice.simple.SimpleChoiceAnswerPart;
import com.examsystem.core.repositories.SimpleChoiceAnswerRepository;
import com.examsystem.core.repositories.SimpleChoicePatternRepository;
import com.examsystem.core.resources.questionanswer.choice.simple.SimpleChoiceAnswerPartResource;
import com.examsystem.core.updaters.api.questionanswer.choice.simple.SimpleChoiceAnswerPartUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class SimpleChoiceAnswerPartUpdaterImpl implements SimpleChoiceAnswerPartUpdater {

    @Autowired
    private SimpleChoiceAnswerRepository simpleChoiceAnswerRepository;

    @Autowired
    private SimpleChoicePatternRepository simpleChoicePatternRepository;

    @Override
    public SimpleChoiceAnswerPart updateEntityFromResource(SimpleChoiceAnswerPart entity, SimpleChoiceAnswerPartResource resource) {
        entity.setSelected(resource.getSelected());
        entity.setChoiceAnswer(simpleChoiceAnswerRepository.findSimpleChoiceAnswerById(resource.getChoiceAnswerId()));
        entity.setChoicePattern(simpleChoicePatternRepository.findSimpleChoicePatternById(resource.getChoicePatternId()));
        return entity;
    }

}
