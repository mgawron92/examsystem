package com.examsystem.core.updaters.api.question.choice.simple;

import com.examsystem.core.entitites.question.choice.simple.SimpleChoiceQuestion;
import com.examsystem.core.resources.question.choice.simple.SimpleChoiceQuestionResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface SimpleChoiceQuestionUpdater extends EntityUpdater<SimpleChoiceQuestion, SimpleChoiceQuestionResource> {

}
