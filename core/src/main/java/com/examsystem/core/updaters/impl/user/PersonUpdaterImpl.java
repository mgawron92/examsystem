package com.examsystem.core.updaters.impl.user;

import com.examsystem.core.entitites.user.Person;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.updaters.api.user.PersonUpdater;
import org.springframework.stereotype.Component;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class PersonUpdaterImpl implements PersonUpdater {

    @Override
    public Person updateEntityFromResource(Person entity, PersonResource resource) {
        entity.setId(resource.getId());
        entity.setLdapLogin(resource.getLdapLogin());
        entity.setEmail(resource.getEmail());
        entity.setActive(resource.getActive());
        entity.setJoinDate(resource.getJoinDate());
        entity.setName(resource.getName());
        entity.setSurname(resource.getSurname());
        return entity;
    }

    @Override
    public PersonResource updateResourceFromEntity(PersonResource resource, Person entity) {
        resource.setId(entity.getId());
        resource.setLdapLogin(entity.getLdapLogin());
        resource.setEmail(entity.getEmail());
        resource.setActive(entity.getActive());
        resource.setJoinDate(entity.getJoinDate());
        resource.setName(entity.getName());
        resource.setSurname(entity.getSurname());
        return resource;
    }

}
