package com.examsystem.core.updaters.api.questionanswer.join;

import com.examsystem.core.entitites.questionanswer.join.JoinAnswer;
import com.examsystem.core.resources.questionanswer.join.JoinAnswerResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface JoinAnswerUpdater extends EntityUpdater<JoinAnswer, JoinAnswerResource> {

}
