package com.examsystem.core.updaters.api.user;

import com.examsystem.core.entitites.user.Person;
import com.examsystem.core.resources.user.PersonResource;
import com.examsystem.core.updaters.api.FullUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface PersonUpdater extends FullUpdater<Person, PersonResource> {

}
