package com.examsystem.core.updaters.api;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface FullUpdater<E, R> extends EntityUpdater<E, R>, ResourceUpdater<E, R> {

}
