package com.examsystem.core.updaters.api.questionanswer.join;

import com.examsystem.core.entitites.questionanswer.join.JoinAnswerPart;
import com.examsystem.core.resources.questionanswer.join.JoinAnswerPartResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface JoinAnswerPartUpdater extends EntityUpdater<JoinAnswerPart, JoinAnswerPartResource> {

}
