package com.examsystem.core.updaters.api.questionanswer.choice.simple;

import com.examsystem.core.entitites.questionanswer.choice.simple.SimpleChoiceAnswerPart;
import com.examsystem.core.resources.questionanswer.choice.simple.SimpleChoiceAnswerPartResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface SimpleChoiceAnswerPartUpdater extends EntityUpdater<SimpleChoiceAnswerPart, SimpleChoiceAnswerPartResource> {

}
