package com.examsystem.core.updaters.impl.question.freetext;

import com.examsystem.core.entitites.question.freetext.FreetextQuestion;
import com.examsystem.core.repositories.FreetextAnswerRepository;
import com.examsystem.core.repositories.TestRepository;
import com.examsystem.core.resources.question.freetext.FreetextQuestionResource;
import com.examsystem.core.updaters.api.question.freetext.FreetextQuestionUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class FreetextQuestionUpdaterImpl implements FreetextQuestionUpdater {

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private FreetextAnswerRepository freetextAnswerRepository;

    @Override
    public FreetextQuestion updateEntityFromResource(FreetextQuestion entity, FreetextQuestionResource resource) {
        entity.setContent(resource.getContent());
        entity.setPosition(resource.getPosition());
        entity.setImageId(resource.getImageId());
        entity.setType(resource.getQuestionType());
        int timeLimitMinutes = resource.getTimeLimitMinutes();
        int timeLimitSeconds = resource.getTimeLimitSeconds();
        entity.setTimeLimit(timeLimitMinutes * 60 + timeLimitSeconds);
        entity.setHasTimeLimit(resource.getHasTimeLimit());
        entity.setMaxPoints(resource.getMaxPoints());
        entity.setTest(testRepository.findTestById(resource.getTestId()));
        entity.setFreetextAnswers(resource.getFreetextAnswerIds().stream().map((Long id) -> freetextAnswerRepository.findFreetextAnswerById(id)).collect(Collectors.toList()));
        return entity;
    }

}
