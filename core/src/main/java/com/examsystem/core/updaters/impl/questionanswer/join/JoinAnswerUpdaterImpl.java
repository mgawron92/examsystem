package com.examsystem.core.updaters.impl.questionanswer.join;

import com.examsystem.core.entitites.questionanswer.join.JoinAnswer;
import com.examsystem.core.mappers.api.questionanswer.join.JoinAnswerPartMapper;
import com.examsystem.core.repositories.JoinQuestionRepository;
import com.examsystem.core.repositories.TestAnswerRepository;
import com.examsystem.core.resources.questionanswer.join.JoinAnswerResource;
import com.examsystem.core.updaters.api.questionanswer.join.JoinAnswerUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class JoinAnswerUpdaterImpl implements JoinAnswerUpdater {

    @Autowired
    private JoinAnswerPartMapper joinAnswerPartMapper;

    @Autowired
    private TestAnswerRepository testAnswerRepository;

    @Autowired
    private JoinQuestionRepository joinQuestionRepository;

    @Override
    public JoinAnswer updateEntityFromResource(JoinAnswer entity, JoinAnswerResource resource) {
        entity.setStartedOn(resource.getStartedOn());
        entity.setFinishedOn(resource.getFinishedOn());
        entity.setType(resource.getQuestionType());
        entity.setTestAnswer(testAnswerRepository.findTestAnswerById(resource.getTestAnswerId()));
        entity.setJoinQuestion(joinQuestionRepository.findJoinQuestionById(resource.getJoinQuestionId()));
        entity.setJoinAnswerParts(resource.getJoinAnswerParts().stream().map(joinAnswerPartMapper::mapResourceToEntity).collect(Collectors.toList()));
        return entity;
    }
    
}
