package com.examsystem.core.updaters.impl.questionanswer.choice.complex;

import com.examsystem.core.entitites.questionanswer.choice.complex.ComplexChoiceAnswer;
import com.examsystem.core.mappers.api.questionanswer.choice.complex.ComplexChoiceAnswerPartMapper;
import com.examsystem.core.repositories.ComplexChoiceQuestionRepository;
import com.examsystem.core.repositories.TestAnswerRepository;
import com.examsystem.core.resources.questionanswer.choice.complex.ComplexChoiceAnswerResource;
import com.examsystem.core.updaters.api.questionanswer.choice.complex.ComplexChoiceAnswerUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class ComplexChoiceAnswerUpdaterImpl implements ComplexChoiceAnswerUpdater {

    @Autowired
    private ComplexChoiceAnswerPartMapper complexChoiceAnswerPartMapper;

    @Autowired
    private ComplexChoiceQuestionRepository complexChoiceQuestionRepository;

    @Autowired
    private TestAnswerRepository testAnswerRepository;

    @Override
    public ComplexChoiceAnswer updateEntityFromResource(ComplexChoiceAnswer entity, ComplexChoiceAnswerResource resource) {
        entity.setStartedOn(resource.getStartedOn());
        entity.setFinishedOn(resource.getFinishedOn());
        entity.setType(resource.getQuestionType());
        entity.setChoiceQuestion(complexChoiceQuestionRepository.findComplexChoiceQuestionById(resource.getChoiceQuestionId()));
        entity.setTestAnswer(testAnswerRepository.findTestAnswerById(resource.getTestAnswerId()));
        entity.setChoiceAnswerParts(resource.getChoiceAnswerParts().stream().map(complexChoiceAnswerPartMapper::mapResourceToEntity).collect(Collectors.toList()));
        return entity;
    }

}
