package com.examsystem.core.updaters.impl.question.choice.complex;

import com.examsystem.core.entitites.question.choice.complex.ComplexChoicePattern;
import com.examsystem.core.entitites.question.choice.complex.ComplexChoiceQuestion;
import com.examsystem.core.enumerations.QuestionType;
import com.examsystem.core.mappers.api.question.choice.complex.ComplexChoicePatternMapper;
import com.examsystem.core.repositories.ComplexChoiceAnswerRepository;
import com.examsystem.core.repositories.TestRepository;
import com.examsystem.core.resources.question.choice.complex.ComplexChoiceQuestionResource;
import com.examsystem.core.updaters.api.question.choice.complex.ComplexChoiceQuestionUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class ComplexChoiceQuestionUpdaterImpl implements ComplexChoiceQuestionUpdater {

    @Autowired
    private ComplexChoicePatternMapper complexChoicePatternMapper;

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private ComplexChoiceAnswerRepository complexChoiceAnswerRepository;

    @Override
    public ComplexChoiceQuestion updateEntityFromResource(ComplexChoiceQuestion entity, ComplexChoiceQuestionResource resource) {
        entity.setContent(resource.getContent());
        entity.setPosition(resource.getPosition());
        entity.setImageId(resource.getImageId());
        entity.setMultiple(resource.getMultiple());
        int timeLimitMinutes = resource.getTimeLimitMinutes();
        int timeLimitSeconds = resource.getTimeLimitSeconds();
        entity.setTimeLimit(timeLimitMinutes * 60 + timeLimitSeconds);
        entity.setHasTimeLimit(resource.getHasTimeLimit());
        entity.setType(resource.getQuestionType());
        entity.setTest(testRepository.findTestById(resource.getTestId()));
        entity.setChoicePatterns(resource.getChoicePatterns().stream().map(complexChoicePatternMapper::mapResourceToEntity).collect(Collectors.toList()));
        entity.setChoiceAnswers(resource.getChoiceAnswerIds().stream().map((Long id) -> complexChoiceAnswerRepository.findComplexChoiceAnswerById(id)).collect(Collectors.toList()));
        if (QuestionType.SIMPLE_COMPLEX_CHOICE.equals(entity.getType())) {
            int maxPoints = 0;
            for (int i = 0; i < entity.getChoicePatterns().size(); ++i) {
                int maxForChoicePattern = 0;
                for (int j = 0; j < entity.getChoicePatterns().size(); ++j) {
                    if (i == j) {
                        maxForChoicePattern += entity.getChoicePatterns().get(i).getSelectedPoints();
                    } else {
                        maxForChoicePattern += entity.getChoicePatterns().get(i).getDeselectedPoints();
                    }
                }
                if (maxForChoicePattern > maxPoints) {
                    maxPoints = maxForChoicePattern;
                }
            }
            entity.setMaxPoints(maxPoints);
        }
        if (QuestionType.MULTIPLE_COMPLEX_CHOICE.equals(entity.getType())) {
            int maxPoints = 0;
            for (ComplexChoicePattern choicePattern : entity.getChoicePatterns()) {
                maxPoints += Math.max(choicePattern.getSelectedPoints(), choicePattern.getDeselectedPoints());
            }
            entity.setMaxPoints(maxPoints);
        }
        return entity;
    }

}
