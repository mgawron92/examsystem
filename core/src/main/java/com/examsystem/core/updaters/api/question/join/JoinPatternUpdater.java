package com.examsystem.core.updaters.api.question.join;

import com.examsystem.core.entitites.question.join.JoinPattern;
import com.examsystem.core.resources.question.join.JoinPatternResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface JoinPatternUpdater extends EntityUpdater<JoinPattern, JoinPatternResource> {

}
