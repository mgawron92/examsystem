package com.examsystem.core.updaters.impl.question.choice.simple;

import com.examsystem.core.entitites.question.choice.simple.SimpleChoicePattern;
import com.examsystem.core.repositories.SimpleChoiceAnswerPartRepository;
import com.examsystem.core.repositories.SimpleChoiceQuestionRepository;
import com.examsystem.core.resources.question.choice.simple.SimpleChoicePatternResource;
import com.examsystem.core.updaters.api.question.choice.simple.SimpleChoicePatternUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class SimpleChoicePatternUpdaterImpl implements SimpleChoicePatternUpdater {

    @Autowired
    private SimpleChoiceQuestionRepository simpleChoiceQuestionRepository;

    @Autowired
    private SimpleChoiceAnswerPartRepository simpleChoiceAnswerPartRepository;

    @Override
    public SimpleChoicePattern updateEntityFromResource(SimpleChoicePattern entity, SimpleChoicePatternResource resource) {
        entity.setLabel(resource.getLabel());
        entity.setChoiceQuestion(simpleChoiceQuestionRepository.findSimpleChoiceQuestionById(resource.getChoiceQuestionId()));
        entity.setChoiceAnswerParts(resource.getChoiceAnswerPartIds().stream().map((Long id) -> simpleChoiceAnswerPartRepository.findSimpleChoiceAnswerPartById(id)).collect(Collectors.toList()));
        entity.setCorrectIfSelected(resource.getCorrectIfSelected());
        return entity;
    }

}
