package com.examsystem.core.updaters.api.question.choice.simple;

import com.examsystem.core.entitites.question.choice.simple.SimpleChoicePattern;
import com.examsystem.core.resources.question.choice.simple.SimpleChoicePatternResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface SimpleChoicePatternUpdater extends EntityUpdater<SimpleChoicePattern, SimpleChoicePatternResource> {

}
