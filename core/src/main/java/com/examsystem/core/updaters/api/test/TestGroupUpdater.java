package com.examsystem.core.updaters.api.test;

import com.examsystem.core.entitites.test.TestGroup;
import com.examsystem.core.resources.test.TestGroupResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface TestGroupUpdater extends EntityUpdater<TestGroup, TestGroupResource> {

}
