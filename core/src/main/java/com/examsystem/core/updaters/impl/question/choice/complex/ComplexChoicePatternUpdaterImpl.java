package com.examsystem.core.updaters.impl.question.choice.complex;

import com.examsystem.core.entitites.question.choice.complex.ComplexChoicePattern;
import com.examsystem.core.repositories.ComplexChoiceAnswerPartRepository;
import com.examsystem.core.repositories.ComplexChoiceQuestionRepository;
import com.examsystem.core.resources.question.choice.complex.ComplexChoicePatternResource;
import com.examsystem.core.updaters.api.question.choice.complex.ComplexChoicePatternUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class ComplexChoicePatternUpdaterImpl implements ComplexChoicePatternUpdater {

    @Autowired
    private ComplexChoiceQuestionRepository complexChoiceQuestionRepository;

    @Autowired
    private ComplexChoiceAnswerPartRepository complexChoiceAnswerPartRepository;

    @Override
    public ComplexChoicePattern updateEntityFromResource(ComplexChoicePattern entity, ComplexChoicePatternResource resource) {
        entity.setLabel(resource.getLabel());
        entity.setSelectedPoints(resource.getSelectedPoints());
        entity.setDeselectedPoints(resource.getDeselectedPoints());
        entity.setChoiceQuestion(complexChoiceQuestionRepository.findComplexChoiceQuestionById(resource.getChoiceQuestionId()));
        entity.setChoiceAnswerParts(resource.getChoiceAnswerPartIds().stream().map((Long id) -> complexChoiceAnswerPartRepository.findComplexChoiceAnswerPartById(id)).collect(Collectors.toList()));
        return entity;
    }

}
