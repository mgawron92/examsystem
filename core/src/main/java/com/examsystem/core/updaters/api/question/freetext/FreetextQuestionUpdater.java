package com.examsystem.core.updaters.api.question.freetext;

import com.examsystem.core.entitites.question.freetext.FreetextQuestion;
import com.examsystem.core.resources.question.freetext.FreetextQuestionResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface FreetextQuestionUpdater extends EntityUpdater<FreetextQuestion, FreetextQuestionResource> {

}
