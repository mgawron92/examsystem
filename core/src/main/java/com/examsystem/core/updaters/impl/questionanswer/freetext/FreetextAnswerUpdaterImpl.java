package com.examsystem.core.updaters.impl.questionanswer.freetext;

import com.examsystem.core.entitites.questionanswer.freetext.FreetextAnswer;
import com.examsystem.core.entitites.testanswer.TestAnswer;
import com.examsystem.core.repositories.FreetextQuestionRepository;
import com.examsystem.core.repositories.TestAnswerRepository;
import com.examsystem.core.resources.questionanswer.freetext.FreetextAnswerResource;
import com.examsystem.core.updaters.api.questionanswer.freetext.FreetextAnswerUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class FreetextAnswerUpdaterImpl implements FreetextAnswerUpdater {

    @Autowired
    private FreetextQuestionRepository freetextQuestionRepository;

    @Autowired
    private TestAnswerRepository testAnswerRepository;

    @Override
    public FreetextAnswer updateEntityFromResource(FreetextAnswer entity, FreetextAnswerResource resource) {
        entity.setContent(resource.getContent());
        entity.setStartedOn(resource.getStartedOn());
        entity.setFinishedOn(resource.getFinishedOn());
        entity.setType(resource.getQuestionType());
        entity.setPoints(resource.getPoints());
        entity.setComment(resource.getComment());
        entity.setFreetextQuestion(freetextQuestionRepository.findFreetextQuestionById(resource.getFreetextQuestionId()));
        TestAnswer testAnswer = testAnswerRepository.findTestAnswerById(resource.getTestAnswerId());
        if (resource.getId() == null && testAnswer != null) {
            testAnswer.setChecked(false);
            testAnswer = testAnswerRepository.save(testAnswer);
        }
        entity.setTestAnswer(testAnswer);
        return entity;
    }

}
