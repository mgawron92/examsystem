package com.examsystem.core.updaters.api;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface ResourceUpdater<E, R> {

    R updateResourceFromEntity(R resource, E entity);

}
