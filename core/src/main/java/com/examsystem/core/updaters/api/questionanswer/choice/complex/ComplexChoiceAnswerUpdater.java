package com.examsystem.core.updaters.api.questionanswer.choice.complex;

import com.examsystem.core.entitites.questionanswer.choice.complex.ComplexChoiceAnswer;
import com.examsystem.core.resources.questionanswer.choice.complex.ComplexChoiceAnswerResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface ComplexChoiceAnswerUpdater extends EntityUpdater<ComplexChoiceAnswer, ComplexChoiceAnswerResource> {

}
