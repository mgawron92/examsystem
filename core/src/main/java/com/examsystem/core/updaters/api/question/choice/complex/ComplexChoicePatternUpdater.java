package com.examsystem.core.updaters.api.question.choice.complex;

import com.examsystem.core.entitites.question.choice.complex.ComplexChoicePattern;
import com.examsystem.core.resources.question.choice.complex.ComplexChoicePatternResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface ComplexChoicePatternUpdater extends EntityUpdater<ComplexChoicePattern, ComplexChoicePatternResource> {

}
