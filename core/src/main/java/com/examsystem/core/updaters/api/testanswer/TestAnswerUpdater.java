package com.examsystem.core.updaters.api.testanswer;

import com.examsystem.core.entitites.testanswer.TestAnswer;
import com.examsystem.core.resources.testanswer.TestAnswerResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface TestAnswerUpdater extends EntityUpdater<TestAnswer, TestAnswerResource> {

}
