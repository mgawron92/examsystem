package com.examsystem.core.updaters.api.questionanswer.choice.simple;

import com.examsystem.core.entitites.questionanswer.choice.simple.SimpleChoiceAnswer;
import com.examsystem.core.resources.questionanswer.choice.simple.SimpleChoiceAnswerResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface SimpleChoiceAnswerUpdater extends EntityUpdater<SimpleChoiceAnswer, SimpleChoiceAnswerResource> {

}
