package com.examsystem.core.updaters.impl.question.choice.simple;

import com.examsystem.core.entitites.question.choice.simple.SimpleChoiceQuestion;
import com.examsystem.core.mappers.api.question.choice.simple.SimpleChoicePatternMapper;
import com.examsystem.core.repositories.SimpleChoiceAnswerRepository;
import com.examsystem.core.repositories.TestRepository;
import com.examsystem.core.resources.question.choice.simple.SimpleChoiceQuestionResource;
import com.examsystem.core.updaters.api.question.choice.simple.SimpleChoiceQuestionUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class SimpleChoiceQuestionUpdaterImpl implements SimpleChoiceQuestionUpdater {

    @Autowired
    private SimpleChoicePatternMapper simpleChoicePatternMapper;

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private SimpleChoiceAnswerRepository simpleChoiceAnswerRepository;

    @Override
    public SimpleChoiceQuestion updateEntityFromResource(SimpleChoiceQuestion entity, SimpleChoiceQuestionResource resource) {
        entity.setContent(resource.getContent());
        entity.setPosition(resource.getPosition());
        entity.setImageId(resource.getImageId());
        entity.setCorrectPoints(resource.getCorrectPoints());
        entity.setIncorrectPoints(resource.getIncorrectPoints());
        entity.setMultiple(resource.getMultiple());
        int timeLimitMinutes = resource.getTimeLimitMinutes();
        int timeLimitSeconds = resource.getTimeLimitSeconds();
        entity.setTimeLimit(timeLimitMinutes * 60 + timeLimitSeconds);
        entity.setHasTimeLimit(resource.getHasTimeLimit());
        entity.setType(resource.getQuestionType());
        entity.setTest(testRepository.findTestById(resource.getTestId()));
        entity.setChoicePatterns(resource.getChoicePatterns().stream().map(simpleChoicePatternMapper::mapResourceToEntity).collect(Collectors.toList()));
        entity.setChoiceAnswers(resource.getChoiceAnswerIds().stream().map((Long id) -> simpleChoiceAnswerRepository.findSimpleChoiceAnswerById(id)).collect(Collectors.toList()));
        entity.setMaxPoints(Math.max(entity.getCorrectPoints(), entity.getIncorrectPoints()));
        return entity;
    }

}
