package com.examsystem.core.updaters.api.question.choice.complex;

import com.examsystem.core.entitites.question.choice.complex.ComplexChoiceQuestion;
import com.examsystem.core.resources.question.choice.complex.ComplexChoiceQuestionResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface ComplexChoiceQuestionUpdater extends EntityUpdater<ComplexChoiceQuestion, ComplexChoiceQuestionResource> {

}
