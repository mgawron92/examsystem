package com.examsystem.core.updaters.api;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface EntityUpdater<E, R> {

    E updateEntityFromResource(E entity, R resource);

}
