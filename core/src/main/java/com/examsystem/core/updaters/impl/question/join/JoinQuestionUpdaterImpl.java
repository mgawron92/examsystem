package com.examsystem.core.updaters.impl.question.join;

import com.examsystem.core.entitites.question.join.JoinQuestion;
import com.examsystem.core.mappers.api.question.join.JoinPatternMapper;
import com.examsystem.core.repositories.JoinAnswerRepository;
import com.examsystem.core.repositories.TestRepository;
import com.examsystem.core.resources.question.join.JoinQuestionResource;
import com.examsystem.core.updaters.api.question.join.JoinQuestionUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class JoinQuestionUpdaterImpl implements JoinQuestionUpdater {

    @Autowired
    private JoinPatternMapper joinPatternMapper;

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private JoinAnswerRepository joinAnswerRepository;

    @Override
    public JoinQuestion updateEntityFromResource(JoinQuestion entity, JoinQuestionResource resource) {
        entity.setContent(resource.getContent());
        entity.setPosition(resource.getPosition());
        entity.setImageId(resource.getImageId());
        int timeLimitMinutes = resource.getTimeLimitMinutes();
        int timeLimitSeconds = resource.getTimeLimitSeconds();
        entity.setTimeLimit(timeLimitMinutes * 60 + timeLimitSeconds);
        entity.setHasTimeLimit(resource.getHasTimeLimit());
        entity.setCorrectPoints(resource.getCorrectPoints());
        entity.setIncorrectPoints(resource.getIncorrectPoints());
        entity.setType(resource.getQuestionType());
        entity.setTest(testRepository.findTestById(resource.getTestId()));
        entity.setJoinPatterns(resource.getJoinPatterns().stream().map(joinPatternMapper::mapResourceToEntity).collect(Collectors.toList()));
        entity.setJoinAnswers(resource.getJoinAnswerIds().stream().map((Long id) -> joinAnswerRepository.findJoinAnswerById(id)).collect(Collectors.toList()));
        entity.setMaxPoints(Math.max(entity.getCorrectPoints(), entity.getIncorrectPoints()));
        return entity;
    }

}
