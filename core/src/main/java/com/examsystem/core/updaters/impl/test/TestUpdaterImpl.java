package com.examsystem.core.updaters.impl.test;

import com.examsystem.core.entitites.test.Test;
import com.examsystem.core.mappers.api.intermediary.TestTestGroupMapper;
import com.examsystem.core.repositories.ExaminerRepository;
import com.examsystem.core.repositories.TestAnswerRepository;
import com.examsystem.core.resources.test.TestResource;
import com.examsystem.core.updaters.api.test.TestUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class TestUpdaterImpl implements TestUpdater {

    @Autowired
    private ExaminerRepository examinerRepository;

    @Autowired
    private TestAnswerRepository testAnswerRepository;

    @Autowired
    private TestTestGroupMapper testTestGroupMapper;

    @Override
    public Test updateEntityFromResource(Test entity, TestResource resource) {
        entity.setActiveFrom(resource.getActiveFrom());
        long activeTill = resource.getActiveFrom().getTime() + resource.getMinutesActive() * 60 * 1000;;
        entity.setActiveTill(new Date(activeTill));
        entity.setCreatedDate(resource.getCreatedDate());
        entity.setDescription(resource.getDescription());
        entity.setName(resource.getName());
        entity.setTimeLimit(resource.getTimeLimit());
        entity.setHasTimeLimit(resource.getHasTimeLimit());
        entity.setExaminer(examinerRepository.findExaminerById(resource.getExaminerId()));
        entity.setTestTestGroups(resource.getTestTestGroups().stream().map(testTestGroupMapper::mapResourceToEntity).collect(Collectors.toList()));
        entity.setRestrictQuestionsNumber(resource.getRestrictQuestionsNumber());
        entity.setUsedQuestionsNumber(resource.getUsedQuestionsNumber());
        entity.setUseRandomSequence(resource.getUseRandomSequence());
        entity.setTestAnswers(resource.getTestAnswerIds().stream().map((Long id) -> testAnswerRepository.findTestAnswerById(id)).collect(Collectors.toList()));
        return entity;
    }

}
