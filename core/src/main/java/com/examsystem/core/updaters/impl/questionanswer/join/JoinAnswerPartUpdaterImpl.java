package com.examsystem.core.updaters.impl.questionanswer.join;

import com.examsystem.core.entitites.questionanswer.join.JoinAnswerPart;
import com.examsystem.core.repositories.JoinAnswerRepository;
import com.examsystem.core.repositories.JoinPatternRepository;
import com.examsystem.core.resources.questionanswer.join.JoinAnswerPartResource;
import com.examsystem.core.updaters.api.questionanswer.join.JoinAnswerPartUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class JoinAnswerPartUpdaterImpl implements JoinAnswerPartUpdater {

    @Autowired
    private JoinPatternRepository joinPatternRepository;

    @Autowired
    private JoinAnswerRepository joinAnswerRepository;

    @Override
    public JoinAnswerPart updateEntityFromResource(JoinAnswerPart entity, JoinAnswerPartResource resource) {
        entity.setFirstOption(resource.getFirstOption());
        entity.setSecondOption(resource.getSecondOption());
        entity.setJoinAnswer(joinAnswerRepository.findJoinAnswerById(resource.getJoinAnswerId()));
        entity.setJoinPattern(joinPatternRepository.findJoinPatternById(resource.getJoinPatternId()));
        return entity;
    }

}
