package com.examsystem.core.updaters.impl.testanswer;

import com.examsystem.core.entitites.testanswer.TestAnswer;
import com.examsystem.core.repositories.QuestionSequenceRepository;
import com.examsystem.core.repositories.StudentRepository;
import com.examsystem.core.repositories.TestRepository;
import com.examsystem.core.resources.testanswer.TestAnswerResource;
import com.examsystem.core.updaters.api.testanswer.TestAnswerUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class TestAnswerUpdaterImpl implements TestAnswerUpdater {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private QuestionSequenceRepository questionSequenceRepository;

    @Override
    public TestAnswer updateEntityFromResource(TestAnswer entity, TestAnswerResource resource) {
        entity.setStartedOn(resource.getStartedOn());
        entity.setFinishedOn(resource.getFinishedOn());
        entity.setChecked(resource.getChecked());
        entity.setStudent(studentRepository.findStudentById(resource.getStudentId()));
        entity.setTest(testRepository.findTestById(resource.getTestId()));
        entity.setQuestionSequence(questionSequenceRepository.findQuestionSequenceById(resource.getQuestionSequenceId()));
        return entity;
    }

}
