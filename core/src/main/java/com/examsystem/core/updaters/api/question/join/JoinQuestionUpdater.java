package com.examsystem.core.updaters.api.question.join;

import com.examsystem.core.entitites.question.join.JoinQuestion;
import com.examsystem.core.resources.question.join.JoinQuestionResource;
import com.examsystem.core.updaters.api.EntityUpdater;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
public interface JoinQuestionUpdater extends EntityUpdater<JoinQuestion, JoinQuestionResource> {

}
