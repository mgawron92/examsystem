package com.examsystem.core.updaters.impl.test;

import com.examsystem.core.entitites.test.TestGroup;
import com.examsystem.core.mappers.api.intermediary.TestTestGroupMapper;
import com.examsystem.core.repositories.ExaminerRepository;
import com.examsystem.core.resources.test.TestGroupResource;
import com.examsystem.core.updaters.api.test.TestGroupUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class TestGroupUpdaterImpl implements TestGroupUpdater {

    @Autowired
    private TestTestGroupMapper testTestGroupMapper;

    @Autowired
    private ExaminerRepository examinerRepository;

    @Override
    public TestGroup updateEntityFromResource(TestGroup entity, TestGroupResource resource) {
        entity.setName(resource.getName());
        entity.setCreatedDate(resource.getCreatedDate());
        entity.setExaminer(examinerRepository.findExaminerById(resource.getExaminerId()));
        entity.setTestTestGroups(resource.getTestTestGroups().stream().map(testTestGroupMapper::mapResourceToEntity).collect(Collectors.toList()));
        return entity;
    }

}
