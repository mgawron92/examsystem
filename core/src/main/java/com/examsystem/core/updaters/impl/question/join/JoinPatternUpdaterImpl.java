package com.examsystem.core.updaters.impl.question.join;

import com.examsystem.core.entitites.question.join.JoinPattern;
import com.examsystem.core.repositories.JoinAnswerPartRepository;
import com.examsystem.core.repositories.JoinQuestionRepository;
import com.examsystem.core.resources.question.join.JoinPatternResource;
import com.examsystem.core.updaters.api.question.join.JoinPatternUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by Mateusz Gawron on 2016-07-15.
 */
@Component
public class JoinPatternUpdaterImpl implements JoinPatternUpdater {

    @Autowired
    private JoinQuestionRepository joinQuestionRepository;

    @Autowired
    private JoinAnswerPartRepository joinAnswerPartRepository;

    @Override
    public JoinPattern updateEntityFromResource(JoinPattern entity, JoinPatternResource resource) {
        entity.setFirstOption(resource.getFirstOption());
        entity.setSecondOption(resource.getSecondOption());
        entity.setJoinQuestion(joinQuestionRepository.findJoinQuestionById(resource.getJoinQuestionId()));
        entity.setJoinAnswerParts(resource.getJoinAnswerPartIds().stream().map((Long id) -> joinAnswerPartRepository.findJoinAnswerPartById(id)).collect(Collectors.toList()));
        return entity;
    }

}
