package com.examsystem.core.entitites.questionanswer.choice.simple;

import com.examsystem.core.entitites.question.choice.simple.SimpleChoicePattern;
import com.examsystem.core.entitites.questionanswer.choice.ChoiceAnswerPart;

import javax.persistence.*;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public class SimpleChoiceAnswerPart extends ChoiceAnswerPart {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "simple_choice_pattern_id", referencedColumnName = "id")
    private SimpleChoicePattern choicePattern;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "simple_choice_answer_id", referencedColumnName = "id")
    private SimpleChoiceAnswer choiceAnswer;

    public SimpleChoiceAnswerPart() {
    }

    public SimpleChoicePattern getChoicePattern() {
        return choicePattern;
    }

    public void setChoicePattern(SimpleChoicePattern choicePattern) {
        this.choicePattern = choicePattern;
    }

    public SimpleChoiceAnswer getChoiceAnswer() {
        return choiceAnswer;
    }

    public void setChoiceAnswer(SimpleChoiceAnswer choiceAnswer) {
        this.choiceAnswer = choiceAnswer;
    }

}
