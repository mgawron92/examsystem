package com.examsystem.core.entitites.test;

import com.examsystem.core.entitites.user.Examiner;
import com.examsystem.core.entitites.intermediary.StudentTestGroup;
import com.examsystem.core.entitites.intermediary.TestTestGroup;

import javax.persistence.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-01.
 */
@Entity
public class TestGroup {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "testGroup", orphanRemoval=true)
    private List<StudentTestGroup> studentTestGroups = new LinkedList<>();

    @OneToMany(mappedBy = "testGroup")
    private List<TestTestGroup> testTestGroups = new LinkedList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "examiner_id", referencedColumnName = "id")
    private Examiner examiner;

    private String name;

    private Date createdDate;

    public TestGroup() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<StudentTestGroup> getStudentTestGroups() {
        return studentTestGroups;
    }

    public void setStudentTestGroups(List<StudentTestGroup> studentTestGroups) {
        this.studentTestGroups.clear();
        if (studentTestGroups != null) {
            this.studentTestGroups.addAll(studentTestGroups);
        }
    }

    public List<TestTestGroup> getTestTestGroups() {
        return testTestGroups;
    }

    public void setTestTestGroups(List<TestTestGroup> testTestGroups) {
        this.testTestGroups.clear();
        if (testTestGroups != null) {
            this.testTestGroups.addAll(testTestGroups);
        }
    }

    public Examiner getExaminer() {
        return examiner;
    }

    public void setExaminer(Examiner examiner) {
        this.examiner = examiner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

}
