package com.examsystem.core.entitites.questionanswer.freetext;

import com.examsystem.core.entitites.question.Question;
import com.examsystem.core.entitites.questionanswer.QuestionAnswer;
import com.examsystem.core.entitites.question.freetext.FreetextQuestion;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public class FreetextAnswer extends QuestionAnswer {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "freetext_question_id", referencedColumnName = "id")
    private FreetextQuestion freetextQuestion;

    private String content;

    private Integer points;

    private String comment;

    public FreetextQuestion getFreetextQuestion() {
        return freetextQuestion;
    }

    public void setFreetextQuestion(FreetextQuestion freetextQuestion) {
        this.freetextQuestion = freetextQuestion;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public Question getQuestion() {
        return freetextQuestion;
    }

}
