package com.examsystem.core.entitites.user;

import com.examsystem.core.entitites.intermediary.StudentTestGroup;
import com.examsystem.core.entitites.testanswer.TestAnswer;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-01.
 */
@Entity
public class Student extends Person {

    @OneToMany(mappedBy = "student")
    private List<StudentTestGroup> studentTestGroups = new LinkedList<>();

    @OneToMany(mappedBy = "student")
    private List<TestAnswer> testAnswers = new LinkedList<>();

    public Student() {
    }

    public List<StudentTestGroup> getStudentTestGroups() {
        return studentTestGroups;
    }

    public void setStudentTestGroups(List<StudentTestGroup> studentTestGroups) {
        this.studentTestGroups = studentTestGroups;
    }

    public List<TestAnswer> getTestAnswers() {
        return testAnswers;
    }

    public void setTestAnswers(List<TestAnswer> testAnswers) {
        this.testAnswers = testAnswers;
    }

}
