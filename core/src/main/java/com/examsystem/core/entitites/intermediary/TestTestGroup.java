package com.examsystem.core.entitites.intermediary;

import com.examsystem.core.entitites.test.Test;
import com.examsystem.core.entitites.test.TestGroup;

import javax.persistence.*;

/**
 * Created by Mateusz Gawron on 2016-04-01.
 */
@Entity
public class TestTestGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "test_id", referencedColumnName = "id")
    private Test test;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "test_group_id", referencedColumnName = "id")
    private TestGroup testGroup;

    public TestTestGroup() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TestGroup getTestGroup() {
        return testGroup;
    }

    public void setTestGroup(TestGroup testGroup) {
        this.testGroup = testGroup;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }
}
