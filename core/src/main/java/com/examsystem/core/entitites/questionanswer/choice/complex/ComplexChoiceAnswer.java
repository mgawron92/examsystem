package com.examsystem.core.entitites.questionanswer.choice.complex;

import com.examsystem.core.entitites.question.Question;
import com.examsystem.core.entitites.question.choice.complex.ComplexChoiceQuestion;
import com.examsystem.core.entitites.questionanswer.choice.ChoiceAnswer;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public class ComplexChoiceAnswer extends ChoiceAnswer {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "complex_choice_question_id", referencedColumnName = "id")
    private ComplexChoiceQuestion choiceQuestion;

    @OneToMany(mappedBy = "choiceAnswer", orphanRemoval=true)
    private List<ComplexChoiceAnswerPart> choiceAnswerParts = new LinkedList<>();

    public ComplexChoiceQuestion getChoiceQuestion() {
        return choiceQuestion;
    }

    public void setChoiceQuestion(ComplexChoiceQuestion choiceQuestion) {
        this.choiceQuestion = choiceQuestion;
    }

    public List<ComplexChoiceAnswerPart> getChoiceAnswerParts() {
        return choiceAnswerParts;
    }

    public void setChoiceAnswerParts(List<ComplexChoiceAnswerPart> choiceAnswerParts) {
        this.choiceAnswerParts.clear();
        if (choiceAnswerParts != null) {
            this.choiceAnswerParts.addAll(choiceAnswerParts);
        }
    }

    @Override
    public Question getQuestion() {
        return choiceQuestion;
    }

}
