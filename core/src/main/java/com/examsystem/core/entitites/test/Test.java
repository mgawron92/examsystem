package com.examsystem.core.entitites.test;

import com.examsystem.core.entitites.user.Examiner;
import com.examsystem.core.entitites.testanswer.TestAnswer;
import com.examsystem.core.entitites.intermediary.TestTestGroup;
import com.examsystem.core.entitites.question.Question;

import javax.persistence.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-01.
 */
@Entity
public class Test {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "test")
    private List<TestTestGroup> testTestGroups = new LinkedList<>();

    @OneToMany(mappedBy = "test", orphanRemoval=true)
    private List<TestAnswer> testAnswers = new LinkedList<>();

    @OneToMany(mappedBy = "test", orphanRemoval=true)
    @OrderBy("position ASC")
    private List<Question> questions = new LinkedList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "examiner_id", referencedColumnName = "id")
    private Examiner examiner;

    private String name;

    private String description;

    private Integer timeLimit;

    private Boolean hasTimeLimit;

    private Date activeFrom;

    private Date activeTill;

    private Date createdDate;

    private Boolean restrictQuestionsNumber;

    private Integer usedQuestionsNumber;

    private Boolean useRandomSequence;

    public Test() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<TestTestGroup> getTestTestGroups() {
        return testTestGroups;
    }

    public void setTestTestGroups(List<TestTestGroup> testTestGroups) {
        this.testTestGroups.clear();
        if (testTestGroups != null) {
            this.testTestGroups.addAll(testTestGroups);
        }
    }

    public List<TestAnswer> getTestAnswers() {
        return testAnswers;
    }

    public void setTestAnswers(List<TestAnswer> testAnswers) {
        this.testAnswers.clear();
        if (testAnswers != null) {
            this.testAnswers.addAll(testAnswers);
        }
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions.clear();
        if (questions != null) {
            this.questions.addAll(questions);
        }
    }

    public Examiner getExaminer() {
        return examiner;
    }

    public void setExaminer(Examiner examiner) {
        this.examiner = examiner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(Integer timeLimit) {
        this.timeLimit = timeLimit;
    }

    public Boolean getHasTimeLimit() {
        return hasTimeLimit;
    }

    public void setHasTimeLimit(Boolean hasTimeLimit) {
        this.hasTimeLimit = hasTimeLimit;
    }

    public Date getActiveFrom() {
        return activeFrom;
    }

    public void setActiveFrom(Date activeFrom) {
        this.activeFrom = activeFrom;
    }

    public Date getActiveTill() {
        return activeTill;
    }

    public void setActiveTill(Date activeTill) {
        this.activeTill = activeTill;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean getRestrictQuestionsNumber() {
        return restrictQuestionsNumber;
    }

    public void setRestrictQuestionsNumber(Boolean restrictQuestionsNumber) {
        this.restrictQuestionsNumber = restrictQuestionsNumber;
    }

    public Integer getUsedQuestionsNumber() {
        return usedQuestionsNumber;
    }

    public void setUsedQuestionsNumber(Integer usedQuestionsNumber) {
        this.usedQuestionsNumber = usedQuestionsNumber;
    }

    public Boolean getUseRandomSequence() {
        return useRandomSequence;
    }

    public void setUseRandomSequence(Boolean useRandomSequence) {
        this.useRandomSequence = useRandomSequence;
    }
}
