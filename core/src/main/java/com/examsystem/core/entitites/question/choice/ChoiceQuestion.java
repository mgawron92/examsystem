package com.examsystem.core.entitites.question.choice;

import com.examsystem.core.entitites.question.Question;

import javax.persistence.Entity;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public abstract class ChoiceQuestion extends Question {

    private Boolean multiple;

    public Boolean getMultiple() {
        return multiple;
    }

    public void setMultiple(Boolean multiple) {
        this.multiple = multiple;
    }
    
}
