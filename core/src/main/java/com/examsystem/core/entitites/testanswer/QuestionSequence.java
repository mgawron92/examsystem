package com.examsystem.core.entitites.testanswer;

import javax.persistence.*;

/**
 * Created by Mateusz Gawron on 2016-04-01.
 */
@Entity
public class QuestionSequence {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "test_answer_id", referencedColumnName = "id")
    private TestAnswer testAnswer;

    private String sequence;

    public QuestionSequence() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TestAnswer getTestAnswer() {
        return testAnswer;
    }

    public void setTestAnswer(TestAnswer testAnswer) {
        this.testAnswer = testAnswer;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

}
