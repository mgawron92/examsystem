package com.examsystem.core.entitites.question.choice.complex;

import com.examsystem.core.entitites.question.choice.ChoiceQuestion;
import com.examsystem.core.entitites.questionanswer.choice.complex.ComplexChoiceAnswer;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public class ComplexChoiceQuestion extends ChoiceQuestion {

    @OneToMany(mappedBy = "choiceQuestion", orphanRemoval=true)
    protected List<ComplexChoiceAnswer> choiceAnswers = new LinkedList<>();

    @OneToMany(mappedBy = "choiceQuestion", orphanRemoval=true)
    protected List<ComplexChoicePattern> choicePatterns = new LinkedList<>();

    public ComplexChoiceQuestion() {
    }

    public List<ComplexChoiceAnswer> getChoiceAnswers() {
        return choiceAnswers;
    }

    public void setChoiceAnswers(List<ComplexChoiceAnswer> choiceAnswers) {
        this.choiceAnswers.clear();
        if (choiceAnswers != null) {
            this.choiceAnswers.addAll(choiceAnswers);
        }
    }

    public List<ComplexChoicePattern> getChoicePatterns() {
        return choicePatterns;
    }

    public void setChoicePatterns(List<ComplexChoicePattern> choicePatterns) {
        this.choicePatterns.clear();
        if (choicePatterns != null) {
            this.choicePatterns.addAll(choicePatterns);
        }
    }

    @Override
    public List<ComplexChoiceAnswer> getQuestionAnswers() {
        return choiceAnswers;
    }

}
