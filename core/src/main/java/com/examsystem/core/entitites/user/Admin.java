package com.examsystem.core.entitites.user;

import javax.persistence.Entity;

/**
 * Created by Mateusz Gawron on 2016-04-01.
 */
@Entity
public class Admin extends Person {

    private String addedBy;

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

}
