package com.examsystem.core.entitites.question.freetext;

import com.examsystem.core.entitites.questionanswer.freetext.FreetextAnswer;
import com.examsystem.core.entitites.question.Question;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public class FreetextQuestion extends Question {

    @OneToMany(mappedBy = "freetextQuestion", orphanRemoval=true)
    private List<FreetextAnswer> freetextAnswers = new LinkedList<>();

    public FreetextQuestion() {
    }

    public List<FreetextAnswer> getFreetextAnswers() {
        return freetextAnswers;
    }

    public void setFreetextAnswers(List<FreetextAnswer> freetextAnswers) {
        this.freetextAnswers.clear();
        if (freetextAnswers != null) {
            this.freetextAnswers.addAll(freetextAnswers);
        }
    }

    @Override
    public List<FreetextAnswer> getQuestionAnswers() {
        return freetextAnswers;
    }

}
