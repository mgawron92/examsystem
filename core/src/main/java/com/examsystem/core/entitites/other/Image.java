package com.examsystem.core.entitites.other;

import com.examsystem.core.entitites.user.Examiner;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Mateusz Gawron on 2016-06-26.
 */
@Entity
public class Image {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "examiner_id", referencedColumnName = "id")
    private Examiner examiner;

    private byte[] image;

    private Date addedOn;

    public Image() {
    }

    public Image(byte[] image) {
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Examiner getExaminer() {
        return examiner;
    }

    public void setExaminer(Examiner examiner) {
        this.examiner = examiner;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

}
