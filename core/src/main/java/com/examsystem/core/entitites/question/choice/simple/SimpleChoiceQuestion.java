package com.examsystem.core.entitites.question.choice.simple;

import com.examsystem.core.entitites.question.choice.ChoiceQuestion;
import com.examsystem.core.entitites.questionanswer.choice.simple.SimpleChoiceAnswer;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public class SimpleChoiceQuestion extends ChoiceQuestion {

    @OneToMany(mappedBy = "choiceQuestion", orphanRemoval=true)
    private List<SimpleChoiceAnswer> choiceAnswers = new LinkedList<>();

    @OneToMany(mappedBy = "choiceQuestion", orphanRemoval=true)
    private List<SimpleChoicePattern> choicePatterns = new LinkedList<>();

    private Integer correctPoints;

    private Integer incorrectPoints;

    public SimpleChoiceQuestion() {
    }

    public List<SimpleChoiceAnswer> getChoiceAnswers() {
        return choiceAnswers;
    }

    public void setChoiceAnswers(List<SimpleChoiceAnswer> choiceAnswers) {
        this.choiceAnswers.clear();
        if (choiceAnswers != null) {
            this.choiceAnswers.addAll(choiceAnswers);
        }
    }

    public List<SimpleChoicePattern> getChoicePatterns() {
        return choicePatterns;
    }

    public void setChoicePatterns(List<SimpleChoicePattern> choicePatterns) {
        this.choicePatterns.clear();
        if (choicePatterns != null) {
            this.choicePatterns.addAll(choicePatterns);
        }
    }

    public Integer getCorrectPoints() {
        return correctPoints;
    }

    public void setCorrectPoints(Integer correctPoints) {
        this.correctPoints = correctPoints;
    }

    public Integer getIncorrectPoints() {
        return incorrectPoints;
    }

    public void setIncorrectPoints(Integer incorrectPoints) {
        this.incorrectPoints = incorrectPoints;
    }

    @Override
    public List<SimpleChoiceAnswer> getQuestionAnswers() {
        return choiceAnswers;
    }

}
