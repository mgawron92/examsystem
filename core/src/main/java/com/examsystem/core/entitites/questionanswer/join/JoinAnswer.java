package com.examsystem.core.entitites.questionanswer.join;

import com.examsystem.core.entitites.question.Question;
import com.examsystem.core.entitites.questionanswer.QuestionAnswer;
import com.examsystem.core.entitites.question.join.JoinQuestion;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public class JoinAnswer extends QuestionAnswer {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "join_question_id", referencedColumnName = "id")
    private JoinQuestion joinQuestion;

    @OneToMany(mappedBy = "joinAnswer", orphanRemoval=true)
    private List<JoinAnswerPart> joinAnswerParts = new LinkedList<>();

    public JoinQuestion getJoinQuestion() {
        return joinQuestion;
    }

    public void setJoinQuestion(JoinQuestion joinQuestion) {
        this.joinQuestion = joinQuestion;
    }

    public List<JoinAnswerPart> getJoinAnswerParts() {
        return joinAnswerParts;
    }

    public void setJoinAnswerParts(List<JoinAnswerPart> joinAnswerParts) {
        this.joinAnswerParts.clear();
        if (joinAnswerParts != null) {
            this.joinAnswerParts.addAll(joinAnswerParts);
        }
    }

    @Override
    public Question getQuestion() {
        return joinQuestion;
    }

}
