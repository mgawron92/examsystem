package com.examsystem.core.entitites.questionanswer;

import com.examsystem.core.entitites.question.Question;
import com.examsystem.core.entitites.testanswer.TestAnswer;
import com.examsystem.core.enumerations.QuestionType;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Mateusz Gawron on 2016-04-01.
 */
@Entity
public abstract class QuestionAnswer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "test_answer_id", referencedColumnName = "id")
    private TestAnswer testAnswer;

    private Date startedOn;

    private Date finishedOn;

    private QuestionType type;

    public QuestionAnswer() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TestAnswer getTestAnswer() {
        return testAnswer;
    }

    public void setTestAnswer(TestAnswer testAnswer) {
        this.testAnswer = testAnswer;
    }

    public Date getStartedOn() {
        return startedOn;
    }

    public void setStartedOn(Date startedOn) {
        this.startedOn = startedOn;
    }

    public Date getFinishedOn() {
        return finishedOn;
    }

    public void setFinishedOn(Date finishedOn) {
        this.finishedOn = finishedOn;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public abstract Question getQuestion();

}
