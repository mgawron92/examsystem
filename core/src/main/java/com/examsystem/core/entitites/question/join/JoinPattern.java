package com.examsystem.core.entitites.question.join;

import com.examsystem.core.entitites.questionanswer.join.JoinAnswerPart;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public class JoinPattern {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "join_question_id", referencedColumnName = "id")
    private JoinQuestion joinQuestion;

    @OneToMany(mappedBy = "joinPattern", orphanRemoval=true)
    private List<JoinAnswerPart> joinAnswerParts = new LinkedList<>();

    private String firstOption;

    private String secondOption;

    public JoinPattern() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public JoinQuestion getJoinQuestion() {
        return joinQuestion;
    }

    public void setJoinQuestion(JoinQuestion joinQuestion) {
        this.joinQuestion = joinQuestion;
    }

    public List<JoinAnswerPart> getJoinAnswerParts() {
        return joinAnswerParts;
    }

    public void setJoinAnswerParts(List<JoinAnswerPart> joinAnswerParts) {
        this.joinAnswerParts = joinAnswerParts;
    }

    public String getFirstOption() {
        return firstOption;
    }

    public void setFirstOption(String firstOption) {
        this.firstOption = firstOption;
    }

    public String getSecondOption() {
        return secondOption;
    }

    public void setSecondOption(String secondOption) {
        this.secondOption = secondOption;
    }

}
