package com.examsystem.core.entitites.testanswer;

import com.examsystem.core.entitites.user.Student;
import com.examsystem.core.entitites.questionanswer.QuestionAnswer;
import com.examsystem.core.entitites.test.Test;

import javax.persistence.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-01.
 */
@Entity
public class TestAnswer {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "test_id", referencedColumnName = "id")
    private Test test;

    @OneToMany(mappedBy = "testAnswer", cascade = CascadeType.ALL, orphanRemoval=true)
    @OrderBy("startedOn ASC")
    private List<QuestionAnswer> questionAnswers = new LinkedList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    private Student student;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "question_sequence_id", referencedColumnName = "id")
    private QuestionSequence questionSequence;

    private Date startedOn;

    private Date finishedOn;

    private Boolean checked;

    private Integer maxPoints;

    public TestAnswer() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public List<QuestionAnswer> getQuestionAnswers() {
        return questionAnswers;
    }

    public void setQuestionAnswers(List<QuestionAnswer> questionAnswers) {
        this.questionAnswers.clear();
        if (questionAnswers != null) {
            this.questionAnswers.addAll(questionAnswers);
        }
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public QuestionSequence getQuestionSequence() {
        return questionSequence;
    }

    public void setQuestionSequence(QuestionSequence questionSequence) {
        this.questionSequence = questionSequence;
    }

    public Date getStartedOn() {
        return startedOn;
    }

    public void setStartedOn(Date startedOn) {
        this.startedOn = startedOn;
    }

    public Date getFinishedOn() {
        return finishedOn;
    }

    public void setFinishedOn(Date finishedOn) {
        this.finishedOn = finishedOn;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Integer getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
    }
}
