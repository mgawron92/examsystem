package com.examsystem.core.entitites.user;

import com.examsystem.core.entitites.other.Image;
import com.examsystem.core.entitites.test.Test;
import com.examsystem.core.entitites.test.TestGroup;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-01.
 */
@Entity
public class Examiner extends Person {

    @OneToMany(mappedBy = "examiner", orphanRemoval=true)
    private List<TestGroup> testGroups = new LinkedList<>();

    @OneToMany(mappedBy = "examiner")
    private List<Test> tests = new LinkedList<>();

    @OneToMany(mappedBy = "examiner")
    private List<Image> images = new LinkedList<>();

    private String addedBy;

    public Examiner() {
    }

    public List<TestGroup> getTestGroups() {
        return testGroups;
    }

    public void setTestGroups(List<TestGroup> testGroups) {
        this.testGroups.clear();
        if (testGroups != null) {
            this.testGroups.addAll(testGroups);
        }
    }

    public List<Test> getTests() {
        return tests;
    }

    public void setTests(List<Test> tests) {
        this.tests = tests;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

}
