package com.examsystem.core.entitites.question.join;

import com.examsystem.core.entitites.questionanswer.join.JoinAnswer;
import com.examsystem.core.entitites.question.Question;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public class JoinQuestion extends Question {

    @OneToMany(mappedBy = "joinQuestion", orphanRemoval=true)
    private List<JoinAnswer> joinAnswers = new LinkedList<>();

    @OneToMany(mappedBy = "joinQuestion", orphanRemoval=true)
    private List<JoinPattern> joinPatterns = new LinkedList<>();

    private Integer correctPoints;

    private Integer incorrectPoints;

    public JoinQuestion() {
    }

    public List<JoinAnswer> getJoinAnswers() {
        return joinAnswers;
    }

    public void setJoinAnswers(List<JoinAnswer> joinAnswers) {
        this.joinAnswers.clear();
        if (joinAnswers != null) {
            this.joinAnswers.addAll(joinAnswers);
        }
    }

    public List<JoinPattern> getJoinPatterns() {
        return joinPatterns;
    }

    public void setJoinPatterns(List<JoinPattern> joinPatterns) {
        this.joinPatterns.clear();
        if (joinPatterns != null) {
            this.joinPatterns.addAll(joinPatterns);
        }
    }

    public Integer getCorrectPoints() {
        return correctPoints;
    }

    public void setCorrectPoints(Integer correctPoints) {
        this.correctPoints = correctPoints;
    }

    public Integer getIncorrectPoints() {
        return incorrectPoints;
    }

    public void setIncorrectPoints(Integer incorrectPoints) {
        this.incorrectPoints = incorrectPoints;
    }

    @Override
    public List<JoinAnswer> getQuestionAnswers() {
        return joinAnswers;
    }
}
