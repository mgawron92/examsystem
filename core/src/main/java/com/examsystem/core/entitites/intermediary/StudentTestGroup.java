package com.examsystem.core.entitites.intermediary;

import com.examsystem.core.entitites.user.Student;
import com.examsystem.core.entitites.test.TestGroup;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Mateusz Gawron on 2016-04-01.
 */
@Entity
public class StudentTestGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    private Student student;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "test_group_id", referencedColumnName = "id")
    private TestGroup testGroup;

    private Date joinDate;

    public StudentTestGroup() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public TestGroup getTestGroup() {
        return testGroup;
    }

    public void setTestGroup(TestGroup testGroup) {
        this.testGroup = testGroup;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }
}
