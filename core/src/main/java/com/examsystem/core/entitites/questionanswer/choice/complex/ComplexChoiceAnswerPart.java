package com.examsystem.core.entitites.questionanswer.choice.complex;

import com.examsystem.core.entitites.question.choice.complex.ComplexChoicePattern;
import com.examsystem.core.entitites.questionanswer.choice.ChoiceAnswerPart;

import javax.persistence.*;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public class ComplexChoiceAnswerPart extends ChoiceAnswerPart {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "complex_choice_pattern_id", referencedColumnName = "id")
    private ComplexChoicePattern choicePattern;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "complex_choice_answer_id", referencedColumnName = "id")
    private ComplexChoiceAnswer choiceAnswer;

    public ComplexChoiceAnswerPart() {
    }


    public ComplexChoicePattern getChoicePattern() {
        return choicePattern;
    }

    public void setChoicePattern(ComplexChoicePattern choicePattern) {
        this.choicePattern = choicePattern;
    }

    public ComplexChoiceAnswer getChoiceAnswer() {
        return choiceAnswer;
    }

    public void setChoiceAnswer(ComplexChoiceAnswer choiceAnswer) {
        this.choiceAnswer = choiceAnswer;
    }

}
