package com.examsystem.core.entitites.other;

import com.examsystem.core.enumerations.SettingsType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by Mateusz Gawron on 2016-04-01.
 */
@Entity
public class Settings {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    private String key;

    private String value;

    private SettingsType settingsType;

    public Settings() {
    }

    public Settings(String key, String value, SettingsType settingsType) {
        this.id = null;
        this.key = key;
        this.value = value;
        this.settingsType = settingsType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public SettingsType getSettingsType() {
        return settingsType;
    }

    public void setSettingsType(SettingsType settingsType) {
        this.settingsType = settingsType;
    }

}
