package com.examsystem.core.entitites.questionanswer.choice;

import javax.persistence.*;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public abstract class ChoiceAnswerPart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Boolean selected;

    public ChoiceAnswerPart() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
}
