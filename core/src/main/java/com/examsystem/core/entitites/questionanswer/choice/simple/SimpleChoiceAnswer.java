package com.examsystem.core.entitites.questionanswer.choice.simple;

import com.examsystem.core.entitites.question.Question;
import com.examsystem.core.entitites.question.choice.simple.SimpleChoiceQuestion;
import com.examsystem.core.entitites.questionanswer.choice.ChoiceAnswer;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public class SimpleChoiceAnswer extends ChoiceAnswer {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "simple_choice_question_id", referencedColumnName = "id")
    private SimpleChoiceQuestion choiceQuestion;

    @OneToMany(mappedBy = "choiceAnswer", orphanRemoval=true)
    private List<SimpleChoiceAnswerPart> choiceAnswerParts = new LinkedList<>();

    public SimpleChoiceQuestion getChoiceQuestion() {
        return choiceQuestion;
    }

    public void setChoiceQuestion(SimpleChoiceQuestion choiceQuestion) {
        this.choiceQuestion = choiceQuestion;
    }

    public List<SimpleChoiceAnswerPart> getChoiceAnswerParts() {
        return choiceAnswerParts;
    }

    public void setChoiceAnswerParts(List<SimpleChoiceAnswerPart> choiceAnswerParts) {
        this.choiceAnswerParts.clear();
        if (choiceAnswerParts != null) {
            this.choiceAnswerParts.addAll(choiceAnswerParts);
        }
    }

    @Override
    public Question getQuestion() {
        return choiceQuestion;
    }

}
