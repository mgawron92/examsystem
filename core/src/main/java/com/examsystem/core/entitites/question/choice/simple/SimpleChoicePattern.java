package com.examsystem.core.entitites.question.choice.simple;

import com.examsystem.core.entitites.question.choice.ChoicePattern;
import com.examsystem.core.entitites.questionanswer.choice.simple.SimpleChoiceAnswerPart;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public class SimpleChoicePattern extends ChoicePattern {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "simple_choice_question_id", referencedColumnName = "id")
    private SimpleChoiceQuestion choiceQuestion;

    @OneToMany(mappedBy = "choicePattern", orphanRemoval=true)
    private List<SimpleChoiceAnswerPart> choiceAnswerParts = new LinkedList<>();

    private Boolean correctIfSelected;

    public SimpleChoicePattern() {
    }

    public SimpleChoiceQuestion getChoiceQuestion() {
        return choiceQuestion;
    }

    public void setChoiceQuestion(SimpleChoiceQuestion choiceQuestion) {
        this.choiceQuestion = choiceQuestion;
    }

    public List<SimpleChoiceAnswerPart> getChoiceAnswerParts() {
        return choiceAnswerParts;
    }

    public void setChoiceAnswerParts(List<SimpleChoiceAnswerPart> choiceAnswerParts) {
        this.choiceAnswerParts.clear();
        if (choiceAnswerParts != null) {
            this.choiceAnswerParts.addAll(choiceAnswerParts);
        }
    }

    public Boolean getCorrectIfSelected() {
        return correctIfSelected;
    }

    public void setCorrectIfSelected(Boolean correctIfSelected) {
        this.correctIfSelected = correctIfSelected;
    }

}
