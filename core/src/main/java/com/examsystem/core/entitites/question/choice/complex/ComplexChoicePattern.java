package com.examsystem.core.entitites.question.choice.complex;

import com.examsystem.core.entitites.question.choice.ChoicePattern;
import com.examsystem.core.entitites.questionanswer.choice.complex.ComplexChoiceAnswerPart;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public class ComplexChoicePattern extends ChoicePattern {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "complex_choice_question_id", referencedColumnName = "id")
    private ComplexChoiceQuestion choiceQuestion;

    @OneToMany(mappedBy = "choicePattern", orphanRemoval=true)
    private List<ComplexChoiceAnswerPart> choiceAnswerParts = new LinkedList<>();

    private Integer selectedPoints;

    private Integer deselectedPoints;

    public ComplexChoicePattern() {
    }

    public ComplexChoiceQuestion getChoiceQuestion() {
        return choiceQuestion;
    }

    public void setChoiceQuestion(ComplexChoiceQuestion choiceQuestion) {
        this.choiceQuestion = choiceQuestion;
    }

    public List<ComplexChoiceAnswerPart> getChoiceAnswerParts() {
        return choiceAnswerParts;
    }

    public void setChoiceAnswerParts(List<ComplexChoiceAnswerPart> choiceAnswerParts) {
        this.choiceAnswerParts.clear();
        if (choiceAnswerParts != null) {
            this.choiceAnswerParts.addAll(choiceAnswerParts);
        }
    }

    public Integer getSelectedPoints() {
        return selectedPoints;
    }

    public void setSelectedPoints(Integer selectedPoints) {
        this.selectedPoints = selectedPoints;
    }

    public Integer getDeselectedPoints() {
        return deselectedPoints;
    }

    public void setDeselectedPoints(Integer deselectedPoints) {
        this.deselectedPoints = deselectedPoints;
    }

}
