package com.examsystem.core.entitites.questionanswer.join;

import com.examsystem.core.entitites.question.join.JoinPattern;

import javax.persistence.*;

/**
 * Created by Mateusz Gawron on 2016-04-02.
 */
@Entity
public class JoinAnswerPart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "join_pattern_id", referencedColumnName = "id")
    private JoinPattern joinPattern;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "join_answer_id", referencedColumnName = "id")
    private JoinAnswer joinAnswer;

    private String firstOption;

    private String secondOption;

    public JoinAnswerPart() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public JoinPattern getJoinPattern() {
        return joinPattern;
    }

    public void setJoinPattern(JoinPattern joinPattern) {
        this.joinPattern = joinPattern;
    }

    public JoinAnswer getJoinAnswer() {
        return joinAnswer;
    }

    public void setJoinAnswer(JoinAnswer joinAnswer) {
        this.joinAnswer = joinAnswer;
    }

    public String getFirstOption() {
        return firstOption;
    }

    public void setFirstOption(String firstOption) {
        this.firstOption = firstOption;
    }

    public String getSecondOption() {
        return secondOption;
    }

    public void setSecondOption(String secondOption) {
        this.secondOption = secondOption;
    }

}
