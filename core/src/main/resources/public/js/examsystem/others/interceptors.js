angular.module('examsystem')
    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('httpInterceptor');
    })

    .factory('httpInterceptor', ['$q', '$location', function($q, $location) {
        return {
            'responseError': function(response) {
                var status = response.status;
                if (status == 401) {
                    if ($location.path() != '/login') {
                        $location.path('/');
                    }
                    return $q.reject(response);
                }
                return response;
            }
        };
    }]);