angular.module('examsystem')

    .filter('secondsToDateTime', [function() {

        return function(seconds) {
            return new Date(1970, 0, 1).setSeconds(seconds);
        };

    }])

    .filter('minutesToDateTime', [function() {

        return function(minutes) {
            return new Date(1970, 0, 1).setSeconds(minutes * 60);
        };

    }])

    .filter('secondsToString', [function() {

        return function(timeInSeconds) {
            var hours = Math.floor(timeInSeconds / 3600);
            timeInSeconds -= hours * 3600;
            var minutes = Math.floor(timeInSeconds / 60);
            timeInSeconds -= minutes * 60;
            var seconds = Math.floor(timeInSeconds);

            var timeStringArray = [];
            if (hours > 0) {
                timeStringArray.push(hours + ' godz.');
            }
            if (minutes > 0) {
                timeStringArray.push(minutes + ' min.');
            }
            if (seconds > 0) {
                timeStringArray.push(seconds + ' sek.');
            }
            return timeStringArray.join(", ");
        };

    }])

    .filter('minutesToString', [function() {

        return function(timeInMinutes) {
            var hours = Math.floor(timeInMinutes / 60);
            timeInMinutes -= hours * 60;
            var minutes = Math.floor(timeInMinutes);

            var timeStringArray = [];
            if (hours > 0) {
                timeStringArray.push(hours + ' godz.');
            }
            if (minutes > 0) {
                timeStringArray.push(minutes + ' min.');
            }
            return timeStringArray.join(", ");
        };

    }])

    .filter('minLength', [function() {

        return function(number, length) {
            var numberString = number + "";
            while (numberString.length < length) numberString = "0" + numberString;
            return numberString;
        };

    }])

    .filter('range', [function() {

        return function(input) {
            var from, to;
            switch (input.length) {
                case 1:
                    from = 0;
                    to = input[0];
                    break;
                case 2:
                    from = input[0];
                    to = input[1];
                    break;
                default:
                    return input;
            }
            var result = [];
            for (var i = from; i <= to; ++i) {
                result.push(i);
            }
            return result;
        };

    }])

    .filter('typeaheadLabel', [function() {

        return function(user) {
            if (user == null || user == "") {
                return '';
            } else {
                return user.name + ' ' + user.surname + ' (' + user.ldapLogin + ')';
            }
        };

    }]);