angular.module('examsystem')

    .factory('rest', ['$http', 'server', function($http, server) {

        var REST_BASE = server.getRestBase();

        var rest = {

            getAdmins: function() {
                return $http.get(REST_BASE + '/admins');
            },
            getAdminsPage: function(page) {
                return $http.get(REST_BASE + '/admins?page=' + page);
            },
            getAdminsByPageAndSize: function(page, size) {
                return $http.get(REST_BASE + '/admins?page=' + page + '&size=' + size);
            },
            saveAdmin: function(admin) {
                return $http.post(REST_BASE + '/admins', admin);
            },
            getAdminById: function(id) {
                return $http.get(REST_BASE + '/admins/' + id);
            },
            removeAdminById: function(id) {
                return $http.delete(REST_BASE + '/admins/' + id);
            },
            getExaminers: function() {
                return $http.get(REST_BASE + '/examiners');
            },
            getExaminersByPageAndSize: function(page, size) {
                return $http.get(REST_BASE + '/examiners?page=' + page + '&size=' + size);
            },
            getExaminersPage: function(page) {
                return $http.get(REST_BASE + '/examiners?page=' + page);
            },
            saveExaminer: function(examiner) {
                return $http.post(REST_BASE + '/examiners', examiner);
            },
            getExaminerById: function(id) {
                return $http.get(REST_BASE + '/examiners/' + id);
            },
            removeExaminerById: function(id) {
                return $http.delete(REST_BASE + '/examiners/' + id);
            },
            getStudentById: function(id) {
                return $http.get(REST_BASE + '/students/' + id);
            },
            getStudents: function() {
                return $http.get(REST_BASE + '/students');
            },
            getStudentsBySize: function(size) {
                return $http.get(REST_BASE + '/students?size=' + size);
            },
            getStudentsByTestId: function(id) {
                return $http.get(REST_BASE + '/tests/' + id + '/students');
            },
            getStudentsByTestGroupId: function(id) {
                return $http.get(REST_BASE + '/testGroups/' + id + '/students');
            },
            getStudentsByTestGroupIdAndSize: function(testGroupId, size) {
                return $http.get(REST_BASE + '/testGroups/' + testGroupId + '/students?size=' + size);
            },
            getStudentsByTestGroupIdAndPageAndSize: function(testGroupId, page, size) {
                return $http.get(REST_BASE + '/testGroups/' + testGroupId + '/students?page=' + page + '&size=' + size);
            },
            saveStudent: function(student) {
                return $http.post(REST_BASE + '/students', student);
            },
            getAvailableUsers: function() {
                return $http.get(REST_BASE + '/users?available=true');
            },
            getBackupSettings: function() {
                return $http.get(REST_BASE + '/settings/backups');
            },
            saveBackupSettings: function(backupSettings) {
                return $http.post(REST_BASE + '/settings/backups', backupSettings);
            },
            removeBackupSettings: function(changePassword) {
                return $http.delete(REST_BASE + '/settings/backups?changePassword=' + changePassword);
            },
            getDefaultSettings: function() {
                return $http.get(REST_BASE + '/settings/defaults');
            },
            saveDefaultSettings: function(defaultSettings) {
                return $http.post(REST_BASE + '/settings/defaults', defaultSettings);
            },
            removeDefaultSettings: function() {
                return $http.delete(REST_BASE + '/settings/defaults');
            },
            getListSettings: function() {
                return $http.get(REST_BASE + '/settings/lists');
            },
            saveListSettings: function(listSettings) {
                return $http.post(REST_BASE + '/settings/lists', listSettings);
            },
            removeListSettings: function() {
                return $http.delete(REST_BASE + '/settings/lists');
            },
            getTestGroupById: function(id) {
                return $http.get(REST_BASE + '/testGroups/' + id);
            },
            getTestGroupsByStudentId: function(id) {
                return $http.get(REST_BASE + '/testGroups?studentId=' + id);
            },
            getTestGroupsByStudentIdAndSize: function(studentId, size) {
                return $http.get(REST_BASE + '/testGroups?studentId=' + studentId + '&size=' + size);
            },
            getTestGroupsByTestIdAndStudentId: function(testId, studentId) {
                return $http.get(REST_BASE + '/tests/' + testId + '/testGroups?studentId=' + studentId);
            },
            getTestGroupsByExaminerId: function(id) {
                return $http.get(REST_BASE + '/testGroups?examinerId=' + id);
            },
            getTestGroupsByExaminerIdAndSize: function(examinerId, size) {
                return $http.get(REST_BASE + '/testGroups?examinerId=' + examinerId + '&size=' + size);
            },
            getTestGroupsPageByStudentIdAndPage: function(studentId, page) {
                return $http.get(REST_BASE + '/testGroups?studentId=' + studentId + '&page=' + page);
            },
            getTestGroupsPageByTestIdAndPage: function(testId, page) {
                return $http.get(REST_BASE + '/tests/' + testId + '/testGroups?page=' + page);
            },
            getTestGroupsPageByTestIdAndSize: function(testId, size) {
                return $http.get(REST_BASE + '/tests/' + testId + '/testGroups?size=' + size);
            },
            getTestGroupsPageByExaminerIdAndPage: function(examinerId, page) {
                return $http.get(REST_BASE + '/testGroups?examinerId=' + examinerId + '&page=' + page);
            },
            getTestGroupsPageByExaminerIdAndPageAndSize: function(examinerId, page, size) {
                return $http.get(REST_BASE + '/testGroups?examinerId=' + examinerId + '&page=' + page + '&size=' + size);
            },
            getTestGroupNamesByExaminerId: function(examinerId) {
                return $http.get(REST_BASE + '/testGroups/names?examinerId=' + examinerId);
            },
            saveTestGroup: function(testGroup) {
                return $http.post(REST_BASE + '/testGroups', testGroup);
            },
            updateTestGroupById: function(testGroupId, testGroup) {
                return $http.put(REST_BASE + '/testGroups/' + testGroupId, testGroup);
            },
            removeTestGroupById: function(id) {
                return $http.delete(REST_BASE + '/testGroups/' + id);
            },
            getTestTestGroupsByTestIdAndTestGroupId: function(testId, testGroupId) {
                return $http.get(REST_BASE + '/testTestGroups?testId=' + testId + '&testGroupId=' + testGroupId);
            },
            getTestTestGroupsByTestIdAndStudentId: function(testId, studentId) {
                return $http.get(REST_BASE + '/testTestGroups?testId=' + testId + '&studentId=' + studentId);
            },
            saveTestTestGroup: function(testTestGroup) {
                return $http.post(REST_BASE + '/testTestGroups', testTestGroup);
            },
            removeTestTestGroupById: function(id) {
                return $http.delete(REST_BASE + '/testTestGroups/' + id);
            },
            getStudentTestGroupsByStudentIdAndTestGroupId: function(studentId, testGroupId) {
                return $http.get(REST_BASE + '/studentTestGroups?studentId=' + studentId + '&testGroupId=' + testGroupId);
            },
            saveStudentTestGroup: function(studentTestGroup) {
                return $http.post(REST_BASE + '/studentTestGroups', studentTestGroup);
            },
            removeStudentTestGroupById: function(id) {
                return $http.delete(REST_BASE + '/studentTestGroups/' + id);
            },
            getTestById: function(id) {
                return $http.get(REST_BASE + '/tests/' + id);
            },
            getTestsByTestGroupIdAndActiveOnly: function(id, activeOnly) {
                return $http.get(REST_BASE + '/testGroups/' + id + '/tests?activeOnly=' + activeOnly);
            },
            getTestsByTestGroupIdAndStudentIdActiveOnlyAndLatestFirstAndNotStarted: function(testGroupId, studentId, activeOnly, latestFirst, notStarted) {
                return $http.get(REST_BASE + '/testGroups/' + testGroupId + '/tests?studentId=' + studentId + '&activeOnly=' + activeOnly + '&latestFirst=' + latestFirst + '&notStarted=' + notStarted);
            },
            getTestsByStudentIdAndAnsweredAndActiveOnly: function(id, answered, activeOnly) {
                return $http.get(REST_BASE + '/tests?studentId=' + id + '&answered=' + answered + '&activeOnly=' + activeOnly);
            },
            getTestsByStudentIdAndPageAndAnsweredAndActiveOnly: function(id, page, answered, activeOnly) {
                return $http.get(REST_BASE + '/tests?studentId=' + id + '&page=' + page + '&answered=' + answered + '&activeOnly=' + activeOnly);
            },
            getTestsByStudentIdAndAnsweredAndActiveOnlyAndNotFinishedAndPageAndSize: function(id, answered, activeOnly, notFinished, page, size) {
                return $http.get(REST_BASE + '/tests?studentId=' + id + '&answered=' + answered + '&activeOnly=' + activeOnly + '&notFinished=' + notFinished + '&page=' + page + '&size=' + size);
            },
            getTestsByExaminerId: function(id) {
                return $http.get(REST_BASE + '/tests?examinerId=' + id);
            },
            getTestsByExaminerIdAndPageAndSize: function(examinerId, page, size) {
                return $http.get(REST_BASE + '/tests?examinerId=' + examinerId + '&page=' + page + '&size=' + size);
            },
            getTestsByExaminerIdAndChecked: function(examinerId, checked) {
                return $http.get(REST_BASE + '/tests?examinerId=' + examinerId + '&checked=' + checked);
            },
            getTestsByExaminerIdAndCheckedAndLatestFirst: function(examinerId, checked, latestFirst) {
                return $http.get(REST_BASE + '/tests?examinerId=' + examinerId + '&checked=' + checked + '&latestFirst=' + latestFirst);
            },
            getTestsByExaminerIdAndCheckedAndPage: function(examinerId, checked, page) {
                return $http.get(REST_BASE + '/tests?examinerId=' + examinerId + '&checked=' + checked + '&page=' + page);
            },
            getTestsByExaminerIdAndCheckedAndPageAndSize: function(examinerId, checked, page, size) {
                return $http.get(REST_BASE + '/tests?examinerId=' + examinerId + '&checked=' + checked + '&page=' + page + '&size=' + size);
            },
            getTestsByExaminerIdAndCheckedAndLatestFirstAndPageAndSize: function(examinerId, checked, latestFirst, page, size) {
                return $http.get(REST_BASE + '/tests?examinerId=' + examinerId + '&checked=' + checked + '&latestFirst=' + latestFirst + '&page=' + page + '&size=' + size);
            },
            getTestsByTestGroupId: function(id) {
                return $http.get(REST_BASE + '/testGroups/' + id + '/tests');
            },
            getTestsPageByTestGroupIdAndPage: function(testGroupId, page) {
                return $http.get(REST_BASE + '/testGroups/' + testGroupId + '/tests?page=' + page);
            },
            getTestsPageByTestGroupIdAndPageAndSize: function(testGroupId, page, size) {
                return $http.get(REST_BASE + '/testGroups/' + testGroupId + '/tests?page=' + page + '&size=' + size);
            },
            getTestsPageByExaminerIdAndPage: function(examinerId, page) {
                return $http.get(REST_BASE + '/tests?examinerId=' + examinerId + '&page=' + page);
            },
            getTestsPageByExaminerIdAndPageAndChecked: function(examinerId, page, checked) {
                return $http.get(REST_BASE + '/tests?examinerId=' + examinerId + '&page=' + page + '&checked=' + checked);
            },
            getTestsPageByExaminerIdAndPageAndCheckedAndLatestFirst: function(examinerId, page, checked, latestFirst) {
                return $http.get(REST_BASE + '/tests?examinerId=' + examinerId + '&page=' + page + '&checked=' + checked + '&latestFirst=' + latestFirst);
            },
            saveTest: function(test) {
                return $http.post(REST_BASE + '/tests', test);
            },
            removeTestById: function(id) {
                return $http.delete(REST_BASE + '/tests/' + id);
            },
            getTestAnswersByStudentId: function(id) {
                return $http.get(REST_BASE + '/testAnswers?studentId=' + id);
            },
            getTestAnswersByStudentIdAndPage: function(studentId, page) {
                return $http.get(REST_BASE + '/testAnswers?studentId=' + studentId + '&page=' + page);
            },
            getTestAnswersByStudentIdAndPageAndSize: function(studentId, page, size) {
                return $http.get(REST_BASE + '/testAnswers?studentId=' + studentId + '&page=' + page + '&size=' + size);
            },
            getTestAnswersByStudentIdAndTestGroupId: function(studentId, testGroupId) {
                return $http.get(REST_BASE + '/testAnswers?studentId=' + studentId + '&testGroupId=' + testGroupId);
            },
            getTestAnswersByStudentIdAndTestGroupIdAndChecked: function(studentId, testGroupId, checked) {
                return $http.get(REST_BASE + '/testAnswers?studentId=' + studentId + '&testGroupId=' + testGroupId + '&checked=' + checked);
            },
            getTestAnswersByStudentIdAndTestGroupIdAndPageAndChecked: function(studentId, testGroupId, page, checked) {
                return $http.get(REST_BASE + '/testAnswers?studentId=' + studentId + '&testGroupId=' + testGroupId + '&page=' + page + '&checked=' + checked);
            },
            getTestAnswersByStudentIdAndTestId: function(studentId, testId) {
                return $http.get(REST_BASE + '/testAnswers?studentId=' + studentId + '&testId=' + testId);
            },
            getTestAnswersByStudentIdAndTestIdAndFinishedOnly: function(studentId, testId, finishedOnly) {
                return $http.get(REST_BASE + '/testAnswers?studentId=' + studentId + '&testId=' + testId + '&finishedOnly=' + finishedOnly);
            },
            getTestAnswersByTestIdAndChecked: function(testId, checked) {
                return $http.get(REST_BASE + '/testAnswers?testId=' + testId + '&checked=' + checked);
            },
            getTestAnswersByTestIdAndStudentId: function(testId, studentId) {
                return $http.get(REST_BASE + '/tests/' + testId + '/testAnswers?studentId=' + studentId);
            },
            getTestAnswersPageByTestIdAndPageAndChecked: function(testId, page, checked) {
                return $http.get(REST_BASE + '/testAnswers?testId=' + testId + '&page=' + page + '&checked=' + checked);
            },
            saveTestAnswer: function(testAnswer) {
                return $http.post(REST_BASE + '/testAnswers', testAnswer);
            },
            updateTestAnswer: function(testAnswerId, testAnswer) {
                return $http.put(REST_BASE + '/testAnswers/' + testAnswerId, testAnswer);
            },
            getQuestionSequenceById: function(id) {
                return $http.get(REST_BASE + '/questionSequences/' + id);
            },
            getQuestionByTestIdAndQuestionId: function(testId, questionId) {
                return $http.get(REST_BASE + '/tests/' + testId + '/questions/' + questionId);
            },
            saveQuestion: function (testId, question) {
                return $http.post(REST_BASE + '/tests/' + testId + '/questions', question);
            },
            updateQuestion: function (testId, questionId, question) {
                return $http.put(REST_BASE + '/tests/' + testId + '/questions/' + questionId, question);
            },
            removeQuestionByTestIdAndQuestionId: function (testId, questionId) {
                return $http.delete(REST_BASE + '/tests/' + testId + '/questions/' + questionId);
            },
            getQuestionAnswerByTestAnswerIdAndQuestionAnswerId: function(testAnswerId, questionAnswerId) {
                return $http.get(REST_BASE + '/testAnswers/' + testAnswerId + '/questionAnswers/' + questionAnswerId);
            },
            saveQuestionAnswer: function(testAnswerId, questionAnswer) {
                return $http.post(REST_BASE + '/testAnswers/' + testAnswerId + '/questionAnswers', questionAnswer);
            },
            updateQuestionAnswer: function(testAnswerId, questionAnswerId, questionAnswer) {
                return $http.put(REST_BASE + '/testAnswers/' + testAnswerId + '/questionAnswers/' + questionAnswerId, questionAnswer);
            },
            getImageById: function(id) {
                return $http.get(REST_BASE + '/images/' + id);
            },
            saveImage: function(image) {
                var config = {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                };
                return $http.post(REST_BASE + '/images', image, config);
            },
            removeImageById: function(id) {
                return $http.delete(REST_BASE + '/images/' + id);
            }

        };

        return rest;

    }]);