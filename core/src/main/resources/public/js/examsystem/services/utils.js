angular.module('examsystem')

    .factory('utils', ['$location', function($location) {

        var utils = {

            setTestStatus: function (test) {
                var activeFrom = test.activeFrom;
                var activeTill = activeFrom + test.minutesActive * 60 * 1000;
                var now = Date.now();

                if (now < activeFrom) {
                    test.testStatus = 'inactive';
                } else if (now >= activeFrom && now <= activeTill) {
                    test.testStatus = 'active';
                } else {
                    test.testStatus = 'finished';
                }
            },
            setTestTimeLimit: function(test) {
                if (test.hasTimeLimit) {
                    var secondsLimit = test.timeLimit * 60;
                    var timeLimit = utils.splitSecondsToTime(secondsLimit);
                    test.timeLimitHours = timeLimit.hours;
                    test.timeLimitMinutes = timeLimit.minutes;
                    test.timeLimitSeconds = timeLimit.seconds;
                }
            },
            setTestActiveFrom: function (test) {
                var calendarDiff = test.activeFromCalendar - test.activeFrom;
                var timeDiff = test.activeFromTime - test.activeFrom;
                test.activeFrom += calendarDiff + timeDiff;
            },
            setTestCalendarAndTime: function (test) {
                test.activeFromCalendar = test.activeFrom;
                test.activeFromTime = test.activeFrom;
            },
            setTestAnswerTimeSpent: function(test, testAnswer) {
                var secondsLimit = test.timeLimit * 60;
                var secondsSpent = Math.floor((testAnswer.finishedOn - testAnswer.startedOn) / 1000);

                if (test.hasTimeLimit && secondsSpent > secondsLimit) {
                    secondsSpent = secondsLimit;
                }

                var timeSpent = utils.splitSecondsToTime(secondsSpent);
                var timeSpentFactor = (secondsLimit > 0) ? (secondsSpent / secondsLimit) : 0;

                testAnswer.timeSpent = secondsSpent;
                testAnswer.timeSpentHours = timeSpent.hours;
                testAnswer.timeSpentMinutes = timeSpent.minutes;
                testAnswer.timeSpentSeconds = timeSpent.seconds;
                testAnswer.timeSpentBarStyle = utils.getTestAnswerTimeSpentProgressBarStyle(timeSpentFactor);
                testAnswer.timeSpentBarClass = utils.getTestAnswerTimeSpentProgressBarClass(timeSpentFactor);
            },
            setTestAnswerPoints: function(testAnswer) {
                var pointsFactor;
                if (testAnswer.maxPoints > 0 && testAnswer.points >= 0) {
                    pointsFactor = testAnswer.points / testAnswer.maxPoints;
                } else {
                    pointsFactor = 0;
                }
                testAnswer.pointsPercentage = Math.floor(100 * pointsFactor);
                testAnswer.pointsBarStyle = utils.getTestAnswerPointsProgressBarStyle(pointsFactor);
                testAnswer.pointsBarClass = utils.getTestAnswerPointsProgressBarClass(pointsFactor);
            },
            setQuestionAnswerTimeSpent: function(question, questionAnswer) {
                var secondsSpent = Math.floor((questionAnswer.finishedOn - questionAnswer.startedOn) / 1000);
                var secondsLimit = 60 * question.timeLimitMinutes + question.timeLimitSeconds;

                if (question.hasTimeLimit && secondsSpent > secondsLimit) {
                    secondsSpent = secondsLimit;
                }

                var timeSpent = utils.splitSecondsToTime(secondsSpent);
                var timeSpentFactor = (secondsLimit > 0) ? (secondsSpent / secondsLimit) : 0;

                questionAnswer.timeSpentMinutes = timeSpent.minutes;
                questionAnswer.timeSpentSeconds = timeSpent.seconds;
                questionAnswer.timeSpentBarStyle = utils.getQuestionAnswerTimeSpentProgressBarStyle(timeSpentFactor);
                questionAnswer.timeSpentBarClass = utils.getQuestionAnswerTimeSpentProgressBarClass(timeSpentFactor);
            },
            setQuestionAnswerPoints: function(question, questionAnswer) {
                var pointsFactor;
                if (question.maxPoints > 0 && questionAnswer.points >= 0) {
                    pointsFactor = questionAnswer.points / question.maxPoints;
                } else {
                    pointsFactor = 0;
                }
                questionAnswer.pointsPercentage = Math.floor(100 * pointsFactor);
                questionAnswer.pointsBarStyle = utils.getQuestionAnswerPointsProgressBarStyle(pointsFactor);
                questionAnswer.pointsBarClass = utils.getQuestionAnswerPointsProgressBarClass(pointsFactor);
                questionAnswer.textClass = utils.getQuestionAnswerPointsTextClass(pointsFactor);
            },
            processQuestionAnswer: function (question, questionAnswer) {
                if (question.type == 'single-complex-choice' && question.maxPoints == questionAnswer.points) {
                    angular.forEach(questionAnswer.choiceAnswerParts, function (choiceAnswerPart) {
                        choiceAnswerPart.correct = true;
                    });
                }
            },
            copyCommonQuestionProperties: function (oldQuestion, newQuestion) {
                newQuestion.testId = oldQuestion.testId;
                newQuestion.content = oldQuestion.content;
                newQuestion.groupNumber = oldQuestion.groupNumber;
                newQuestion.timeLimitMinutes = oldQuestion.timeLimitMinutes;
                newQuestion.timeLimitSeconds = oldQuestion.timeLimitSeconds;
                newQuestion.hasTimeLimit = oldQuestion.hasTimeLimit;
                newQuestion.position = oldQuestion.position;
            },
            isQuestionAnswerManuallyChecked: function (questionAnswer) {
                var manuallyCheckedQuestionAnswerTypes = ['freetext'];
                return (manuallyCheckedQuestionAnswerTypes.indexOf(questionAnswer.type) >= 0);
            },
            setQuestionImagePath: function(question) {
                if (question.imageId) {
                    question.imageThumbnail = 'rest/images/' + question.imageId;
                }
            },
            checkIfTestIsActive: function (test) {
                return Date.now() >= test.activeFrom;
            },
            checkIfTestIsFinished: function (test) {
                var finishDate = test.activeFrom + test.minutesActive * 60 * 1000;
                return Date.now() > finishDate;
            },
            checkIfTestHasQuestions: function (test) {
                return test.questionIds.length > 0;
            },
            checkIfQuestionSequenceHasQuestions: function (questionSequence) {
                return questionSequence.sequence.length > 0;
            },
            checkIfTestAnswerIsFinished: function (testAnswer) {
                return testAnswer.finishedOn != null;
            },
            checkIfTimeLimitIsExceeded: function (test, testAnswer) {
                if (testAnswer.startedOn && test.hasTimeLimit) {
                    var finishDate = testAnswer.startedOn + test.timeLimit * 60 * 1000;
                    return Date.now() > finishDate;
                }
                return false;
            },
            checkIfTestSubpageIsCorrect: function (test) {
                var regexp = [
                    new RegExp("/student/tests/([0-9]*)/(take|summary)"),
                    new RegExp("/student/tests/([0-9]*)/questions/[0-9]*")
                ];
                return regexp.some(function (regexp) {
                    var match = regexp.exec($location.path());
                    return (match != null && test.id == match[1]);
                });
            },
            checkIfQuestionSubpageIsCorrect: function (test, question) {
                var regexp = [
                    new RegExp("/student/tests/([0-9]*)/questions/([0-9]*)/take")
                ];
                return regexp.some(function (regexp) {
                    var match = regexp.exec($location.path());
                    return (match != null && test.id == match[1] && question.id == match[2]);
                });
            },
            validateFieldValueUnique: function(field, constraint, value, values) {
                if (typeof(values) != "undefined") {
                    if (values.indexOf(value) >= 0) {
                        return utils.validateField(field, constraint, false);
                    }
                }
                return utils.validateField(field, constraint, true);
            },
            validateFieldValueNotUnique: function(field, constraint, value, values) {
                if (typeof(values) != "undefined") {
                    if (values.indexOf(value) >= 0) {
                        return utils.validateField(field, constraint, true);
                    }
                }
                return utils.validateField(field, constraint, false);
            },
            validateField: function (field, constraint, isValid) {
                field.$setValidity(constraint, isValid);
                return isValid;
            },
            initTestTimeoutInfo: function (test, testAnswer) {
                var timeSpentSeconds = Math.floor((Date.now() - testAnswer.startedOn) / 1000);
                var timeLimitSeconds = 60 * test.timeLimit;
                var timeLeftSeconds = timeLimitSeconds - timeSpentSeconds;
                var timeLeftFactor = timeLeftSeconds / timeLimitSeconds;
                var timeLeft = utils.splitSecondsToTime(timeLeftSeconds);

                return {
                    "startedOn": testAnswer.startedOn,
                    "secondsAvailable": test.timeLimit * 60,
                    "timeLeftHours": timeLeft.hours,
                    "timeLeftMinutes": timeLeft.minutes,
                    "timeLeftSeconds": timeLeft.seconds,
                    "progressBarStyle": utils.getTimeProgressBarStyle(timeLeftFactor),
                    "progressBarClass": utils.getTimeProgressBarClass(timeLeftFactor)
                };
            },
            updateTestTimeoutInfo: function (testTimeoutInfo) {
                var timeUsed = (Date.now() - testTimeoutInfo.startedOn) / 1000;
                var secondsUsed = Math.floor(timeUsed);
                var timeLeft = testTimeoutInfo.secondsAvailable - timeUsed;
                var secondsLeft = testTimeoutInfo.secondsAvailable - secondsUsed;
                var timeLeftInfo = utils.splitSecondsToTime(secondsLeft);
                var timeLeftFactor = timeLeft / testTimeoutInfo.secondsAvailable;

                testTimeoutInfo.timeLeftHours = timeLeftInfo.hours;
                testTimeoutInfo.timeLeftMinutes = timeLeftInfo.minutes;
                testTimeoutInfo.timeLeftSeconds = timeLeftInfo.seconds;
                testTimeoutInfo.secondsLeft = secondsLeft;
                testTimeoutInfo.timeLeftFactor = timeLeftFactor;

                testTimeoutInfo.progressBarStyle = utils.getTimeProgressBarStyle(timeLeftFactor);
                testTimeoutInfo.progressBarClass = utils.getTimeProgressBarClass(timeLeftFactor);
            },
            initQuestionTimeoutInfo: function (question, questionAnswer) {
                var timeSpentSeconds = Math.floor((Date.now() - questionAnswer.startedOn) / 1000);
                var timeLimitSeconds = 60 * question.timeLimitMinutes + question.timeLimitSeconds;
                var timeLeftSeconds = timeLimitSeconds - timeSpentSeconds;
                var timeLeftFactor = timeLeftSeconds / timeLimitSeconds;
                var timeLeft = utils.splitSecondsToTime(timeLeftSeconds);

                return {
                    "timeStarted": questionAnswer.startedOn,
                    "timeLeftMinutes": timeLeft.minutes,
                    "timeLeftSeconds": timeLeft.seconds,
                    "secondsAvailable": 60 * question.timeLimitMinutes + question.timeLimitSeconds,
                    "secondsLeft": 60 * timeLeft.minutes + timeLeft.seconds,
                    "progressBarStyle": utils.getTimeProgressBarStyle(timeLeftFactor),
                    "progressBarClass": utils.getTimeProgressBarClass(timeLeftFactor)
                };
            },
            updateQuestionTimeoutInfo: function (questionTimeoutInfo) {
                var timeUsed = (Date.now() - questionTimeoutInfo.timeStarted) / 1000;
                var secondsUsed = Math.floor(timeUsed);
                var timeLeft = questionTimeoutInfo.secondsAvailable - timeUsed;
                var secondsLeft = questionTimeoutInfo.secondsAvailable - secondsUsed;
                var timeLeftFactor = timeLeft / questionTimeoutInfo.secondsAvailable;
                var timeLeftInfo = utils.splitSecondsToTime(secondsLeft);

                questionTimeoutInfo.timeLeftMinutes = timeLeftInfo.minutes;
                questionTimeoutInfo.timeLeftSeconds = timeLeftInfo.seconds;
                questionTimeoutInfo.secondsLeft = secondsLeft;
                questionTimeoutInfo.progressBarStyle = utils.getTimeProgressBarStyle(timeLeftFactor);
                questionTimeoutInfo.progressBarClass = utils.getTimeProgressBarClass(timeLeftFactor);
            },
            getProgressBarStyle: function (factor) {
                return {
                    'width': (100 * factor) + '%'
                };
            },
            getTimeProgressBarClass: function (factor) {
                return {
                    'progress-bar-info': factor > 0.4,
                    'progress-bar-warning': (factor > 0.1 && factor <= 0.4),
                    'progress-bar-danger': factor <= 0.1
                };
            },
            getTimeSpentProgressBarClass: function (factor) {
                return {
                    'progress-bar-info': factor <= 0.6,
                    'progress-bar-warning': (factor > 0.6 && factor <= 0.9),
                    'progress-bar-danger': factor > 0.9
                };
            },
            getPointsProgressBarClass: function (factor) {
                return {
                    'progress-bar-info': factor >= 0.4,
                    'progress-bar-warning': (factor >= 0.1 && factor < 0.4),
                    'progress-bar-danger': factor < 0.1
                };
            },
            getPointsTextClass: function (factor) {
                return {
                    'text-info': factor >= 0.4,
                    'text-warning': (factor >= 0.1 && factor < 0.4),
                    'text-danger': factor < 0.1
                };
            },
            getTimeProgressBarStyle: function (factor) {
                return utils.getProgressBarStyle(factor);
            },
            getTestAnswerTimeSpentProgressBarStyle: function (factor) {
                return utils.getProgressBarStyle(factor);
            },
            getTestAnswerTimeSpentProgressBarClass: function (factor) {
                return utils.getTimeSpentProgressBarClass(factor);
            },
            getQuestionAnswerTimeSpentProgressBarStyle: function (factor) {
                return utils.getProgressBarStyle(factor);
            },
            getQuestionAnswerTimeSpentProgressBarClass: function (factor) {
                return utils.getTimeSpentProgressBarClass(factor);
            },
            getTestAnswerPointsProgressBarStyle: function (factor) {
                return utils.getProgressBarStyle(factor);
            },
            getTestAnswerPointsProgressBarClass: function (factor) {
                return utils.getPointsProgressBarClass(factor);
            },
            getQuestionAnswerPointsProgressBarStyle: function (factor) {
                return utils.getProgressBarStyle(factor);
            },
            getQuestionAnswerPointsProgressBarClass: function (factor) {
                return utils.getPointsProgressBarClass(factor);
            },
            getQuestionAnswerPointsTextClass: function (factor) {
                return utils.getPointsTextClass(factor);
            },
            parseTypeaheadInput: function (typeaheadInput) {
                var typeaheadInputRegex = new RegExp("[^\(]*[\(]([^\(\)]*)[\)]");
                var match = typeaheadInputRegex.exec(typeaheadInput);
                if (match != null) {
                    return match[1];
                } else {
                    return typeaheadInput;
                }
            },
            splitSecondsToTime: function (time) {
                var hours, minutes, seconds;
                hours = Math.floor(time / 3600);
                time -= hours * 3600;
                minutes = Math.floor(time / 60);
                time -= minutes * 60;
                seconds = Math.floor(time % 60);
                return {
                    'hours': hours,
                    'minutes': minutes,
                    'seconds': seconds
                };
            },
            removeFromArrayIfExists: function (array, value) {
                var index = array.indexOf(value);
                if (index >= 0) {
                    array.splice(index, 1);
                }
            },
            shuffleArray: function (array) {
                var firstIndex, secondIndex, tmp;
                for (firstIndex = array.length; firstIndex > 0; --firstIndex) {
                    secondIndex = Math.floor(Math.random() * firstIndex);
                    tmp = array[firstIndex - 1];
                    array[firstIndex - 1] = array[secondIndex];
                    array[secondIndex] = tmp;
                }
            }
        };

        return utils;

    }]);