angular.module('examsystem')

    .factory('combobox', [function() {

        var combobox = {

            getBackupFrequencies: function() {
                return [
                    {'value': 0, 'label': 'nigdy'},
                    {'value': 1, 'label': '1 godzina'},
                    {'value': 2, 'label': '2 godziny'},
                    {'value': 6, 'label': '6 godzin'},
                    {'value': 12, 'label': '12 godzin'},
                    {'value': 24, 'label': '24 godziny'}
                ];
            },
            getListValues: function() {
                var listValues = [];
                var from = 3;
                var to = 15;
                for (var i = from; i <= to; ++i) {
                    listValues.push(i);
                }
                return listValues;
            },
            getQuestionTypes: function () {
                return [
                    { "label": "Pytanie jednokrotnego wyboru", "value": "single-simple-choice" },
                    { "label": "Pytanie jednokrotnego wyboru (tryb zaawansowany)", "value": "single-complex-choice" },
                    { "label": "Pytanie wielokrotnego wyboru", "value": "multiple-simple-choice" },
                    { "label": "Pytanie wielokrotnego wyboru (tryb zaawansowany)", "value": "multiple-complex-choice" },
                    { "label": "Połączenie opcji z dwóch grup", "value": "join" },
                    { "label": "Pytanie otwarte", "value": "freetext" }
                ];
            }

        };

        return combobox;

    }]);