angular.module('examsystem')

    .factory('auth', ['$rootScope', '$http', '$location', 'server', 'resource', function($rootScope, $http, $location, server, resource) {

        var SERVER_BASE = server.getServerBase();

        var ADMIN_ROLE = "ADMIN";
        var EXAMINER_ROLE = "EXAMINER";
        var STUDENT_ROLE = "STUDENT";

        var auth = {

            check: function(role) {
                return $http.get(SERVER_BASE + 'rest/users/me').then(function (user) {
                    if (user.data == "") {
                        auth.proceedAfterCheckFailed();
                    } else {
                        auth.checkRole(user, role);
                        auth.setAuthenticatedUser(true, user.data, user.data.role);
                        auth.proceedAfterCheckSucceeded();
                    }
                }, function () {
                    auth.proceedAfterCheckFailed();
                });
            },
            checkForAdmin: function() {
                return this.check(ADMIN_ROLE);
            },
            checkForExaminer: function() {
                return this.check(EXAMINER_ROLE);
            },
            checkForStudent: function() {
                return this.check(STUDENT_ROLE);
            },
            checkForAnyUser: function () {
                return $http.get(SERVER_BASE + 'rest/users/me').then(function (user) {
                    if (user.data != "") {
                        $location.path("/router");
                    }
                });
            },
            checkRole: function (user, role) {
                if (user.data.role != role) {
                    $location.path('/router');
                }
            },
            proceedAfterCheckSucceeded: function () {
                if ($location.path() == '/login') {
                    $location.path('/router');
                }
            },
            proceedAfterCheckFailed: function () {
                $rootScope.authenticated = false;
                if ($location.path() != '/login') {
                    $location.path('/login');
                }
                throw({'message' : 'Authentication error'});
            },
            login: function(username, password, role) {
                var loginData = resource.getLoginData(username, password, role);
                var loginHeaders = resource.getLoginHeaders();

                return $http.post(SERVER_BASE + 'login', loginData, loginHeaders).then(function () {
                    return $http.get(SERVER_BASE + 'rest/users/me').then(function (user) {
                        auth.setAuthenticatedUser(true, user, role);
                        $location.path('/router');
                    }, function () {
                        throw({'message': 'Brak odpowiednich uprawnień do zalogowania na wybrany rodzaj konta'});
                    });
                }, function () {
                    throw({'message': 'Wprowadzono błędny login lub hasło'});
                });
            },
            logout: function () {
                var logoutHeaders = resource.getLogoutHeaders();
                auth.setAuthenticatedUser(false, null, null);
                return $http.post(SERVER_BASE + 'logout', logoutHeaders).then(function () {
                    $location.path('/login');
                });
            },
            getAuthenticatedUser: function() {
                return $rootScope.authenticatedUser;
            },
            setAuthenticatedUser: function (authenticated, user, role) {
                $rootScope.authenticated = authenticated;
                $rootScope.authenticatedUser = user;
                $rootScope.role = role;
            }

        };

        return auth;

    }]);