angular.module('examsystem')

    .factory('server', [function() {

        var SERVER_BASE = '';
        var REST_BASE = SERVER_BASE + 'rest';

        var server = {

            getServerBase: function () {
                return SERVER_BASE;
            },
            getRestBase: function () {
                return REST_BASE;
            }

        };

        return server;

    }]);