angular.module('examsystem')

    .factory('resource', ['rest', 'utils', function(rest, utils) {

        var resource = {

            getDefaultSettings: function() {
                return {
                    'testActiveAfterDays': 7,
                    'testActiveTime': 5,
                    'testLimitTime': 60,
                    'hasTestTimeLimit': true,
                    'restrictQuestionsNumber': true,
                    'useRandomSequence': false,
                    'questionLimitTimeMinutes': 5,
                    'questionLimitTimeSeconds': 0,
                    'hasQuestionTimeLimit': true
                };
            },
            getAdmin: function(ldapLogin, addedBy) {
                return {
                    "ldapLogin": ldapLogin,
                    "active": true,
                    "joinDate": Date.now(),
                    "role": 'ADMIN',
                    "addedBy": addedBy
                };
            },
            getExaminer: function(ldapLogin, addedBy) {
                return {
                    "ldapLogin": ldapLogin,
                    "active": true,
                    "joinDate": Date.now(),
                    "role": 'EXAMINER',
                    "addedBy": addedBy
                };
            },
            getTestGroup: function(examinerId, testGroupName) {
                return {
                    "id": null,
                    "studentTestGroups": [],
                    "testTestGroups": [],
                    "examinerId": examinerId,
                    "name": testGroupName,
                    "createdDate": Date.now()
                };
            },
            getStudentTestGroup: function(studentId, testGroupId) {
                return {
                    "id": null,
                    "studentId": studentId,
                    "testGroupId": testGroupId,
                    "joinDate": Date.now()
                };
            },
            getTestTestGroup: function (testGroupId, testId) {
                return {
                    'id': null,
                    'testGroupId': testGroupId,
                    'testId': testId
                };
            },
            getNewTest: function(testGroupId, examinerId) {
                return rest.getDefaultSettings().then(function (defaultSettings) {
                    defaultSettings = defaultSettings.data;
                    return {
                        "id": null,
                        "testTestGroups": [
                            resource.getTestTestGroup(testGroupId, null)
                        ],
                        "testAnswerIds": [],
                        "questionIds": [],
                        "examinerId": examinerId,
                        "name": "",
                        "description": "",
                        "timeLimit": defaultSettings.testLimitTime,
                        "hasTimeLimit": defaultSettings.hasTestTimeLimit,
                        "activeFrom": Date.now() + defaultSettings.testActiveAfterDays * 24 * 3600 * 1000,
                        "minutesActive": defaultSettings.testActiveTime,
                        "createdDate": Date.now(),
                        "groups": 1,
                        "maxPoints": null,
                        "restrictQuestionsNumber": defaultSettings.restrictQuestionsNumber,
                        "usedQuestionsNumber": 0,
                        "useRandomSequence": defaultSettings.useRandomSequence
                    };
                });
            },
            getNewQuestion: function (questionType, position) {
                return rest.getDefaultSettings().then(function (defaultSettings) {

                    defaultSettings = defaultSettings.data;

                    var question = null;
                    switch (questionType) {
                        case 'single-complex-choice':
                        case 'single-simple-choice':
                            question = resource.getNewSingleChoiceQuestion(questionType, position);
                            break;
                        case 'multiple-complex-choice':
                        case 'multiple-simple-choice':
                            question = resource.getNewMultipleChoiceQuestion(questionType, position);
                            break;
                        case 'join':
                            question = resource.getNewJoinQuestion(questionType, position);
                            break;
                        case 'freetext':
                            question = resource.getNewFreetextQuestion(questionType, position);
                            break;
                    }

                    question.timeLimitMinutes = defaultSettings.questionLimitTimeMinutes;
                    question.timeLimitSeconds = defaultSettings.questionLimitTimeSeconds;
                    question.hasTimeLimit = defaultSettings.hasQuestionTimeLimit;

                    return question;
                });
            },
            getNewSingleChoiceQuestion: function (questionType, position) {
                return {
                    "type": questionType,
                    "id": null,
                    "testId": null,
                    "questionAnswerIds": [],
                    "content": "",
                    "groupNumber": null,
                    "timeLimitMinutes": 0,
                    "timeLimitSeconds": 0,
                    "hasTimeLimit": false,
                    "position": position,
                    "imageId": null,
                    "multiple": false,
                    "choiceAnswerIds": [],
                    "choicePatterns": [],
                    "correctPoints": 1,
                    "incorrectPoints": 0,
                    "newChoicePattern": {
                        "label": "",
                        "selectedPoints": 0,
                        "deselectedPoints": 0
                    },
                    "maxPoints": null
                };
            },
            getNewMultipleChoiceQuestion: function (questionType, position) {
                return {
                    "type": questionType,
                    "id": null,
                    "testId": null,
                    "questionAnswerIds": [],
                    "content": "",
                    "groupNumber": null,
                    "timeLimitMinutes": 0,
                    "timeLimitSeconds": 0,
                    "hasTimeLimit": false,
                    "position": position,
                    "imageId": null,
                    "multiple": true,
                    "choiceAnswerIds": [],
                    "choicePatterns": [],
                    "correctPoints": 1,
                    "incorrectPoints": 0,
                    "newChoicePattern": {
                        "label": "",
                        "selectedPoints": 0,
                        "deselectedPoints": 0
                    },
                    "maxPoints": null
                };
            },
            getNewJoinQuestion: function (questionType, position) {
                return {
                    "type": questionType,
                    "id": null,
                    "testId": null,
                    "questionAnswerIds": [],
                    "content": "",
                    "groupNumber": null,
                    "timeLimitMinutes": 0,
                    "timeLimitSeconds": 0,
                    "hasTimeLimit": false,
                    "position": position,
                    "imageId": null,
                    "joinAnswerIds": [],
                    "joinPatterns": [],
                    "correctPoints": 1,
                    "incorrectPoints": 0,
                    "newJoinPattern": {
                        "firstOption": "",
                        "secondOption": ""
                    },
                    "maxPoints": null
                };
            },
            getNewFreetextQuestion: function (questionType, position) {
                return {
                    "type": questionType,
                    "id": null,
                    "testId": null,
                    "questionAnswerIds": [],
                    "content": "",
                    "groupNumber": null,
                    "timeLimitMinutes": 0,
                    "timeLimitSeconds": 0,
                    "hasTimeLimit": false,
                    "position": position,
                    "imageId": null,
                    "freetextAnswerIds": [],
                    "maxPoints": 0
                };
            },
            getNewChoicePattern: function(choiceQuestionId, label, selectedPoints, deselectedPoints) {
                return {
                    "id": null,
                    "choiceQuestionId": choiceQuestionId,
                    "choiceAnswerPartIds": [],
                    "label": label,
                    "selectedPoints": selectedPoints,
                    "deselectedPoints": deselectedPoints
                };
            },
            getNewJoinPattern: function(joinQuestionId, firstOption, secondOption) {
                return {
                    "id": null,
                    "joinQuestionId": joinQuestionId,
                    "joinAnswerPartIds": [],
                    "firstOption": firstOption,
                    "secondOption": secondOption
                };
            },
            cleanQuestion: function (question) {
                question.id = null;
                question.testId = null;
                question.questionAnswerIds = [];
                switch (question.type) {
                    case 'single-complex-choice':
                    case 'single-simple-choice':
                    case 'multiple-complex-choice':
                    case 'multiple-simple-choice':
                        resource.cleanChoiceQuestion(question);
                        break;
                    case 'join':
                        resource.cleanJoinQuestion(question);
                        break;
                    case 'freetext':
                        resource.cleanFreetextQuestion(question);
                        break;
                }
            },
            cleanChoiceQuestion: function (question) {
                question.choiceAnswerIds = [];
                angular.forEach(question.choicePatterns, function (choicePattern) {
                    choicePattern.id = null;
                    choicePattern.choiceQuestionId = null;
                    choicePattern.choiceAnswerPartIds = [];
                });
            },
            cleanJoinQuestion: function (question) {
                question.joinAnswerIds = [];
                angular.forEach(question.joinPatterns, function (joinPattern) {
                    joinPattern.id = null;
                    joinPattern.joinQuestionId = null;
                    joinPattern.joinAnswerPartIds = [];
                });
            },
            cleanFreetextQuestion: function (question) {
                question.freetextAnswerIds = [];
            },
            prepareQuestion: function(question) {
                if (question.imageId) {
                    question.imageThumbnail = 'rest/images/' + question.imageId;
                }
                switch (question.type) {
                    case 'single-simple-choice':
                    case 'multiple-simple-choice':
                    case 'single-complex-choice':
                    case 'multiple-complex-choice':
                        resource.prepareChoiceQuestion(question);
                        break;
                    case 'join':
                        resource.prepareJoinQuestion(question);
                        break;
                }
            },
            prepareChoiceQuestion: function(question) {
                question.newChoicePattern = resource.getNewChoicePattern(null, "", 0, 0);
                switch (question.type) {
                    case 'single-simple-choice':
                        resource.prepareSingleSimpleChoiceQuestion(question);
                        break;
                    case 'multiple-simple-choice':
                        resource.prepareMultipleSimpleChoiceQuestion(question);
                        break;
                }
            },
            prepareSingleSimpleChoiceQuestion: function(question) {
                for (var i = 0; i < question.choicePatterns.length; ++i) {
                    if (question.choicePatterns[i].correctIfSelected == true) {
                        question.selectedChoicePatternIndex = i;
                    }
                }
            },
            prepareMultipleSimpleChoiceQuestion: function(question) {
                angular.forEach(question.choicePatterns, function (choicePattern) {
                    if (choicePattern.correctIfSelected) {
                        choicePattern.selected = true;
                    } else {
                        choicePattern.selected = false;
                    }
                });
            },
            prepareJoinQuestion: function(question) {
                question.newJoinPattern = resource.getNewJoinPattern(question.id, "", "");
            },
            processQuestion: function(question) {
                switch (question.type) {
                    case 'single-simple-choice':
                        resource.processSingleSimpleChoiceQuestion(question);
                        break;
                    case 'multiple-simple-choice':
                        resource.processMultipleSimpleChoiceQuestion(question);
                        break;
                }
            },
            processSingleSimpleChoiceQuestion: function(question) {
                angular.forEach(question.choicePatterns, function (choicePattern) {
                    choicePattern.correctIfSelected = false;
                });
                if (question.selectedChoicePatternIndex) {
                    question.choicePatterns[question.selectedChoicePatternIndex].correctIfSelected = true;
                }
            },
            processMultipleSimpleChoiceQuestion: function(question) {
                angular.forEach(question.choicePatterns, function (choicePattern) {
                    if (choicePattern.selected) {
                        choicePattern.correctIfSelected = true;
                    } else {
                        choicePattern.correctIfSelected = false;
                    }
                });
            },
            getNewTestAnswer: function(testId, studentId) {
                return {
                    "testId": testId,
                    "questionAnswerIds": [],
                    "studentId": studentId,
                    "startedOn": Date.now()
                };
            },
            getQuestionIdForQuestionAnswer: function(questionAnswer) {
                switch (questionAnswer.type) {
                    case 'single-simple-choice':
                    case 'multiple-simple-choice':
                    case 'single-complex-choice':
                    case 'multiple-complex-choice':
                        return questionAnswer.choiceQuestionId;
                    case 'join':
                        return questionAnswer.joinQuestionId;
                    case 'freetext':
                        return questionAnswer.freetextQuestionId;
                }
            },
            getQuestionAnswerForQuestion: function(testAnswer, question) {
                switch (question.type) {
                    case 'single-complex-choice':
                    case 'single-simple-choice':
                        return resource.getSingleChoiceAnswer(testAnswer, question);
                    case 'multiple-complex-choice':
                    case 'multiple-simple-choice':
                        return resource.getMultipleChoiceAnswer(testAnswer, question);
                    case 'join':
                        return resource.getJoinAnswer(testAnswer, question);
                    case 'freetext':
                        return resource.getFreetextAnswer(testAnswer, question);
                }
            },
            getSingleChoiceAnswer: function(testAnswer, question) {
                var choiceAnswerParts = [];

                angular.forEach(question.choicePatterns, function (choicePattern) {
                    choiceAnswerParts.push(resource.getChoiceAnswerPart(choicePattern.id));
                });

                return {
                    "type": question.type,
                    "id": null,
                    "testAnswerId": testAnswer.id,
                    "questionId": question.id,
                    "startedOn": Date.now(),
                    "finishedOn": null,
                    "choiceQuestionId": question.id,
                    "choiceAnswerParts": choiceAnswerParts,
                    "selectedChoicePatternId": null,
                    "points": 0,
                    "maxPoints": null
                };
            },
            getMultipleChoiceAnswer: function(testAnswer, question) {
                var choiceAnswerParts = [];

                angular.forEach(question.choicePatterns, function (choicePattern) {
                    choiceAnswerParts.push(resource.getChoiceAnswerPart(choicePattern.id));
                });

                return {
                    "type": question.type,
                    "id": null,
                    "testAnswerId": testAnswer.id,
                    "questionId": question.id,
                    "startedOn": Date.now(),
                    "finishedOn": null,
                    "choiceQuestionId": question.id,
                    "choiceAnswerParts": choiceAnswerParts,
                    "points": 0,
                    "maxPoints": null
                };
            },
            getChoiceAnswerPart: function(choicePatternId) {
                return {
                    "id": null,
                    "choicePatternId": choicePatternId,
                    "choiceAnswerId": null,
                    "selected": false,
                    "points": null,
                    "maxPoints": null,
                    "correct": null
                };
            },
            getJoinAnswer: function(testAnswer, question) {
                var joinAnswerParts = [];
                var joinPatternOptions = [];
                var joinPatternAssociations = [];

                angular.forEach(question.joinPatterns, function (joinPattern) {
                    joinAnswerParts.push(resource.getJoinAnswerPart(joinPattern.id, joinPattern.firstOption, joinPattern.secondOption));
                });

                angular.forEach(question.joinPatterns, function (joinPattern) {
                    joinPatternOptions.push(joinPattern);
                    joinPatternAssociations.push({});
                });

                return {
                    "type": question.type,
                    "id": null,
                    "testAnswerId": testAnswer.id,
                    "questionId": question.id,
                    "startedOn": Date.now(),
                    "finishedOn": null,
                    "joinQuestionId": question.id,
                    "joinAnswerParts": joinAnswerParts,
                    "points": 0,
                    "maxPoints": null,
                    "joinPatternOptions": joinPatternOptions,
                    "joinPatternAssociations": joinPatternAssociations,
                };
            },
            getJoinAnswerPart: function(joinPatternId, firstOption, secondOption) {
                return {
                    "id": null,
                    "joinPatternId": joinPatternId,
                    "joinAnswerId": null,
                    "firstOption": firstOption,
                    "secondOption": secondOption,
                    "correct": null
                };
            },
            getFreetextAnswer: function(testAnswer, question) {
                return {
                    "type": question.type,
                    "id": null,
                    "testAnswerId": testAnswer.id,
                    "questionId": question.id,
                    "startedOn": Date.now(),
                    "finishedOn": null,
                    "freetextQuestionId": question.id,
                    "content": "",
                    "points": 0,
                    "maxPoints": null,
                    "comment": ""
                };
            },
            prepareQuestionAnswer: function(question, questionAnswer) {
                switch (question.type) {
                    case 'join':
                        resource.prepareJoinAnswer(question, questionAnswer);
                        break;
                    case 'single-complex-choice':
                    case 'single-simple-choice':
                    case 'multiple-complex-choice':
                    case 'multiple-simple-choice':
                    case 'freetext':
                        break;
                }
            },
            prepareJoinAnswer: function(question, questionAnswer) {
                var joinPatternOptions = [];
                var joinPatternAssociations = [];

                angular.forEach(question.joinPatterns, function (joinPattern) {
                    joinPatternOptions.push(joinPattern);
                    joinPatternAssociations.push({});
                });
                
                utils.shuffleArray(joinPatternOptions);
                questionAnswer.joinPatternOptions = joinPatternOptions;
                questionAnswer.joinPatternAssociations = joinPatternAssociations;
            },
            processTestAnswer: function(testAnswer) {
                testAnswer.finishedOn = Date.now();
                if (testAnswer.checked == null) {
                    testAnswer.checked = true;
                }
            },
            processQuestionAnswer: function(question, questionAnswer) {
                questionAnswer.finishedOn = Date.now();
                switch (question.type) {
                    case 'single-complex-choice':
                    case 'single-simple-choice':
                        resource.processSingleChoiceAnswer(questionAnswer);
                        break;
                    case 'join':
                        resource.processJoinAnswer(question, questionAnswer);
                        break;
                    case 'multiple-complex-choice':
                    case 'multiple-simple-choice':
                    case 'freetext':
                        break;
                }
            },
            processSingleChoiceAnswer: function(questionAnswer) {
                angular.forEach(questionAnswer.choiceAnswerParts, function (choiceAnswerPart) {
                    choiceAnswerPart.selected = (choiceAnswerPart.choicePatternId == questionAnswer.selectedChoicePatternId);
                });
            },
            processJoinAnswer: function(question, questionAnswer) {
                for (var i = 0; i < questionAnswer.joinAnswerParts.length; ++i) {
                    questionAnswer.joinAnswerParts[i].firstOption = question.joinPatterns[i].firstOption;
                    if (questionAnswer.joinPatternAssociations[i].secondOption) {
                        questionAnswer.joinAnswerParts[i].secondOption = questionAnswer.joinPatternAssociations[i].secondOption;
                    } else {
                        questionAnswer.joinAnswerParts[i].secondOption = '';
                    }
                }
            },
            getLandingPages: function () {
                var landingPages = [];
                landingPages['STUDENT'] = '/student';
                landingPages['EXAMINER'] = '/examiner';
                landingPages['ADMIN'] = '/admin';
                return landingPages;
            },
            getLoginData: function (username, password, role) {
                return 'username=' + username + '&password=' + password + '&role=' + role;
            },
            getFormHeaders: function () {
                var formHeaders = {};
                formHeaders.headers = {
                    'Content-Type': 'application/x-www-form-urlencoded'
                };
                return formHeaders;
            },
            getLoginHeaders: function () {
                return resource.getFormHeaders();
            },
            getLogoutHeaders: function () {
                return resource.getFormHeaders();
            }

        };

        return resource;

    }]);