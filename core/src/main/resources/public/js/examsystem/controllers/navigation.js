angular.module('examsystem')

    .controller('navigation', function($rootScope, $scope, $http, $location, auth) {

        $scope.isLoading = function() {
            return $http.pendingRequests.length > 0;
        };

        $scope.goToDashboard = function() {
            $location.path('/');
            $('#navbar-dashboard').blur();
        };

        $scope.logout = function() {
            return auth.logout();
        };

    });