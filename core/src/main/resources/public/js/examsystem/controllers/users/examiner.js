angular.module('examsystem')

    .controller('examiner-dashboard', function($scope, $q, auth, rest) {

        auth.checkForExaminer().then(function () {
            return rest.getListSettings();
        }).then(function (listSettings) {
            listSettings = listSettings.data;
            return $q.all([
                rest.getTestGroupsPageByExaminerIdAndPageAndSize(auth.getAuthenticatedUser().id, 0, listSettings.testGroups),
                rest.getTestsByExaminerIdAndCheckedAndLatestFirstAndPageAndSize(auth.getAuthenticatedUser().id, false, false, 0, listSettings.testsToCheck),
                rest.getTestsByExaminerIdAndCheckedAndPageAndSize(auth.getAuthenticatedUser().id, true, 0, listSettings.testResults)
            ]);
        }).then(
            $q.spread(function (testGroupsPage, testsToCheckPage, testsPage) {
                $scope.testGroupsPage = testGroupsPage.data;
                $scope.testsToCheckPage = testsToCheckPage.data;
                $scope.testsPage = testsPage.data;

                var testsToCheckPromises = [];
                angular.forEach($scope.testsToCheckPage.content, function (testToCheck) {
                    var promise = $q.when(testToCheck);
                    if (testToCheck.testTestGroups.length == 1) {
                        promise.then(function (testToCheck) {
                            return rest.getTestGroupById(testToCheck.testTestGroups[0].testGroupId).then(function (testGroup) {
                                testToCheck.testGroup = testGroup.data;
                                return testToCheck;
                            });
                        });
                    } else {
                        testToCheck.testGroup = null;
                    }
                    testsToCheckPromises.push(promise);
                });

                var testsPromises = [];
                angular.forEach($scope.testsPage.content, function (test) {
                    var promise = rest.getTestGroupById(test.testTestGroups[0].testGroupId).then(function (testGroup) {
                        test.testGroup = testGroup.data;
                        return test;
                    });
                    testsPromises.push(promise);
                });
            })
        );

    })

    .controller('examiner-test-groups', function($scope, auth, rest) {

        $scope.getTestGroupPage = function(page) {
            return rest.getTestGroupsPageByExaminerIdAndPage(auth.getAuthenticatedUser().id, page).then(function (testGroupsPage) {
                $scope.testGroupsPage = testGroupsPage.data;
            });
        };

        auth.checkForExaminer().then(function () {
            return $scope.getTestGroupPage(0);
        });

    })

    .controller('examiner-test-groups-add', function($scope, auth, rest, utils, resource) {

        $scope.validateTestGroupNameUnique = function(field, constraint, value, values) {
            return utils.validateFieldValueUnique(field, constraint, value, values);
        };

        $scope.addTestGroup = function() {
            var testGroup = resource.getTestGroup(auth.getAuthenticatedUser().id, $scope.groupName);
            return rest.saveTestGroup(testGroup).then(function (testGroup) {
                testGroup = testGroup.data;
                $scope.addedTestGroups.push(testGroup);
                $scope.testGroupNames.push(testGroup.name);
                $scope.groupName = '';
                return rest.getTestGroupsPageByExaminerIdAndPage(auth.getAuthenticatedUser().id, 0);
            }).then(function (testGroupsPage) {
                $scope.testGroupsPage = testGroupsPage.data;
                return $scope.addedTestGroups[$scope.addedTestGroups.length - 1];
            });
        };

        auth.checkForExaminer().then(function () {
            $scope.addedTestGroups = [];
            return rest.getTestGroupsPageByExaminerIdAndPage(auth.getAuthenticatedUser().id, 0);
        }).then(function (testGroupsPage) {
            $scope.testGroupsPage = testGroupsPage.data;
            return rest.getTestGroupNamesByExaminerId(auth.getAuthenticatedUser().id);
        }).then(function (testGroupNames) {
            $scope.testGroupNames = testGroupNames.data;
            return rest.getTestGroupNamesByExaminerId(auth.getAuthenticatedUser().id);
        });

    })

    .controller('examiner-test-groups-edit', function($scope, $routeParams, $location, $q, auth, rest, utils, resource) {

        $scope.checkIfIsAssociatedStudent = function (ldapLogin) {
            var found = false;
            angular.forEach($scope.associatedStudents, function (associatedStudent) {
                if (associatedStudent.ldapLogin == ldapLogin) {
                    found = true;
                }
            });
            return found;
        };

        $scope.findStudentIndex = function (students, ldapLogin) {
            for (var i = 0; i < students.length; ++i) {
                if (students[i].ldapLogin == ldapLogin) {
                    return i;
                }
            }
            return -1;
        };

        $scope.findTypeaheadStudentIndex = function (ldapLogin) {
            return $scope.findStudentIndex($scope.typeaheadStudents, ldapLogin);
        };

        $scope.findAssociatedStudentIndex = function (ldapLogin) {
            return $scope.findStudentIndex($scope.associatedStudents, ldapLogin);
        };

        $scope.findStudentTestGroupByStudentId = function (studentId) {
            var result = null;
            angular.forEach($scope.testGroup.studentTestGroups, function (studentTestGroup) {
                if (studentTestGroup.studentId == studentId) {
                    result = studentTestGroup;
                }
            });
            return result;
        };

        $scope.compareNullables = function (first, second) {
            if (first == null && second == null) {
                return 0;
            } else if (first == null) {
                return -1;
            } else if (second == null) {
                return 1;
            } else {
                return first.localeCompare(second);
            }
        };

        $scope.sortStudents = function (students) {
            students.sort(function (firstStudent, secondStudent) {
                var surnameComparision = $scope.compareNullables(firstStudent.surname, secondStudent.surname);
                if (surnameComparision == 0) {
                    return $scope.compareNullables(firstStudent.name, secondStudent.name);
                } else {
                    return surnameComparision;
                }
            });
        };

        $scope.addStudent = function() {
            var studentLdapLogin = utils.parseTypeaheadInput($scope.studentLdapLogin);
            var typeaheadStudentIndex = $scope.findTypeaheadStudentIndex(studentLdapLogin);

            if ($scope.checkIfIsAssociatedStudent(studentLdapLogin)) {
                throw({'message': 'Student zostal juz dodany do grupy'});
            }

            if (typeaheadStudentIndex < 0) {
                throw({'message': 'Student dodawany do grupy nie istnieje'});
            }

            var studentExists = ($scope.findStudentIndex($scope.existingStudents, studentLdapLogin) > -1);
            var promise = null;
            if (studentExists) {
                promise = $q.when($scope.typeaheadStudents[typeaheadStudentIndex]);
            } else {
                $scope.typeaheadStudents[typeaheadStudentIndex].active = true;
                promise = rest.saveStudent($scope.typeaheadStudents[typeaheadStudentIndex]);
            }

            promise.then(function (student) {
                if (!studentExists) {
                    student = student.data;
                    $scope.existingStudents.push(student);
                } else {
                    var index = $scope.findStudentIndex($scope.existingStudents, studentLdapLogin);
                    student = $scope.existingStudents[index];
                }
                var studentTestGroup = resource.getStudentTestGroup(student.id, $scope.testGroup.id);
                $scope.typeaheadStudents.splice(typeaheadStudentIndex, 1);
                return rest.saveStudentTestGroup(studentTestGroup);
            }).then(function () {
                return $q.all([
                    rest.getTestGroupsPageByExaminerIdAndPage(auth.getAuthenticatedUser().id, 0),
                    rest.getTestGroupById($scope.testGroup.id),
                    rest.getStudentsByTestGroupId($scope.testGroup.id)
                ]);
            }).then(
                $q.spread(function (testGroupsPage, testGroup, associatedStudents){
                    $scope.testGroupsPage = testGroupsPage.data;
                    $scope.testGroup = testGroup.data;
                    return rest.getStudentsByTestGroupIdAndSize($scope.testGroup.id, associatedStudents.data.totalElements);
                })
            ).then(function (associatedStudents) {
                $scope.associatedStudents = associatedStudents.data.content;
                $scope.studentLdapLogin = null;
                angular.element('#studentLdapLogin').focus();
            });
        };

        $scope.removeStudent = function(studentId) {
            var studentTestGroup = $scope.findStudentTestGroupByStudentId(studentId);

            rest.removeStudentTestGroupById(studentTestGroup.id).then(function () {
                return $q.all([
                    rest.getTestGroupsPageByExaminerIdAndPage(auth.getAuthenticatedUser().id, 0),
                    rest.getTestGroupById($scope.testGroup.id),
                    rest.getStudentsByTestGroupId($scope.testGroup.id)
                ]);
            }).then(
                $q.spread(function (testGroupsPage, testGroup, associatedStudents) {
                    $scope.testGroupsPage = testGroupsPage.data;
                    $scope.testGroup = testGroup.data;
                    return rest.getStudentsByTestGroupIdAndSize($scope.testGroup.id, associatedStudents.data.totalElements);
                })
            ).then(function (associatedStudents) {
                $scope.associatedStudents = associatedStudents.data.content;
                return rest.getStudentById(studentTestGroup.studentId);
            }).then(function (student) {
                $scope.typeaheadStudents.push(student.data);
                $scope.sortStudents($scope.typeaheadStudents);
            });
        };

        $scope.saveChanges = function() {
            return rest.updateTestGroupById($scope.testGroup.id, $scope.testGroup).then(function () {
                return rest.getTestGroupsPageByExaminerIdAndPage(auth.getAuthenticatedUser().id, 0);
            }).then(function (testGroupsPage) {
                $scope.testGroupsPage = testGroupsPage.data;
            });
        };

        $scope.validateTestGroupNameUnique = function(field, constraint, value, values) {
            return utils.validateFieldValueUnique(field, constraint, value, values);
        };

        auth.checkForExaminer().then(function () {
            return rest.getListSettings();
        }).then(function (listSettings) {
            listSettings = listSettings.data;
            return $q.all([
                rest.getTestGroupsPageByExaminerIdAndPage(auth.getAuthenticatedUser().id, 0),
                rest.getTestGroupById($routeParams.testGroupId),
                rest.getTestsPageByTestGroupIdAndPageAndSize($routeParams.testGroupId, 0, listSettings.associatedTests),
                rest.getStudentsByTestGroupIdAndPageAndSize($routeParams.testGroupId, 0, listSettings.associatedStudents)
            ]);
        }).then(
            $q.spread(function (testGroupsPage, testGroup, associatedTests, associatedStudents) {
                $scope.testGroupsPage = testGroupsPage.data;
                $scope.testGroup = testGroup.data;
                $scope.oldTestGroupName = $scope.testGroup.name;
                $scope.associatedTests = associatedTests.data;
                return rest.getStudentsByTestGroupIdAndSize($scope.testGroup.id, associatedStudents.data.totalElements);
            })
        ).then(function (associatedStudents) {
            $scope.associatedStudents = associatedStudents.data.content;
            return rest.getAvailableUsers();
        }).then(function (availableUsers) {
            $scope.typeaheadStudents = availableUsers.data;
            return rest.getStudents();
        }).then(function (students) {
            return rest.getStudentsBySize(students.data.totalElements);
        }).then(function (students) {
            $scope.existingStudents = students.data.content;
            var typeaheadStudentsLdapLogins = [];
            angular.forEach($scope.typeaheadStudents, function (typeaheadStudent) {
                typeaheadStudentsLdapLogins.push(typeaheadStudent.ldapLogin);
            });
            angular.forEach($scope.associatedStudents, function(associatedStudent) {
                var index = typeaheadStudentsLdapLogins.indexOf(associatedStudent.ldapLogin);
                if (index >= 0) {
                    typeaheadStudentsLdapLogins.splice(index, 1);
                    $scope.typeaheadStudents.splice(index, 1);
                }
            });
            $scope.sortStudents($scope.typeaheadStudents);
            return rest.getTestGroupNamesByExaminerId(auth.getAuthenticatedUser().id);
        }).then(function (testGroupNames) {
            $scope.isTestGroupCopy = ($location.path().indexOf('copy') !== -1);
            $scope.testGroupNames = testGroupNames.data;
            if (!$scope.isTestGroupCopy) {
                var testGroupNameIndex = $scope.testGroupNames.indexOf($scope.testGroup.name);
                if (testGroupNameIndex >= 0) {
                    $scope.testGroupNames.splice(testGroupNameIndex, 1);
                }
            } else {
                return rest.saveTestGroup(resource.getTestGroup(auth.getAuthenticatedUser().id, $scope.testGroup.name + ' - kopia')).then(function (testGroup) {
                    var originalTestGroup = $scope.testGroup;
                    $scope.testGroup = testGroup.data;
                    $scope.testGroup.studentTestGroups = originalTestGroup.studentTestGroups;
                    $scope.testGroup.testTestGroups = originalTestGroup.testTestGroups;
                    $scope.oldTestGroupName = originalTestGroup.name;

                    var studentTestGroupsPromises = [];
                    angular.forEach($scope.testGroup.studentTestGroups, function (studentTestGroup) {
                        var promise = rest.saveStudentTestGroup(resource.getStudentTestGroup(studentTestGroup.studentId, $scope.testGroup.id)).then(function (response) {
                            response = response.data;
                            studentTestGroup.id = response.id;
                            studentTestGroup.testGroupId = response.testGroupId;
                        });
                        studentTestGroupsPromises.push(promise);
                    });

                    var testTestGroupsPromises = [];
                    angular.forEach($scope.testGroup.testTestGroups, function (testTestGroup) {
                        var promise = rest.saveTestTestGroup(resource.getTestTestGroup($scope.testGroup.id, testTestGroup.testId)).then(function (response) {
                            response = response.data;
                            testTestGroup.id = response.id;
                            testTestGroup.testId = response.testId;
                        });
                        testTestGroupsPromises.push(promise);
                    });

                    return $q.all([
                        $q.all(studentTestGroupsPromises),
                        $q.all(testTestGroupsPromises)
                    ]);
                }).then(
                    $q.spread(function (studentTestGroups, testTestGroups) {
                        $location.path('/examiner/test-groups/' + $scope.testGroup.id + '/edit');
                        return $scope.testGroup;
                    })
                );
            }
            return $scope.testGroup;
        });

    })

    .controller('examiner-test-groups-remove', function($scope, $routeParams, $location, $q, auth, rest) {

        $scope.removeTestGroup = function() {
            return rest.removeTestGroupById($routeParams.testGroupId).then(function () {
                $location.path("/examiner/test-groups");
            });
        };

        auth.checkForExaminer().then(function () {
            $scope.addedTestGroups = [];
            return $q.all([
                rest.getTestGroupsPageByExaminerIdAndPage(auth.getAuthenticatedUser().id, 0),
                rest.getTestGroupById($routeParams.testGroupId)
            ]);
        }).then(
            $q.spread(function (testGroupsPage, testGroup) {
                $scope.testGroupsPage = testGroupsPage.data;
                $scope.testGroup = testGroup.data;
            })
        );

    })


    .controller('examiner-test-groups-tests', function($scope, $routeParams, $q, auth, rest) {

        $scope.getTestsPage = function(page) {
            return rest.rest.getTestsPageByTestGroupIdAndPage($routeParams.testGroupId, page).then(function (testsPage) {
                $scope.testsPage = testsPage.data;
            });
        };

        auth.checkForExaminer().then(function () {
            return $q.all([
                rest.getTestsPageByTestGroupIdAndPage($routeParams.testGroupId, 0),
                rest.getTestGroupById($routeParams.testGroupId)
            ]);
        }).then(
            $q.spread(function (testsPage, testGroup) {
                $scope.testsPage = testsPage.data;
                $scope.testGroup = testGroup.data;
            })
        );

    })

    .controller('examiner-test-groups-tests-remove-from-group', function($scope, $routeParams, $location, $q, auth, rest) {

        $scope.removeTestFromTestGroup = function() {
            if ($scope.testTestGroup) {
                rest.removeTestTestGroupById($scope.testTestGroup.id).then(function () {
                    $location.path("/examiner/test-groups/" + $routeParams.testGroupId + "/edit");
                });
            } else {
                throw({'message': 'Nie znaleziono powiazania pomiedzy testem i grupa testowa'});
            }
        };

        auth.checkForExaminer().then(function () {
            return rest.getTestById($routeParams.testId);
        }).then(function (test) {
            $scope.test = test.data;
            return $q.all([
                rest.getTestsPageByTestGroupIdAndPage($routeParams.testGroupId, 0),
                rest.getTestGroupById($routeParams.testGroupId)
            ]);
        }).then(
            $q.spread(function (testsPage, testGroup) {
                $scope.testsPage = testsPage.data;
                $scope.testGroup = testGroup.data;
                return rest.getTestTestGroupsByTestIdAndTestGroupId($routeParams.testId, $routeParams.testGroupId);
            })
        ).then(function (testTestGroups) {
            $scope.testTestGroup = (testTestGroups.data.length > 0) ? testTestGroups.data[0] : null;
        });

    })

    .controller('examiner-tests-edit', function($scope, $rootScope, $routeParams, $timeout, $location, $q, $window, auth, rest, utils, resource, combobox) {

        $scope.setTestSaveSuccess = function () {
            $scope.testSaveSuccess = true;
            $scope.testSaveFailure = false;
            $window.scrollTo(0, 0);
        };

        $scope.setTestSaveFailure = function () {
            $scope.testSaveSuccess = false;
            $scope.testSaveFailure = true;
            $window.scrollTo(0, 0);
        };

        $scope.cleanTestSaveMessages = function () {
            $scope.testSaveSuccess = false;
            $scope.testSaveFailure = false;
        };

        $scope.moveUp = function(position) {
            var questionIndex = position - 1;
            var prevQuestionIndex = position - 2;

            var tmp = $scope.questions[questionIndex];
            $scope.questions[questionIndex] = $scope.questions[prevQuestionIndex];
            $scope.questions[prevQuestionIndex] = tmp;

            $scope.questions[prevQuestionIndex].position = position - 1;
            $scope.questions[questionIndex].position = position;
        };

        $scope.moveDown = function(position) {
            var questionIndex = position - 1;
            var nextQuestionIndex = position;

            var tmp = $scope.questions[questionIndex];
            $scope.questions[questionIndex] = $scope.questions[nextQuestionIndex];
            $scope.questions[nextQuestionIndex] = tmp;

            $scope.questions[questionIndex].position = position;
            $scope.questions[nextQuestionIndex].position = position + 1;
        };

        $scope.removeQuestion = function(position) {
            var questionIndex = position - 1;

            $scope.questions.splice(questionIndex, 1);

            for (var i = 0; i < $scope.questions.length; ++i) {
                if ($scope.questions[i].position > position) {
                    $scope.questions[i].position = $scope.questions[i].position - 1;
                }
            }
        };

        $scope.addVariant = function(questionPosition) {
            var questionIndex = questionPosition - 1;
            var question = $scope.questions[questionIndex];
            question.choicePatterns.push(question.newChoicePattern);
            question.newChoicePattern = resource.getNewChoicePattern(question.id, "", 0, 0);
        };

        $scope.removeVariant = function(questionPosition, variantIndex) {
            var questionIndex = questionPosition - 1;
            $scope.questions[questionIndex].choicePatterns.splice(variantIndex, 1);
        };

        $scope.addLink = function(questionPosition) {
            var questionIndex = questionPosition - 1;
            var question = $scope.questions[questionIndex];
            question.joinPatterns.push(question.newJoinPattern);
            question.newJoinPattern = resource.getNewJoinPattern(question.id, "", "");
        };

        $scope.removeLink = function(questionPosition, linkIndex) {
            var questionIndex = questionPosition - 1;
            $scope.questions[questionIndex].joinPatterns.splice(linkIndex, 1);
        };

        $scope.changeQuestionType = function(position) {
            var questionIndex = position - 1;
            var oldQuestion = $scope.questions[questionIndex];
            return resource.getNewQuestion(oldQuestion.type).then(function (newQuestion) {
                utils.copyCommonQuestionProperties(oldQuestion, newQuestion);
                $scope.questions[questionIndex] = newQuestion;
            });
        };

        $scope.addQuestionToArray = function(array, question) {
            array.push(question);
            array.sort(function (firstQuestion, secondQuestion) {
                return firstQuestion.position - secondQuestion.position;
            });
        };

        $scope.addNewQuestion = function() {
            if ($scope.newQuestionType !== "") {
                return resource.getNewQuestion($scope.newQuestionType, $scope.questions.length + 1).then(function (question) {
                    $scope.addQuestionToArray($scope.questions, question);
                    $scope.newQuestionType = "";
                });
            }
        };

        $scope.refreshTestGroups = function() {
            $scope.testGroups = [];
            var associatedTestGroupsPromises = [];
            angular.forEach($scope.associatedTestGroups, function (associatedTestGroup) {
                var promise = rest.getTestsByTestGroupId(associatedTestGroup.id).then(function (tests) {
                    associatedTestGroup.tests = tests.data;
                    $scope.testGroups.push(associatedTestGroup);
                    return associatedTestGroup;
                });
                associatedTestGroupsPromises.push(promise);
            });
            return $q.all(associatedTestGroupsPromises).then(function () {
                $scope.testGroups.sort(function(firstTestGroup, secondTestGroup) {
                    return firstTestGroup.name.localeCompare(secondTestGroup.name);
                });
            });
        };

        $scope.parseTypeaheadTestGroupName = function () {
            return (typeof($scope.addedTestGroup.name) == "undefined") ? $scope.addedTestGroup : $scope.addedTestGroup.name;
        };

        $scope.getTestGroupIndex = function (testGroups, testGroupName) {
            for (var i = 0; i < testGroups.length; ++i) {
                if (testGroups[i].name == testGroupName) {
                    return i;
                }
            }
            return -1;
        };

        $scope.addTestToTestGroup = function() {
            if ($scope.addedTestGroup == null || typeof($scope.addedTestGroup) == "undefined") {
                return;
            }

            var testGroupName = $scope.parseTypeaheadTestGroupName();
            var typeaheadIndex = $scope.getTestGroupIndex($scope.typeaheadTestGroups, testGroupName);
            var associatedIndex = $scope.getTestGroupIndex($scope.associatedTestGroups, testGroupName);
            var foundInTypeahead = (typeaheadIndex >= 0);
            var foundInAssociated = (associatedIndex >= 0);
            var testGroup = (foundInTypeahead) ? $scope.typeaheadTestGroups[typeaheadIndex] : null;

            if (foundInTypeahead && !foundInAssociated) {
                $scope.typeaheadTestGroups.splice(typeaheadIndex, 1);
                $scope.associatedTestGroups.push(testGroup);
                $scope.addedTestGroup = "";

                if ($routeParams.testId) {
                    return rest.saveTestTestGroup(resource.getTestTestGroup(testGroup.id, $routeParams.testId)).then(function (testTestGroup) {
                        testTestGroup = testTestGroup.data;
                        $scope.test.testTestGroups.push(testTestGroup);
                        $scope.associatedTestGroups[$scope.associatedTestGroups.length - 1].testTestGroups.push(testTestGroup);
                        $('#addedTestGroup').focus();
                        return $scope.refreshTestGroups();
                    });
                } else {
                    return $scope.associatedTestGroups;
                }

            }
        };

        $scope.addTypeaheadTestGroup = function (testGroup) {
            $scope.typeaheadTestGroups.push(testGroup);
            $scope.typeaheadTestGroups.sort(function(firstTestGroup, secondTestGroup) {
                return firstTestGroup.name.localeCompare(secondTestGroup.name);
            });
        };

        $scope.removeTestFromTestGroup = function(testId, testGroupId) {
            var isTestIdValid = (testId != null && typeof(testId) != "undefined");
            for (var i = 0; i < $scope.associatedTestGroups.length; ++i) {
                var associatedTestGroup = $scope.associatedTestGroups[i];
                if (associatedTestGroup.id == testGroupId) {
                    if (isTestIdValid) {
                        for (var j = 0; j < $scope.test.testTestGroups.length; ++j) {
                            if ($scope.test.testTestGroups[j].testGroupId == testGroupId) {
                                rest.removeTestTestGroupById($scope.test.testTestGroups[j].id).then(function () {
                                    $scope.refreshTestGroups();
                                    $scope.test.testTestGroups.splice(j, 1);
                                });
                            }
                        }
                    }
                    associatedTestGroup.testTestGroups.splice(associatedTestGroup.testTestGroups.length - 1, 1);
                    $scope.addTypeaheadTestGroup(associatedTestGroup);
                    $scope.associatedTestGroups.splice(i, 1);
                }
            }
        };

        $scope.checkTestValid = function() {
            var isTestValid = true;
            if (typeof($scope.test) != "undefined" && $scope.test.restrictQuestionsNumber) {
                if ($scope.test.usedQuestionsNumber < 1) {
                    $scope.testForm.usedQuestionsNumberManual.$setValidity("validUsedQuestionsNumberManual", false);
                    isTestValid = false;
                } else if ($scope.test.usedQuestionsNumber <= $scope.questions.length) {
                    $scope.testForm.usedQuestionsNumberManual.$setValidity("validUsedQuestionsNumberManual", true);
                }
            } else {
                if (typeof($scope.questions) != "undefined" && $scope.questions.length < 1) {
                    $scope.testForm.usedQuestionsNumberAuto.$setValidity("validUsedQuestionsNumberAuto", false);
                    isTestValid = false;
                } else {
                    $scope.testForm.usedQuestionsNumberAuto.$setValidity("validUsedQuestionsNumberAuto", true);
                }
            }
            if ($scope.testForm.$invalid) {
                return false;
            }
            for (var form in $scope.forms) {
                if ($scope.forms.hasOwnProperty(form)) {
                    if (typeof($scope.forms[form]) != "undefined" && $scope.forms[form].$invalid) {
                        return false;
                    }
                }
            }
            return isTestValid;
        };

        $scope.getChosenTestGroupName = function () {
            if (typeof($scope.chosenTestGroup) == "undefined") {
                var chosenTestGroupName = $scope.testForm.chosenTestGroup.$viewValue.name;
                if (typeof(chosenTestGroupName) == "undefined") {
                    return null;
                } else {
                    $scope.chosenTestGroup = $scope.testForm.chosenTestGroup.$viewValue.name;
                    return chosenTestGroupName;
                }
            } else {
                if ($scope.chosenTestGroup.name) {
                    return $scope.chosenTestGroup.name;
                } else {
                    return $scope.chosenTestGroup;
                }
            }
        };

        $scope.checkTestGroupValid = function() {
            if (typeof($scope.testGroupNames) == "undefined") {
                return true;
            }

            var chosenTestGroupName = $scope.getChosenTestGroupName();
            if (!chosenTestGroupName) {
                return false;
            }

            return utils.validateFieldValueNotUnique($scope.testForm.chosenTestGroup, "validTestGroupName", chosenTestGroupName, $scope.testGroupNames);
        };

        $scope.saveImageForQuestion = function(question) {
            var image = new FormData();
            image.append('file', question.imageFile);
            image.append('examinerId', auth.getAuthenticatedUser().id);
            rest.saveImage(image).then(function (imageId) {
                question.imageThumbnail = 'rest/images/' + imageId.data;
                question.imageId = imageId.data;
            });
        };

        $scope.removeImageFromQuestion = function(question) {
            rest.removeImageById(question.imageId).then(function () {
                question.imageFile = null;
                question.imageThumbnail = null;
                question.imageId = null;
            });
        };

        $scope.imageFileChanged = function(question) {
            $scope.saveImageForQuestion(question);
        };

        $scope.saveTestTestGroups = function () {
            $scope.test.testTestGroups = [];
            angular.forEach($scope.associatedTestGroups, function (associatedTestGroup) {
                var testTestGroup = resource.getTestTestGroup(associatedTestGroup.id, $scope.test.id);
                rest.saveTestTestGroup(testTestGroup).then(function (testTestGroup) {
                    $scope.test.testTestGroups.push(testTestGroup.data);
                });
            });
        };

        $scope.saveChanges = function() {
            var isNewTest = ($scope.test.id == null);
            var questionsToRemoveIds = $scope.test.questionIds;
            utils.setTestActiveFrom($scope.test);

            rest.saveTest($scope.test).then(function (test) {
                if (test.status == 200) {
                    $scope.test = test.data;
                    if (isNewTest) {
                        $scope.saveTestTestGroups();
                    }
                    utils.setTestCalendarAndTime($scope.test);
                    $scope.processedQuestions = [];

                    var questionsPromises = [];
                    angular.forEach($scope.questions, function (question) {
                        utils.removeFromArrayIfExists(questionsToRemoveIds, question.id);
                        question.testId = $scope.test.id;
                        resource.processQuestion(question);

                        var promise = null;
                        if (question.id == null) {
                            promise = rest.saveQuestion($scope.test.id, question);
                        } else {
                            promise = rest.updateQuestion($scope.test.id, question.id, question);
                        }
                        promise.then(function (question) {
                            question = question.data;
                            resource.prepareQuestion(question);
                            $scope.addQuestionToArray($scope.processedQuestions, question);
                            $scope.test.questionIds.push(question.id);
                            return question;
                        });
                        questionsPromises.push(promise);
                    });
                    return $q.all(questionsPromises);
                } else {
                    throw({'message': 'Wystąpił błąd podczas zapisywania testu'})
                }
            }).then(
                $q.spread(function (questions) {
                    $scope.questions = $scope.processedQuestions;

                    var questionsPromises = [];
                    angular.forEach(questionsToRemoveIds, function (questionId) {
                        var promise = rest.removeQuestionByTestIdAndQuestionId($scope.test.id, questionId).then(function (question) {
                            return question.data;
                        });
                        questionsPromises.push(promise);
                    });
                    return $q.all(questionsPromises);
                })
            ).then(
                $q.spread(function (questions) {
                    if (isNewTest) {
                        $rootScope.newTestAdded = true;
                        $location.path('/examiner/tests/' + $scope.test.id + '/edit');
                    } else {
                        $scope.refreshTestGroups();
                        $scope.setTestSaveSuccess();
                    }
                })
            ).catch(function (error) {
                $scope.setTestSaveFailure();
            });
        };

        $scope.createNewTest = function () {
            return $q.all([
                rest.getTestsByTestGroupId($routeParams.testGroupId),
                rest.getTestGroupById($routeParams.testGroupId),
                rest.getTestGroupsByExaminerId(auth.getAuthenticatedUser().id)
            ]).then(
                $q.spread(function (testsPage, testGroup, typeaheadTestGroups) {
                    $scope.testsPage = testsPage.data;
                    $scope.testGroup = testGroup.data;
                    $scope.chosenTestGroup = $scope.testGroup.name;
                    $scope.associatedTestGroups = [$scope.testGroup];
                    $scope.questions = [];

                    return $q.all([
                        resource.getNewTest($scope.testGroupId, auth.getAuthenticatedUser().id),
                        $q.all($scope.refreshTestGroups()),
                        rest.getTestGroupsByExaminerIdAndSize(auth.getAuthenticatedUser().id, typeaheadTestGroups.data.totalElements)
                    ]);
                })
            ).then(
                $q.spread(function (test, associatedTestGroups, typeaheadTestGroups) {
                    $scope.test = test;
                    $scope.typeaheadTestGroups = typeaheadTestGroups.data.content;

                    utils.setTestCalendarAndTime($scope.test);

                    for (var i = 0; i < $scope.typeaheadTestGroups.length; ++i) {
                        if ($scope.typeaheadTestGroups[i].name == $scope.testGroup.name) {
                            $scope.typeaheadTestGroups.splice(i, 1);
                            break;
                        }
                    }

                    return $scope.test;
                }
            ));
        };

        $scope.editExistingTest = function () {
            return rest.getTestById($routeParams.testId).then(function (test) {
                $scope.test = test.data;
                utils.setTestCalendarAndTime($scope.test);
                return $q.all([
                    rest.getTestGroupsByExaminerId(auth.getAuthenticatedUser().id),
                    rest.getTestGroupsPageByTestIdAndPage($routeParams.testId, 0)
                ]);
            }).then(
                $q.spread(function (typeaheadTestGroups, associatedTestGroups) {
                    return $q.all([
                        rest.getTestGroupsByExaminerIdAndSize(auth.getAuthenticatedUser().id, typeaheadTestGroups.data.totalElements),
                        rest.getTestGroupsPageByTestIdAndSize($routeParams.testId, associatedTestGroups.data.totalElements)
                    ]);
                })
            ).then(
                $q.spread(function (typeaheadTestGroups, associatedTestGroups) {
                    $scope.typeaheadTestGroups = typeaheadTestGroups.data.content;
                    $scope.associatedTestGroups = associatedTestGroups.data.content;

                    for (var i = 0; i < $scope.associatedTestGroups.length; ++i) {
                        for (var j = 0; j < $scope.typeaheadTestGroups.length; ++j) {
                            if ($scope.typeaheadTestGroups[j].name == $scope.associatedTestGroups[i].name) {
                                $scope.typeaheadTestGroups.splice(j, 1);
                                break;
                            }
                        }
                    }

                    return $scope.refreshTestGroups();
                })
            ).then(function () {
                $scope.questions = [];
                var questionsPromises = [];
                angular.forEach($scope.test.questionIds, function (questionId) {
                    var promise = rest.getQuestionByTestIdAndQuestionId($scope.test.id, questionId).then(function (question) {
                        question = question.data;
                        resource.prepareQuestion(question);
                        $scope.addQuestionToArray($scope.questions, question);
                        return question;
                    });
                    questionsPromises.push(promise);
                });
                return $q.all(questionsPromises);
            });
        };

        $scope.setElementReadOnly = function(element) {
            var inputs = element.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                inputs[i].disabled = true;
                inputs[i].className += " input-disabled";
            }
            var selects = element.getElementsByTagName("select");
            for (var i = 0; i < selects.length; i++) {
                selects[i].disabled = true;
                selects[i].className += " input-disabled";
            }
            var textareas = element.getElementsByTagName("textarea");
            for (var i = 0; i < textareas.length; i++) {
                textareas[i].disabled = true;
                textareas[i].className += " input-disabled";
            }
            var buttons = element.getElementsByTagName("button");
            for (var i = 0; i < buttons.length; i++) {
                buttons[i].disabled = true;
            }
            var labels = element.getElementsByTagName("label");
            for (var i = 0; i < labels.length; i++) {
                labels[i].disabled = true;
            }
        };

        $scope.cleanTestCopy = function () {
            return rest.getDefaultSettings().then(function (defaultSettings) {
                defaultSettings = defaultSettings.data;

                $scope.test.activeFrom = Date.now() + defaultSettings.testActiveAfterDays * 24 * 60 * 60 * 1000;
                utils.setTestCalendarAndTime($scope.test);

                $scope.test.id = null;
                $scope.test.questionIds = [];
                $scope.test.name = $scope.test.name + ' - kopia';
                $scope.test.createdDate = Date.now();

                angular.forEach($scope.questions, function(question) {
                    resource.cleanQuestion(question);
                });

                return $scope.test;
            });
        };

        $scope.checkQuestionReadOnly = function (question) {
            if ($scope.isTestInactive && !$scope.isTestCopy) {
                $scope.setElementReadOnly(document.getElementById("question_" + question.position));
            }
        };

        $scope.checkChoicePatternReadOnly = function (question, index) {
            if ($scope.isTestInactive && !$scope.isTestCopy) {
                $scope.setElementReadOnly(document.getElementById("question_" + question.position + "_choice_" + index));
            }
        };

        $scope.checkJoinPatternReadOnly = function (question, index) {
            if ($scope.isTestInactive && !$scope.isTestCopy) {
                $scope.setElementReadOnly(document.getElementById("question_" + question.position + "_join_" + index));
            }
        };

        auth.checkForExaminer().then(function () {
            $scope.forms = {};
            $scope.newQuestionType = "";
            $scope.questionTypes = combobox.getQuestionTypes();
            if ($routeParams.testId) {
                return $scope.editExistingTest().then(function () {
                    if ($rootScope.newTestAdded) {
                        $rootScope.newTestAdded = false;
                        $scope.setTestSaveSuccess();
                    }
                    $scope.isTestCopy = ($location.path().indexOf('copy') !== -1);
                    $scope.isTestInactive = ($scope.test.activeFrom < Date.now());
                    if ($scope.isTestCopy) {
                        $scope.cleanTestCopy();
                    }
                    if ($scope.isTestInactive && !$scope.isTestCopy) {
                        $scope.isTestReadOnly = true;
                        $scope.setElementReadOnly(document)
                    } else {
                        $scope.isTestReadOnly = false;
                    }
                    return $scope.test;
                });
            } else {
                return $scope.createNewTest();
            }
        }).then(function (test) {

        });

    })

    .controller('examiner-tests-remove', function($scope, $routeParams, $location, $q, auth, rest) {

        $scope.removeTest = function() {
            return rest.removeTestById($routeParams.testId).then(function () {
                if ($scope.testGroups.length > 1) {
                    $location.path("/examiner");
                } else {
                    if ($scope.testGroups[0]) {
                        $location.path("/examiner/test-groups/" + $scope.testGroups[0].id + "/tests");
                    }
                }
            });
        };

        auth.checkForExaminer().then(function () {
            return rest.getTestById($routeParams.testId);
        }).then(function (test) {
            $scope.test = test.data;
            return rest.getTestGroupsPageByTestIdAndPage($routeParams.testId, 0);
        }).then(function (testGroupsPage) {
            return rest.getTestGroupsPageByTestIdAndSize($routeParams.testId, testGroupsPage.data.totalElements);
        }).then(function (testGroupsPage) {
            $scope.testGroups = testGroupsPage.data.content;

            var testGroupsPromises = [];
            angular.forEach($scope.testGroups, function (testGroup) {
                var promise = rest.getTestsPageByTestGroupIdAndPage(testGroup.id, 0).then(function (testsPage) {
                    testGroup.tests = testsPage.data;
                    return testGroup;
                });
                testGroupsPromises.push(promise);
            });
            return $q.all(testGroupsPromises);
        }).then(
            $q.spread(function (testGroups) {

            })
        );

    })

    .controller('examiner-tests-results', function($scope, $routeParams, $q, auth, rest) {

        $scope.getTestsPage = function (page) {
            return rest.getTestsByExaminerIdAndCheckedAndPage(auth.getAuthenticatedUser().id, true, page).then(function (testsPage) {
                $scope.testsPage = testsPage.data;

                var testsPromises = [];
                angular.forEach($scope.testsPage.content, function (test) {
                    var promise = $q.when(test).then(function (test) {
                        if (test.testTestGroups.length == 1) {
                            return rest.getTestGroupById(test.testTestGroups[0].testGroupId).then(function (testGroup) {
                                test.testGroup = testGroup.data;
                                return test;
                            });
                        } else {
                            test.testGroup = null;
                            return test;
                        }
                    });
                    testsPromises.push(promise);
                });
                return $q.all(testsPromises);
            }).then(
                $q.spread(function (tests) {

                })
            );
        };

        auth.checkForExaminer().then(function () {
            return $scope.getTestsPage(0);
        });

    })

    .controller('examiner-test-results', function($scope, $routeParams, $q, auth, rest) {

        $scope.getTestAnswersPage = function (page) {
            return rest.getTestAnswersPageByTestIdAndPageAndChecked($scope.test.id, page, true).then(function (testAnswersPage) {
                $scope.testAnswersPage = testAnswersPage.data;

                var testAnswersPromises = [];
                angular.forEach($scope.testAnswersPage.content, function (testAnswer) {
                    testAnswer.timeSpent = Math.floor((testAnswer.finishedOn - testAnswer.startedOn) / 1000);
                    var promise = rest.getStudentById(testAnswer.studentId).then(function (student) {
                        testAnswer.student = student.data;
                    });
                    testAnswersPromises.push(promise);
                });

                return $q.all(testAnswersPromises);
            });
        };

        auth.checkForExaminer().then(function () {
            return $q.all([
                rest.getTestsPageByExaminerIdAndPageAndChecked(auth.getAuthenticatedUser().id, 0, true),
                rest.getTestById($routeParams.testId)
            ]);
        }).then(
            $q.spread(function (testsPage, test) {
                $scope.testsPage = testsPage.data;
                $scope.test = test.data;

                var testsPromises = [];
                angular.forEach($scope.testsPage.content, function (test) {
                    var promise = $q.when(test).then(function (test) {
                        if (test.testTestGroups.length == 1) {
                            return rest.getTestGroupById(test.testTestGroups[0].testGroupId).then(function (testGroup) {
                                test.testGroup = testGroup.data;
                                return test;
                            });
                        } else {
                            test.testGroup = null;
                            return test;
                        }
                    });
                    testsPromises.push(promise);
                });

                var testPromise = $q.when($scope.test).then(function (test) {
                    if ($scope.test.testTestGroups.length == 1) {
                        return rest.getTestGroupById($scope.test.testTestGroups[0].testGroupId).then(function (testGroup) {
                            $scope.test.testGroup = testGroup.data;
                            return $scope.test;
                        })
                    } else {
                        $scope.test.testGroup = null;
                        return $scope.test;
                    }
                }).then(function () {
                    return $scope.getTestAnswersPage(0);
                }).then(function () {
                    return $scope.test;
                });

                return $q.all([
                    $q.all(testsPromises),
                    testPromise
                ]);
            })
        ).then(
            $q.spread(function (tests, test) {

            })
        );

    })

    .controller('examiner-test-students-results-points', function($scope, $routeParams, $q, auth, rest, utils, resource) {

        auth.checkForExaminer().then(function () {
            return $q.all([
                rest.getStudentById($routeParams.studentId),
                rest.getTestById($routeParams.testId)
            ])
        }).then(
            $q.spread(function (student, test) {
                $scope.student = student.data;
                $scope.test = test.data;

                return $q.all([
                    rest.getTestGroupById($scope.test.testTestGroups[0].testGroupId),
                    rest.getTestAnswersByTestIdAndChecked($scope.test.id , true),
                    rest.getTestAnswersByStudentIdAndTestId($routeParams.studentId, $routeParams.testId)
                ]);
            })
        ).then(
            $q.spread(function (testGroup, testAnswersPage, testAnswers) {
                $scope.testAnswersPage = testAnswersPage.data;
                $scope.testAnswer = testAnswers.data.content[0];

                if ($scope.test.testTestGroups.length == 1) {
                    $scope.testGroup = testGroup.data;
                } else {
                    $scope.testGroup = null;
                }

                var testAnswersPromises = [];
                angular.forEach($scope.testAnswersPage.content, function (testAnswer) {
                    var promise = rest.getStudentById(testAnswer.studentId).then(function (student) {
                        testAnswer.student = student.data;
                    });
                    testAnswersPromises.push(promise);
                });

                utils.setTestAnswerPoints($scope.testAnswer);

                return $q.all([
                    $q.all(testAnswersPromises),
                    rest.getQuestionSequenceById($scope.testAnswer.questionSequenceId)
                ]);
            })
        ).then(
            $q.spread(function (testAnswers, questionSequence) {
                $scope.questionSequence = questionSequence.data;
                $scope.questionAnswersMap = [];

                var questionAnswersPromises = [];
                angular.forEach($scope.testAnswer.questionAnswerIds, function (questionAnswerId) {
                    var promise = rest.getQuestionAnswerByTestAnswerIdAndQuestionAnswerId($scope.testAnswer.id, questionAnswerId).then(function (questionAnswer) {
                        questionAnswer = questionAnswer.data;
                        var questionId = resource.getQuestionIdForQuestionAnswer(questionAnswer);
                        return $q.all([
                            $q.when(questionAnswer),
                            rest.getQuestionByTestIdAndQuestionId($scope.testAnswer.testId, questionId)
                        ]);
                    }).then(
                        $q.spread(function (questionAnswer, question) {
                            question = question.data;
                            utils.setQuestionAnswerPoints(question, questionAnswer);
                            question.positionInSequence = $scope.questionSequence.sequence.indexOf(question.id) + 1;
                            $scope.questionAnswersMap.push({
                                'question': question,
                                'questionAnswer': questionAnswer
                            });
                            return questionAnswer;
                        })
                    );
                    questionAnswersPromises.push(promise);
                });
                return $q.all(questionAnswersPromises);
            })
        ).then(function (questionAnswers) {
            var questionsPromises = [];
            for (var i = $scope.testAnswer.questionAnswerIds.length; i < $scope.questionSequence.sequence.length; ++i) {
                var promise = rest.getQuestionByTestIdAndQuestionId($scope.testAnswer.testId, $scope.questionSequence.sequence[i]).then(function (question) {
                    question = question.data;
                    question.positionInSequence = $scope.questionSequence.sequence.indexOf(question.id) + 1;
                    $scope.questionAnswersMap.push({
                        'question': question,
                        'questionAnswer': null
                    });
                });
                questionsPromises.push(promise);
            }
            return questionsPromises;
        });

    })

    .controller('examiner-test-students-results-times', function($scope, $routeParams, $q, auth, rest, utils, resource) {

        auth.checkForExaminer().then(function () {
            return $q.all([
                rest.getStudentById($routeParams.studentId),
                rest.getTestById($routeParams.testId)
            ]);
        }).then(
            $q.spread(function (student, test) {
                $scope.student = student.data;
                $scope.test = test.data;

                return $q.all([
                    rest.getTestGroupById($scope.test.testTestGroups[0].testGroupId),
                    rest.getTestAnswersByTestIdAndChecked($scope.test.id, true),
                    rest.getTestAnswersByStudentIdAndTestId($routeParams.studentId, $scope.test.id)
                ]);
            })
        ).then(
            $q.spread(function (testGroup, testAnswersPage, testAnswers) {
                $scope.testAnswersPage = testAnswersPage.data;
                $scope.testAnswer = testAnswers.data.content[0];

                if ($scope.test.testTestGroups.length == 1) {
                    $scope.testGroup = testGroup.data;
                } else {
                    $scope.testGroup = null;
                }

                var testAnswersPromises = [];
                angular.forEach($scope.testAnswersPage.content, function (testAnswer) {
                    var promise = rest.getStudentById(testAnswer.studentId).then(function (student) {
                        testAnswer.student = student.data;
                        return testAnswer;
                    });
                    testAnswersPromises.push(promise);
                });

                utils.setTestTimeLimit($scope.test);
                utils.setTestAnswerTimeSpent($scope.test, $scope.testAnswer);

                return $q.all([
                    $q.all(testAnswersPromises),
                    rest.getQuestionSequenceById($scope.testAnswer.questionSequenceId)
                ]);
            })
        ).then(
            $q.spread(function (testAnswers, questionSequence) {
                $scope.questionSequence = questionSequence.data;
                $scope.questionAnswersMap = [];

                var questionAnswersPromises = [];
                angular.forEach($scope.testAnswer.questionAnswerIds, function (questionAnswerId) {
                    var promise = rest.getQuestionAnswerByTestAnswerIdAndQuestionAnswerId($scope.testAnswer.id, questionAnswerId).then(function (questionAnswer) {
                        questionAnswer = questionAnswer.data;
                        var questionId = resource.getQuestionIdForQuestionAnswer(questionAnswer);
                        return $q.all([
                            $q.when(questionAnswer),
                            rest.getQuestionByTestIdAndQuestionId($scope.testAnswer.testId, questionId)
                        ]);
                    }).then(
                        $q.spread(function (questionAnswer, question) {
                            question = question.data;
                            utils.setQuestionAnswerTimeSpent(question, questionAnswer);
                            question.positionInSequence = $scope.questionSequence.sequence.indexOf(question.id) + 1;
                            $scope.questionAnswersMap.push({
                                'question': question,
                                'questionAnswer': questionAnswer
                            });
                            return questionAnswer;
                        })
                    );
                    questionAnswersPromises.push(promise);
                });
                return $q.all(questionAnswersPromises);
            })
        ).then(function (questionAnswers) {
            var questionsPromises = [];
            for (var i = $scope.testAnswer.questionAnswerIds.length; i < $scope.questionSequence.sequence.length; ++i) {
                var promise = rest.getQuestionByTestIdAndQuestionId($scope.testAnswer.testId, $scope.questionSequence.sequence[i]).then(function (question) {
                    question = question.data;
                    question.positionInSequence = $scope.questionSequence.sequence.indexOf(question.id) + 1;
                    $scope.questionAnswersMap.push({
                        'question': question,
                        'questionAnswer': null
                    });
                });
                questionsPromises.push(promise);
            }
            return $q.all(questionsPromises);
        });

    })

    .controller('examiner-tests-check', function($scope, $routeParams, $q, auth, rest) {

        $scope.getTestsToCheckPage = function (page) {
            return rest.getTestsPageByExaminerIdAndPageAndCheckedAndLatestFirst(auth.getAuthenticatedUser().id, page, false, false).then(function (testsToCheckPage) {
                $scope.testsToCheckPage = testsToCheckPage.data;

                var testsPromises = [];
                angular.forEach($scope.testsToCheckPage.content, function (testToCheck) {
                    var promise = rest.getTestGroupById(testToCheck.testTestGroups[0].testGroupId).then(function (testGroup) {
                            if (testToCheck.testTestGroups.length == 1) {
                                testToCheck.testGroup = testGroup.data;
                            } else {
                                testToCheck.testGroup =  null;
                            }
                    });
                    testsPromises.push(promise);
                });
                return $q.all(testsPromises);
            });
        };

        auth.checkForExaminer().then(function () {
            return $scope.getTestsToCheckPage(0);
        });

    })

    .controller('examiner-test-students-results-questions', function($scope, $routeParams, $q, auth, rest, utils, resource) {

        auth.checkForExaminer().then(function () {
            return $q.all([
                rest.getStudentById($routeParams.studentId),
                rest.getTestById($routeParams.testId)
            ]);
        }).then(
            $q.spread(function (student, test) {
                $scope.student = student.data;
                $scope.test = test.data;
                return rest.getTestAnswersByStudentIdAndTestId($routeParams.studentId, $scope.test.id);
            })
        ).then(function (testAnswers) {
            $scope.testAnswer = testAnswers.data.content[0];
            return rest.getQuestionByTestIdAndQuestionId($scope.test.id, $routeParams.questionId);
        }).then(function (question) {
            $scope.question = question.data;
            resource.prepareQuestion($scope.question);
            return rest.getQuestionSequenceById($scope.testAnswer.questionSequenceId);
        }).then(function (questionSequence) {
            $scope.questionSequence = questionSequence.data;
            $scope.questionPositionInSequence = $scope.questionSequence.sequence.indexOf($scope.question.id) + 1;
            var questionAnswerIndex = $scope.questionPositionInSequence - 1;
            if (questionAnswerIndex < $scope.testAnswer.questionAnswerIds.length) {
                var questionAnswerId = $scope.testAnswer.questionAnswerIds[questionAnswerIndex];

                return rest.getQuestionAnswerByTestAnswerIdAndQuestionAnswerId($scope.testAnswer.id, questionAnswerId).then(function (questionAnswer) {
                    $scope.questionAnswer = questionAnswer.data;
                    utils.setQuestionAnswerTimeSpent($scope.question, $scope.questionAnswer);
                    utils.setQuestionAnswerPoints($scope.question, $scope.questionAnswer);
                    utils.processQuestionAnswer($scope.question, $scope.questionAnswer);
                });

            } else {
                throw({'message': 'Brak zapisanej odpowiedzi na wybrane pytanie'});
            }
        });

    })


    .controller('examiner-test-check', function($scope, $routeParams, $q, auth, rest) {

        $scope.getTestAnswersToCheckPage = function (page) {
            return rest.getTestAnswersPageByTestIdAndPageAndChecked($scope.test.id, false, page).then(function (testsToAnswersCheckPage) {
                $scope.testsToAnswersCheckPage = testsToAnswersCheckPage.data;

                var testAnswersPromises = [];
                angular.forEach($scope.testsToAnswersCheckPage.content, function (testAnswer) {
                    testAnswer.timeSpent = (testAnswer.finishedOn - testAnswer.startedOn) / 1000;
                    var promise = rest.getStudentById(testAnswer.studentId).then(function (student) {
                        testAnswer.student = student.data;
                        return testAnswer;
                    });
                    testAnswersPromises.push(promise);
                });
                return $q.all(testAnswersPromises);
            });
        };

        auth.checkForExaminer().then(function () {
            return $q.all([
                rest.getTestsByExaminerIdAndCheckedAndLatestFirst(auth.getAuthenticatedUser().id, false, false),
                rest.getTestById($routeParams.testId)
            ]);
        }).then(
            $q.spread(function (testsToCheckPage, test) {
                $scope.testsToCheckPage = testsToCheckPage.data;

                var testsToCheckPromises = [];
                angular.forEach($scope.testsToCheckPage.content, function (testToCheck) {
                    var promise = rest.getTestGroupById(testToCheck.testTestGroups[0].testGroupId).then(function (testGroup, students, testAnswersPage) {
                        if (testToCheck.testTestGroups.length == 1) {
                            testToCheck.testGroup = testGroup.data;
                        } else {
                            testToCheck.testGroup = null;
                        }
                        return testToCheck;
                    });
                    testsToCheckPromises.push(promise);
                });
                return $q.all(testsToCheckPromises);
            })
        ).then(function (testsToCheck) {
            return rest.getTestById($routeParams.testId);
        }).then(function (test) {
            $scope.test = test.data;
            return rest.getTestGroupById($scope.test.testTestGroups[0].testGroupId);
        }).then(function (testGroup) {
            if ($scope.test.testTestGroups.length == 1) {
                $scope.test.testGroup = testGroup.data;
            } else {
                $scope.test.testGroup = null;
            }
            return $scope.getTestAnswersToCheckPage(0);
        });

    })

    .controller('examiner-test-students-check', function($scope, $routeParams, $location, $q, $window, auth, rest, utils, resource) {

        $scope.setTestSaveSuccess = function () {
            $scope.cleanQuestionSaveMessages();
            $scope.testSaveSuccess = true;
            $scope.testSaveFailure = false;
            $window.scrollTo(0, 0);
        };

        $scope.setTestSaveFailure = function () {
            $scope.cleanQuestionSaveMessages();
            $scope.testSaveSuccess = false;
            $scope.testSaveFailure = true;
            $window.scrollTo(0, 0);
        };

        $scope.cleanTestSaveMessages = function () {
            $scope.testSaveSuccess = false;
            $scope.testSaveFailure = false;
        };

        $scope.setQuestionSaveSuccess = function () {
            $scope.cleanTestSaveMessages();
            $scope.questionSaveSuccess = true;
            $scope.questionSaveFailure = false;
        };

        $scope.setQuestionSaveFailure = function () {
            $scope.cleanTestSaveMessages();
            $scope.questionSaveSuccess = false;
            $scope.questionSaveFailure = true;
        };

        $scope.cleanQuestionSaveMessages = function () {
            $scope.questionSaveSuccess = false;
            $scope.questionSaveFailure = false;
        };

        $scope.saveQuestionGrade = function() {
            var questionAnswer = $scope.questionAnswersMap[$scope.currentQuestionAnswerIndex].questionAnswer;

            return rest.updateQuestionAnswer(questionAnswer.testAnswerId, questionAnswer.id, questionAnswer).then(function (questionAnswer) {
                if (questionAnswer.status == 200) {
                    questionAnswer = questionAnswer.data;
                    questionAnswer.timeSpent = Math.floor((questionAnswer.finishedOn - questionAnswer.startedOn) / 1000);
                    $scope.questionAnswersMap[$scope.currentQuestionAnswerIndex].questionAnswer = questionAnswer;
                    $scope.setQuestionSaveSuccess();
                } else {
                    $scope.setQuestionSaveFailure();
                }
            }, function (response) {
                $scope.setQuestionSaveFailure();
            });
        };

        $scope.saveTestGrade = function() {
            var testAnswer = $scope.testAnswer;
            $scope.testAnswer.checked = true;

            return rest.updateTestAnswer(testAnswer.id, testAnswer).then(function (testAnswer) {
                if (testAnswer.status == 200) {
                    $scope.testAnswer = testAnswer.data;
                    $scope.setTestSaveSuccess();
                } else {
                    $scope.setTestSaveFailure();
                }
            }, function () {
                $scope.setTestSaveFailure();
            });
        };

        $scope.prevQuestionAnswer = function() {
            if ($scope.currentQuestionAnswerIndex > 0) {
                $scope.cleanQuestionSaveMessages();
                --$scope.currentQuestionAnswerIndex;
            }
        };

        $scope.nextQuestionAnswer = function() {
            if ($scope.currentQuestionAnswerIndex < $scope.questionAnswersMap.length - 1) {
                $scope.cleanQuestionSaveMessages();
                ++$scope.currentQuestionAnswerIndex;
            }
        };

        auth.checkForExaminer().then(function () {
            return rest.getTestById($routeParams.testId);
        }).then(function (test) {
            $scope.test = test.data;
            return $q.all([
                rest.getTestGroupById($scope.test.testTestGroups[0].testGroupId),
                rest.getTestAnswersByTestIdAndChecked($routeParams.testId, false),
                rest.getStudentById($routeParams.studentId),
                rest.getTestAnswersByTestIdAndStudentId($routeParams.testId, $routeParams.studentId)
            ])
        }).then(
            $q.spread(function (testGroup, testAnswersPage, student, testAnswers) {
                $scope.testAnswersPage = testAnswersPage.data;
                $scope.student = student.data;
                $scope.testAnswer = testAnswers.data[0];
                $scope.questionAnswersMap = [];
                $scope.currentQuestionAnswerIndex = 0;

                if ($scope.test.testTestGroups.length == 1) {
                    $scope.test.testGroup = testGroup.data;
                } else {
                    $scope.test.testGroup = null;
                }

                var testAnswersPromises = [];
                angular.forEach($scope.testAnswersPage.content, function (testAnswer) {
                    var promise = rest.getStudentById(testAnswer.studentId).then(function (student) {
                        testAnswer.student = student.data;
                        return testAnswer;
                    });
                    testAnswersPromises.push(promise);
                });

                return $q.all([
                    $q.all(testAnswersPromises),
                    rest.getQuestionSequenceById($scope.testAnswer.questionSequenceId)
                ]);
            })
        ).then(
            $q.spread(function (testAnswers, questionSequence) {
                $scope.questionSequence = questionSequence.data;

                var questionAnswersPromises = [];
                angular.forEach($scope.testAnswer.questionAnswerIds, function (questionAnswerId) {
                    var promise = rest.getQuestionAnswerByTestAnswerIdAndQuestionAnswerId($scope.testAnswer.id, questionAnswerId).then(function (questionAnswer) {
                        questionAnswer = questionAnswer.data;
                        questionAnswer.timeSpent = Math.floor((questionAnswer.finishedOn - questionAnswer.startedOn) / 1000);

                        if (utils.isQuestionAnswerManuallyChecked(questionAnswer)) {
                            return rest.getQuestionByTestIdAndQuestionId($scope.test.id, questionAnswer.freetextQuestionId).then(function (question) {
                                question = question.data;
                                resource.prepareQuestion(question);
                                question.positionInSequence = $scope.questionSequence.sequence.indexOf(question.id) + 1;
                                $scope.questionAnswersMap.push({
                                    'question': question,
                                    'questionAnswer': questionAnswer
                                });
                                $scope.questionAnswersMap.sort(function(firstQuestionAnswer, secondQuestionAnswer) {
                                    return firstQuestionAnswer.question.positionInSequence - secondQuestionAnswer.question.positionInSequence;
                                });
                                return questionAnswer;
                            });
                        }

                        return questionAnswer;
                    });
                    questionAnswersPromises.push(promise);
                });
                return $q.all(questionAnswersPromises);
            })
        ).then(function (questionAnswers) {
            $scope.cleanTestSaveMessages();
            $scope.cleanQuestionSaveMessages();
            if ($scope.questionAnswersMap.length == 0) {
                $scope.saveTestGrade();
            }
        });

    });