angular.module('examsystem')

    .controller('viewport', function($rootScope, $scope, $http, $window, $location) {

        $scope.routeChanging = true;

        $scope.isLoading = function() {
            var loadingInProgress = ($http.pendingRequests.length > 0);
            if ($scope.routeChanging && !loadingInProgress) {
                $scope.routeChanging = false;
            }
            return loadingInProgress;
        };

        $scope.isLoginPage = function() {
            return $location.path() == '/login';
        };

        $scope.$on('$routeChangeStart', function(angularEvent, next, current) {
            $scope.routeChanging = true;
        });

        $scope.$on('$routeChangeSuccess', function(angularEvent, current, previous) {
            $window.scrollTo(0, 0);
        });

        $scope.$on('$routeChangeError', function(current, previous, rejection) {
            $window.scrollTo(0, 0);
        });

    });