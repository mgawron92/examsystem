angular.module('examsystem')

    .controller('student-dashboard', function($scope, $q, auth, rest, utils) {

        auth.checkForStudent().then(function () {
            return rest.getListSettings();
        }).then(function (listSettings) {
            listSettings = listSettings.data;
            return $q.all([
                rest.getTestGroupsByStudentIdAndSize(auth.getAuthenticatedUser().id, listSettings.testGroups),
                rest.getTestsByStudentIdAndAnsweredAndActiveOnlyAndNotFinishedAndPageAndSize(auth.getAuthenticatedUser().id, false, true, true, 0, listSettings.tests),
                rest.getTestAnswersByStudentIdAndPageAndSize(auth.getAuthenticatedUser().id, 0, listSettings.testResults),
            ]);
        }).then(
            $q.spread(function (testGroupsPage, testsPage, testAnswersPage) {
                $scope.testGroupsPage = testGroupsPage.data;
                $scope.testsPage = testsPage.data;
                $scope.testAnswersPage = testAnswersPage.data;

                var testGroupsPromises = [];
                angular.forEach($scope.testGroupsPage.content, function(testGroup) {
                    var promise = rest.getTestsByTestGroupIdAndActiveOnly(testGroup.id, true).then(function (tests) {
                        testGroup.activeTestsCount = tests.data.content.length;
                        return testGroup;
                    });
                    testGroupsPromises.push(promise);
                });

                var testsPromises = [];
                angular.forEach($scope.testsPage.content, function (test) {
                    utils.setTestStatus(test);
                    var promise = rest.getTestTestGroupsByTestIdAndStudentId(test.id, auth.getAuthenticatedUser().id).then(function (testTestGroups) {
                        test.testTestGroups = testTestGroups.data;
                        test.testGroupId = testTestGroups.data[0].testGroupId;
                        return rest.getTestGroupById(test.testGroupId);
                    }).then(function (testGroup) {
                        test.testGroup = testGroup.data;
                        return test;
                    });
                    testsPromises.push(promise);
                });

                var testAnswersPromises = [];
                angular.forEach($scope.testAnswersPage.content, function (testAnswer) {
                    var promise = rest.getTestById(testAnswer.testId).then(function (test) {
                        testAnswer.test = test.data;
                        utils.setTestAnswerTimeSpent(testAnswer.test, testAnswer);
                        utils.setTestTimeLimit(testAnswer.test);
                        return rest.getTestGroupsByTestIdAndStudentId(test.data.id, auth.getAuthenticatedUser().id);
                    }).then(function (testGroups) {
                        if (testGroups.data.content.length > 1) {
                            testAnswer.testGroups = testGroups.data.content;
                        } else {
                            testAnswer.testGroup = testGroups.data.content[0];
                        }
                        return testAnswer;
                    });
                    testAnswersPromises.push(promise);
                });
            })
        );

    })

    .controller('student-test-groups', function($scope, $q, auth, rest) {

        $scope.getTestGroupPage = function(page) {
            return rest.getTestGroupsPageByStudentIdAndPage(auth.getAuthenticatedUser().id, page).then(function (testGroupsPage) {
                $scope.testGroupsPage = testGroupsPage.data;

                var testGroupsPromises = [];
                angular.forEach($scope.testGroupsPage.content, function (testGroup) {
                    var promise = rest.getStudentTestGroupsByStudentIdAndTestGroupId(auth.getAuthenticatedUser().id, testGroup.id).then(function (studentTestGroups) {
                        testGroup.studentTestGroup = studentTestGroups.data[0];
                        return rest.getExaminerById(testGroup.examinerId);
                    }).then(function (examiner) {
                        testGroup.examiner = examiner.data;
                        return rest.getTestsByTestGroupIdAndActiveOnly(testGroup.id, true);
                    }).then(function (tests) {
                        testGroup.activeTestsCount = tests.data.content.length;
                        return testGroup;
                    });
                    testGroupsPromises.push(promise);
                });
            });
        };

        auth.checkForStudent().then(function () {
            return $scope.getTestGroupPage(0);
        }).then(
            function (testGroups) {

            }
        );

    })

    .controller('student-test-groups-details', function($scope, $routeParams, $q, auth, rest, utils) {

        auth.checkForStudent().then(function () {
            $scope.testGroupId = $routeParams.testGroupId;
            return $q.all([
                rest.getTestGroupsByStudentId(auth.getAuthenticatedUser().id),
                rest.getTestGroupById($scope.testGroupId),
                rest.getTestsByTestGroupIdAndStudentIdActiveOnlyAndLatestFirstAndNotStarted($scope.testGroupId, auth.getAuthenticatedUser().id, true, false, true),
                rest.getTestAnswersByStudentIdAndTestGroupId(auth.getAuthenticatedUser().id, $scope.testGroupId)
            ]);
        }).then(
            $q.spread(function (testGroupsPage, testGroup, associatedTests, associatedTestAnswers) {
                $scope.testGroupsPage = testGroupsPage.data;
                $scope.testGroup = testGroup.data;
                $scope.associatedTests = associatedTests.data;
                $scope.associatedTestAnswers = associatedTestAnswers.data;

                var testGroupsPromises = [];
                angular.forEach($scope.testGroupsPage.content, function (testGroup) {
                    var promise = rest.getTestsByTestGroupIdAndActiveOnly(testGroup.id, true).then(function (tests) {
                        testGroup.activeTestsCount = tests.data.content.length;
                    });
                    testGroupsPromises.push(promise);
                });

                var testPromise = rest.getStudentTestGroupsByStudentIdAndTestGroupId(auth.getAuthenticatedUser().id, $scope.testGroup.id).then(function (studentTestGroups) {
                    $scope.testGroup.studentTestGroup = studentTestGroups.data[0];
                    return rest.getExaminerById($scope.testGroup.examinerId);
                }).then(function (examiner) {
                    $scope.testGroup.examiner = examiner.data;
                    return $scope.testGroup;
                });

                angular.forEach($scope.associatedTests.content, function (associatedTest) {
                    utils.setTestStatus(associatedTest);
                });

                var associatedTestAnswersPromises = [];
                angular.forEach($scope.associatedTestAnswers.content, function (associatedTestAnswer) {
                    var promise = rest.getTestById(associatedTestAnswer.testId).then(function (test) {
                        associatedTestAnswer.test = test.data;
                        utils.setTestAnswerTimeSpent(associatedTestAnswer.test, associatedTestAnswer);
                        utils.setTestTimeLimit(associatedTestAnswer.test);
                        return associatedTestAnswer;
                    });
                    associatedTestAnswersPromises.push(promise);
                });
            })
        );

    })

    .controller('student-test-groups-leave', function($scope, $routeParams, $location, $q, auth, rest) {

        $scope.leaveTestGroup = function() {
            return rest.getStudentTestGroupsByStudentIdAndTestGroupId(auth.getAuthenticatedUser().id, $scope.testGroup.id).then(function (studentTestGroups) {
                return rest.removeStudentTestGroupById(studentTestGroups.data[0].id);
            }).then(function () {
                $location.path("/student/test-groups");
            });
        };

        auth.checkForStudent().then(function () {
            return $q.all([
                rest.getTestGroupsByStudentId(auth.getAuthenticatedUser().id),
                rest.getTestGroupById($routeParams.testGroupId)
            ]);
        }).then(
            $q.spread(function (testGroupsPage, testGroup) {
                $scope.testGroupsPage = testGroupsPage.data;
                $scope.testGroup = testGroup.data;

                var testGroupsPromises = [];
                angular.forEach($scope.testGroupsPage.content, function (testGroup) {
                    var promise = rest.getTestsByTestGroupIdAndActiveOnly(testGroup.id, true).then(function (tests) {
                        testGroup.activeTestsCount = tests.data.content.length;
                    });
                    testGroupsPromises.push(promise);
                });

                var testGroupPromise = rest.getStudentTestGroupsByStudentIdAndTestGroupId(auth.getAuthenticatedUser().id, $scope.testGroup.id).then(function (studentTestGroups) {
                    $scope.testGroup.studentTestGroup = studentTestGroups.data[0];
                    return rest.getExaminerById($scope.testGroup.examinerId);
                }).then(function (examiner) {
                    $scope.testGroup.examiner = examiner.data;
                    return $scope.testGroup;
                });
            })
        );

    })

    .controller('student-test-groups-tests', function($scope, $routeParams, $q, auth, rest, utils) {

        $scope.getTestsPage = function(page) {
            return rest.getTestsPageByTestGroupIdAndPage($routeParams.testGroupId, page).then(function (testsPage) {
                $scope.testsPage = testsPage.data;

                angular.forEach($scope.testsPage.content, function (test) {
                    rest.getTestAnswersByStudentIdAndTestIdAndFinishedOnly(auth.getAuthenticatedUser().id, test.id, true).then(function (testAnswers) {
                        utils.setTestStatus(test);
                        if (testAnswers.data.totalElements > 0) {
                            test.testStatus = 'finished';
                        }
                    });
                });

                return $scope.testsPage;
            });
        };

        auth.checkForStudent().then(function () {
            return $q.all([
                $scope.getTestsPage(0),
                rest.getTestGroupById($routeParams.testGroupId)
            ]);
        }).then(
            $q.spread(function (testsPage, testGroup) {
                $scope.testGroup = testGroup.data;
                return rest.getStudentTestGroupsByStudentIdAndTestGroupId(auth.getAuthenticatedUser().id, $scope.testGroup.id);
            })
        ).then(function (studentTestGroup) {
            $scope.testGroup.studentTestGroup = studentTestGroup.data[0];
            return rest.getExaminerById($scope.testGroup.examinerId);
        }).then(function (examiner) {
            $scope.testGroup.examiner = examiner.data;
        });

    })

    .controller('student-test-groups-tests-details', function($scope, $q, auth, rest, utils, $rootScope, $routeParams) {

        auth.checkForStudent().then(function () {
            return $q.all([
                rest.getTestsPageByTestGroupIdAndPage($routeParams.testGroupId, 0),
                rest.getTestById($routeParams.testId),
                rest.getTestGroupById($routeParams.testGroupId),
                rest.getTestAnswersByStudentIdAndTestIdAndFinishedOnly(auth.getAuthenticatedUser().id, $routeParams.testId, true)
            ])
        }).then(
            $q.spread(function (testsPage, test, testGroup, testAnswers) {
                $scope.testsPage = testsPage.data;
                $scope.test = test.data;
                $scope.testGroup = testGroup.data;
                $scope.testAnswers = testAnswers.data;

                utils.setTestStatus($scope.test);
                if ($scope.testAnswers.totalElements > 0) {
                    $scope.test.testStatus = 'finished';
                }

                return rest.getStudentTestGroupsByStudentIdAndTestGroupId(auth.getAuthenticatedUser().id, $scope.testGroup.id).then(function (studentTestGroups) {
                    $scope.testGroup.studentTestGroup = studentTestGroups.data[0];
                    return rest.getExaminerById($scope.testGroup.examinerId);
                }).then(function (examiner) {
                    $scope.testGroup.examiner = examiner.data;
                    return $scope.testGroup;
                });
            })
        );

    })

    .controller('student-test-groups-tests-take', function($scope, $rootScope, $routeParams, $location, $timeout, $q, auth, rest, utils, resource) {

        $rootScope.cancelTestTimeout = function() {
            if ($rootScope.testTimeout) {
                $timeout.cancel($rootScope.testTimeout);
                $rootScope.testTimeout = null;
            }
        };

        $rootScope.checkIfTestTimeoutValid = function() {
            var testSubpageCorrect = utils.checkIfTestSubpageIsCorrect($rootScope.test);
            if (!testSubpageCorrect) {
                $rootScope.cancelTestTimeout();
            }
            return testSubpageCorrect;
        };

        $rootScope.makeTestCycle = function() {
            if ($rootScope.checkIfTestTimeoutValid()) {
                utils.updateTestTimeoutInfo($rootScope.testTimeoutInfo);
                if ($rootScope.testTimeoutInfo.secondsLeft > 0) {
                    $rootScope.testTimeout = $timeout($rootScope.makeTestCycle, 1000);
                } else {
                    $rootScope.finishTest();
                }
            }
        };

        $rootScope.finishTest = function() {
            if ($rootScope.checkIfTestTimeoutValid()) {
                $rootScope.cancelTestTimeout();
                if ($location.path() != '/student/tests/' + $rootScope.test.id + '/summary') {
                    $rootScope.finishQuestion().then(function () {
                        $location.path('/student/tests/' + $rootScope.test.id + '/summary');
                    })
                }
            }
        };

        auth.checkForStudent().then(function () {
            return $q.all([
                rest.getTestAnswersByStudentIdAndTestIdAndFinishedOnly(auth.getAuthenticatedUser().id, $routeParams.testId, false),
                rest.getTestAnswersByStudentIdAndTestIdAndFinishedOnly(auth.getAuthenticatedUser().id, $routeParams.testId, true)
            ]);
        }).then(
            $q.spread(function (pendingTestAnswers, finishedTestAnswers) {
                $scope.testAnswer = (pendingTestAnswers.data.content.length > 0) ? pendingTestAnswers.data.content[0] : null;
                if (!$scope.testAnswer) {
                    $scope.testAnswer = (finishedTestAnswers.data.content.length > 0) ? finishedTestAnswers.data.content[0] : null;
                }
                if ($scope.testAnswer) {
                    return rest.getQuestionSequenceById($scope.testAnswer.questionSequenceId);
                } else {
                    return rest.getTestById($routeParams.testId).then(function (test) {
                        $scope.test = test.data;
                        utils.setTestStatus($scope.test);

                        if (!utils.checkIfTestIsActive($scope.test)) {
                            throw({'message': 'Test nie został jeszcze rozpoczęty. Brak dostępu'});
                        }
                        if (utils.checkIfTestIsFinished($scope.test)) {
                            throw({'message': 'Upłynął czas aktywności testu. Brak dostępu'});
                        }
                        if (!utils.checkIfTestHasQuestions($scope.test)) {
                            throw({'message': 'Brak pytań w wybranym teście'});
                        }

                        return rest.saveTestAnswer(resource.getNewTestAnswer($scope.test.id, auth.getAuthenticatedUser().id));
                    }).then(function (testAnswer) {
                        $scope.testAnswer = testAnswer.data;
                        return rest.getQuestionSequenceById($scope.testAnswer.questionSequenceId);
                    });
                }
            })
        ).then(function (questionSequence) {
            $scope.questionSequence = questionSequence.data;

            if (!utils.checkIfQuestionSequenceHasQuestions($scope.questionSequence)) {
                throw({'message': 'Brak pytań w wersji testu dla danego studenta'});
            }

            return rest.getTestById($routeParams.testId);
        }).then(function (test) {
            $scope.test = test.data;
            $rootScope.test = test.data;

            if (utils.checkIfTestAnswerIsFinished($scope.testAnswer)) {
                throw({'message': 'Wypełnianie testu przez użytkownika zostało już zakończone. Brak dostępu'});
            }
            if (utils.checkIfTimeLimitIsExceeded($scope.test, $scope.testAnswer)) {
                throw({'message': 'Upłynął czas przeznaczony na wypełnianie testu. Brak dostępu'});
            }

            if ($scope.test.hasTimeLimit) {
                $rootScope.testTimeoutInfo = utils.initTestTimeoutInfo($scope.test, $scope.testAnswer);
                $rootScope.testTimeout = $timeout($rootScope.makeTestCycle, 1000);
            }
            if ($scope.testAnswer.questionAnswerIds.length > 0) {
                var questionAnswerIndex = $scope.testAnswer.questionAnswerIds.length - 1;
                var questionIndex = questionAnswerIndex;
                var questionAnswerId = $scope.testAnswer.questionAnswerIds[questionAnswerIndex];
                if ($scope.testAnswer.questionAnswerIds.length >= $scope.questionSequence.sequence.length) {
                    rest.getQuestionAnswerByTestAnswerIdAndQuestionAnswerId($scope.testAnswer.id, questionAnswerId).then(function (questionAnswer) {
                        questionAnswer = questionAnswer.data;
                        if (questionAnswer.finishedOn != null) {
                            $location.path('/student/tests/' + $routeParams.testId + '/summary');
                        } else {
                            var questionId = $scope.questionSequence.sequence[questionIndex];
                            $location.path('/student/tests/' + $routeParams.testId + '/questions/' + questionId + '/take');
                        }
                    });
                } else {
                    rest.getQuestionAnswerByTestAnswerIdAndQuestionAnswerId($scope.testAnswer.id, questionAnswerId).then(function (questionAnswer) {
                        questionAnswer = questionAnswer.data;
                        if (questionAnswer.finishedOn != null) {
                            ++questionIndex;
                        }
                        var questionId = $scope.questionSequence.sequence[questionIndex];
                        $location.path('/student/tests/' + $routeParams.testId + '/questions/' + questionId + '/take');
                    });
                }
            } else {
                $location.path('/student/tests/' + $routeParams.testId + '/questions/' + $scope.questionSequence.sequence[0] + '/take');
            }
        }).catch(function (error) {
            var errorPath = ($routeParams.testGroupId) ? '/student/test-groups/' + $routeParams.testGroupId + '/details' : '/student/dashboard';
            $location.path(errorPath);
        });

    })

    .controller('student-tests-questions-take', function($scope, $rootScope, $routeParams, $q, auth, rest, utils, resource, $location, $timeout) {

        $rootScope.cancelQuestionTimeout = function() {
            if ($rootScope.questionTimeout) {
                $timeout.cancel($rootScope.questionTimeout);
                $rootScope.questionTimeout = null;
            }
        };

        $rootScope.checkIfQuestionTimeoutValid = function() {
            var questionSubpageCorrect = utils.checkIfQuestionSubpageIsCorrect($rootScope.test, $rootScope.question);
            if (!questionSubpageCorrect) {
                $rootScope.cancelQuestionTimeout();
            }
            return questionSubpageCorrect;
        };

        $rootScope.makeQuestionCycle = function() {
            if ($rootScope.checkIfQuestionTimeoutValid()) {
                utils.updateQuestionTimeoutInfo($rootScope.questionTimeoutInfo);
                if ($rootScope.questionTimeoutInfo.secondsLeft > 0) {
                    $rootScope.questionTimeout = $timeout($rootScope.makeQuestionCycle, 1000);
                } else {
                    $rootScope.finishQuestion();
                }
            }
        };

        $rootScope.finishQuestion = function() {
            if ($scope.checkIfQuestionTimeoutValid()) {
                $rootScope.cancelQuestionTimeout();
                resource.processQuestionAnswer($scope.question, $scope.questionAnswer);
                return rest.updateQuestionAnswer($scope.testAnswer.id, $scope.questionAnswer.id, $scope.questionAnswer).then(function (questionAnswer) {
                    $location.path('/student/tests/' + $routeParams.testId + '/questions/' + $routeParams.questionId + '/next');
                });
            }
        };

        auth.checkForStudent().then(function () {
            return $q.all([
                rest.getTestById($routeParams.testId),
                rest.getQuestionByTestIdAndQuestionId($routeParams.testId, $routeParams.questionId)
            ]);
        }).then(
            $q.spread(function (test, question) {
                $scope.test = test.data;

                if (!$rootScope.test) {
                    $location.path("/student/tests/" + $scope.test.id + "/take");
                    throw({'message': 'Question answer page refreshed'});
                }

                utils.setTestTimeLimit($scope.test);
                $scope.question = question.data;
                $rootScope.question = question.data;
                utils.setQuestionImagePath($scope.question);
                return rest.getTestAnswersByStudentIdAndTestIdAndFinishedOnly(auth.getAuthenticatedUser().id, $scope.test.id, false);
            })
        ).then(function (testAnswers) {
            $scope.testAnswer = testAnswers.data.content[0];
            return rest.getQuestionSequenceById($scope.testAnswer.questionSequenceId);
        }).then(function (questionSequence) {
            $scope.questionSequence = questionSequence.data;
            $scope.questionPositionInSequence = $scope.questionSequence.sequence.indexOf($scope.question.id) + 1;
            if ($scope.testAnswer.questionAnswerIds.length == $scope.questionPositionInSequence) {
                var questionAnswerIndex = $scope.testAnswer.questionAnswerIds.length - 1;
                var questionAnswerId = $scope.testAnswer.questionAnswerIds[questionAnswerIndex];
                return rest.getQuestionAnswerByTestAnswerIdAndQuestionAnswerId($scope.testAnswer.id, questionAnswerId).then(function (questionAnswer) {
                    $scope.questionAnswer = questionAnswer.data;
                    resource.prepareQuestionAnswer($scope.question, $scope.questionAnswer);

                    var questionTimeLimit;
                    if ($scope.question.hasTimeLimit) {
                        questionTimeLimit = ($scope.question.timeLimitMinutes * 60 + $scope.question.timeLimitSeconds) * 1000;
                    } else {
                        questionTimeLimit = 6 * 60 * 60 * 1000;
                    }
                    var maxFinishDate = $scope.questionAnswer.startedOn + questionTimeLimit;
                    if (Date.now() > maxFinishDate) {
                        $location.path('/student/tests/' + $scope.test.id + "/questions/" + $scope.question.id + "/next");
                        throw({'message': 'Time limit for sending question answer has exceeded'});
                    }

                });
            } else {
                $scope.questionAnswer = resource.getQuestionAnswerForQuestion($scope.testAnswer, $scope.question);
                return rest.saveQuestionAnswer($scope.testAnswer.id, $scope.questionAnswer).then(function (questionAnswer) {
                    $scope.questionAnswer = questionAnswer.data;
                    resource.prepareQuestionAnswer($scope.question, $scope.questionAnswer);
                });
            }
        }).then(function () {
            if ($scope.question.hasTimeLimit) {
                $rootScope.questionTimeoutInfo = utils.initQuestionTimeoutInfo($scope.question, $scope.questionAnswer);
                $rootScope.questionTimeout = $timeout($rootScope.makeQuestionCycle, 1000);
            }
        }).catch(function (error) {

        });

    })

    .controller('student-tests-questions-next', function($scope, $routeParams, $location, auth, rest) {

        auth.checkForStudent().then(function () {
            if (!$routeParams.testId) {
                throw({'mssage': 'Brak identyfikatora testu'});
            }
            return rest.getTestAnswersByStudentIdAndTestIdAndFinishedOnly(auth.getAuthenticatedUser().id, $routeParams.testId, false);
        }).then(function (testAnswers) {
            $scope.testAnswer = (testAnswers.data.content.length > 0) ? testAnswers.data.content[0] : null;
            return rest.getQuestionSequenceById($scope.testAnswer.questionSequenceId);
        }).then(function (questionSequence) {
            $scope.questionSequence = questionSequence.data;
            var questionPosition = $scope.questionSequence.sequence.indexOf(parseInt($routeParams.questionId));
            if (questionPosition == null) {
                $location.path('/student/test-groups/' + $scope.test.id + '/details');
            } else if (questionPosition == $scope.questionSequence.sequence.length - 1) {
                $location.path('/student/tests/' + $routeParams.testId + '/summary');
            } else {
                $location.path('/student/tests/' + $routeParams.testId + '/questions/' + $scope.questionSequence.sequence[questionPosition + 1] + '/take');
            }
        }).catch(function (error) {
            $location.path('/student/dashboard');
        });

    })

    .controller('student-tests-questions-summary', function($scope, $routeParams, $location, $q, auth, rest, utils, resource) {

        auth.checkForStudent().then(function () {
            return rest.getTestById($routeParams.testId);
        }).then(function (test) {
            $scope.test = test.data;
            return rest.getTestAnswersByStudentIdAndTestIdAndFinishedOnly(auth.getAuthenticatedUser().id, $routeParams.testId, false);
        }).then(function (testAnswers) {
            $scope.testAnswer = (testAnswers.data.content.length > 0) ? testAnswers.data.content[0] : null;
            return rest.getQuestionSequenceById($scope.testAnswer.questionSequenceId);
        }).then(function (questionSequence) {
            $scope.questionSequence = questionSequence.data;
            if ($scope.testAnswer) {
                if ($scope.testAnswer.finishedOn) {
                    return $q.when($scope.testAnswer);
                } else {
                    resource.processTestAnswer($scope.testAnswer);
                    return rest.updateTestAnswer($scope.testAnswer.id, $scope.testAnswer).then(function (testAnswer) {
                        $scope.testAnswer = testAnswer.data;
                        return $scope.testAnswer;
                    });
                }
            } else {
                throw({'message': 'Nie znaleziono odpowiedzi dla wybranego testu'});
            }
        }).then(function (testAnswer) {
            utils.setTestAnswerTimeSpent($scope.test, $scope.testAnswer);
            utils.setTestTimeLimit($scope.test);

            $scope.questionAnswersMap = [];

            var questionAnswerPromises = [];
            angular.forEach($scope.testAnswer.questionAnswerIds, function (questionAnswerId) {
                questionAnswerPromises.push(rest.getQuestionAnswerByTestAnswerIdAndQuestionAnswerId($scope.testAnswer.id, questionAnswerId));
            });

            return $q.all(questionAnswerPromises);
        }).then(function (questionAnswers) {

            var questionsPromises = [];
            angular.forEach(questionAnswers, function (questionAnswer) {
                questionAnswer = questionAnswer.data;
                var questionId = resource.getQuestionIdForQuestionAnswer(questionAnswer);
                var promise = rest.getQuestionByTestIdAndQuestionId($scope.testAnswer.testId, questionId).then(function (question) {
                    question = question.data;
                    utils.setQuestionAnswerTimeSpent(question, questionAnswer);
                    question.positionInSequence = $scope.questionSequence.sequence.indexOf(question.id) + 1;

                    $scope.questionAnswersMap.push({
                        'question': question,
                        'questionAnswer': questionAnswer
                    });

                    return question;
                });
                questionsPromises.push(promise);
            });
            return $q.all(questionsPromises);

        }).then(function (questions) {
            var questionsPromises = [];
            for (var i = $scope.testAnswer.questionAnswerIds.length; i < $scope.questionSequence.sequence.length; ++i) {
                var promise = rest.getQuestionByTestIdAndQuestionId($scope.testAnswer.testId, $scope.questionSequence.sequence[i]).then(function (question) {
                    question = question.data;
                    question.positionInSequence = $scope.questionSequence.sequence.indexOf(question.id) + 1;
                    $scope.questionAnswersMap.push({
                        'question': question,
                        'questionAnswer': null
                    });
                    return question;
                });
                questionsPromises.push(promise);
            }
            return $q.all(questionsPromises);
        }).catch(function (error) {
            $location.path('/student/test-groups/' + $scope.test.testGroupId + '/details');
        });

    })

    .controller('student-tests-results-points', function($scope, $routeParams, $q, auth, rest, utils, resource) {

        auth.checkForStudent().then(function () {
            return rest.getTestById($routeParams.testId);
        }).then(function (test) {
            $scope.test = test.data;
            return rest.getTestGroupsByTestIdAndStudentId($scope.test.id, auth.getAuthenticatedUser().id);
        }).then(function (testGroups) {
            $scope.testGroups = testGroups.data.content;

            var testGroupsPromises = [];
            angular.forEach($scope.testGroups, function (testGroup) {
                var testGroupPromise = rest.getTestAnswersByStudentIdAndTestGroupIdAndChecked(auth.getAuthenticatedUser().id, testGroup.id, true).then(function (testAnswers) {
                    testGroup.testAnswersPage = testAnswers.data;

                    var testsPromises = [];
                    angular.forEach(testGroup.testAnswersPage.content, function (testAnswer) {
                        var testPromise = rest.getTestById(testAnswer.testId).then(function (test) {
                            testAnswer.test = test.data;
                            return test;
                        });
                        testsPromises.push(testPromise);
                    });
                    return $q.all(testsPromises);

                }).then(function (tests) {
                    return testGroup;
                });
                testGroupsPromises.push(testGroupPromise);
            });
            return $q.all(testGroupsPromises);
        }).then(function (testGroups) {
            return rest.getTestAnswersByStudentIdAndTestId(auth.getAuthenticatedUser().id, $scope.test.id);
        }).then(function (testAnswers) {
            $scope.testAnswer = (testAnswers.data.content.length > 0) ? testAnswers.data.content[0] : null;
            utils.setTestAnswerPoints($scope.testAnswer);
            return rest.getQuestionSequenceById($scope.testAnswer.questionSequenceId);
        }).then(function (questionSequence) {
            $scope.questionSequence = questionSequence.data;
            $scope.questionAnswersMap = [];

            var questionAnswersPromises = [];
            angular.forEach($scope.testAnswer.questionAnswerIds, function (questionAnswerId) {
                questionAnswersPromises.push(rest.getQuestionAnswerByTestAnswerIdAndQuestionAnswerId($scope.testAnswer.id, questionAnswerId));
            });

            return $q.all(questionAnswersPromises);
        }).then(function (questionAnswers) {
            var questionsPromises = [];
            angular.forEach(questionAnswers, function (questionAnswer) {
                questionAnswer = questionAnswer.data;
                var questionId = resource.getQuestionIdForQuestionAnswer(questionAnswer);
                var promise = rest.getQuestionByTestIdAndQuestionId($scope.testAnswer.testId, questionId).then(function (question) {
                    question = question.data;
                    utils.setQuestionAnswerPoints(question, questionAnswer);
                    question.positionInSequence = $scope.questionSequence.sequence.indexOf(question.id) + 1;

                    $scope.questionAnswersMap.push({
                        'question': question,
                        'questionAnswer': questionAnswer
                    });

                    return question;
                });
                questionsPromises.push(promise);
            });
            return $q.all(questionsPromises)
        }).then(function (questions) {
            var questionsPromises = [];
            for (var i = $scope.testAnswer.questionAnswerIds.length; i < $scope.questionSequence.sequence.length; ++i) {
                var promise = rest.getQuestionByTestIdAndQuestionId($scope.testAnswer.testId, $scope.questionSequence.sequence[i]).then(function (question) {
                    question = question.data;
                    question.positionInSequence = $scope.questionSequence.sequence.indexOf(question.id) + 1;
                    $scope.questionAnswersMap.push({
                        'question': question,
                        'questionAnswer': null
                    });
                    return question;
                });
                questionsPromises.push(promise);
            }
            return $q.all(questionsPromises);
        });

    })

    .controller('student-tests-results-times', function($scope, $routeParams, $q, auth, rest, utils, resource) {

        auth.checkForStudent().then(function () {
            return rest.getTestById($routeParams.testId);
        }).then(function (test) {
            $scope.test = test.data;
            return rest.getTestGroupsByTestIdAndStudentId($scope.test.id, auth.getAuthenticatedUser().id);
        }).then(function (testGroups) {
            $scope.testGroups = testGroups.data.content;

            var testGroupsPromises = [];
            angular.forEach($scope.testGroups, function (testGroup) {
                var testGroupPromise = rest.getTestAnswersByStudentIdAndTestGroupIdAndChecked(auth.getAuthenticatedUser().id, testGroup.id, true).then(function (testAnswersPage) {
                    testGroup.testAnswersPage = testAnswersPage.data;

                    var testsPromises = [];
                    angular.forEach(testGroup.testAnswersPage.content, function (testAnswer) {
                        var testPromise = rest.getTestById(testAnswer.testId).then(function (test) {
                            testAnswer.test = test.data;
                            return test;
                        });
                        testsPromises.push(testPromise);
                    });
                    return $q.all(testsPromises);
                });
                testGroupsPromises.push(testGroupPromise);
            });
            return $q.all(testGroupsPromises);
        }).then(function (testGroups) {
            return rest.getTestAnswersByStudentIdAndTestGroupIdAndChecked(auth.getAuthenticatedUser().id, $scope.testGroups[0].id, true);
        }).then(function (testAnswersPage) {
            $scope.testAnswersPage = testAnswersPage.data;

            var testAnswersPromises = [];
            angular.forEach($scope.testAnswersPage.content, function (testAnswer) {
                var promise = rest.getTestById(testAnswer.testId).then(function (test) {
                    testAnswer.test = test.data;
                    return testAnswer;
                });
                testAnswersPromises.push(promise);
            });
            return $q.all(testAnswersPromises);
        }).then(function (testAnswers) {
            return rest.getTestAnswersByStudentIdAndTestId(auth.getAuthenticatedUser().id, $scope.test.id);
        }).then(function (testAnswers) {
            $scope.testAnswer = (testAnswers.data.content.length > 0) ? testAnswers.data.content[0] : null;
            utils.setTestTimeLimit($scope.test);
            utils.setTestAnswerTimeSpent($scope.test, $scope.testAnswer);
            return rest.getQuestionSequenceById($scope.testAnswer.questionSequenceId);
        }).then(function (questionSequence) {
            $scope.questionSequence = questionSequence.data;
            $scope.questionAnswersMap = [];

            var questionAnswersPromises = [];
            angular.forEach($scope.testAnswer.questionAnswerIds, function (questionAnswerId) {
                questionAnswersPromises.push(rest.getQuestionAnswerByTestAnswerIdAndQuestionAnswerId($scope.testAnswer.id, questionAnswerId));
            });
            return $q.all(questionAnswersPromises);
        }).then(function (questionAnswers) {
            var questionsPromises = [];
            angular.forEach(questionAnswers, function (questionAnswer) {
                questionAnswer = questionAnswer.data;
                var questionId = resource.getQuestionIdForQuestionAnswer(questionAnswer);
                var promise = rest.getQuestionByTestIdAndQuestionId($scope.testAnswer.testId, questionId).then(function (question) {
                    question = question.data;
                    utils.setQuestionAnswerTimeSpent(question, questionAnswer);
                    question.positionInSequence = $scope.questionSequence.sequence.indexOf(question.id) + 1;

                    $scope.questionAnswersMap.push({
                        'question': question,
                        'questionAnswer': questionAnswer
                    });

                    return question;
                });
                questionsPromises.push(promise);
            });
            return $q.all(questionsPromises);
        }).then(function (questions) {
            var questionsPromises = [];
            for (var i = $scope.testAnswer.questionAnswerIds.length; i < $scope.questionSequence.sequence.length; ++i) {
                var promise = rest.getQuestionByTestIdAndQuestionId($scope.testAnswer.testId, $scope.questionSequence.sequence[i]).then(function (question) {
                    question = question.data;
                    question.positionInSequence = $scope.questionSequence.sequence.indexOf(question.id) + 1;

                    $scope.questionAnswersMap.push({
                        'question': question,
                        'questionAnswer': null
                    });
                    return question;
                });
                questionsPromises.push(promise);
            }
            return $q.all(questionsPromises);
        });

    })

    .controller('student-tests-results-questions', function($scope, $routeParams, $location, auth, rest, utils) {

        auth.checkForStudent().then(function () {
            return rest.getTestById($routeParams.testId);
        }).then(function (test) {
            $scope.test = test.data;
            return rest.getTestAnswersByStudentIdAndTestId(auth.getAuthenticatedUser().id, $scope.test.id);
        }).then(function (testAnswersPage) {
            $scope.testAnswer = (testAnswersPage.data.content.length > 0) ? testAnswersPage.data.content[0] : null;
            return rest.getQuestionByTestIdAndQuestionId($scope.test.id, $routeParams.questionId);
        }).then(function (question) {
            $scope.question = question.data;
            utils.setQuestionImagePath($scope.question);
            return rest.getQuestionSequenceById($scope.testAnswer.questionSequenceId);
        }).then(function (questionSequence) {
            $scope.questionSequence = questionSequence.data;
            $scope.questionPositionInSequence = $scope.questionSequence.sequence.indexOf($scope.question.id) + 1;
            var questionAnswerIndex = $scope.questionPositionInSequence - 1;

            if (questionAnswerIndex >= $scope.testAnswer.questionAnswerIds.length) {
                throw({'message': 'Nie znaleziono odpowiedzi dla wybranego pytania'});
            }

            var questionAnswerId = $scope.testAnswer.questionAnswerIds[questionAnswerIndex];
            return rest.getQuestionAnswerByTestAnswerIdAndQuestionAnswerId($scope.testAnswer.id, questionAnswerId);
        }).then(function (questionAnswer) {
            $scope.questionAnswer = questionAnswer.data;
            utils.setQuestionAnswerTimeSpent($scope.question, $scope.questionAnswer);
            utils.setQuestionAnswerPoints($scope.question, $scope.questionAnswer);
            utils.processQuestionAnswer($scope.question, $scope.questionAnswer);
        }).catch(function (error) {
            $location.path('/student/tests/' + $routeParams.testId + '/results/points');
        });

    })

    .controller('student-test-groups-results', function($scope, $routeParams, $location, $q, auth, rest, utils) {

        $scope.getTestAnswersPage = function (page) {
            return rest.getTestAnswersByStudentIdAndTestGroupIdAndPageAndChecked(auth.getAuthenticatedUser().id, $scope.testGroup.id, page, true).then(function (testAnswersPage) {
                $scope.testAnswersPage = testAnswersPage.data;

                var testsPromises = [];
                angular.forEach($scope.testAnswersPage.content, function (testAnswer) {
                    var promise = rest.getTestById(testAnswer.testId).then(function (test) {
                        test = test.data;
                        testAnswer.test = test;
                        utils.setTestAnswerTimeSpent(test, testAnswer);
                        utils.setTestTimeLimit(test);
                        return test;
                    });
                    testsPromises.push(promise);
                });
                return $q.all(testsPromises);
            });
        };

        auth.checkForStudent().then(function () {
            return rest.getTestGroupById($routeParams.testGroupId);
        }).then(function (testGroup) {
            $scope.testGroup = testGroup.data;
            return $scope.getTestAnswersPage(0);
        });

    })

    .controller('student-test-results', function($scope, $routeParams, $q, auth, rest, utils) {

        $scope.getTestAnswersPage = function (page) {
            return rest.getTestAnswersByStudentIdAndPage(auth.getAuthenticatedUser().id, page).then(function (testAnswersPage) {
                $scope.testAnswersPage = testAnswersPage.data;

                var testsPromises = [];
                angular.forEach($scope.testAnswersPage.content, function (testAnswer) {
                    var promise = rest.getTestById(testAnswer.testId).then(function (test) {
                        test = test.data;
                        testAnswer.test = test;
                        utils.setTestAnswerTimeSpent(test, testAnswer);
                        utils.setTestTimeLimit(test);
                        return test;
                    });
                    testsPromises.push(promise);
                });
                return $q.all(testsPromises);
            });
        };

        auth.checkForStudent().then(function () {
            return $scope.getTestAnswersPage(0);
        });

    })

    .controller('student-tests-all', function($scope, $routeParams, $q, auth, rest, utils) {

        $scope.getTestsPage = function (page) {
            return rest.getTestsByStudentIdAndPageAndAnsweredAndActiveOnly(auth.getAuthenticatedUser().id, page, false, true).then(function (testsPage) {
                $scope.testsPage = testsPage.data;

                angular.forEach($scope.testsPage.content, function (test) {
                    utils.setTestStatus(test);
                });
            });
        };

        auth.checkForStudent().then(function () {
            return $scope.getTestsPage(0);
        });

    });