angular.module('examsystem')

    .controller('router', function($rootScope, $http, $location, auth, resource) {

        auth.check($rootScope.role).then(function () {
            var landingPages = resource.getLandingPages();
            if ($rootScope.role in landingPages) {
                $location.path(landingPages[$rootScope.role]);
            } else {
                auth.setAuthenticatedUser(false, null, null);
                $location.path('/login');
            }
        });

        $rootScope.debugMode = false;

    })

    .config(function($routeProvider, $httpProvider) {

        $routeProvider
            .when('/login', {
                templateUrl : 'templates/login/login.html',
                controller : 'login',
                controllerAs: 'controller'
            })
            .when('/router', {
                template : '',
                controller : 'router',
                controllerAs: 'controller'
            })
            .otherwise('/login');

        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

    })

    .config(function($routeProvider) {

        $routeProvider
            .when('/admin', {
                templateUrl : 'templates/admin/dashboard/dashboard.html',
                controller : 'admin-dashboard',
                controllerAs: 'controller'
            })
            .when('/admin/admins', {
                templateUrl : 'templates/admin/admins/list.html',
                controller : 'admin-admins-list',
                controllerAs: 'controller'
            })
            .when('/admin/admins/add', {
                templateUrl : 'templates/admin/admins/add.html',
                controller : 'admin-admins-add',
                controllerAs: 'controller'
            })
            .when('/admin/admins/:adminId/details', {
                templateUrl : 'templates/admin/admins/details.html',
                controller : 'admin-admins-details',
                controllerAs: 'controller'
            })
            .when('/admin/admins/:adminId/remove', {
                templateUrl : 'templates/admin/admins/remove.html',
                controller : 'admin-admins-remove',
                controllerAs: 'controller'
            })
            .when('/admin/examiners', {
                templateUrl : 'templates/admin/examiners/list.html',
                controller : 'admin-examiners-list',
                controllerAs: 'controller'
            })
            .when('/admin/examiners/add', {
                templateUrl : 'templates/admin/examiners/add.html',
                controller : 'admin-examiners-add',
                controllerAs: 'controller'
            })
            .when('/admin/examiners/:examinerId/details', {
                templateUrl : 'templates/admin/examiners/details.html',
                controller : 'admin-examiners-details',
                controllerAs: 'controller'
            })
            .when('/admin/examiners/:examinerId/remove', {
                templateUrl : 'templates/admin/examiners/remove.html',
                controller : 'admin-examiners-remove',
                controllerAs: 'controller'
            })
            .when('/admin/backups', {
                templateUrl : 'templates/admin/options/backups.html',
                controller : 'admin-backups',
                controllerAs: 'controller'
            })
            .when('/admin/defaults', {
                templateUrl : 'templates/admin/options/defaults.html',
                controller : 'admin-defaults',
                controllerAs: 'controller'
            })
            .when('/admin/lists', {
                templateUrl : 'templates/admin/options/lists.html',
                controller : 'admin-lists',
                controllerAs: 'controller'
            });

    })

    .config(function($routeProvider) {

        $routeProvider
            .when('/examiner', {
                templateUrl : 'templates/examiner/dashboard/dashboard.html',
                controller : 'examiner-dashboard',
                controllerAs: 'controller'
            })
            .when('/examiner/test-groups', {
                templateUrl : 'templates/examiner/test-groups/list.html',
                controller : 'examiner-test-groups',
                controllerAs: 'controller'
            })
            .when('/examiner/test-groups/add', {
                templateUrl : 'templates/examiner/test-groups/test-group/add.html',
                controller : 'examiner-test-groups-add',
                controllerAs: 'controller'
            })
            .when('/examiner/test-groups/:testGroupId/edit', {
                templateUrl : 'templates/examiner/test-groups/test-group/edit.html',
                controller : 'examiner-test-groups-edit',
                controllerAs: 'controller'
            })
            .when('/examiner/test-groups/:testGroupId/copy', {
                templateUrl : 'templates/examiner/test-groups/test-group/edit.html',
                controller : 'examiner-test-groups-edit',
                controllerAs: 'controller'
            })
            .when('/examiner/test-groups/:testGroupId/remove', {
                templateUrl : 'templates/examiner/test-groups/test-group/remove.html',
                controller : 'examiner-test-groups-remove',
                controllerAs: 'controller'
            })
            .when('/examiner/test-groups/:testGroupId/tests', {
                templateUrl : 'templates/examiner/test-groups/test/list.html',
                controller : 'examiner-test-groups-tests',
                controllerAs: 'controller'
            })
            .when('/examiner/test-groups/:testGroupId/tests/:testId/remove-from-group', {
                templateUrl : 'templates/examiner/test-groups/test/remove-from-group.html',
                controller : 'examiner-test-groups-tests-remove-from-group',
                controllerAs: 'controller'
            })
            .when('/examiner/tests/:testId/edit', {
                templateUrl : 'templates/examiner/tests/edit.html',
                controller : 'examiner-tests-edit',
                controllerAs: 'controller'
            })
            .when('/examiner/tests/:testId/copy', {
                templateUrl : 'templates/examiner/tests/edit.html',
                controller : 'examiner-tests-edit',
                controllerAs: 'controller'
            })
            .when('/examiner/test-groups/:testGroupId/tests/add', {
                templateUrl : 'templates/examiner/tests/add.html',
                controller : 'examiner-tests-edit',
                controllerAs: 'controller'
            })
            .when('/examiner/test-groups/:testGroupId/tests/:testId/remove', {
                templateUrl : 'templates/examiner/tests/remove.html',
                controller : 'examiner-tests-remove',
                controllerAs: 'controller'
            })
            .when('/examiner/tests/:testId/remove', {
                templateUrl : 'templates/examiner/tests/remove.html',
                controller : 'examiner-tests-remove',
                controllerAs: 'controller'
            })
            .when('/examiner/results', {
                templateUrl : 'templates/examiner/results/list.html',
                controller : 'examiner-tests-results',
                controllerAs: 'controller'
            })
            .when('/examiner/tests/:testId/results', {
                templateUrl : 'templates/examiner/results/test/list.html',
                controller : 'examiner-test-results',
                controllerAs: 'controller'
            })
            .when('/examiner/tests/:testId/students/:studentId/results/points', {
                templateUrl : 'templates/examiner/results/student/points.html',
                controller : 'examiner-test-students-results-points',
                controllerAs: 'controller'
            })
            .when('/examiner/tests/:testId/students/:studentId/results/times', {
                templateUrl : 'templates/examiner/results/student/times.html',
                controller : 'examiner-test-students-results-times',
                controllerAs: 'controller'
            })
            .when('/examiner/tests/:testId/students/:studentId/questions/:questionId/results', {
                templateUrl : 'templates/examiner/results/student/question/question.html',
                controller : 'examiner-test-students-results-questions',
                controllerAs: 'controller'
            })
            .when('/examiner/check', {
                templateUrl : 'templates/examiner/check/list.html',
                controller : 'examiner-tests-check',
                controllerAs: 'controller'
            })
            .when('/examiner/tests/:testId/check', {
                templateUrl : 'templates/examiner/check/test/check.html',
                controller : 'examiner-test-check',
                controllerAs: 'controller'
            })
            .when('/examiner/tests/:testId/students/:studentId/check', {
                templateUrl : 'templates/examiner/check/student/check.html',
                controller : 'examiner-test-students-check',
                controllerAs: 'controller'
            });

    })

    .config(function($routeProvider) {

        $routeProvider
            .when('/student', {
                templateUrl : 'templates/student/dashboard/dashboard.html',
                controller : 'student-dashboard',
                controllerAs: 'controller'
            })
            .when('/student/test-groups', {
                templateUrl : 'templates/student/test-groups/list.html',
                controller : 'student-test-groups',
                controllerAs: 'controller'
            })
            .when('/student/test-groups/:testGroupId/details', {
                templateUrl : 'templates/student/test-groups/details.html',
                controller : 'student-test-groups-details',
                controllerAs: 'controller'
            })
            .when('/student/test-groups/:testGroupId/leave', {
                templateUrl : 'templates/student/test-groups/leave.html',
                controller : 'student-test-groups-leave',
                controllerAs: 'controller'
            })
            .when('/student/test-groups/:testGroupId/tests', {
                templateUrl : 'templates/student/test-groups/tests/list.html',
                controller : 'student-test-groups-tests',
                controllerAs: 'controller'
            })
            .when('/student/test-groups/:testGroupId/tests/:testId/details', {
                templateUrl : 'templates/student/test-groups/tests/details.html',
                controller : 'student-test-groups-tests-details',
                controllerAs: 'controller'
            })
            .when('/student/test-groups/:testGroupId/tests/:testId/take', {
                templateUrl : 'templates/common/blank.html',
                controller : 'student-test-groups-tests-take',
                controllerAs: 'controller'
            })
            .when('/student/tests/:testId/take', {
                templateUrl : 'templates/common/blank.html',
                controller : 'student-test-groups-tests-take',
                controllerAs: 'controller'
            })
            .when('/student/tests/:testId/questions/:questionId/take', {
                templateUrl : 'templates/student/tests/questions/take.html',
                controller : 'student-tests-questions-take',
                controllerAs: 'controller'
            })
            .when('/student/tests/:testId/questions/:questionId/next', {
                templateUrl : 'templates/common/blank.html',
                controller : 'student-tests-questions-next',
                controllerAs: 'controller'
            })
            .when('/student/tests/:testId/summary', {
                templateUrl : 'templates/student/tests/summary/summary.html',
                controller : 'student-tests-questions-summary',
                controllerAs: 'controller'
            })
            .when('/student/tests/:testId/results/points', {
                templateUrl : 'templates/student/results/tests/points.html',
                controller : 'student-tests-results-points',
                controllerAs: 'controller'
            })
            .when('/student/tests/:testId/results/times', {
                templateUrl : 'templates/student/results/tests/times.html',
                controller : 'student-tests-results-times',
                controllerAs: 'controller'
            })
            .when('/student/tests/:testId/questions/:questionId/results', {
                templateUrl : 'templates/student/results/questions/question.html',
                controller : 'student-tests-results-questions',
                controllerAs: 'controller'
            })
            .when('/student/test-groups/:testGroupId/results', {
                templateUrl : 'templates/student/test-groups/results/list.html',
                controller : 'student-test-groups-results',
                controllerAs: 'controller'
            })
            .when('/student/results', {
                templateUrl : 'templates/student/results/list.html',
                controller : 'student-test-results',
                controllerAs: 'controller'
            })
            .when('/student/tests', {
                templateUrl : 'templates/student/tests/list.html',
                controller : 'student-tests-all',
                controllerAs: 'controller'
            });

    });