angular.module('examsystem')

    .controller('login', function($rootScope, $scope, $http, $location, auth) {

        $scope.hasError = false;
        $scope.role = 'STUDENT';

        $scope.setError = function (hasError, error) {
            $scope.hasError = hasError;
            $scope.error = error;
        };

        $scope.clearLoginForm = function () {
            $scope.username = '';
            $scope.password = '';
        };

        $scope.login = function() {
            return auth.login($scope.username, $scope.password, $scope.role).catch(function (error) {
                auth.setAuthenticatedUser(false, null, null);
                $scope.clearLoginForm();
                $scope.setError(true, error.message);
            });
        };

        auth.checkForAnyUser();

    });