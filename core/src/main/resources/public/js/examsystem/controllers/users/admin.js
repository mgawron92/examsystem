angular.module('examsystem')

    .controller('admin-dashboard', function($scope, $q, auth, rest) {

        auth.checkForAdmin().then(function () {
            return rest.getListSettings();
        }).then(function (listSettings) {
            listSettings = listSettings.data;
            return $q.all([
                rest.getExaminersByPageAndSize(0, listSettings.examiners),
                rest.getAdminsByPageAndSize(0, listSettings.admins)
            ])
        }).then(
            $q.spread(function (examiners, admins) {
                $scope.examinersPage = examiners.data;
                $scope.adminsPage = admins.data;
            })
        );

    })

    .controller('admin-admins-list', function($scope, auth, rest) {

        $scope.getAdminsPage = function(page) {
            rest.getAdminsPage(page).then(
                function (adminsPage) {
                    $scope.adminsPage = adminsPage.data;
                }
            );
        };

        auth.checkForAdmin().then(
            function () {
                return rest.getAdminsPage(0);
            }
        ).then(
            function (adminsPage) {
                $scope.adminsPage = adminsPage.data;
            }
        );

    })

    .controller('admin-admins-add', function($scope, $q, auth, rest, utils, resource) {

        $scope.adminLdapLogin = '';
        $scope.addedAdmins = [];

        $scope.getAdminsPage = function(page) {
            rest.getAdminsPage(page).then(
                function (adminsPage) {
                    $scope.adminsPage = adminsPage.data;
                }
            );
        };

        $scope.checkIfIsAdmin = function (ldapLogin) {
            var found = false;
            angular.forEach($scope.allAdmins, function (admin) {
                if (admin.ldapLogin == ldapLogin) {
                    found = true;
                }
            });
            return found;
        };

        $scope.findTypeaheadAdminIndex = function (ldapLogin) {
            for (var i = 0; i < $scope.typeaheadAdmins.length; ++i) {
                if ($scope.typeaheadAdmins[i].ldapLogin == ldapLogin) {
                    return i;
                }
            }
            return -1;
        };

        $scope.addAdmin = function() {
            var ldapLogin = utils.parseTypeaheadInput($scope.ldapLogin);
            var addedBy = auth.getAuthenticatedUser().ldapLogin;
            var admin = resource.getAdmin(ldapLogin, addedBy);
            var typeaheadAdminIndex = $scope.findTypeaheadAdminIndex(ldapLogin);

            if ($scope.checkIfIsAdmin(ldapLogin)) {
                throw({'message': 'Administrator o podanym identyfikatorze zostal juz dodany do systemu'});
            }

            if (typeaheadAdminIndex < 0) {
                throw({'message': 'Uzytkownik o podanym identyfikatorze nie istnieje'});
            }

            rest.saveAdmin(admin).then(
                function (admin) {
                    $scope.addedAdmins.push(admin.data);
                    $scope.typeaheadAdmins.splice(typeaheadAdminIndex, 1);
                    $scope.ldapLogin = '';
                    $scope.getAdminsPage(0);
                }
            );
        };

        auth.checkForAdmin().then(
            function () {
                return $q.all([
                    rest.getAdminsPage(0),
                    rest.getAvailableUsers()
                ]);
            }
        ).then(
            $q.spread(function (adminsPage, availableUsers) {
                $scope.adminsPage = adminsPage.data;
                $scope.typeaheadAdmins = availableUsers.data;
                return rest.getAdminsByPageAndSize(0, adminsPage.data.totalElements);
            })
        ).then(function (admins) {
            $scope.allAdmins = admins.data.content;
            angular.forEach($scope.allAdmins, function (admin) {
                var index = $scope.findTypeaheadAdminIndex(admin.ldapLogin);
                if (index > -1) {
                    $scope.typeaheadAdmins.splice(index, 1);
                }
            });
        });

    })

    .controller('admin-admins-details', function($scope, $routeParams, $q, auth, rest) {

        $scope.getAdminsPage = function(page) {
            rest.getAdminsPage(page).then(function (adminsPage) {
                $scope.adminsPage = adminsPage.data;
            });
        };

        auth.checkForAdmin().then(function () {
            return $q.all([
                rest.getAdminById($routeParams.adminId),
                rest.getAdminsPage(0)
            ]);
        }).then(
            $q.spread(function (admin, adminsPage) {
                $scope.admin = admin.data;
                $scope.adminsPage = adminsPage.data;
            })
        );

    })

    .controller('admin-admins-remove', function($scope, $routeParams, $location, $q, auth, rest) {

        $scope.getAdminsPage = function(page) {
            rest.getAdminsPage(page).then(function (adminsPage) {
                $scope.adminsPage = adminsPage.data;
            });
        };

        $scope.removeAdmin = function() {
            if ($routeParams.adminId != auth.getAuthenticatedUser().id) {
                rest.removeAdminById($routeParams.adminId).then(function () {
                    $location.path('/admin/admins');
                });
            }
        };

        auth.checkForAdmin().then(function () {
            return $q.all([
                rest.getAdminById($routeParams.adminId),
                rest.getAdminsPage(0)
            ]);
        }).then(
            $q.spread(function (admin, adminsPage) {
                $scope.admin = admin.data;
                $scope.adminsPage = adminsPage.data;
            })
        );

    })

    .controller('admin-examiners-list', function($scope, auth, rest) {

        $scope.getExaminersPage = function(page) {
            rest.getExaminersPage(page).then(function (examinersPage) {
                $scope.examinersPage = examinersPage.data;
            });
        };

        auth.checkForAdmin().then(function () {
            return rest.getExaminersPage(0);
        }).then (function (examinersPage) {
            $scope.examinersPage = examinersPage.data;
        });

    })

    .controller('admin-examiners-add', function($scope, $q, auth, rest, resource, utils) {

        $scope.examinerLdapLogin = '';
        $scope.addedExaminers = [];

        $scope.getExaminersPage = function(page) {
            rest.getExaminersPage(page).then(function (examinersPage) {
                $scope.examinersPage = examinersPage.data;
            });
        };

        $scope.checkIfIsExaminer = function (ldapLogin) {
            var found = false;
            angular.forEach($scope.allExaminers, function (examiner) {
                if (examiner.ldapLogin == ldapLogin) {
                    found = true;
                }
            });
            return found;
        };

        $scope.findTypeaheadExaminerIndex = function (ldapLogin) {
            for (var i = 0; i < $scope.typeaheadExaminers.length; ++i) {
                if ($scope.typeaheadExaminers[i].ldapLogin == ldapLogin) {
                    return i;
                }
            }
            return -1;
        };

        $scope.addExaminer = function() {
            var ldapLogin = utils.parseTypeaheadInput($scope.ldapLogin);
            var addedBy = auth.getAuthenticatedUser().ldapLogin;
            var examiner = resource.getExaminer(ldapLogin, addedBy);
            var typeaheadExaminerIndex = $scope.findTypeaheadExaminerIndex(ldapLogin);

            if ($scope.checkIfIsExaminer(ldapLogin)) {
                throw({'message': 'Egzaminator o podanym identyfikatorze zostal juz dodany do systemu'});
            }

            if (typeaheadExaminerIndex < 0) {
                throw({'message': 'Uzytkownik o podanym identyfikatorze nie istnieje'});
            }

            rest.saveExaminer(examiner).then(function (examiner) {
                $scope.addedExaminers.push(examiner.data);
                $scope.typeaheadExaminers.splice(typeaheadExaminerIndex, 1);
                $scope.ldapLogin = '';
                $scope.getExaminersPage(0);
            });
        };

        auth.checkForAdmin().then(function () {
            return $q.all([
                rest.getExaminersPage(0),
                rest.getAvailableUsers()
            ]);
        }).then(
            $q.spread(function (examinersPage, availableUsers) {
                $scope.examinersPage = examinersPage.data;
                $scope.typeaheadExaminers = availableUsers.data;
                return rest.getExaminersByPageAndSize(0, examinersPage.data.totalElements);
            })
        ).then(function (examiners) {
            $scope.allExaminers = examiners.data.content;
            angular.forEach($scope.allExaminers, function (examiner) {
                var index = $scope.findTypeaheadExaminerIndex(examiner.ldapLogin);
                if (index > -1) {
                    $scope.typeaheadExaminers.splice(index, 1);
                }
            });
        });

    })

    .controller('admin-examiners-details', function($scope, $routeParams, $q, auth, rest) {

        $scope.getExaminersPage = function(page) {
            rest.getExaminersPage(page).then(function (examinersPage) {
                $scope.examinersPage = examinersPage.data;
            });
        };

        auth.checkForAdmin().then(function () {
            return $q.all([
                rest.getExaminerById($routeParams.examinerId),
                rest.getExaminersPage(0)
            ]);
        }).then(
            $q.spread(function (examiner, examinersPage) {
                $scope.examiner = examiner.data;
                $scope.examinersPage = examinersPage.data;
            })
        );

    })

    .controller('admin-examiners-remove', function($scope, $routeParams, $q, $location, auth, rest) {

        $scope.removeExaminer = function() {
            rest.removeExaminerById($routeParams.examinerId).then(function () {
                $location.path('/admin/examiners');
            });
        };

        $scope.getExaminersPage = function(page) {
            rest.getExaminersPage(page).then(function (examinersPage) {
                $scope.examinersPage = examinersPage.data;
            });
        };

        auth.checkForAdmin().then(function () {
            return $q.all([
                rest.getExaminersPage(0),
                rest.getExaminerById($routeParams.examinerId)
            ]);
        }).then(
            $q.spread(function (examinersPage, examiner) {
                $scope.examinersPage = examinersPage.data;
                $scope.examiner = examiner.data;
            })
        );

    })

    .controller('admin-backups', function($scope, auth, rest, combobox) {

        $scope.adminModule = 'backups';
        $scope.backupFrequencies = combobox.getBackupFrequencies();

        $scope.saveChanges = function() {
            rest.removeBackupSettings($scope.backups.changePassword).then(function () {
                return rest.saveBackupSettings($scope.backups);
            }).then(function (backupSettings) {
                $scope.backups = backupSettings.data;
            });
        };

        auth.checkForAdmin().then(function () {
            return rest.getBackupSettings();
        }).then(function (backupSettings) {
            $scope.backups = backupSettings.data;
        });

    })

    .controller('admin-defaults', function($scope, $routeParams, auth, rest, resource) {

        $scope.adminModule = 'defaults';

        $scope.saveChanges = function() {
            rest.removeDefaultSettings().then(function () {
                return rest.saveDefaultSettings($scope.defaults);
            }).then(function (defaultSettings) {
                $scope.defaults = defaultSettings.data;
            });
        };

        auth.checkForAdmin().then(function () {
            $scope.formValuesLoaded = true;
            $scope.defaults = resource.getDefaultSettings();
            return rest.getDefaultSettings();
        }).then(function (defaultSettings) {
            $scope.defaults = defaultSettings.data;
        });

    })

    .controller('admin-lists', function($scope, $routeParams, auth, rest, combobox) {

        $scope.adminModule = 'lists';
        $scope.listValues = combobox.getListValues();

        $scope.saveChanges = function() {
            rest.removeListSettings().then(function () {
                return rest.saveListSettings($scope.lists);
            }).then(function (listSettings) {
                $scope.lists = listSettings.data;
            });
        };

        auth.checkForAdmin().then(function () {
            return rest.getListSettings();
        }).then(function (listSettings) {
            $scope.lists = listSettings.data;
        });

    });